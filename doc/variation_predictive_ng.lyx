#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
The Variational Predictive Natural Gradient
\end_layout

\begin_layout Standard
Following the paper with the same title as this document by Tang et al,
 2019.
 I translate their notation into my own.
 We start with the familiar problem statement for the joint distribution
 of the model and data:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{eqnarray*}
p\left(\mathbf{y},\mathbf{z}\right) & = & p\text{\left(\mathbf{y}|\mathbf{z}\right)}p\left(\mathbf{z}\right)
\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
Then we make an ELBO approximation with a variational distribution 
\begin_inset Formula $q\left(\mathbf{z}\right)$
\end_inset

.
 In our case, 
\begin_inset Formula $q\left(\mathbf{z}\right)$
\end_inset

 is:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{eqnarray*}
q\left(\mathbf{z}\right) & \sim & \phi\left(\mathcal{N}\left(\mathbf{u};\mathbf{m},\mathbf{S}\right)\right)\\
\bm{\lambda} & \equiv & \left\{ \mathbf{m},\mathbf{S}\right\} \\
q\left(\mathbf{z}|\bm{\lambda}\right) & \sim & \phi\left(\mathcal{N}\left(\mathbf{u};\mathbf{m},\mathbf{S}\right)\right)
\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
Where 
\begin_inset Formula $\phi\left(\cdot\right)$
\end_inset

 is the link function.
 The paper is clearly aimed at variational auto-encoders and they consistently
 use the slightly weird notation that the variational distribution is 
\begin_inset Formula $q\left(\mathbf{z}|\mathbf{x}\right)$
\end_inset

, while the observed data is also 
\begin_inset Formula $\mathbf{x}$
\end_inset

.
 This makes sense for an auto-encoder, where 
\begin_inset Formula $\mathbf{x}$
\end_inset

 defines the parameters of the fully factorized normal distribution that
 gives 
\begin_inset Formula $\mathbf{z}$
\end_inset

.
 We want to sample 
\begin_inset Formula $q$
\end_inset

 via reparameterization, so we can write it as:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{eqnarray*}
g\left(\bm{\epsilon},\bm{\lambda}\right) & \sim & q\left(\mathbf{z}|\bm{\lambda}\right)\\
\bm{\epsilon} & \sim & \mathcal{N}\left(\bm{0},\mathbf{I}\right)\\
g\left(\bm{\epsilon},\bm{\lambda}\right) & = & \phi\left(\mathbf{S}^{1/2}\bm{\epsilon}+\mathbf{m}\right)
\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
Very early on in the paper, things get confusing.
 They tell us we want to compute the negative Hessian of the variational
 joint model: 
\begin_inset Formula $p\left(\mathbf{y}|\mathbf{z}\right)q\left(\mathbf{z}\right)$
\end_inset

.
 Okay, that seems fine, but then it gets weird with a requirement for invertibil
ity between 
\begin_inset Formula $\mathbf{z}$
\end_inset

 and 
\begin_inset Formula $\bm{\epsilon}$
\end_inset

, which we do satisfy if we define 
\begin_inset Formula $g$
\end_inset

 as I have above.
 In the end, they make an approximation: 
\begin_inset Formula $p\left(\mathbf{y},\mathbf{z}\right)\approx Q\left(\mathbf{y}\right)q\left(\mathbf{z}\right)$
\end_inset

, where 
\begin_inset Formula $Q\left(\mathbf{y}\right)$
\end_inset

 is the empirical data distribution.
 I don't actually see them even using the empirical data distribution in
 final expressions, however.
 They declare 
\begin_inset Formula $p\left(\mathbf{y}_{i}|\mathbf{z}_{i}=g\left(\bm{\epsilon},\bm{\lambda}\right)\right)$
\end_inset

 to be the 
\begin_inset Quotes eld
\end_inset

reparameterized predictive model distribution
\begin_inset Quotes erd
\end_inset

, which I will denote 
\begin_inset Formula $p_{r}\left(\mathbf{y}|...\right)$
\end_inset

.
 We now require that we can draw samples from this distribution and that
 we can compute the gradient of this distribution, through reparameterization.
 I think this means we cannot use Poisson, since it maps predictions to
 discrete values, but we can use something like the over dispersed gaussian:
 
\begin_inset Formula $p\left(y_{q,n}|z_{q,n}\right)\sim\mathcal{N}\left(y_{q,n};z_{q,n},z_{q,n}+\sigma^{2}\right)$
\end_inset

, where 
\begin_inset Formula $n\in1,N$
\end_inset

 indexes the shots and 
\begin_inset Formula $q\in1,Q$
\end_inset

 indexes the output pixel.
 I might be wrong about not being able to use discrete predictions, though,
 because they have examples with discrete outputs.
 After some discussion with Anton, we think that the purpose here is to
 generate synthetic observations and then evaluate the probability of those
 synthetic observations given the likelihood expression.
 This cuts the data out of the loop, but in order to incorporate the SASE
 realistically we still need SASE spectra.
 To be explicit for a single SASE shot (in the RIXS case):
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{eqnarray*}
\bm{\zeta}_{n} & \equiv & \left(\mathbf{I}_{0}\otimes\mathbf{x}_{n}\right)g\left(\bm{\epsilon},\bm{\lambda}\right)=\left\{ z_{q,n}\right\} _{q\in1,Q}\\
p_{r}\left(\mathbf{y}|\bm{\zeta}_{n}\right) & = & \mathcal{N}\left(\mathbf{y};\bm{\zeta}_{n},\text{diag}\left(\bm{\zeta}_{n}\right)+\sigma^{2}\mathbf{I}\right)\\
\mathbf{y}_{n}^{\prime} & \sim & p_{r}\left(\mathbf{y}|\bm{\zeta}_{n}\right)\\
\mathbf{y}_{n}^{\prime},\bm{\zeta}_{n} & \in & \mathbb{R}^{Q}
\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
The sample 
\begin_inset Formula $\mathbf{y}_{n}^{\prime}$
\end_inset

 is drawn from the likelihood expression and note that its indexed still
 by 
\begin_inset Formula $n$
\end_inset

, indicating that it depends on the shot index still.
 If we could handle Poisson distributed data, it would be like saying 
\begin_inset Quotes eld
\end_inset

given a photon rate for the output axis, draw a likely observed emission
 spectrum
\begin_inset Quotes erd
\end_inset

; in the over dispersed gaussian case it's not counts per se, but I guess
 corrected ADU or something.
 Anyway, with these fake samples of the data in hand, we can plug them back
 in to the likelihood and evaluate it.
 Meaning:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{eqnarray*}
\mathbf{b}_{n} & \equiv & \log p\left(\mathbf{y}_{n}^{\prime}|\bm{\zeta}_{n}\right)
\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
If this seems strange, remember that our draws of 
\begin_inset Formula $\mathbf{y}_{n}^{\prime}$
\end_inset

 added the noise associated with the likelihood and so when we go back to
 plug them into the log probability we will get some random numbers.
 If we treat the emission spectrum as a quantity to predict unto itself,
 rather than individual pixels of it, then I think it's reasonable to reduce
 here over 
\begin_inset Formula $q$
\end_inset

.
 In fact, if we are to consider some charge sharing likelihood, then we
 could not evaluate the probability of a single pixel alone and would need
 to evaluate over the entire vector.
 In any event, I'm looking for any excuse to reduce before computing a gradient,
 so we have:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{eqnarray*}
c_{k,n} & = & \sum_{i}^{Q}\log p\left(\mathbf{y}_{n,i}^{\prime}|\bm{\zeta}_{n,i}\right)
\end{eqnarray*}

\end_inset

Where I've now added an index 
\begin_inset Formula $k$
\end_inset

 that denotes MC samples of the likelihood / latent space 
\begin_inset Formula $\bm{\zeta}$
\end_inset

.
 And then we compute the gradient of 
\begin_inset Formula $c_{k,n}$
\end_inset

 with respect to the model parameters 
\begin_inset Formula $\bm{\eta}$
\end_inset

 by reparameterization.
 And so their expression for the Fisher looks like:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{eqnarray*}
\hat{F} & = & \frac{1}{K\cdot N}\sum_{k}\sum_{n}\left[\nabla_{\bm{\eta}^{\top}}c_{k,n}\nabla_{\bm{\eta}}c_{k,n}\right]
\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
Where 
\begin_inset Formula $n$
\end_inset

indexes some number of shots, perhaps SASE shots in the minibatch being
 considered.
 But maybe we can avoid the reduction over SASE shots by instead considering
 just the average SASE weight, meaning 
\begin_inset Formula $\bar{\bm{\zeta}}_{k}=\left(\mathbf{I}_{0}\otimes\bar{\mathbf{x}}\right)g\left(\bm{\epsilon}_{k},\bm{\lambda}\right)$
\end_inset

.
 This would give us the estimate
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{eqnarray*}
\hat{F} & = & \frac{1}{K}\sum_{k}\left[\nabla_{\bm{\eta}^{\top}}\bar{c}_{k}\nabla_{\bm{\eta}}\bar{c}_{n}\right]
\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
This form is nice because it has at most rank K, being a sum of K outer
 products.
 As a matter of course during our model estimation we produce around 400
 samples of the latent space anyhow and thus these could be re-used to generate
 the predictive fisher.
 So now we have a low-rank form for the Fisher::
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{eqnarray*}
\left\{ \nabla_{\bm{\eta}^{\top}}\bar{c}_{k}\right\} _{k\in1,K} & \equiv & \mathbf{C}\in\mathbb{R}^{\text{len}\left(\bm{\eta}\right)\times K}\\
\hat{F} & = & \mathbf{C}\mathbf{C}^{\top}\\
\hat{F}^{-1} & \approx & \left(\mathbf{C}\mathbf{C}^{\top}+\epsilon\mathbf{I}\right)^{-1}\\
\end{eqnarray*}

\end_inset


\end_layout

\end_body
\end_document
