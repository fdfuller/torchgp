FROM pytorch/pytorch:1.4-cuda10.1-cudnn7-runtime
MAINTAINER Franklin fdfuller@gmail.com
RUN conda install jupyter=1.0.0 matplotlib=3.1.1 h5py=2.9.0 scipy=1.2.1 pandas=0.24.1 pillow=5.4.1 
RUN pip install papermill==1.2.1 jupytext==1.2.4 gpytorch==1.0.1
ENV PYTHONPATH=/workspace/torchgp
COPY . /workspace/torchgp 
RUN cd /opt/conda/lib/python3.7/site-packages && python /workspace/torchgp/setup.py develop && cd /workspace
# Add Tini. Tini operates as a process subreaper for jupyter. This prevents kernel crashes.
ENV TINI_VERSION v0.6.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini
ENTRYPOINT ["/usr/bin/tini", "--"]
CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]
