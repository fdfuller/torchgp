import numpy as np
import scipy.stats as stats
from scipy.ndimage import gaussian_filter1d
import torch
from ..symbolic import *
from ..transforms import *
from ..parameterizations.titsias import *
from ..structure import rkr_mv
from ..kernels import *

__all__ = [
    "make_fake_herfd_fully_observed",
    "make_fake_herfd_weighted",
    "make_fake_rixs_weighted",
    "make_fake_rixs_fully_observed",
    "make_sase",
    "SASE1DSignal",
    "NonlinearAndLinear",
    "TwoSASE1DSignals",
    "make_fake_rixs_weighted_with_mono",
]

## Anton simulations

DEFAULT_EDGE = 6.55


def sase_spectrum(ev, center, width, resolution, noisefactor, modulate_power=True):
    sigma = width / 2.355
    sigma_r = resolution / 2.355
    mean = np.sqrt(2 * np.pi) * sigma * stats.norm.pdf(ev, loc=center, scale=sigma)

    df = 2.355 * np.diff(ev.reshape(-1)).max()

    sase = stats.expon.rvs(mean, scale=noisefactor * mean, size=mean.shape)
    data = gaussian_filter1d(sase, sigma_r / df, axis=-1)

    if modulate_power:
        power = stats.invgamma(a=6.1)
        data *= power.rvs(size=(ev.shape[0], 1))

    return data


def sigm(ev):
    return 0.5 * (np.tanh(ev) + 1)


def lorentzian(axis, loc, fwhm):
    """calculates a lorentzian

    Args:
        axis: energy axis
        loc: center of lorentzian
        fwhm: full-width at half-max

    Returns:
        computed lorentzian

    """
    axis = np.atleast_1d(axis)
    norm = np.pi * fwhm
    return norm * stats.cauchy.pdf(axis, loc=loc, scale=fwhm)


def make_lineshape(term, axis, edge=DEFAULT_EDGE, **params):
    """constructs a lineshape given a set of parameters

    Args:
        term: a string identifying the lineshape
        axis: a 1-d array, the energy axis
        params: a dictionary of additional arguments that are lineshape specific

    Keyword Args:
        edge: position of the K-edge

    Returns:
        a 1-d array representing the calculated lineshape

    Notes:
        c_absorption is the absorption lineshape from the ground state to the continuum. If passed the additional
        wiggles parameter, the lineshape will include EXAFS-modulated absorption.

        *_fluorescence: all fluorescence terms but the d-edge are the same, representing the core-hole being filled
            from the valance band from one of two orbitals. The d-edge has a shift given by the "RIXS shift".
        Fluorescence lifetimes are in units of fs such that the energy is in units of eV

    """
    axis = np.atleast_1d(axis)
    data = np.zeros_like(axis)

    PLANCK_CONSTANT = 0.24179892967830705 / 1000  # in units of KeV*fs

    if term in ["k2p_absorption", "pc_absorption", "k1p_absorption"]:

        oxidation_edge_shift = params.get("oxidation_edge_shift", 0.002)  # ev
        fluorescence_1_amplitude = params.get("fluorescence_1_amplitude", 1)
        fluorescence_1_lifetime = params.get("fluorescence_1_lifetime", 0.3)
        fluorescence_2_amplitude = params.get("fluorescence_2_amplitude", 0.6)
        fluorescence_2_lifetime = params.get("fluorescence_2_lifetime", 0.3)
        time_delay = params.get("time_delay", 0.0)
        k2p_decay_rate = params.get("k2p_decay_rate", 0.2)

        k2p_lifetime = params.get("k2p_lifetime", 0.3)
        k2p_loc = params.get("k2p_loc", DEFAULT_EDGE - 0.005)
        k2p_fwhm = PLANCK_CONSTANT / k2p_lifetime

        # compute initial populations
        comp_pop0 = params.get(
            "initial_populations", [fluorescence_1_amplitude, fluorescence_2_amplitude]
        )
        comp_pop0 = np.array(comp_pop0)[:, np.newaxis]

        comp_rates = np.array([fluorescence_1_lifetime, fluorescence_2_lifetime])[
            :, np.newaxis
        ]
        comp_pop = comp_pop0 * np.exp(-comp_rates * time_delay)
        k2p_amplitude = np.sum(
            comp_rates
            / (comp_rates - k2p_decay_rate)
            * (comp_pop0 * np.exp(-k2p_decay_rate * time_delay) - comp_pop),
            axis=0,
        )
        pc_amplitude = comp_pop0.sum() - (k2p_amplitude + comp_pop.sum(axis=0))

        if term == "k2p_absorption":
            # absorption of the pre-edge from the intermediate state
            k2p_shift = oxidation_edge_shift * sigm(0.2 * time_delay - 4)
            data += k2p_amplitude * lorentzian(axis, k2p_loc + k2p_shift, k2p_fwhm)

            edge = edge + oxidation_edge_shift
            k2_amplitude = params.get("k2_amplitude", 0.2)
            term = "k2_absorption"
            params["k2_amplitude"] = pc_amplitude * k2_amplitude

        elif term == "pc_absorption":  # shifted ground state
            edge = edge + oxidation_edge_shift
            term = "c_absorption"
            params["edge_amplitude"] = pc_amplitude

        elif term == "k1p_absorption":
            edge = edge + oxidation_edge_shift
            term = "k1_absorption"
            k1_amplitude = params.get("k1_amplitude", 1.0)
            params["k1_amplitude"] = pc_amplitude * k1_amplitude

    if term == "c_absorption":
        edge_amplitude = params.get("edge_amplitude", 1)
        edge_transition_rate = params.get("edge_transition_rate", 1500)
        edge_decay_rate = params.get("edge_decay_rate", 1)
        saxis = axis - edge
        data += edge_amplitude * sigm(edge_transition_rate * saxis)
        data *= np.clip(
            1 + np.expm1(-edge_decay_rate * saxis) * (1 - (axis < edge)), 0, None
        )

        if params.get("wiggles", False):
            wiggles_rate_1 = params.get("wiggles_rate_1", 300)
            wiggles_rate_2 = params.get("wiggles_rate_2", 30)
            wiggles_scale = params.get("wiggles_scale", 0.2)
            wiggles = (
                np.sin(2 * np.pi * wiggles_rate_1 * saxis + np.pi)
                * (edge < axis)
                * np.clip(1 + np.expm1(-200 * saxis * (1 - (axis < edge))), 0, None)
            )
            wiggles += (
                0.3
                * np.sin(2 * np.pi * wiggles_rate_2 * saxis + np.pi + 0.3)
                * (edge < axis)
                * np.clip(1 + np.expm1(-20 * saxis) * (1 - (axis < edge)), 0, None)
            )
            data -= edge_amplitude * wiggles_scale * wiggles.mean()
            data += edge_amplitude * wiggles_scale * wiggles

    elif term == "k1_absorption":  # resonant absorption shape
        k1_amplitude = params.get("k1_amplitude", 1.0)
        k1_lifetime = params.get("k1_lifetime", 0.3)
        k1_loc = params.get("k1_loc", edge)
        k1_fwhm = PLANCK_CONSTANT / k1_lifetime

        data += k1_amplitude * lorentzian(axis, k1_loc, k1_fwhm)

    elif term == "k2_absorption":  # pre-edge
        k2_amplitude = params.get("k2_amplitude", 0.2)
        k2_lifetime = params.get("k2_lifetime", 5.0)
        k2_loc = params.get("k2_loc", edge - 0.003)
        k2_fwhm = PLANCK_CONSTANT / k2_lifetime

        k2_amplitude_b = params.get("k2_amplitude", 0.2)
        k2_lifetime_b = params.get("k2_lifetime", 0.3)
        k2_loc_b = params.get("k2_loc", edge - 0.002)
        k2_fwhm_b = PLANCK_CONSTANT / k2_lifetime_b

        data += k2_amplitude * lorentzian(axis, k2_loc, k2_fwhm) + k2_amplitude_b * lorentzian(axis, k2_loc_b, k2_fwhm_b)


    elif "fluorescence" in term:
        rixs_shift = params.get("rixs_shift", 0)
        fluorescence_1_amplitude = params.get("fluorescence_1_amplitude", 1)
        fluorescence_1_lifetime = params.get("fluorescence_1_lifetime", 0.3)
        fluorescence_1_loc = params.get("fluorescence_1_loc", DEFAULT_EDGE)
        fluorescence_2_amplitude = params.get("fluorescence_2_amplitude", 0.6)
        fluorescence_2_lifetime = params.get("fluorescence_2_lifetime", 0.3)
        fluorescence_2_loc = params.get("fluorescence_2_loc", DEFAULT_EDGE - 0.005)

        fwhm_1 = PLANCK_CONSTANT / fluorescence_1_lifetime
        fwhm_2 = PLANCK_CONSTANT / fluorescence_2_lifetime
        loc_1 = fluorescence_1_loc + rixs_shift
        loc_2 = fluorescence_2_loc + rixs_shift

        data = fluorescence_1_amplitude * lorentzian(axis, loc_1, fwhm_1)
        data += fluorescence_2_amplitude * lorentzian(axis, loc_2, fwhm_2)

    return data


def rixs_fancy_model(
    oute, ine1=[0.0], terms=["kedge", "continuum", "preedge"], **params
):
    ine1 = np.array(ine1)

    data = np.zeros((len(ine1), oute.shape[0]))
    params["rixs_shift"] = params.get(
        "rixs_shift",
        0.0005 * sigm(1000 * (ine1 - DEFAULT_EDGE + 0.0025))[:, np.newaxis],
    )

    if "preedge" in terms:
        ab = make_lineshape("k2_absorption", ine1, **params)
        fluor = make_lineshape(
            "k2_fluorescence", oute, **params
        )  # RIXS dimension is first
        np.einsum("i,ik->ik", ab, fluor, out=data)

    # ignore RIXS shift for the other terms
    params["rixs_shift"] = np.zeros_like(ine1)[:, np.newaxis]

    if "continuum" in terms:
        ab = make_lineshape("c_absorption", ine1, **params)
        fluor = make_lineshape("k1_fluorescence", oute, **params)
        data += np.einsum("i,ik->ik", ab, fluor)

    if "kedge" in terms:
        ab = make_lineshape("k1_absorption", ine1, **params)
        fluor = make_lineshape("k1_fluorescence", oute, **params)
        data += np.einsum("i,ik->ik", ab, fluor)

    return data


def add_poisson_noise(x, alpha):
    return np.array(np.random.poisson(x * alpha), x.dtype)


def make_fake_rixs_weighted(
    nshots,
    input_pixels,
    output_pixels,
    noise_type="Gaussian",
    mean_snr=10.0,
    sase_bw_factor=3,
):
    ev_in = np.linspace(6.54, 6.56, input_pixels)
    ev_out = np.linspace(6.54, 6.555, output_pixels)
    rixs2d = rixs_fancy_model(
        ev_out, terms=["preedge", "kedge", "continuum"], wiggles=True, ine1=ev_in
    )
    rixs2d = rixs2d / rixs2d.max()
    idx = np.arange(nshots)
    sase_idx, sase_ine = np.meshgrid(idx, ev_in, indexing="ij")
    X = sase_spectrum(
        sase_ine,
        center=(ev_in.min() + ev_in.max()) / 2,
        width=(ev_in.max() - ev_in.min()) / sase_bw_factor,
        resolution=0.0003,
        noisefactor=100.0,
        modulate_power=True,
    )
    X /= np.sum(X, axis=1).mean()
    # X = np.copy(X[:,::10])
    Y = np.einsum("ik,kj", X, rixs2d[:, :])
    c = Y[:].mean()

    if noise_type == "Poisson":
        Yc = add_poisson_noise(Y / c, alpha=np.sqrt(mean_snr))
        gt = np.sqrt(mean_snr) * rixs2d / c

    else:
        Yc = Y / c + np.random.normal(loc=0.0, scale=1 / mean_snr, size=Y.shape)
        gt = rixs2d / c

    return X, Yc, gt


def make_fake_rixs_weighted_with_mono(
    nshots,
    input_pixels,
    output_pixels,
    noise_type="Gaussian",
    mean_snr=10.0,
    sase_bw_factor=3,
):
    """ Note: nshots must be integer divisible by input_pixels"""
    ev_in = np.linspace(6.54, 6.56, input_pixels)
    ev_out = np.linspace(6.54, 6.555, output_pixels)
    rixs2d = rixs_fancy_model(
        ev_out, terms=["preedge", "kedge", "continuum"], wiggles=True, ine1=ev_in
    )
    rixs2d = rixs2d / rixs2d.max()
    idx = np.arange(nshots)
    sase_idx, sase_ine = np.meshgrid(idx, ev_in, indexing="ij")
    X = sase_spectrum(
        sase_ine,
        center=(ev_in.min() + ev_in.max()) / 2,
        width=(ev_in.max() - ev_in.min()) / sase_bw_factor,
        resolution=0.0003,
        noisefactor=100.0,
        modulate_power=True,
    )
    X /= np.sum(X, axis=1).mean()
    Xmono = np.tile(np.eye(input_pixels), (X.shape[0] // input_pixels, 1)) * X
    # X = np.copy(X[:,::10])
    Y = np.einsum("ik,kj", X, rixs2d[:, :])
    c = Y[:].mean()

    if noise_type == "Poisson":
        Yc = add_poisson_noise(Y / c, alpha=np.sqrt(mean_snr))
        gt = np.sqrt(mean_snr) * rixs2d / c

    else:
        Yc = Y / c + np.random.normal(loc=0.0, scale=1 / mean_snr, size=Y.shape)
        gt = rixs2d / c

    Ymono = np.einsum("ik,kj", Xmono, gt)
    if noise_type == "Poisson":
        Ycmono = add_poisson_noise(Ymono, alpha=1.0)
    else:
        Ycmono = Ymono + np.random.normal(loc=0.0, scale=1 / mean_snr, size=Y.shape)

    return X, Xmono, (ev_in, ev_out), Yc, Ycmono, gt


def make_fake_rixs_fully_observed(
    n_reps, input_pixels, output_pixels, noise_type="Gaussian", mean_snr=10.0
):
    ev_in = np.linspace(6.54, 6.56, input_pixels)
    ev_out = np.linspace(6.54, 6.555, output_pixels)
    rixs2d = rixs_fancy_model(
        ev_out, terms=["preedge", "kedge", "continuum"], wiggles=True, ine1=ev_in
    )
    rixs2d = rixs2d / rixs2d.max()
    Y = np.tile(rixs2d[:, :], [n_reps, 1, 1])
    c = Y[:].mean()

    if noise_type == "Poisson":
        Yc = add_poisson_noise(Y / c, alpha=np.sqrt(mean_snr))

    else:
        Yc = Y / c + np.random.normal(loc=0.0, scale=1 / mean_snr, size=Y.shape)

    return (ev_in, ev_out), Yc, rixs2d / c


def make_fake_herfd_weighted(
    nshots, input_pixels, noise_type="Gaussian", mean_snr=10.0, gain=None
):
    ev_in = np.linspace(6.54, 6.56, input_pixels)
    herfd = rixs_fancy_model(
        np.array([6.55]),
        terms=["preedge", "kedge", "continuum"],
        wiggles=True,
        ine1=ev_in,
    )
    herfd = herfd / herfd.max()
    idx = np.arange(nshots)
    sase_idx, sase_ine = np.meshgrid(idx, ev_in, indexing="ij")
    X = sase_spectrum(
        sase_ine,
        center=(ev_in.min() + ev_in.max()) / 2,
        width=(ev_in.max() - ev_in.min()) / 1,
        resolution=0.0003,
        noisefactor=100.0,
        modulate_power=True,
    )
    X /= np.sum(X, axis=1).mean()
    Y = np.einsum("ik,kj", X, herfd)
    c = Y[:].mean()

    if noise_type == "Poisson":
        Yc = add_poisson_noise(np.ascontiguousarray(Y / c), alpha=np.sqrt(mean_snr))

    elif noise_type == "ODGaussian":
        if gain is None:
            gain = 1.0
        Yc = (Y / c) + np.random.normal(loc=0.0, scale=gain*(Y/c) + 1/mean_snr, size=(Y).shape)

    else:
        Yc = Y / c + np.random.normal(loc=0.0, scale=1 / mean_snr, size=(Y).shape)

    return X, Yc, herfd / c


def make_fake_herfd_fully_observed(
    n_reps, input_pixels, noise_type="Gaussian", mean_snr=10.0
):
    ev_in = np.linspace(6.54, 6.56, input_pixels)
    herfd = rixs_fancy_model(
        np.array([6.55]),
        terms=["preedge", "kedge", "continuum"],
        wiggles=True,
        ine1=ev_in,
    )
    herfd /= herfd.max()

    y = np.tile(np.squeeze(herfd), n_reps)[:, None]
    x = np.tile(normalize_axis(ev_in), n_reps)[:, None]
    c = y.mean()

    if noise_type == "Poisson":
        yc = add_poisson_noise(y / c, alpha=np.sqrt(mean_snr))

    else:
        yc = y / c + np.random.normal(loc=0.0, scale=1 / mean_snr, size=y.shape)

    return x, yc, herfd / c


def normalize_axis(x):
    r = x / (0.5 * (x.max() - x.min()))
    r -= r.min() + 1
    return r


def make_sase(input_pixels, nshots, width_factor=3):
    ev_in = np.linspace(6.54, 6.56, input_pixels)
    idx = np.arange(nshots)
    sase_idx, sase_ine = np.meshgrid(idx, ev_in, indexing="ij")
    X = sase_spectrum(
        sase_ine,
        center=(ev_in.min() + ev_in.max()) / 2,
        width=(ev_in.max() - ev_in.min()) / width_factor,
        resolution=0.0003,
        noisefactor=100.0,
        modulate_power=True,
    )
    return X


class SASE1DSignal(torch.nn.Module):
    def __init__(self, UA, F, sase_width_factor=3):
        """
        This is a fake data generator for 1D gpmm. It's not a physical signal, just one
        where the signal has known hyper parameters. Useful for checking proper convergence of the
        optimization problem.
        """

        super().__init__()
        self.UA = UA
        self.F = F
        self.sase_width_factor = sase_width_factor
        m1 = Vector("m1")
        KA = Kernel("KA")
        ua = Vector("ua")
        f = Vector("f")
        mf_A = mean_1D(KA, ua, f, m1)
        graph = expr2graph(Label(String("mf_A"), mf_A))
        self._mf_A = graph2ast(graph, compiled=True, debug=False)
        self._makepos = SoftplusTransform()
        self.stored_kernels = torch.nn.ModuleDict(
            {
                "KA": ARDRBFKernel(
                    lengthscale=torch.tensor([[0.05]]),
                    transforms=[self._makepos, self._makepos],
                ),
                "KB": ARDRBFKernel(
                    lengthscale=torch.tensor([[0.05]]),
                    transforms=[self._makepos, self._makepos],
                ),
            }
        )
        with torch.no_grad():
            m1 = torch.empty(UA, 1).normal_() * torch.linspace(-1, 1, UA).div(0.8).pow(
                2
            ).neg().exp().reshape(-1, 1)
            self.params = torch.nn.ParameterDict(
                {
                    "m1": torch.nn.Parameter(m1.pow(2)),
                    "f": torch.nn.Parameter(torch.linspace(-1, 1, F).unsqueeze(1)),
                    "ua": torch.nn.Parameter(torch.linspace(-1, 1, UA).unsqueeze(1)),
                }
            )

    def mean_A(self):
        leaves = {**self.params, **self.stored_kernels}
        r = self._mf_A(**leaves)
        return r.mf_A.detach()

    def make_fake_data(self, Nshots):
        S_A = self.mean_A()
        X = torch.from_numpy(
            make_sase(self.F, Nshots, width_factor=self.sase_width_factor).astype(
                "float32"
            )
        )
        X = X / X.max()
        #         c0_A = X.sum(dim=1).mean()
        #         W_A = X/c0_A
        W_A = X
        y_A = W_A @ S_A
        n_A = y_A.mean()
        y_A /= n_A
        self.n_A = n_A

        return y_A, X


class TwoSASE1DSignals(torch.nn.Module):
    def __init__(self, UA, UB, F):
        """
        This is a fake data generator for chain GP problem. It's not a physical signal, just one
        where the signal has known hyper parameters. Useful for checking proper convergence of the
        optimization problem.
        """

        super().__init__()
        self.UA = UA
        self.UB = UB
        self.F = F
        m1 = Vector("m1")
        m2 = Vector("m2")
        KA = Kernel("KA")
        KB = Kernel("KB")
        ua = Vector("ua")
        ub = Vector("ub")
        f = Vector("f")
        mf_A = mean_1D(KA, ua, f, m1)
        mf_B = mean_1D(KB, ub, f, m2)
        graph = expr2graph(Label(String("mf_B"), mf_B))
        self._mf_B = graph2ast(graph, compiled=True, debug=False)
        graph = expr2graph(Label(String("mf_A"), mf_A))
        self._mf_A = graph2ast(graph, compiled=True, debug=False)
        self._makepos = SoftplusTransform()
        self.stored_kernels = torch.nn.ModuleDict(
            {
                "KA": ARDRBFKernel(
                    lengthscale=torch.tensor([[0.05]]),
                    transforms=[self._makepos, self._makepos],
                ),
                "KB": ARDRBFKernel(
                    lengthscale=torch.tensor([[0.05]]),
                    transforms=[self._makepos, self._makepos],
                ),
            }
        )
        with torch.no_grad():
            m1 = torch.empty(UA, 1).normal_() * torch.linspace(-1, 1, UA).div(0.8).pow(
                2
            ).neg().exp().reshape(-1, 1)
            m2 = torch.empty(UB, 1).normal_() * torch.linspace(-1, 1, UB).div(0.8).pow(
                2
            ).neg().exp().reshape(-1, 1)
            self.params = torch.nn.ParameterDict(
                {
                    "m1": torch.nn.Parameter(m1.pow(2)),
                    "m2": torch.nn.Parameter(m2.pow(2)),
                    "f": torch.nn.Parameter(torch.linspace(-1, 1, F).unsqueeze(1)),
                    "ua": torch.nn.Parameter(torch.linspace(-1, 1, UA).unsqueeze(1)),
                    "ub": torch.nn.Parameter(torch.linspace(-1, 1, UB).unsqueeze(1)),
                }
            )

    def mean_B(self):
        leaves = {**self.params, **self.stored_kernels}
        r = self._mf_B(**leaves)
        return r.mf_B.detach()

    def mean_A(self):
        leaves = {**self.params, **self.stored_kernels}
        r = self._mf_A(**leaves)
        return r.mf_A.detach()

    def make_fake_data(self, Nshots):
        S_A = self.mean_A()
        X = torch.from_numpy(make_sase(self.F, Nshots).astype("float32"))
        X = X / X.max()
        #         c0_A = X.sum(dim=1).mean()
        #         W_A = X/c0_A
        W_A = X
        y_A = W_A @ S_A
        n_A = y_A.mean()
        y_A /= n_A
        self.n_A = n_A

        S_B = self.mean_B()
        Z = torch.from_numpy(make_sase(self.F, Nshots).astype("float32"))
        Z = Z / Z.max()
        #         c0_A = X.sum(dim=1).mean()
        #         W_A = X/c0_A
        W_B = Z
        y_B = W_B @ S_B
        n_B = y_B.mean()
        y_B /= n_B
        self.n_B = n_B

        return y_A, y_B, X, Z


class NonlinearAndLinear(torch.nn.Module):
    def __init__(self, UA, UB, F):
        """
        This is a fake data generator for chain GP problem. It's not a physical signal, just one
        where the signal has known hyper parameters. Useful for checking proper convergence of the
        optimization problem.
        """

        super().__init__()
        self.UA = UA
        self.UB = UB
        self.F = F
        m1 = Vector("m1")
        m2 = Vector("m2")
        KA = Kernel("KA")
        KB = Kernel("KB")
        ua = Vector("ua")
        ub = Vector("ub")
        f = Vector("f")
        mf_A = mean_1D(KA, ua, f, m1)
        mf_B = mean_2D(KB, KB, ub, ub, f, f, m2)
        graph = expr2graph(Label(String("mf_B"), mf_B))
        self._mf_B = graph2ast(graph, compiled=True, debug=False)
        graph = expr2graph(Label(String("mf_A"), mf_A))
        self._mf_A = graph2ast(graph, compiled=True, debug=False)
        self._makepos = SoftplusTransform()
        self.stored_kernels = torch.nn.ModuleDict(
            {
                "KA": ARDRBFKernel(
                    lengthscale=torch.tensor([[0.05]]),
                    transforms=[self._makepos, self._makepos],
                ),
                "KB": ARDRBFKernel(
                    lengthscale=torch.tensor([[0.05]]),
                    transforms=[self._makepos, self._makepos],
                ),
            }
        )
        with torch.no_grad():
            filter2d = torch.ger(
                torch.linspace(-1, 1, UB).div(0.8).pow(2).neg().exp(),
                torch.linspace(-1, 1, UB).div(0.8).pow(2).neg().exp(),
            )
            m1 = torch.empty(UA, 1).normal_() * torch.linspace(-1, 1, UA).div(0.8).pow(
                2
            ).neg().exp().reshape(-1, 1)
            m2 = torch.empty(UB * UB, 1).normal_() * filter2d.reshape(-1, 1)
            self.params = torch.nn.ParameterDict(
                {
                    "m1": torch.nn.Parameter(m1.pow(2)),
                    "m2": torch.nn.Parameter(m2.pow(2)),
                    "f": torch.nn.Parameter(torch.linspace(-1, 1, F).unsqueeze(1)),
                    "ua": torch.nn.Parameter(torch.linspace(-1, 1, UA).unsqueeze(1)),
                    "ub": torch.nn.Parameter(torch.linspace(-1, 1, UB).unsqueeze(1)),
                }
            )

    def mean_B(self):
        leaves = {**self.params, **self.stored_kernels}
        r = self._mf_B(**leaves).mf_B.reshape(self.F, self.F).detach()
        return 0.5 * r + 0.5 * r.t()

    def mean_A(self):
        leaves = {**self.params, **self.stored_kernels}
        r = self._mf_A(**leaves)
        return r.mf_A.detach()

    def make_fake_data(self, Nshots):
        S_A = self.mean_A()
        X = torch.from_numpy(make_sase(self.F, Nshots).astype("float32"))
        X = X / X.max()
        #         c0_A = X.sum(dim=1).mean()
        #         W_A = X/c0_A
        W_A = X
        y_A = W_A @ S_A
        n_A = y_A.mean()
        y_A /= n_A
        self.n_A = n_A

        S_B = self.mean_B().reshape(-1, 1)
        y_B = rkr_mv([X, X], S_B)
        n_B = y_B.mean()
        y_B /= n_B
        self.n_B = n_B

        return y_A, y_B, X  # , Xp
