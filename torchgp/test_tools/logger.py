import torch

__all__ = ["ScalarLogger"]


class ScalarLogger:
    """
    parameters:
    model = the torch module containing parameters you want to log
    model_keys = a dictionary of keys and corresponding transforms to apply to them in logging
    extra_keys = a dictionary of keys and correspondind transforms to apply to them in logging,
                these keys are not in the model, they're made during optimization; e.g loss values.

    example instantiation:
        Log = ScalarLogger(model, {'hyper_params.std_lik': model._makepos}, {'loss': lambda x: x})
    """

    def __init__(self, model, model_keys, extra_keys=None):
        self.model_dict = {key: val for key, val in model.named_parameters()}
        self.model_keys = model_keys
        self.extra_keys = extra_keys
        if extra_keys is not None:
            self.val_dict = {
                **{k: [] for k in model_keys.keys()},
                **{k: [] for k in extra_keys.keys()},
            }
        else:
            self.val_dict = {k: [] for k in model_keys.keys()}

    def __call__(self, extra_vals=None):
        with torch.no_grad():
            for key in self.model_keys.keys():
                self.val_dict[key].append(
                    self.model_keys[key](self.model_dict[key]).item()
                )
            if extra_vals is not None:
                for key, val in zip(self.extra_keys.keys(), extra_vals):
                    self.val_dict[key].append(self.extra_keys[key](val))

    def __repr__(self):
        return self.val_dict.__repr__()

    def report(self, initial_string=""):
        s = ""
        s += initial_string
        for key in self.val_dict.keys():
            s += key + ": " + "{:g}".format(self.val_dict[key][-1]) + ", "
        return s

    def keys(self):
        return self.val_dict.keys()
