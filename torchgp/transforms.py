import torch
from torch.distributions import (
    Transform,
    constraints,
    ExpTransform,
    ComposeTransform,
    SigmoidTransform,
    AffineTransform,
)

__all__ = [
    "SoftplusTransform",
    "ExpTransform",
    "IdentityTransform",
    "ComposeTransform",
    "SigmoidTransform",
    "AffineTransform",
]

IdentityTransform = ComposeTransform([])


class SoftplusTransform(Transform):
    r"""
    Transform via the mapping :math:`y = \exp(x)`.
    """
    domain = constraints.real
    codomain = constraints.positive
    bijective = True
    sign = +1

    def __init__(self, cache_size=0):
        super().__init__(cache_size=cache_size)

    def __eq__(self, other):
        return isinstance(other, SoftplusTransform)

    def _call(self, x):
        result = torch.where(x < 100.0, torch.nn.functional.softplus(x), x)
        return result

    def _inverse(self, y):
        result = torch.where(y < 100.0, torch.expm1(y).log(), y)
        return result

    def log_abs_det_jacobian(self, x, y):
        return torch.nn.functional.sigmoid(-x).abs()


class BoxCoxTransform(Transform):
    r"""
    Transform via the mapping :math:`y = \exp(x)`.
    """
    domain = constraints.real
    codomain = constraints.real
    bijective = False
    sign = +1

    def __init__(self, lam=0.0, cache_size=0):
        super().__init__(cache_size=cache_size)
        self._lam = lam

    def __eq__(self, other):
        if not isinstance(other, BoxCoxTransform):
            return False

        return other._lam == self._lam

    def _call(self, x):
        if self._lam != 0.0:  # use the log transform
            return self._lam.reciprocal() * (
                self.sign(x) * torch.abs(x).pow(self._lam) - 1.0
            )
        else:
            return x.log()

    def _inverse(self, y):
        if self._lam != 0.0:
            tmp = self._lam * y + 1.0
            return tmp.sign() * tmp.abs().pow(self._lam.reciprocal())
        else:
            return y.exp()

    def log_abs_det_jacobian(self, x, y):
        return x.abs().pow(self._lam - 1)
