import torch
from .functional import prod


def kron(op1, op2, batched=False):
    """evaluate kronecker product of components

    Args:
        op1,op2: components to obtain the kronecker product of
        batched: whether the inputs represent a batched product

    Returns:
        a huge tensor
    """
    if not batched:
        # if passing in vectors, expand to proper dimensions
        if op1.dim() == 1:
            op1 = op1.unsqueeze(-1)
        if op2.dim() == 1:
            op2 = op2.unsqueeze(-1)

        # I had an error where if A or B are not contiguous memory, then ger will error out.
        # calling .contiguous is a no-op if it already is. I hope this is a harmless change.
        A = op1.reshape(-1).contiguous()
        B = op2.reshape(-1).contiguous()
        C = (
            torch.ger(A, B)
            .reshape(op1.shape + op2.shape)
            .permute([0, 2, 1, 3])
            .contiguous()
        )

        return C.view(op1.shape[0] * op2.shape[0], op1.shape[1] * op2.shape[1])

    # batched case
    if op1.dim() == 2:
        op1 = op1.unsqueeze(-1)
    if op2.dim() == 2:
        op2 = op2.unsqueeze(-1)
    batch_size = op1.shape[0]

    A = op1.reshape(batch_size, -1, 1)
    B = op2.reshape(batch_size, 1, -1)
    C = (
        torch.bmm(A, B)
        .reshape((batch_size,) + op1.shape[1:] + op2.shape[1:])
        .permute([0, 1, 3, 2, 4])
        .contiguous()
    )

    return C.view(batch_size, op1.shape[1] * op2.shape[1], op1.shape[2] * op2.shape[2])


def _swap(seq, p1, p2):
    """swaps values at two index positions in list
    """
    lst = list(seq)
    tmp = lst[p1]
    lst[p1] = lst[p2]
    lst[p2] = tmp
    return lst


# def kron_mvprod(oplist, vec):
#     """evaluate kron mvprod given a list of components
#
#     Args:
#         oplist: components representing kronecker product
#         vec: a batch of vectors of length (batch, n) where n is product of column dimensions
#
#     Returns:
#
#     """
#     # normalize operands to have a batch dimension
#     if oplist[0].dim() < 3:
#         oplist = [op.unsqueeze(-3) for op in oplist]
#
#     colsizes = [op.shape[2] for op in oplist]
#
#     if vec.dim() < 3:
#         # broadcast to 3d
#         vec = vec.view(1, -1, 1)
#     elif vec.dim() == 3:
#         pass
#     else:
#         assert False, "cannot understand vec dimensions"
#
#     assert prod(colsizes) == vec[0].numel(), "incompatible vector length"
#     # TODO: warn about unbroadcastable operator/vec dimensions here
#
#     # partition vec to operator dimensions
#     vec_shape = (vec.shape[0],) + torch.Size(colsizes)
#     vec = vec.clone().view(*vec_shape)
#     order = list(range(len(vec.shape)))
#     D = len(oplist)
#
#     #print(f'colsizes: {colsizes!s}\nvec shape: {vec.shape}')
#     for d in range(D):
#         #lorder = order.copy()
#         order = _swap(order, d + 1, -1)
#         vec = vec.permute(order)
#         #print(f'd: {d}, original: {lorder!s}, permuted:{order}')
#         op = oplist[d].permute(0, -1, -2)
#         #print(f'd: {d}, vec shape: {vec.shape}, op shape: {op.shape}')
#         vec = torch.matmul(vec, op)
#         vec = vec.permute(order)
#         order = _swap(order, d + 1, -1)
#         #print(f'd: {d}, order: {order}, vec shape: {vec.shape}')
#
#     vec = vec.view(vec_shape[0], -1, 1)
#     return vec.squeeze(0)


def kron_mvprod(oplist, vec):
    """evaluate kron mvprod given a list of components

    Args:
        oplist: components representing kronecker product
        vec: a batch of vectors of length (n, batch) where n is product of column dimensions

    Returns:

    """
    # normalize operands to have a batch dimension
    if oplist[0].dim() < 3:
        oplist = [op.unsqueeze(-3) for op in oplist]

    colsizes = [op.shape[2] for op in oplist]

    if vec.dim() == 1:
        vec = vec.unsqueeze(1)
    if vec.dim() > 2:
        # broadcast to 3d
        assert False, "cannot understand vec dimensions"
    # convert matrix to batched vector, i.e. n x b -> b x n x 1
    vec = vec.t().unsqueeze(-1)

    assert prod(colsizes) == vec[0].numel(), f"incompatible vector length; got {vec[0].numel()}, expected {prod(colsizes)}"
    # TODO: warn about unbroadcastable operator/vec dimensions here

    # partition vec to operator dimensions
    vec_shape = (vec.shape[0],) + torch.Size(colsizes)
    vec = vec.clone().reshape(*vec_shape)
    order = list(range(len(vec.shape)))
    D = len(oplist)

    # print(f'colsizes: {colsizes!s}\nvec shape: {vec.shape}')
    for d in range(D):
        # lorder = order.copy()
        order = _swap(order, d + 1, -1)
        vec = vec.permute(order)
        # print(f'd: {d}, original: {lorder!s}, permuted:{order}')
        op = oplist[d].permute(0, -1, -2)
        # print(f'd: {d}, vec shape: {vec.shape}, op shape: {op.shape}')
        vec = torch.matmul(vec, op)
        vec = vec.permute(order)
        order = _swap(order, d + 1, -1)
        # print(f'd: {d}, order: {order}, vec shape: {vec.shape}')
    vec = vec.reshape(vec_shape[0], -1, 1)
    vec = vec.squeeze(-1)
    return vec.t()


def component_cols(L):
    return [l.shape[1] for l in L]


def component_rows(L):
    return [l.shape[0] for l in L]


def allsame(lst):
    # http://stackoverflow.com/q/3844948/
    return not lst or lst.count(lst[0]) == len(lst)


def ckr(*ops):
    """evaluate column partitioned Khatri Rao (CKR) product

    Args:
        ops: components of the CKR, they should all have the same number of columns

    Returns:
        A big matrix
    """
    rs = component_rows(ops)
    cols = ops[0].shape[1]
    assert allsame([op.shape[1] for op in ops])
    prod_rs = torch.prod(torch.tensor(rs, dtype=torch.int64)).item()
    comp_str = "az"
    ind = ord("a")
    for _ in range(1, len(ops)):
        ind += 1
        comp_str += ","
        comp_str += chr(ind)
        comp_str += "z"
    comp_str += "->"
    ind = ord("a")
    comp_str += chr(ind)
    for _ in range(1, len(ops)):
        ind += 1
        comp_str += chr(ind)
    comp_str += "z"
    return torch.einsum(comp_str, ops).reshape(prod_rs, cols)


def rkr(*ops):
    """evaluate row partitioned Khatri Rao (RKR) product

    Args:
        ops: components of the RKR, they should all have the same number of rows

    Returns:
        A big matrix
    """
    cs = component_cols(ops)
    rows = ops[0].shape[0]
    assert allsame([op.shape[0] for op in ops])
    prod_cs = torch.prod(torch.tensor(cs, dtype=torch.int64)).item()
    comp_str = "ab"
    ind = ord("b")
    for _ in range(1, len(ops)):
        ind += 1
        comp_str += ","
        comp_str += "a"
        comp_str += chr(ind)
    comp_str += "->a"
    ind = ord("b")
    comp_str += chr(ind)
    for _ in range(1, len(ops)):
        ind += 1
        comp_str += chr(ind)
    return torch.einsum(comp_str, ops).reshape(rows, prod_cs)


def rkr_mv_2d(A, B, v):
    """

    :param A: component 1, R x M
    :param B: component 2, R x N
    :param v: 2D or 3D tensor, either MN x 1 or batch x MN x 1
    :return: R x 1 or batch x R x 1, depending on dimensionality of v

    batch mode intended for use on, e.g. multiple monte carlo samples
    """
    assert A.shape[0] == B.shape[0]
    if v.dim() > 2:
        V = v.reshape(v.shape[0], A.shape[1], B.shape[1])
        return ((A.unsqueeze(0) @ V) * B.unsqueeze(0)).sum(dim=-1).unsqueeze(-1)
    else:
        V = v.reshape(v.shape[0], A.shape[1], B.shape[1])
        return ((A @ V) * B).sum(dim=1).unsqueeze(1)


def fold_2_batched(v, C1, C2):
    """
    Take a vector and transform it into a 3D tensor
    of shape: b x C1 x C2, where b = len(v)//(C1*C2)

    we assume that no memory reordering is needed. If you
    want memory juggling, you need to do a reshape, dim permute, and contiguous call
    """
    #     if len(v)%(C1*C2) > 0:
    #         raise ValueError(f"size not correct: len(v): {len(v)}, C1: {C1}, C2: {C2}")
    b = len(v) // (C1 * C2)
    return v.reshape(b, C1, C2)


def rkr_mv(ops, v):
    # We expect v to be prod(cols) by 1 (vector case) or by b (batched vector case, aka matrix)
    if v.dim() == 1:
        v = v.unsqueeze(0)  # reshape to 2D: batch x rows
    elif v.dim() == 2 and v.shape[-1] >= 1:
        v = v.t().contiguous()  # we want to have the non-batch dims be last
    else:
        raise ValueError(
            f"Expecting a v to be a vector or matrix, got v.shape = {v.shape}"
        )
    p = ops[0].shape[0]
    # These asserts have appreciable overhead (50 microseconds)
    #     for op in ops:
    #         assert op.shape[0] == p, "all ops must have same number of rows"
    #         assert op.dim() == 2, "all ops must be matrices"
    cols = [op.shape[1] for op in ops]
    if len(ops) == 1:
        return ops[0] @ v
    if len(ops) >= 2:
        # form a 3D tensor b x cols[-2] x cols[-1], b = batch * prod(cols[:-2])
        V = fold_2_batched(v.reshape(-1), cols[-2], cols[-1])
        V = (((ops[-2].unsqueeze(0) @ V) * ops[-1].unsqueeze(0))).sum(dim=-1)
        # we are left with b x p, so we can permute with a vanilla transpose
        V = V.t().contiguous()
        # remove two final ops
        _ = ops.pop()
        _ = ops.pop()
        _ = cols.pop()
        _ = cols.pop()
        while len(ops) > 0:
            b = V.shape[-1]
            V = V.reshape(p, b // cols[-1], cols[-1])
            V = V.permute(1, 0, 2).contiguous()  # now b//cols[-1] x p x cols[-1]
            V = (ops[-1].unsqueeze(0) * V).sum(dim=-1)
            V = V.t().contiguous()  # now p x b//cols[-1]
            _ = ops.pop()
            _ = cols.pop()
        return V


def fast_2D_ckr_mm(A, B, M):
    """
    CKR(A,B) @ M, where M is p x m, m << p and {A,B}.shape[0] = p
    """
    p, m = M.shape
    t1 = A.expand(m, -1, -1) * M.expand(A.shape[0], -1, -1).permute(2, 0, 1)
    return t1.bmm(B.t().expand(m, -1, -1)).reshape(m, -1).t()
