import torch
from .symbolic.core import AstMixin, TensorOperation, Select, Constant, Shape
from matchpy import Arity, Symbol
from ast import parse
from .functional import regrid_fun

__all__ = [
            "ConstantMean",
            "ZeroMean",
            "LinearMean",
            "BatchConstantMean",
            "MeanSymbol",
            "EvaluateMean",
            "ConstantPerPixelMean2D",
            'mean_factory'
        ]


class ConstantMean(torch.nn.Module):
    """computes a mean
    """

    def __init__(self, value, dtype=torch.get_default_dtype()):
        super().__init__()
        assert value.numel() == 1, "can only have one value"

        self.value = torch.nn.Parameter(value.reshape(1).to(dtype=dtype))

    def forward(self, axis):
        return self.value.expand(axis.shape[0]).reshape(-1, 1)

    def extra_repr(self):
        return "value={}".format(self.value.item())

class ZeroMean(torch.nn.Module):
    """computes a mean
    """

    def __init__(self):
        super().__init__()
    def forward(self, axis):
        return torch.zeros(axis.shape[0], dtype=axis.dtype, device=axis.device)

    def extra_repr(self):
        return "zero mean"


class LinearMean(torch.nn.Module):
    """Applies a linear transformation to the incoming data: y = x @ W

    Args:
        in_features: size of each input sample
        out_features: size of each output sample
        frozen weights: whether to store the weights as an optimizable parameter or static buffer,
                        defaults to True
        input_sample: a sample of the inputs expected to be seen; used to compute the projector when
                        in_features > out_features. Defaults to None, in which case no projector is
                        computed and an identity matrix is used.

    Shape:
        ...J @ JD -> ...D

    Attributes:
        weight: the transforming matrix W

    Special quirks:
        By default the weight matrix initializes to the identity matrix, even when in_features > out_features.
        The behavior intended from  reference: https://arxiv.org/pdf/1705.08933.pdf is to have the
        mean function project the outputs into the top SVD components of the input. You can recover this
        behavior by supplying a sample of the inputs you expect this mean function, and it will
        estimate the projector from the SVD of the sample, saving that projector into the weight.
    """
    __constants__ = ['in_features', 'out_features']

    def __init__(self, in_features, out_features, dtype=torch.get_default_dtype(), **kwargs):
        super().__init__()
        self.dtype = dtype
        self.in_features = in_features
        self.out_features = out_features
        W = torch.zeros(in_features, out_features, dtype=self.dtype)
        W.diagonal(dim1=-1, dim2=-2).add_(1.)
        self.weight = torch.nn.Parameter(W)  # initialize to identity
        if 'custom_init' in kwargs:
            W = kwargs['custom_init']
            assert W.dim() == 2, f"custom init must be 2D, got dim = {W.dim()}"
            assert W.shape[-2] == self.in_features, f"custom init must have {self.in_features} rows"
            assert W.shape[-1] == self.out_features, f"custom init must have {self.out_features} columns"
            with torch.no_grad():
                self.weight.data[...] = W.data

        if 'freeze_weights' in kwargs:
            self.weight.requires_grad_(False)

    def forward(self, x):
        """

        :param x: a batched tensor like ...NI
        :return: ...NI @ ID -> ...ND
        """
        return x @ self.weight

    def extra_repr(self):
        return f'in_features={self.in_features}, out_features={self.out_features}'


class BatchConstantMean(torch.nn.Module):
    """Effectively applies the following transformation y = 0 @ x + b, where b is a vector of
        size 1 x Dout, and is broad cast to match the size of y in batch dims. Alternatively stated,
        if x is a tensor of size ...J, create a tensor of size ...Dout, where the value of the tensor
        is the same for all dims ...

        It's the batched version of a constant mean, okay? We just allow the constant to be change
        for the output dims

    Args:
        out_features: size of each output sample
        frozen bias: whether to store the weights as an optimizable parameter or static buffer,
                        defaults to True

    Shape:
        ...J + 1D -> ...D

    Attributes:
        bias: the 1D tensor explained earlier
    """
    __constants__ = ['out_features']

    def __init__(self, out_features, bias_init=None,
                 dtype=torch.get_default_dtype(), **kwargs):
        super().__init__()
        self.dtype = dtype
        self.out_features = int(out_features)
        if bias_init is None:
            bias_init = torch.zeros(int(out_features), dtype=self.dtype)
        else:
            assert torch.is_tensor(bias_init), \
                "if you are passing an initializer in, it must be a tensor"
            assert bias_init.dim() == 1 and len(bias_init) == self.out_features, \
                f"must be a 1D tensor of length {out_features}"
        self.bias = torch.nn.Parameter(bias_init.type(self.dtype))

    def forward(self, x):
        b = self.bias
        for k in range(x.dim() - 1):
            b = b.unsqueeze(0)
        _, r = torch.broadcast_tensors(x[..., -1].unsqueeze(-1), b)
        # r = self.bias.unsqueeze(0)
        return r

    def custom_init(self, x):
        assert x.dim() == 1 and len(x) == self.out_features, \
            f"must be a 1D tensor of length {self.out_features}"
        with torch.no_grad():
            self.bias.data[...] = x.data

    def extra_repr(self):
        return f"bias={self.bias}"


def mean_factory(dict_in):
    registry = {
        'Linear': LinearMean,
        'Constant': BatchConstantMean,
        'None': None,
    }
    for key in dict_in.keys():
        if key in registry.keys():
            return None if registry[key] is None else registry[key](**dict_in[key])


class ConstantPerPixelMean2D(torch.nn.Module):
    """computes a where the values are interpolated linearly from control points
        held as parameters. The returned mean will be constant over axis1, but
        non-constant over axis0 (hence per pixel constant, when axis0 = output pixels).
    """

    def __init__(self, control_points, dtype=torch.get_default_dtype()):
        super().__init__()
        if not torch.is_tensor(control_points):
            raise ValueError("Expected a tensor for initializaiton")
        if not control_points.dim()==1:
            raise ValueError("Expected a vector (1D) for initialization")
        self.control_points = torch.nn.Parameter(control_points.unsqueeze(1).clone().detach())

    def forward(self, axis):
        """

        :param axis: a 2D axis, where column 1 encodes the 0th axis, and col 2 the 1st axis.
                    We allow for gridded or ungridded points, but note that this function
                    will only USE the 0th column.
        :return: a column vector corresponding to the mean evaluated at the requested points axis
        """
        if not axis.dim() == 2:
            raise ValueError("expected a 2D axis, no more D no less")
        return regrid_fun(self.control_points, axis[:,0], axis1=None)

    def extra_repr(self):
        return "avg_mean={}".format(self.control_points.mean().item())


class MeanSymbol(AstMixin, Symbol):
    default_function = lambda obj, *args: print(
        obj.name + " did not receive an implementation"
    )

    def __init__(self, name, data=None, variable_name=None):
        self.data = self.default_function if not data else data

        super().__init__(name, variable_name=variable_name)

    @property
    def evaluated(self):
        return self.data

    def to_ast(self, label, source=False):
        # construct a rename operation
        src = "{label} = {self_name}".format(label=label, self_name=self.name)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

    def __repr__(self):
        return "{type}('{name}')".format(type=type(self).__name__, name=self.name)


class EvaluateMean(TensorOperation):
    name = "EvaluateMean"
    arity = Arity(2, True)

    def __str__(self):
        mean = self.operands[0]
        axis = self.operands[1]
        return f"{mean}[{axis}]"

    # @property
    # def shape(self):
    #     Shape0 = lambda op: Select(Constant(0), Shape(op))
    #     return Shape0(self.operands[1]), Constant(1)
    # I'm depreciating these shape methods as... I don't think we need them

    @property
    def evaluated(self):
        mean = self.operands[0]
        assert type(mean).__name__ == "MeanSymbol", "can only evaluate mean symbol"
        axis = self.operands[1].evaluated
        return mean(axis)

    def to_ast(self, label, *arglabels, source=False):
        mean_fun = arglabels[0]
        axis = arglabels[1]

        src = f"{label} = {mean_fun}({axis})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result
