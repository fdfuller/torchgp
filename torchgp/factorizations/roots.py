from torchgp.symbolic import *
from torchgp.reparameterization import Sqrt
from matchpy import (
    Arity,
    ReplacementRule,
    replace_all,
    Pattern,
    CustomConstraint,
    Wildcard,
)
from functools import wraps

### First some replacement rules to make our functions handle general cases efficiently as I can ####


class LowerTriangularInverse(TensorOperation):
    # used to mark something as the inverse of a lower triangular matrix
    name = "LowerTriangularInverse"
    arity = Arity.unary
    associative = True


def detect_any_lti(x):
    try:
        return x.head == LowerTriangularInverse or (
            x.head == Transpose and x.operands[0].head == LowerTriangularInverse
        )
    except:
        return False


def detect_lower_triangular_inverse(x):
    try:
        return x.head == LowerTriangularInverse
    except:
        return False


def detect_lower_triangular_inverse_transpose(x):
    try:
        return x.head == Transpose and x.operands[0].head == LowerTriangularInverse
    except:
        return False


def detect_ckr_transposed(x):
    """ Finds Transpose(CKR(_,_,...)) """
    try:
        return x.head == Transpose and x.operands[0].head == CKR
    except:
        return False


def detect_rkr(x):
    """ Check if op is RKR"""
    try:
        return x.head == RKR
    except:
        return False


def detect_rkr_transposed(x):
    """ Finds Transpose(RKR(_,_,...)) """
    try:
        return x.head == Transpose and x.operands[0].head == RKR
    except:
        return False


def detect_2d_ckr(x):
    """ looks for 2D CKR """
    try:
        return x.head == CKR and len(x.operands) == 2
    except:
        return False


def detect_transpose(x):
    """ looks for transposed operations"""
    try:
        return x.head == Transpose
    except:
        return False


def convert_ckr_trans_to_rkrmv(star1, x, y, star2):
    """Transpose(CKR(_,_,...) @ v -> RKRMV(_.T,_.T,...,v)"""
    z = x.operands[0]  # CKR(_,_,..)
    new_ops = [Transpose(op) for op in z.operands]
    expr = Dot(*star1, RKRMVProd(*new_ops, y), *star2)
    return expr


def convert_rkr_dot_to_rkrmv(star1, x, y, star2):
    """RKR(_,_,..) @ v -> RKRMV(_,_,...,v)"""
    expr = Dot(*star1, RKRMVProd(*x.operands, y), *star2)
    return expr


def convert_ckr2d_dot_to_fast(star1, x, y, star2):
    """CKR(A,B) @ v -> CKRMM2D(A,B,v)"""
    return Dot(*star1, CKRMM2D(*x.operands, y), *star2)


def convert_rkr_trans_to_ckr_mv(star1, x, y, star2):
    """Transpose(RKR(_, _, ...)) -> CKR(_.T,_.T)"""
    z = x.operands[0]  # RKR(_,_,...)
    new_ops = [Transpose(op) for op in z.operands]
    expr = Dot(*star1, CKR(*new_ops) @ y, *star2)
    return expr


def convert_lti_dot_x_to_tsolve_left_apply(star1, x, y, star2):
    """LowerTriangularInverse(x) @ y -> CholeskySolve(x,y)"""
    z = x.operands[0]
    return Dot(*star1, CholeskySolve(z, y), *star2)


def convert_trans_lti_dot_x_to_tsolve_left_apply(star1, x, y, star2):
    """ Transpose(LowerTriangularInverse(x)) @ y -> CholeskySolveT(x,y)"""
    z = x.operands[0].operands[0]
    return Dot(*star1, CholeskySolveT(z, y), *star2)


def convert_lti_dot_x_to_tsolve_right_apply(star1, x, y, star2):
    """x @ LowerTriangularInverse(y) -> Transpose(CholeskySolveT(y,Transpose(x)))"""
    z = y.operands[0]
    return Dot(*star1, Transpose(CholeskySolveT(z, Transpose(x))), *star2)


def convert_trans_lti_dot_x_to_tsolve_right_apply(star1, x, y, star2):
    """ x @ Transpose(LowerTriangularInverse(y)) -> Transpose(CholeskySolve(y,Transpose(x)))"""
    z = y.operands[0].operands[0]
    return Dot(*star1, Transpose(CholeskySolve(z, Transpose(x))), *star2)


### This debug mode decorator is my debugging insurance so I can see how many replacements of a given kind are made.
# Change default to false if you want it off


def debug_mode(f, debug=True):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if debug:
            print(f.__doc__)
        return f(*args, **kwargs)

    return wrapper


convert_ckr_trans_to_rkrmv = debug_mode(convert_ckr_trans_to_rkrmv)
convert_rkr_dot_to_rkrmv = debug_mode(convert_rkr_dot_to_rkrmv)
convert_ckr2d_dot_to_fast = debug_mode(convert_ckr2d_dot_to_fast)
convert_rkr_trans_to_ckr_mv = debug_mode(convert_rkr_trans_to_ckr_mv)
convert_lti_dot_x_to_tsolve_left_apply = debug_mode(
    convert_lti_dot_x_to_tsolve_left_apply
)
convert_trans_lti_dot_x_to_tsolve_left_apply = debug_mode(
    convert_trans_lti_dot_x_to_tsolve_left_apply
)
convert_lti_dot_x_to_tsolve_right_apply = debug_mode(
    convert_lti_dot_x_to_tsolve_right_apply
)
convert_trans_lti_dot_x_to_tsolve_right_apply = debug_mode(
    convert_trans_lti_dot_x_to_tsolve_right_apply
)

### The rules themselves

x_ = Wildcard.dot("x")
y_ = Wildcard.dot("y")
star1_ = Wildcard.star("star1")
star2_ = Wildcard.star("star2")


rule_ckrtrans_to_rkrmv = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(lambda star1, x, y, star2: detect_ckr_transposed(x)),
    ),
    convert_ckr_trans_to_rkrmv,
)
rule_rkr_to_rkrmv = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(lambda star1, x, y, star2: detect_rkr(x)),
    ),
    convert_rkr_dot_to_rkrmv,
)
rule_rkr_trans_to_ckr = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(lambda star1, x, y, star2: detect_rkr_transposed(x)),
    ),
    convert_rkr_trans_to_ckr_mv,
)
rule_ckr2D_to_fast = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(lambda star1, x, y, star2: detect_2d_ckr(x)),
    ),
    convert_ckr2d_dot_to_fast,
)
rule_remove_dual_transpose = ReplacementRule(
    Pattern(Transpose(x_), CustomConstraint(lambda x: detect_transpose(x))),
    lambda x: x.operands[0],
)
rule_lti_left_dot_to_tsolve = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(
            lambda star1, x, y, star2: detect_lower_triangular_inverse(x)
            and not detect_any_lti(y)
        ),
    ),
    convert_lti_dot_x_to_tsolve_left_apply,
)
rule_trans_lti_left_dot_to_tsolve = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(
            lambda star1, x, y, star2: detect_lower_triangular_inverse_transpose(x)
            and not detect_any_lti(y)
        ),
    ),
    convert_trans_lti_dot_x_to_tsolve_left_apply,
)
rule_lti_right_dot_to_tsolve = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(
            lambda star1, x, y, star2: detect_lower_triangular_inverse(y)
            and not detect_any_lti(x)
        ),
    ),
    convert_lti_dot_x_to_tsolve_right_apply,
)
rule_trans_lti_right_dot_to_tsolve = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(
            lambda star1, x, y, star2: detect_lower_triangular_inverse_transpose(y)
            and not detect_any_lti(x)
        ),
    ),
    convert_trans_lti_dot_x_to_tsolve_right_apply,
)

rules_for_U = [
    rule_ckrtrans_to_rkrmv,
    rule_rkr_to_rkrmv,
    rule_rkr_trans_to_ckr,
    rule_ckr2D_to_fast,
    rule_remove_dual_transpose,
]
rules_for_K = [
    rule_lti_left_dot_to_tsolve,
    rule_trans_lti_left_dot_to_tsolve,
    rule_lti_right_dot_to_tsolve,
    rule_trans_lti_right_dot_to_tsolve,
    rule_remove_dual_transpose,
]


### These functions compute pieces to apply roots from the left using the results from:
### https://arxiv.org/abs/1405.0223


def lrr_components(U, K):
    """
    Uses only cholesky decomposition
    K is expected to be dense and symmetric (rules allow for it to be a lower triangular inverse)
    Computes components to factor the form I + U @ K @ U.T as W @ W.T
    The components are: L, M

    """
    A = Transpose(U) @ U
    A = replace_all(A, rules_for_U)
    L = CholeskyRoot(A)
    T = AddJitter(Transpose(L) @ K @ L, Constant(1.0))
    T = replace_all(T, rules_for_K)
    M = CholeskyRoot(T)
    return L, M  # both of these are always dense


def lrr_qr_components(U, K):
    """
    Uses qr decomposition for improved numerical stability. We cannot use this for structured U, however. Otherwise
    same as lrr_components, but the returned outputs are different.
    """
    Q, R = Select(Constant(0), QR(U)), Select(Constant(1), QR(U))
    T = AddJitter(R @ K @ Transpose(R), Constant(1.0))
    T = replace_all(T, rules_for_K)
    M = CholeskyRoot(T)
    return Q, M


def left_apply_lrr(U, K, right):
    """
    Apply a low rank root to right, where right is a Matrix or Vector
    """
    L, M = lrr_components(U, K)
    t0 = Transpose(U) @ right  # t0 is now of size p
    t0 = replace_all(t0, rules_for_U)
    t1 = CholeskySolve(L, t0)
    t2 = M @ t1  # dense op
    t3 = t2 - t1  # dense op
    t4 = CholeskySolveT(L, t3)  # dense op
    t5 = U @ t4  # U potentially not dense, result is dense
    t5 = replace_all(t5, rules_for_U)
    return t5 + right


def left_apply_lrr_transpose(U, K, right):
    """
    Apply the transpose of a low rank root to right, where right is a Matrix or Vector
    """
    L, M = lrr_components(U, K)
    t0 = Transpose(U) @ right  # t0 is now of size p
    t0 = replace_all(t0, rules_for_U)
    t1 = CholeskySolve(L, t0)
    t2 = Transpose(M) @ t1  # dense op
    t3 = t2 - t1  # dense op
    t4 = CholeskySolveT(L, t3)  # dense op
    t5 = U @ t4  # U potentially not dense, result is dense
    t5 = replace_all(t5, rules_for_U)
    return t5 + right


def left_apply_lrr_inv(U, K, right):
    """
    Apply a low rank root inverse to right, where right is a Matrix or Vector
    """
    L, M = lrr_components(U, K)
    t1 = Transpose(U) @ right  # U potentially not dense, result is dense
    t1 = replace_all(t1, rules_for_U)
    t2 = CholeskySolve(L, t1)  # dense
    t3 = CholeskySolve(M, t2)  # dense
    t4 = t2 - t3  # dense
    t5 = CholeskySolveT(L, t4)  # dense
    t6 = U @ t5  # U potentially not dense
    t6 = replace_all(t6, rules_for_U)
    return right - t6


def left_apply_lrr_inv_transpose(U, K, right):
    """
    Apply a low rank root inverse transpose to right, where right is a Matrix or Vector
    """
    L, M = lrr_components(U, K)
    t1 = Transpose(U) @ right  # U potentially not dense, result is dense
    t1 = replace_all(t1, rules_for_U)
    t2 = CholeskySolve(L, t1)  # dense
    t3 = CholeskySolveT(M, t2)  # dense
    t4 = t2 - t3  # dense
    t5 = CholeskySolveT(L, t4)  # dense
    t6 = U @ t5  # U potentially not dense
    t6 = replace_all(t6, rules_for_U)
    return right - t6


def left_apply_qr_lrr(U, K, right):
    Q, M = lrr_qr_components(U, K)
    t1 = Transpose(Q) @ right
    t2 = M @ t1
    t3 = t2 - t1
    t4 = Q @ t3
    return t4 + right


def left_apply_qr_lrr_transpose(U, K, right):
    Q, M = lrr_qr_components(U, K)
    t1 = Transpose(Q) @ right
    t2 = Transpose(M) @ t1
    t3 = t2 - t1
    t4 = Q @ t3
    return t4 + right


def left_apply_qr_lrr_inv(U, K, right):
    Q, M = lrr_qr_components(U, K)
    t1 = Transpose(Q) @ right
    t2 = CholeskySolve(M, t1)
    t3 = t1 - t2
    t4 = Q @ t3
    return right - t4


def left_apply_qr_lrr_inv_transpose(U, K, right):
    Q, M = lrr_qr_components(U, K)
    t1 = Transpose(Q) @ right
    t2 = CholeskySolveT(M, t1)
    t3 = t1 - t2
    t4 = Q @ t3
    return right - t4


### Now its time to get specific to the problem. For FITC root, we use only the QR version, since experimentation has
### shown it is substantially more numerically stable


class FITCRoot(TensorOperation):
    # A dummy operation that will be replaced with left apply rules where appropriate via replacement rules
    name = "FITCRoot"
    arity = Arity(4, True)
    associative = True


class FITCRootInverse(TensorOperation):
    # A dummy operation that will be replaced with left apply rules where appropriate via replacement rules
    name = "FITCRootInverse"
    arity = Arity(4, True)
    associative = True


structured_roots = [FITCRoot, FITCRootInverse]


def fitc_root_components(K, u, f, jitter):
    """
    :param K: The kernel symbol
    :param u: inducing axis
    :param f: evaluation axis
    :param jitter: for numerical stability of Cholesky
    :return: Delta, U and Core, 3 pieces needed to construct a low rank FITC root
    """
    Kfu = EvaluateKernel(K, f, u)
    Kuu = EvaluateKernelSymmetric(K, u)
    L = CholeskyRoot(AddJitter(Kuu, jitter))
    Core = Transpose(LowerTriangularInverse(L)) @ LowerTriangularInverse(
        L
    )  # replacement rules are applied in lrr left apply funcs
    t0 = CholeskySolve(L, Transpose(Kfu))
    Qdiag = Transpose(PartialSum(t0 * t0, Constant(0)))
    Kff_diag = EvaluateKernelDiagonal(K, f)
    D = Kff_diag - Qdiag
    Delta = Sqrt(D)
    U = Reciprocal(Delta) * Kfu  # equivalent to Diag(Delta) @ Kfu
    return (Delta, U, Core)


def detect_fitc_root(x):
    try:
        return x.head == FITCRoot
    except:
        return False


def detect_fitc_root_inverse(x):
    try:
        return x.head == FITCRootInverse
    except:
        return False


def detect_fitc_root_transpose(x):
    try:
        return x.head == Transpose and x.operands[0].head == FITCRoot
    except:
        return False


def detect_fitc_root_inverse_transpose(x):
    try:
        return x.head == Transpose and x.operands[0].head == FITCRootInverse
    except:
        return False


def detect_any_structured_root(x):
    try:
        return x.head in structured_roots or (
            x.head == Transpose and x.operands[0].head in structured_roots
        )
    except:
        return False


# R @ y    = D @ W @ y
# R^T @ y  = W^T @ D @ y
# R^-1 @ y = W^-1 @ D^-1 @ y
# R^-T @ y = D^-1 @ W^-T @ y


def convert_fitc_root_left_dot(star1, x, y, star2):
    """ FITCRoot(K,u,f,jitter) @ y -> left_apply_fitc_root(K,u,f,jitter,y)
        shorcuts out to Cholesky when f == u
    """
    if x.operands[1].name == x.operands[2].name:  # u = f case
        L = CholeskyRoot(
            AddJitter(
                EvaluateKernelSymmetric(x.operands[0], x.operands[1]), x.operands[3]
            )
        )
        return Dot(*star1, L @ y, *star2)
    else:
        Delta, U, Core = fitc_root_components(*x.operands)
        t0 = left_apply_qr_lrr(U, Core, y)
        return Dot(*star1, Delta * t0, *star2)


def convert_fitc_root_inverse_left_dot(star1, x, y, star2):
    """ FITCRootInverse(K,u,f,jitter) @ y -> left_apply_lrr_inverse(K,u,f,jitter,y * Reciprocal(Delta))
        shortcuts out to Cholesky when f == u"""
    if x.operands[1].name == x.operands[2].name:  # u = f case
        L = CholeskyRoot(
            AddJitter(
                EvaluateKernelSymmetric(x.operands[0], x.operands[1]), x.operands[3]
            )
        )
        return Dot(*star1, CholeskySolve(L, y), *star2)
    else:
        Delta, U, Core = fitc_root_components(*x.operands)
        t0 = Reciprocal(Delta) * y
        return Dot(*star1, left_apply_qr_lrr_inv(U, Core, t0), *star2)


def convert_fitc_root_transpose_left_dot(star1, x, y, star2):
    """ Transpose(FITCRoot(K,u,f,jitter)) @ y -> left_apply_fitc_root_transposed(K,u,f,jitter,y)
        shorcuts out to Cholesky when f == u"""
    z = x.operands[0]
    if z.operands[1].name == z.operands[2].name:  # u = f case
        L = CholeskyRoot(
            AddJitter(
                EvaluateKernelSymmetric(z.operands[0], z.operands[1]), z.operands[3]
            )
        )
        return Dot(*star1, Transpose(L) @ y, *star2)
    else:
        Delta, U, Core = fitc_root_components(*z.operands)
        t0 = Delta * y
        return Dot(*star1, left_apply_qr_lrr_transpose(U, Core, t0), *star2)


def convert_fitc_root_inverse_transpose_left_dot(star1, x, y, star2):
    """ Transpose(FITCRootInverse(K,u,f,jitter)) @ y -> left_apply_lrr_inverse_transpose(K,u,f,jitter,y * Reciprocal(Delta))"""
    z = x.operands[0]
    if z.operands[1].name == z.operands[2].name:  # u = f case
        L = CholeskyRoot(
            AddJitter(
                EvaluateKernelSymmetric(z.operands[0], z.operands[1]), z.operands[3]
            )
        )
        return Dot(*star1, CholeskySolveT(L, y), *star2)
    else:
        Delta, U, Core = fitc_root_components(*z.operands)
        t0 = left_apply_qr_lrr_inv_transpose(U, Core, y)
        return Dot(*star1, Reciprocal(Delta) * t0, *star2)


convert_fitc_root_left_dot = debug_mode(convert_fitc_root_left_dot)
convert_fitc_root_inverse_left_dot = debug_mode(convert_fitc_root_inverse_left_dot)
convert_fitc_root_transpose_left_dot = debug_mode(convert_fitc_root_transpose_left_dot)
convert_fitc_root_inverse_transpose_left_dot = debug_mode(
    convert_fitc_root_inverse_transpose_left_dot
)

# x @ R    = x @ D @ W       = (W^T @ D @ x^T)^T
# x @ R^-1 = x @ W^-1 @ D^-1 = (D^-1 @ W^-T @ x^T)^T
# x @ R^T  = x @ W^T @ D     = (D @ W @ x^T)^T
# x @ R^-T = x @ D^-1 @ W^-T = (W^-1 @ D^-1 @ x^T)^T


def convert_fitc_root_right_dot(star1, x, y, star2):
    """ x @ FITCRoot(K,u,f,jitter) -> Transpose(left_apply_fitc_root_transpose(K,u,f,jitter,Delta * Transpose(x)))
        shorcuts out to Cholesky when f == u
    """
    if y.operands[1].name == y.operands[2].name:  # u = f case
        L = CholeskyRoot(
            AddJitter(
                EvaluateKernelSymmetric(y.operands[0], y.operands[1]), y.operands[3]
            )
        )
        return Dot(*star1, Transpose(Transpose(L) @ Transpose(x)), *star2)
    else:
        Delta, U, Core = fitc_root_components(*y.operands)
        t0 = Delta * Transpose(x)
        return Dot(*star1, Transpose(left_apply_qr_lrr_transpose(U, Core, t0)), *star2)


def convert_fitc_root_inverse_right_dot(star1, x, y, star2):
    """x @ FITCRootInverse(K,u,f,jitter) -> Transpose(left_apply_lrr_inverse_transpose(K,u,f,jitter,Transpose(x)) * Reciprocal(Delta))
        shortcuts out to Cholesky when f == u"""
    if y.operands[1].name == y.operands[2].name:  # u = f case
        L = CholeskyRoot(
            AddJitter(
                EvaluateKernelSymmetric(y.operands[0], y.operands[1]), y.operands[3]
            )
        )
        return Dot(*star1, Transpose(CholeskySolveT(L, Transpose(x))), *star2)
    else:
        Delta, U, Core = fitc_root_components(*y.operands)
        t0 = left_apply_qr_lrr_inv_transpose(U, Core, Transpose(x))
        return Dot(*star1, Transpose(Reciprocal(Delta) * t0), *star2)


def convert_fitc_root_transpose_right_dot(star1, x, y, star2):
    """x @ Transpose(FITCRoot(K,u,f,jitter)) -> Transpose(left_apply_fitc_root_transposed(K,u,f,jitter,Transpose(x)) * Delta)
        shorcuts out to Cholesky when f == u"""
    z = y.operands[0]
    if z.operands[1].name == z.operands[2].name:  # u = f case
        L = CholeskyRoot(
            AddJitter(
                EvaluateKernelSymmetric(z.operands[0], z.operands[1]), z.operands[3]
            )
        )
        return Dot(*star1, Transpose(L @ Transpose(x)), *star2)
    else:
        Delta, U, Core = fitc_root_components(*z.operands)
        t0 = left_apply_qr_lrr(U, Core, Transpose(x))
        return Dot(*star1, Transpose(Delta * t0), *star2)


def convert_fitc_root_inverse_transpose_right_dot(star1, x, y, star2):
    """x @ Transpose(FITCRootInverse(K,u,f,jitter)) -> Transpose(left_apply_lrr_inverse(K,u,f,jitter,Reciprocal(Delta) * Transpose(x)))"""
    z = y.operands[0]
    if z.operands[1].name == z.operands[2].name:  # u = f case
        L = CholeskyRoot(
            AddJitter(
                EvaluateKernelSymmetric(z.operands[0], z.operands[1]), z.operands[3]
            )
        )
        return Dot(*star1, CholeskySolve(L, Transpose(x)), *star2)
    else:
        Delta, U, Core = fitc_root_components(*z.operands)
        t0 = Reciprocal(Delta) * Transpose(x)
        return Dot(*star1, Transpose(left_apply_qr_lrr_inv(U, Core, t0)), *star2)


convert_fitc_root_right_dot = debug_mode(convert_fitc_root_right_dot)
convert_fitc_root_inverse_right_dot = debug_mode(convert_fitc_root_inverse_right_dot)
convert_fitc_root_transpose_right_dot = debug_mode(
    convert_fitc_root_transpose_right_dot
)
convert_fitc_root_inverse_transpose_right_dot = debug_mode(
    convert_fitc_root_inverse_transpose_right_dot
)


rule_fitc_root_apply_left = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(
            lambda star1, x, y, star2: detect_fitc_root(x)
            and not detect_any_structured_root(y)
        ),
    ),
    convert_fitc_root_left_dot,
)
rule_fitc_root_inv_apply_left = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(
            lambda star1, x, y, star2: detect_fitc_root_inverse(x)
            and not detect_any_structured_root(y)
        ),
    ),
    convert_fitc_root_inverse_left_dot,
)
rule_fitc_root_transpose_apply_left = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(
            lambda star1, x, y, star2: detect_fitc_root_transpose(x)
            and not detect_any_structured_root(y)
        ),
    ),
    convert_fitc_root_transpose_left_dot,
)
rule_fitc_root_inv_transpose_apply_left = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(
            lambda star1, x, y, star2: detect_fitc_root_inverse_transpose(x)
            and not detect_any_structured_root(y)
        ),
    ),
    convert_fitc_root_inverse_transpose_left_dot,
)

rule_fitc_root_apply_right = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(
            lambda star1, x, y, star2: detect_fitc_root(y)
            and not detect_any_structured_root(x)
        ),
    ),
    convert_fitc_root_right_dot,
)
rule_fitc_root_inv_apply_right = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(
            lambda star1, x, y, star2: detect_fitc_root_inverse(y)
            and not detect_any_structured_root(x)
        ),
    ),
    convert_fitc_root_inverse_right_dot,
)
rule_fitc_root_transpose_apply_right = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(
            lambda star1, x, y, star2: detect_fitc_root_transpose(y)
            and not detect_any_structured_root(x)
        ),
    ),
    convert_fitc_root_transpose_right_dot,
)
rule_fitc_root_inv_transpose_apply_right = ReplacementRule(
    Pattern(
        Dot(star1_, x_, y_, star2_),
        CustomConstraint(
            lambda star1, x, y, star2: detect_fitc_root_inverse_transpose(y)
            and not detect_any_structured_root(x)
        ),
    ),
    convert_fitc_root_inverse_transpose_right_dot,
)

rules_for_fitc_roots = [
    rule_fitc_root_apply_left,
    rule_fitc_root_inv_apply_left,
    rule_fitc_root_transpose_apply_left,
    rule_fitc_root_inv_transpose_apply_left,
    rule_fitc_root_apply_right,
    rule_fitc_root_inv_apply_right,
    rule_fitc_root_transpose_apply_right,
    rule_fitc_root_inv_transpose_apply_right,
    rule_remove_dual_transpose,
]

# For now I disallow structured_root @ structured root. If you want that, do: structured_root @ I @ structured_root. Most common use case
# anticipated would be structured_root @ structured_root @ x, in which case we can apply to x first.
