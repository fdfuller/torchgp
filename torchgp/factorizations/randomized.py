import torch
from ..functional import qr_plus, batched_vec_identity, auto_nugget
from ..symbolic import *
from ast import parse
from matchpy import Arity
import math
from ..structure import kron_mvprod

__all__ = [
    "rrr_ul",
    "rrr_ul_var_rank",
    "rrr_uz_variant",
    "rrr_uz_variant_nugget",
    "UZ",
    "UL",
    "RRUL",
    "ul_determinant",
    "z_given_u_nugget_variant",
    "auto_nugget",
    "UZ2DKron",
    "UZKron",
    "rrr_uz_2Dkron_variant",
    "rrr_uz_kron_variant",
    "batched_vec_identity",
]

"""
This module implements several randomized factorizations. For some references on the topic see:
RRR_UVD: https://arxiv.org/abs/1811.08597 and the first author's thesis:
"Low-Rank Matrix Approximations andApplications"
RSVD: there's a paper out there that explains you need to orthonormalize between applications of power iterations,
when I find it, I'll put it here. But note that I could not find this mentioned in the vast pile of papers out there.
Neverthless, re-orthonormalization in the power method I find is absolutely critical for float32.

"""


def dense_randomized_svd(A, k, power_iterations=1, jitter=None):
    """

    :param A: the matrix to be factorized
    :param k: the maximum rank we seek
    :param power_iterations: usually 1 or 0, default 1. More are too expensive
    :param jitter: the cut off for singular value. We assume that the singular values
                    returned are accurate, which may not be true if the SVs don't decay quickly enough
    :return: U, S, V such that U @ S.diag() @ V.t() ~= A
    """
    if jitter is None:
        if A.dtype is torch.float64:
            jitter = 1e-8
        if A.dtype is torch.float32:
            jitter = 1e-4
    m = A.shape[0]
    n = A.shape[1]
    E = torch.randn(m, k, dtype=A.dtype, device=A.device)
    H = A @ E
    Q, R = qr_plus(H)
    for q in range(power_iterations):
        Q, _ = qr_plus(A.t() @ Q)
        Q, _ = qr_plus(A @ Q)
    C = Q.t() @ A
    U, S, V = C.svd()
    return Q @ U, S, V


def pinv(A):
    """

    :param A: a matrix
    :return: the moore-penrose pseudo-inverse, computed with QR
    """
    Q, R = qr_plus(A)
    t0 = torch.triangular_solve(A.t(), R, transpose=True, upper=True).solution
    return torch.triangular_solve(t0, R, upper=True).solution


def rrr_uzv(A, k, power_iterations=1):
    """

    :param A: A matrix
    :param k: the maximum rank we seek
    :param power_iterations: generally 0 or 1, default 1. Higher #s is computationally prohibitive
    :return: U, Z, V such that U @ Z @ V.t() ~= A, where U and Z are orthonormal and Z is square matrix
    """
    m = A.shape[0]
    n = A.shape[1]
    E = torch.randn(m, k, dtype=A.dtype, device=A.device)
    F = A @ E
    U, R = qr_plus(F)
    for q in range(power_iterations):
        U, _ = qr_plus(A.t() @ U)
        U, R = qr_plus(A @ U)
    T = A.t() @ U
    V, S = qr_plus(T)
    Z = (U.t() @ F) @ pinv(V.t() @ E)
    Zvals = Z.diag().abs().sort(descending=True)
    s = Zvals.indices
    Us = U[:, s]
    Vs = V[:, s]
    Zs = Us.t() @ (A @ Vs)
    return Us, Zs, Vs


def rrr_uz(A, k, power_iterations=1):
    """

        :param A: A symmetric matrix (this is not checked however)
        :param k: the maximum rank we seek
        :param power_iterations: generally 0 or 1, default 1. Higher #s is computationally prohibitive
        :return: U, Z such that U @ Z @ U.t() ~= A, where U and Z are orthonormal and Z is square, symmetric matrix
    """
    m = A.shape[0]
    E = torch.randn(m, k, dtype=A.dtype, device=A.device)
    F = A @ E
    U, R = qr_plus(F)
    for q in range(power_iterations):
        U, _ = qr_plus(A.t() @ U)
        U, R = qr_plus(A @ U)
    Z = (U.t() @ F) @ pinv(U.t() @ E)
    Zvals = Z.diag().abs().sort(descending=True)
    s = Zvals.indices
    Us = U[:, s]
    Zs = Us.t() @ (A @ Us)
    return Us, Zs


def rrr_uz_variant(A, k, power_iterations=1):
    """
    This variant avoids the use of the pseudo-inverse and so you can be assured that the returned Z is properly
    sorted. Slightly more expensive.

        :param A: A symmetric matrix (this is not checked however)
        :param k: the maximum rank we seek
        :param power_iterations: generally 0 or 1, default 1. Higher #s is computationally prohibitive
        :return: U, Z such that U @ Z @ U.t() ~= A, where U and Z are orthonormal and Z is square, symmetric matrix
    """
    m = A.shape[0]
    E = torch.randn(m, k, dtype=A.dtype, device=A.device)
    F = A @ E
    U, R = qr_plus(F)
    for q in range(power_iterations):
        U, _ = qr_plus(A.t() @ U)
        U, R = qr_plus(A @ U)
    # Z = (U.t() @ F) @ pinv(U.t() @ E)
    Z = U.t() @ (A @ U)
    Zvals = Z.diag().abs().sort(descending=True)
    s = Zvals.indices
    Us = U[:, s]
    Zs = Z[s, :]
    Zs = Zs[:, s]
    return Us, Zs


def rrr_uz_variant_nugget(A, k, power_iterations=1, cond_limit=1e6, jitter=1e-4):
    """

        :param A: A symmetric matrix (this is not checked however)
        :param k: the maximum rank we seek
        :param power_iterations: generally 0 or 1, default 1. Higher #s is computationally prohibitive
        :return: U, Z such that U @ Z @ U.t() ~= A, where U and Z are orthonormal and Z is square, symmetric matrix
    """
    m = A.shape[0]
    E = torch.randn(m, k, dtype=A.dtype, device=A.device)
    F = A @ E
    U, R = qr_plus(F)
    for q in range(power_iterations):
        U, _ = qr_plus(A.t() @ U)
        U, R = qr_plus(A @ U)
    Z = U.t() @ (A @ U)
    Zvals = Z.diag().abs().sort(descending=True)
    s = Zvals.indices
    Us = U[:, s]
    Zs = Z[s, :]
    Zs = Zs[:, s]
    with torch.no_grad():
        rank = (
            (Zvals.values > ((1 / cond_limit) * Zvals.values[0]))
            .nonzero()
            .reshape(-1)[-1]
        )
    Zs_nugget = (
        Zs
        + torch.eye(Zs.shape[0], dtype=Zs.dtype, device=Zs.device)
        * Zs.diag()[:rank].mean()
        * (A.shape[1] / A.shape[0])
        * jitter
    )
    return Us, Zs_nugget


def z_given_u_nugget_variant(A, U, cond_limit=1e6, jitter=1e-4):
    Z = U.t() @ (A @ U)
    Zvals = Z.diag().abs().sort(descending=True)
    s = Zvals.indices
    with torch.no_grad():
        rank = (
            (Zvals.values > ((1 / cond_limit) * Zvals.values[0]))
            .nonzero()
            .reshape(-1)[-1]
        )
        adjusted_jitter = (
            Zvals.values[:rank].mean() * (A.shape[1] / A.shape[0]) * jitter
        )
    Z_nugget = (
        Z + torch.eye(Z.shape[0], dtype=Z.dtype, device=Z.device) * adjusted_jitter
    )
    return Z_nugget


def rrr_ul(A, k, power_iterations=1, jitter=1e-4):
    """

    :param A: A symmetrc matrix
    :param k: the maximum rank we seek
    :param power_iterations: usually 1 or 0, default 1. More are too expensive
    :param jitter: we lower bound the singular values with this value (scaled by the mean singular value of full matrix).
    :return: U, L such that U @ L @ L.t() @ U.t() ~= A
    """

    U, Z = rrr_uz(A, k, power_iterations=power_iterations)
    Z = Z + torch.eye(Z.shape[0]) * Z.diag().mean() * (k / A.shape[0]) * jitter
    L = Z.cholesky()
    return U, L


def rrr_ul_var_rank(A, k, power_iterations=1, jitter=1e-4):
    """

    :param A: A symmetrc matrix
    :param k: the maximum rank we seek
    :param power_iterations: usually 1 or 0, default 1. More are too expensive
    :param jitter: value of the approximate singular value amplitude where we will cut off
    :return: U, L such that U @ L @ L.t() @ U.t() ~= A. Note we return a matrix that is of variable
            rank, making use of the rank-revealing property that this distribution affords. Useful in
            root factorization applications.
    """
    U, Z = rrr_uz(A, k, power_iterations=power_iterations)
    with torch.no_grad():
        svals = Z.diag().abs()
        cond_est = svals.max() / svals.min()
        scaled_jitter = svals.max() * jitter
        probe = (svals < scaled_jitter).nonzero()
        if len(probe > 0):
            rank = probe[0]
        else:
            rank = k
    Zt = Z[:rank, :rank]
    Ut = U[:, :rank]
    try:
        L = Zt.cholesky()
    except RuntimeError:
        print(f"condition estimate was {cond_est}")
        print(f"rank was {rank}")
        print(f"sval min was {svals.min()}, max was {svals.max()}")
        print(f"truncated condition was {svals.max()/svals[rank]}")
    return Ut, L


def orthogonalize_tall_matrix_nugget(A, jitter=1e-4, cond_limit=1e6):
    """
    A is presumed to be "tall", ie row dim >> column dim.
    """
    ATA = A.t() @ A
    Q, R = qr_plus(ATA)
    Z = Q.t() @ ATA @ Q
    d = Z.diag().abs()
    s = torch.sort(d, descending=True)
    Zs = Z[:, s.indices]
    Zs = Zs[s.indices, :]
    Qs = Q[:, s.indices]
    with torch.no_grad():
        rank = (s.values > ((1 / cond_limit) * s.values[0])).nonzero().reshape(-1)[-1]
        adjusted_jitter = Zs.diag()[:rank].mean() * (A.shape[1] / A.shape[0]) * jitter
    Zs_nugget = (
        Zs + torch.eye(Zs.shape[0], dtype=Zs.dtype, device=Zs.device) * adjusted_jitter
    )
    L = Zs_nugget.cholesky()
    LiQ = torch.triangular_solve(Qs.t(), L, upper=False, transpose=False).solution
    U = (LiQ @ A.t()).t()
    return U, rank

# WIP !!! def orthogonalize_tall_matrix_svd(A, jitter=1e-4, cond_limit=1e6):
#     """
#     A is presumed to be "tall", ie row dim >> column dim.
#     """
#     ATA = A.t() @ A
#     U, S, V = ATA.svd()
#     with torch.no_grad():
#         rank = (s.values > ((1 / cond_limit) * s.values[0])).nonzero().reshape(-1)[-1]
#         adjusted_jitter = Zs.diag()[:rank].mean() * (A.shape[1] / A.shape[0]) * jitter
#     Zs_nugget = (
#         Zs + torch.eye(Zs.shape[0], dtype=Zs.dtype, device=Zs.device) * adjusted_jitter
#     )
#     L = Zs_nugget.cholesky()
#     LiQ = torch.triangular_solve(Qs.t(), L, upper=False, transpose=False).solution
#     U = (LiQ @ A.t()).t()
#     return U, rank


def orthogonalize_tall_matrix_trunc(A, jitter=1e-4, cond_limit=1e6):
    """
    A is presumed to be "tall", ie row dim >> column dim.
    """
    ATA = A.t() @ A
    Q, R = qr_plus(ATA)
    Z = Q.t() @ ATA @ Q
    d = Z.diag().abs()
    s = torch.sort(d, descending=True)
    with torch.no_grad():
        rank = (s.values > ((1 / cond_limit) * s.values[0])).nonzero().reshape(-1)[-1]
    s = s.indices[:rank]
    Zs = Z[:, s]
    Zs = Zs[s, :]
    Qs = Q[:, s]
    L = Zs.cholesky()
    LiQ = torch.triangular_solve(Qs.t(), L, upper=False, transpose=False).solution
    U = (LiQ @ A.t()).t()
    return U, rank


def rrr_uz_kron_variant(As, k_global, power_iterations=1, jitter=1e-4):
    """
        Finds a rank k_global approximation to Kron{[A_0, A_1, ..., A_n]}, of the form
        U @ Z @ U.t(), where U are orthonormal matrices. This method estimates the rank
        necessary to make this approximation and limits the rank of U accordingly. The
        matrix U is zero-padded if there is too much requested rank. Rank is selected
        to limit the condition number of a subordinate process, so this does not mean that
        the returned decomposition is machine precision accurate. Note: U will be of full
        size on the row axis. So, if your kronecker product represents a matrix of size 1E6 x 1E6,
        and you ask for rank 1E3, U will have 1E9 numbers (~4 gigs of ram). We will never do
        a decomposition that takes more than k_global^3 complexity, so most of the compute
        cost comes from kron_mvprod, which scales as k_global * n, where n is the expanded row-size
        of the kronecker product.

        :param As: A list of symmetric matrices (symmetry is not checked, but assumed)
        :param k_global: rank of the
        :param power_iterations: generally 0 or 1, default 1. Higher #s is computationally prohibitive
        :return: U, Z such that U @ Z @ U.t() ~= A, where U and Z are orthonormal and Z is square, symmetric matrix
    """
    with torch.no_grad():
        Eglobal = torch.randn(
            torch.prod(torch.tensor([A.shape[0] for A in As])),
            k_global,
            dtype=As[0].dtype,
            device=As[0].device,
        )
        Eglobal = Eglobal / (
            torch.prod(torch.tensor([A.shape[0] for A in As])).float().sqrt()
        )
    F = kron_mvprod(As, Eglobal)
    U, rank = orthogonalize_tall_matrix_nugget(F, jitter=jitter)
    # power iterations
    for _ in range(power_iterations):
        U, rank = orthogonalize_tall_matrix_nugget(
            kron_mvprod([A.t() for A in As], U), jitter=jitter
        )
        U, rank = orthogonalize_tall_matrix_nugget(kron_mvprod(As, U), jitter=jitter)
    U, rank = orthogonalize_tall_matrix_nugget(U, jitter=0.0)  # this is a clean up step
    Z = U.t() @ kron_mvprod(As, U)
    Zvals = Z.diag().abs().sort(descending=True)
    s = Zvals.indices
    Us = U[:, s]
    Zs = Z[s, :]
    Zs = Zs[:, s]
    return Us, Zs


def rrr_uz_2Dkron_variant(A1, A2, k, power_iterations=1, jitter=1e-4, cond_limit=1e6):
    """
        As rrr_uz_kron_variant, but specialized to use the vec identity

        :param As: A list of symmetric matrices (symmetry is not checked, but assumed)
        :param k_global: rank of the
        :param power_iterations: generally 0 or 1, default 1. Higher #s is computationally prohibitive
        :return: U, Z such that U @ Z @ U.t() ~= A, where U and Z are orthonormal and Z is square, symmetric matrix
    """
    with torch.no_grad():
        Eglobal = torch.randn(
            A1.shape[1] * A2.shape[1], k, dtype=A1.dtype, device=A1.device
        )
        Eglobal = Eglobal / (
            torch.prod(torch.tensor([A1.shape[1], A2.shape[1]])).float().sqrt()
        )
        F = batched_vec_identity(A1, A2, Eglobal)
        U, rank = orthogonalize_tall_matrix_nugget(F, jitter=jitter)
        # power iterations
        for _ in range(power_iterations):
            U, rank = orthogonalize_tall_matrix_nugget(
                batched_vec_identity(A1, A2, U), jitter=jitter
            )
            U, rank = orthogonalize_tall_matrix_nugget(
                batched_vec_identity(A1, A2, U), jitter=jitter
            )
        U, rank = orthogonalize_tall_matrix_nugget(
            U, jitter=0.0
        )  # this is a clean up step
    Z = U.t() @ batched_vec_identity(A1, A2, U)
    Zvals = Z.diag().abs().sort(descending=True)
    with torch.no_grad():
        rank = (
            (Zvals.values > ((1 / cond_limit) * Zvals.values[0]))
            .nonzero()
            .reshape(-1)[-1]
        )
        adjusted_jitter = Zvals.values[:rank].mean() * jitter
    # print(f"sketching jitter: {adjusted_jitter}")
    s = Zvals.indices
    Us = U[:, s]
    Zs = Z[s, :]
    Zs = Zs[:, s]
    Zs = Zs + torch.eye(Zs.shape[0], dtype=Zs.dtype, device=Zs.device) * adjusted_jitter
    return Us, Zs

def rrr_ud_kron_low_rank(As, B, C, k_global, power_iterations=1, jitter=1e-4):
    """
        Finds a rank k_global approximation to Kron{[A_0, A_1, ..., A_n]} + B @ C @ B.t(), of the form
        U @ D @ U.t(), where U are orthonormal matrices and D is diagonal. This method estimates the rank
        necessary to make this approximation and limits the rank of U accordingly. Rank is selected
        to limit the condition number of a subordinate process, so this does not mean that
        the returned decomposition is machine precision accurate. Note: U will be of full
        size on the row axis. So, if your kronecker product represents a matrix of size 1E6 x 1E6,
        and you ask for rank 1E3, U will have 1E9 numbers (~4 gigs of ram). We will never do
        a decomposition that takes more than k_global^3 complexity, so most of the compute
        cost comes from kron_mvprod, which scales as k_global * n, where n is the expanded row-size
        of the kronecker product.

        :param As: A list of symmetric matrices (symmetry is not checked, but assumed)
        :param k_global: rank of the
        :param power_iterations: generally 0 or 1, default 1. Higher #s is computationally prohibitive
        :return: U, Z such that U @ Z @ U.t() ~= A, where U and Z are orthonormal and Z is square, symmetric matrix
    """
    with torch.no_grad():
        Eglobal = torch.randn(
            torch.prod(torch.tensor([A.shape[0] for A in As])),
            k_global,
            dtype=As[0].dtype,
            device=As[0].device,
        )
        Eglobal = Eglobal / (
            torch.prod(torch.tensor([A.shape[0] for A in As])).float().sqrt()
        )
        F = kron_mvprod(As, Eglobal) + B @ (C @ (B.t() @ Eglobal))
        U, rank = orthogonalize_tall_matrix_nugget(F, jitter=jitter)
        # power iterations
        for _ in range(power_iterations):
            U, rank = orthogonalize_tall_matrix_nugget(
                kron_mvprod([A.t() for A in As], U), jitter=jitter
            )
            U, rank = orthogonalize_tall_matrix_nugget(kron_mvprod(As, U), jitter=jitter)
    U, rank = orthogonalize_tall_matrix_nugget(U, jitter=0.0)  # this is a clean up step
    Z = U.t() @ kron_mvprod(As, U)
    Zvals = Z.diag().abs().sort(descending=True)
    s = Zvals.indices
    Us = U[:, s]
    Zs = Z[s, :]
    Zs = Zs[:, s]
    return Us, Zs


# def rrr_uz_2Dkron_variant(A1, A2, k, power_iterations=1, jitter=1e-4, cond_limit=1e6):
#     """
#         WARNING: experimental version that returns variable size!
#
#
#         As rrr_uz_kron_variant, but specialized to use the vec identity
#
#         :param As: A list of symmetric matrices (symmetry is not checked, but assumed)
#         :param k_global: rank of the
#         :param power_iterations: generally 0 or 1, default 1. Higher #s is computationally prohibitive
#         :return: U, Z such that U @ Z @ U.t() ~= A, where U and Z are orthonormal and Z is square, symmetric matrix
#     """
#     with torch.no_grad():
#         Eglobal = torch.randn(A1.shape[1]*A2.shape[1],k, dtype=A1.dtype, device=A1.device)
#         Eglobal = Eglobal/(torch.prod(torch.tensor([A1.shape[1],A2.shape[1]])).float().sqrt())
#     F = batched_vec_identity(A1, A2, Eglobal)
#     U, rank = orthogonalize_tall_matrix_trunc(F, jitter=jitter)
#     #power iterations
#     for _ in range(power_iterations):
#         U, rank = orthogonalize_tall_matrix_trunc(batched_vec_identity(A1, A2, U), jitter=jitter)
#         U, rank = orthogonalize_tall_matrix_trunc(batched_vec_identity(A1, A2, U), jitter=jitter)
#     U, rank = orthogonalize_tall_matrix_trunc(U, jitter=0.) #this is a clean up step
#     Z = U.t() @ batched_vec_identity(A1, A2, U)
#     Zvals = Z.diag().abs().sort(descending=True)
#     with torch.no_grad():
#         rank = (Zvals.values > ((1 / cond_limit) * Zvals.values[0])).nonzero().reshape(-1)[-1]
#         # adjusted_jitter = Zvals.values[:rank].mean() * jitter
#     # print(f"sketching jitter: {adjusted_jitter}")
#     s = Zvals.indices[:rank]
#     Us = U[:, s]
#     Zs = Z[s,:]
#     Zs = Zs[:,s]
#     # Zs = Zs + torch.eye(Zs.shape[0], dtype=Zs.dtype, device=Zs.device) * adjusted_jitter
#     return Us, Zs


class UZKron(TensorOperation):
    """
        Args: max rank [k] an integer scalar, [jitter] a float valued scalar, Symmetric matrices N x N [ops], which are
        understood to be components of a Kronecker product: op = KronProd([ops])
        Returns: orthonormal [U] of size N x k and lower triangular [L] of size k x k.
        """

    name = "UZKron"
    arity = Arity(3, False)

    @property
    def evaluated(self):
        k = self.operands[0].evaluated
        jitter = self.operands[1].evaluated
        ops = [op.evaluated for op in self.operands[2:]]
        U, Z = rrr_uz_kron_variant(ops, k, jitter=jitter)
        return U, Z

    def to_ast(self, label, *arglabels, source=False):
        k = self.operands[0].data
        jitter = self.operands[1].data
        ops = ", ".join(arglabels[2:])

        src = f"{label} = rrr_uz_kron_variant({ops},{k},jitter={jitter})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class UZ2DKron(TensorOperation):
    """
        Args: max rank [k] an integer scalar, [jitter] a float valued scalar, Symmetric matrices N x N [ops], which are
        understood to be 2 components of a Kronecker product: op = KronProd([ops])
        Returns: orthonormal [U] of size N x k and symmetric  [Z] of size k x k that has already been "nuggeted" and so
        is ready for cholesky application
        """

    name = "UZ2DKron"
    arity = Arity(4, True)

    @property
    def evaluated(self):
        k = self.operands[0].evaluated
        jitter = self.operands[1].evaluated
        A1 = self.operands[2].evaluated
        A2 = self.operands[3].evaluated
        U, Z = rrr_uz_kron_variant(A1, A2, k, jitter=jitter)
        return U, Z

    def to_ast(self, label, *arglabels, source=False):
        k = self.operands[0].data
        jitter = arglabels[1]
        A1 = arglabels[2]
        A2 = arglabels[3]

        src = f"{label} = rrr_uz_2Dkron_variant({A1},{A2},{k},jitter={jitter})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class UL(TensorOperation):
    """
    Args: Symmetric matrix N x N [op], max rank [k] an integer scalar, [jitter] a float valued scalar
    Returns: orthonormal [U] of size N x k and lower triangular [L] of size k x k.
    """

    name = "UL"
    arity = Arity(3, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        k = self.operands[1].evaluated
        jitter = self.operands[2].evaluated
        U, L = rrr_ul(op, k, jitter=jitter)
        return U, L

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        k = self.operands[1].data
        jitter = self.operands[2].data

        src = f"{label} = rrr_ul({op},{k},jitter={jitter})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class UZ(TensorOperation):
    """
    Args: Symmetric matrix N x N [op], max rank [k] an integer scalar, [jitter] a float valued scalar
    Returns: orthonormal [U] of size N x k and square [Z] of size k x k.
    """

    name = "UZ"
    arity = Arity(3, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        k = self.operands[1].evaluated
        jitter = self.operands[2].evaluated
        U, Z = rrr_uz_variant(op, k, jitter=jitter)
        return U, Z

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        k = arglabels[1]
        jitter = arglabels[2]

        src = f"{label} = rrr_uz_variant_nugget({op},{k},jitter={jitter})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class RRUL(TensorOperation):
    """
    Args: Symmetric matrix N x N [op], max rank [k] an integer scalar, [jitter] a float valued scalar
    Returns: orthonormal [U] of size N x p and lower triangular [L] of size p x p, where p is the
    discovered rank of the matrix [op] (defined as the # of columns with singular value > jitter)
    """

    name = "RRUL"
    arity = Arity(3, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        k = self.operands[1].evaluated
        jitter = self.operands[2].evaluated
        U, L = rrr_ul_var_rank(op, k, jitter=jitter)
        return U, L

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        k = self.operands[1].data
        jitter = arglabels[2]

        src = f"{label} = rrr_ul_var_rank({op},{k},jitter={jitter})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


def ul_determinant(A, max_rank, jitter):
    """
    This function gives an estimate of the log determinant of A based on its UL decomposition,
    i.e. A = A @ L @ L.t() @ U.t()
    :param A, the matrix for which we want the determinant
    :param max_rank: constant rank of the UL factorization
    :param jitter: constant jitter factor
    :return: symbol for the determinant
    """
    F = UL(A, max_rank, jitter)
    U = Select(Constant(0), F)
    L = Select(Constant(1), F)
    log_jitter = Constant(math.log(jitter.data))
    return Constant(2) * SumLogDiagonal(ExtractDiagonal(L)) + log_jitter * (
        Select(Constant(0), Shape(A)) - max_rank
    )
