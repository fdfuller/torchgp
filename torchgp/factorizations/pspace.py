import torch
from collections import namedtuple
from ..structure import kron
from ..symbolic import *
from ast import parse
from matchpy import Arity
from .randomized import rrr_uz_variant
from ..structure import ckr

__all__ = ["top_p", "TopP", "SelectColumns", "UZ2DKronDCT", "rr_dct_UZ_2D_nugget"]


Sorted = namedtuple("Sorted", ["values", "indices"])


def sortp(v, p):
    v = v.squeeze(-1)  # eliminate column index if it's there
    x = torch.sort(v, descending=True)
    return Sorted(values=x.values[:p], indices=x.indices[:p])


def top_p(singular_vecs, p):
    """
    This is algorithm 1 from:
    Scalable Gaussian Processes with Grid-Structured Eigenfunctions (GP-GRIEF).

    singular_vecs are assumed to be sorted in descending order
    """

    with torch.no_grad():
        log_singular_vecs = [x.abs().log() for x in singular_vecs]
        lam = sortp(log_singular_vecs[0], p)
        idxs = lam.indices.unsqueeze(1)
        for k, v in enumerate(log_singular_vecs[1:]):
            m = len(v)
            lam = sortp(
                kron(lam.values, torch.ones(m, dtype=v.dtype, device=v.device))
                + kron(torch.ones(len(lam.values), dtype=v.dtype, device=v.device), v),
                p,
            )
            idxs = torch.cat(
                (idxs[lam.indices.div(m), :], lam.indices.remainder(m).unsqueeze(-1)),
                dim=-1,
            )
        return torch.split(idxs, 1, dim=1)  # returns a tuple of column vectors


def rr_dct_UZ_2D_nugget(A1, A2, D1, D2, k, jitter=1e-4, cond_limit=1e6):
    """
    :param A1, A2: symmetric components in a kronecker product kron(A1, A2) is the matrix we're factorizing.
    :param D1, D2: DCT basis for A1 and A2; must be a matrix matching A1/A2 in size
    :param k: integer number of basis function we'll allow for (<< A1.shape[0]*A2.shape[0])
    :return B, Z, where B is the A1.shape[0]*A2.shape[0] x k orthonormal basis for kron(A1,A2) and Z is the spectrum.
            B Z B.t() approx kron(A1,A2)
    """
    with torch.no_grad():
        zd1 = (D1.t() * (A1 @ D1).t()).sum(dim=1)
        zd2 = (D2.t() * (A2 @ D2).t()).sum(dim=1)
        zd1s = torch.sort(zd1.abs(), descending=True)
        zd2s = torch.sort(zd2.abs(), descending=True)
        sis = top_p([zd1s.values, zd2s.values], k)
    B1 = D1[:, sis[0].squeeze(1)]
    B2 = D2[:, sis[1].squeeze(1)]
    Z1 = B1.t() @ A1 @ B1
    Z2 = B2.t() @ A2 @ B2
    Z = Z1 * Z2
    with torch.no_grad():
        zds = Z.diag().abs()
        rank = (zds > ((1 / cond_limit) * zds[0])).nonzero().reshape(-1)[-1]
        adjusted_jitter = Z.diag().abs()[:rank].mean() * jitter
    U = ckr(B1, B2)
    Z_nugget = (
        Z + torch.eye(Z.shape[0], dtype=Z.dtype, device=Z.device) * adjusted_jitter
    )
    return U, Z_nugget


class UZ2DKronDCT(TensorOperation):
    """
        Args: max rank [k] an integer scalar, [jitter] a float valued scalar, Symmetric matrices N x N [ops], which are
        understood to be 2 components of a Kronecker product: op = KronProd([ops])
        Returns: orthonormal [U] of size N x k and symmetric  [Z] of size k x k that has already been "nuggeted" and so
        is ready for cholesky application

        This flavor uses DCT basis functions to construct the basis.
        """

    name = "UZ2DKronDCT"
    arity = Arity(6, True)

    @property
    def evaluated(self):
        k = self.operands[0].evaluated
        jitter = self.operands[1].evaluated
        A1 = self.operands[2].evaluated
        A2 = self.operands[3].evaluated
        D1 = self.operands[4].evaluated
        D2 = self.operands[5].evaluated
        U, Z = rr_dct_UZ_2D_nugget(A1, A2, D1, D1, k, jitter=jitter)
        return U, Z

    def to_ast(self, label, *arglabels, source=False):
        k = self.operands[0].data
        jitter = arglabels[1]
        A1 = arglabels[2]
        A2 = arglabels[3]
        D1 = arglabels[4]
        D2 = arglabels[5]

        src = f"{label} = rr_dct_UZ_2D_nugget({A1},{A2},{D1},{D2},{k},jitter={jitter})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class TopP(TensorOperation):
    """
    Args: P, a scalar (integer), Ops a list of vectors
    """

    name = "TopP"
    arity = Arity(2, False)

    @property
    def evaluated(self):
        p = self.operands[0].evaluated
        ops = [op.evaluated for op in self.operands[1:]]
        return top_p(ops, p)

    def to_ast(self, label, *arglabels, source=False):
        p = arglabels[0]
        ops = ", ".join(arglabels[1:])

        src = f"{label} = top_p([{ops}],{p})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class SelectColumns(TensorOperation):
    """
    Args: P, a scalar (integer), Ops a list of vectors
    """

    name = "SelectColumns"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        inds = self.operands[1].evaluated
        return op[:, inds]

    @property
    def shape(self):
        return (
            Select(Constant(0), Shape(self.operands[0])),
            Select(Constant(0), Shape(self.operands[1])),
        )

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        inds = arglabels[1]

        src = f"{label} = {op}[:,{inds}.squeeze(-1)]"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result
