import torch
from torchgp.transforms import SoftplusTransform


def vec2img(x):
    return x.unsqueeze(0)


def interp_fun(f, s):
    # f is some function of shape 1 x N
    # s is some set of samples M x 1
    I = vec2img(f).unsqueeze(0)
    g = torch.cat(
        [s, torch.zeros(s.shape[0], dtype=s.dtype, device=s.device).unsqueeze(1)],
        dim=-1,
    )
    g.unsqueeze_(0).unsqueeze_(0)
    r = torch.nn.functional.grid_sample(I, g, padding_mode="border")
    return r.squeeze(0).squeeze(0).squeeze(0).unsqueeze(1)


class MonotoneVectorNormalized(torch.nn.Module):
    def __init__(self, axis_len: int, hidden_len: int, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        self.f = torch.nn.Sequential(
            torch.nn.Linear(axis_len, hidden_len),
            torch.nn.ELU(),
            torch.nn.Linear(hidden_len, axis_len),
        )
        self.map = SoftplusTransform()
        self.register_buffer(
            "axis", torch.linspace(-1, 1, axis_len, dtype=self.dtype).unsqueeze(0)
        )

    def forward(self, x):
        f = self.map(self.f(self.axis))
        f = 2 * f.div(torch.sum(f, dim=-1))
        F = torch.cumsum(f, dim=-1) - 1.0
        r = interp_fun(F, x)
        return r
