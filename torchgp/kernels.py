from typing import List, Tuple
import torch
import torch.nn.functional as F
import math
from torch.distributions import constraints
from .functional import ard_rbf_kernel, my_rbf_kernel
from .constraints import default_mapping
from .transforms import SoftplusTransform
import numbers
import numpy as np

__all__ = [
    "RBFKernel",
    "MaternKernel",
    "LinearKernel",
    "RationalQuadraticKernel",
    "PeriodicKernel",
    "ConstantKernel",
    "kernel_factory",
    "AbstractMOKernel",
    "CombinationKernel",
    "CombinationSharedKernel",
    "NeuralKernelNetwork",
    "KernelMixer",
    "BatchedKernel",
    "ScaledGibbsKernel",
    "BatchedGibbsKernel",
    ### Old style kernels for backward compatibility after this ###
    "ARDRBFKernel",
    "SafeARDRBFKernel",
    "Matern32",
    "Matern52",
    "GibbsKernel",
    "Linear",
    "HadamardKernel",
    "KroneckerKernel",
    "SpectralMixtureKernel"
]


class Kernel(torch.nn.Module):
    def __init__(self, constraint_mapping=None):
        super().__init__()
        self.constraints = dict()
        if constraint_mapping is None:
            self.constraint_mapping = default_mapping

    def forward(self, x1, x2):
        raise NotImplementedError()

    def diagonal(self, x):
        raise NotImplementedError()

    def symmetric(self, x):
        return self.forward(x, x)


def _sq_dist(x1, x2, x1_eq_x2=False):
    # TODO: use torch squared cdist once implemented: https://github.com/pytorch/pytorch/pull/25799
    adjustment = x1.mean(-2, keepdim=True)
    x1 = x1 - adjustment
    x2 = x2 - adjustment  # x1 and x2 should be identical in all dims except -2 at this point

    # Compute squared distance matrix using quadratic expansion
    x1_norm = x1.pow(2).sum(dim=-1, keepdim=True)
    x1_pad = torch.ones_like(x1_norm)
    if x1_eq_x2:
        x2_norm, x2_pad = x1_norm, x1_pad
    else:
        x2_norm = x2.pow(2).sum(dim=-1, keepdim=True)
        x2_pad = torch.ones_like(x2_norm)
    x1_ = torch.cat([-2. * x1, x1_norm, x1_pad], dim=-1)
    x2_ = torch.cat([x2, x2_pad, x2_norm], dim=-1)
    res = x1_.matmul(x2_.transpose(-2, -1))

    if x1_eq_x2:
        res.diagonal(dim1=-2, dim2=-1).fill_(0)

    # Zero out negative values
    res.clamp_min_(0)
    return res


def _dist(x1, x2, x1_eq_x2=False):
    # TODO: use torch cdist once implementation is improved: https://github.com/pytorch/pytorch/pull/25799
    res = _sq_dist(x1, x2, x1_eq_x2=x1_eq_x2)
    res = res.clamp_min_(1e-30).sqrt_()
    return res


def validate_scalar_parameter(inp, name, dtype, device):
    """
    Useful to clean up scalar parameters. We allow them to be passed in as scalar tensors or something belonging
    to numbers.Number. Returns a scalar tensor and handles error checking. If inp is None, then this returns None.

    if type/device are already correct, the inp is passed through, else we clone and enforce

    :param inp: the parameter to be validated
    :param name: the parameter name (a string), which is used for error statements
    :return: a scalar tensor, None, or some exception
    """
    if inp is not None:
        if torch.is_tensor(inp):
            if inp.dim() == 0:
                if inp.device == device and inp.dtype == dtype:
                    return inp
                else:
                    return inp.clone().detach().type(dtype).to(device)
            else:
                raise ValueError(f'Expected scalar tensor for parameter {name}')
        elif isinstance(inp, numbers.Number):
            return torch.tensor(inp).type(dtype).to(device)
        else:
            raise ValueError(f'unexpected type for {name}, got: {inp}')
    else:
        return None


def validate_ard_parameter(inp, name, dtype, device):
    """
    Useful to clean up ARD parameters, i.e. tensors of shape 1xD. When inp is None, it's treated as a pass-through.

    if type/device are already correct, inp is passed through, else the tensor will be cloned.

    :param inp: tensor, None, or scalar
    :param name: a string name, used for error handling
    :param dtype: tensor dtype to enforce
    :param device: tensor device to enforce
    :return:
    """
    if inp is not None:
        if torch.is_tensor(inp):
            if inp.dim() == 2 and inp.shape[0] == 1:
                if inp.device == device and inp.dtype == dtype:
                    return inp
            elif inp.dim() == 1:
                return inp.clone().detach().unsqueeze(0).type(dtype).to(device)
            elif inp.dim() == 0:
                return inp.clone().detach().unsqueeze(0).unsqueeze(0).type(dtype).to(device)
            else:
                raise ValueError(f'Expected {name} to have shape 1xD, but got {inp.shape}')
        elif isinstance(inp, numbers.Number):
            return torch.tensor(inp).unsqueeze(0).unsqueeze(0).type(dtype).to(device)
        else:
            # we just try to make it a tensor and pass the buck to torch
            tinp = torch.tensor(inp, dtype=dtype, device=device)
            if tinp.dim() == 2 and tinp.shape[0] == 1:
                return tinp
            elif tinp.dim() == 1:
                return tinp.clone().detach().unsqueeze(0)
            elif tinp.dim() == 0:
                return tinp.clone().detach().unsqueeze(0).unsqueeze(0)
            else:
                raise ValueError(f'Expected {name} to have shape 1xD, but got {inp.shape}')
    else:
        return None


def is_postive_tensor(t):
    if (t < 0).byte().any():
        return False
    else:
        return True


def attach_dyn_prop(instance, prop_name, prop_fn, name_modifier='Scaled'):
    """Attach prop_fn to instance with name prop_name.
    Assumes that prop_fn takes self as an argument.
    Reference: https://stackoverflow.com/a/1355444/509706
    """
    class_name = instance.__class__.__name__ + name_modifier
    child_class = type(class_name, (instance.__class__,), {prop_name: property(prop_fn)})

    instance.__class__ = child_class

class BatchedKernel(torch.nn.Module):
    def __init__(self, constraint_mapping=None):
        super().__init__()
        self.constraints = dict()
        if constraint_mapping is None:
            self.constraint_mapping = default_mapping

    def forward(self, x1, x2):
        raise NotImplementedError()

    def diagonal(self, x):
        raise NotImplementedError()

    def symmetric(self, x):
        return self.forward(x, x)

    def covar_dist(self, x1, x2, square_dist=False):
        r"""
        ...Adapted and simplified from gpytorch...

        This is a helper method for computing the Euclidean distance between
        all pairs of points in x1 and x2.
        Args:
            :attr:`x1` (Tensor `n x d` or `b1 x ... x bk x n x d`):
                First set of data.
            :attr:`x2` (Tensor `m x d` or `b1 x ... x bk x m x d`):
                Second set of data.
            :attr:`square_dist` (bool):
                Should we square the distance matrix before returning?
        Returns:
            (:class:`Tensor`, :class:`Tensor) corresponding to the distance matrix between `x1` and `x2`.
        """
        x1_eq_x2 = torch.equal(x1, x2)
        res = None
        if square_dist:
            res = _sq_dist(x1, x2, x1_eq_x2)
        else:
            res = _dist(x1, x2, x1_eq_x2)
        return res

class RBFKernel(BatchedKernel):
    def __init__(self, lengthscale, amplitude=None, constraint_mapping=None, dtype=torch.get_default_dtype(),
                 device=torch.device(type='cpu'), **kwargs):
        super().__init__(constraint_mapping)
        self.constraints["amplitude"] = constraints.positive
        self.constraints["lengthscale"] = constraints.positive
        lengthscale = validate_ard_parameter(lengthscale, 'lengthscale', dtype, device)
        assert is_postive_tensor(
            lengthscale), f"expected positive initialization for lengthscale, but found a negative or zero number: {lengthscale}"
        self._lengthscale_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["lengthscale"]).inv(lengthscale)
        )

        self.post_process = lambda x: x
        amplitude = validate_scalar_parameter(amplitude, 'amplitude', dtype, device)
        if amplitude is not None:
            assert is_postive_tensor(
                amplitude), f"expected positive initialization for amplitude, but found a negative or zero number {amplitude}"
            self._amplitude_storage = torch.nn.Parameter(
                self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
            )
            attach_dyn_prop(self, 'amplitude',
                            lambda self: self.constraint_mapping(self.constraints["amplitude"])(
                                self._amplitude_storage))
            self.post_process = lambda x: self.amplitude * x

    @staticmethod
    def is_ard():
        return True

    @staticmethod
    def ard_params():
        return ['lengthscale']

    @property
    def lengthscale(self):
        transform = self.constraint_mapping(self.constraints["lengthscale"])
        return transform(self._lengthscale_storage)

    def extra_repr(self):
        return f"lengthscale={self.lengthscale.data}"

    def forward(self, x1, x2, diag=False, **params):
        x1_ = x1.div(self.lengthscale)
        x2_ = x2.div(self.lengthscale)
        r = self.covar_dist(x1_, x2_, square_dist=True)
        return self.post_process(r.div_(-2).exp_())

    def diagonal(self, x):
        if x.dim() == 1:
            x = x.unsqueeze(1)
        assert x.dim() in {2, 3}, "axis must be of (n, d) shape or (b, n, d) shape"
        if x.dim() == 3:
            o = torch.ones(x.shape[-3], x.shape[-2], device=x.device, dtype=x.dtype)
            return self.post_process(o.unsqueeze(-1))
        else:
            o = torch.ones(x.shape[-2], device=x.device, dtype=x.dtype)
            return self.post_process(o.unsqueeze(-1))


class MaternKernel(BatchedKernel):
    def __init__(self, lengthscale, nu=2.5, amplitude=None, constraint_mapping=None, dtype=torch.get_default_dtype(),
                 device=torch.device(type='cpu'), **kwargs):
        if nu not in {0.5, 1.5, 2.5}:
            raise RuntimeError("nu expected to be 0.5, 1.5, or 2.5")
        super().__init__(constraint_mapping)
        self.nu = nu
        self.constraints["amplitude"] = constraints.positive
        self.constraints["lengthscale"] = constraints.positive

        lengthscale = validate_ard_parameter(lengthscale, 'lengthscale', dtype, device)
        assert is_postive_tensor(
            lengthscale), f"expected positive initialization for lengthscale, but found a negative or zero number: {lengthscale}"
        self._lengthscale_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["lengthscale"]).inv(lengthscale)
        )

        self.post_process = lambda x: x
        amplitude = validate_scalar_parameter(amplitude, 'amplitude', dtype, device)
        if amplitude is not None:
            assert is_postive_tensor(
                amplitude), f"expected positive initialization for amplitude, but found a negative or zero number {amplitude}"
            self._amplitude_storage = torch.nn.Parameter(
                self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
            )
            attach_dyn_prop(self, 'amplitude',
                            lambda self: self.constraint_mapping(self.constraints["amplitude"])(
                                self._amplitude_storage))
            self.post_process = lambda x: self.amplitude * x

    @staticmethod
    def is_ard():
        return True

    @staticmethod
    def ard_params():
        return ['lengthscale']

    @property
    def lengthscale(self):
        transform = self.constraint_mapping(self.constraints["lengthscale"])
        return transform(self._lengthscale_storage)

    def extra_repr(self):
        return f"lengthscale={self.lengthscale.data}"

    def forward(self, x1, x2):
        mean = x1.reshape(-1, x1.size(-1)).mean(0)[(None,) * (x1.dim() - 1)]
        x1_ = (x1 - mean).div(self.lengthscale)
        x2_ = (x2 - mean).div(self.lengthscale)
        distance = self.covar_dist(x1_, x2_)
        exp_component = torch.exp(-math.sqrt(self.nu * 2) * distance)
        if self.nu == 0.5:
            constant_component = 1
        elif self.nu == 1.5:
            constant_component = (math.sqrt(3) * distance).add(1)
        elif self.nu == 2.5:
            constant_component = (math.sqrt(5) * distance).add(1).add(5.0 / 3.0 * distance ** 2)
        return self.post_process(constant_component * exp_component)

    def diagonal(self, x):
        if x.dim() == 1:
            x = x.unsqueeze(1)
        assert x.dim() in {2, 3}, "axis must be of (n, d) shape or (b, n, d) shape"
        if x.dim() == 3:
            o = torch.ones(x.shape[-3], x.shape[-2], device=x.device, dtype=x.dtype)
            return self.post_process(o.unsqueeze(-1))
        else:
            o = torch.ones(x.shape[-2], device=x.device, dtype=x.dtype)
            return self.post_process(o.unsqueeze(-1))


class LinearKernel(BatchedKernel):
    def __init__(self, offset, amplitude=None, constraint_mapping=None, dtype=torch.get_default_dtype(),
                 device=torch.device(type='cpu'), **kwargs):
        super().__init__(constraint_mapping)
        self.constraints["amplitude"] = constraints.positive

        offset = validate_scalar_parameter(offset, 'offset', dtype, device)
        self._offset_storage = torch.nn.Parameter(offset)

        self.post_process = lambda x: x
        amplitude = validate_scalar_parameter(amplitude, 'amplitude', dtype, device)
        if amplitude is not None:
            assert is_postive_tensor(
                amplitude), f"expected positive initialization for amplitude, but found a negative or zero number {amplitude}"
            self._amplitude_storage = torch.nn.Parameter(
                self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
            )
            attach_dyn_prop(self, 'amplitude',
                            lambda self: self.constraint_mapping(self.constraints["amplitude"])(
                                self._amplitude_storage))
            self.post_process = lambda x: self.amplitude * x

    @staticmethod
    def is_ard():
        return False

    @property
    def offset(self):
        return self._offset_storage

    def extra_repr(self):
        return f"offset={self.offset.data}"

    def forward(self, x1, x2):
        x1_ = x1 - self.offset
        x2_ = x2 - self.offset
        if x1_.dim() == 3 and x2_.dim() == 3:
            return self.post_process(torch.bmm(x1_, x2_.transpose(-2, -1)))
        elif x1_.dim() == 2 and x2_.dim() == 2:
            return self.post_process(torch.mm(x1_, x2_.t()))
        elif x1_.dim() > 3 and x2_.dim() == x1_.dim():
            return self.post_process(x1_ @ x2_.transpose(-2, -1))
        else:
            raise ValueError(
                f'Unsupported dimensionality of args, got x1.dim() = {x1_.dim()} and x2.dim() = {x1_.dim()}')

    def diagonal(self, x1):
        x1_ = x1 - self.offset
        return self.post_process((x1_ * x1_).sum(dim=-1, keepdim=True))


class RationalQuadraticKernel(BatchedKernel):
    def __init__(self, lengthscale, alpha=1.0, amplitude=None, constraint_mapping=None, dtype=torch.get_default_dtype(),
                 device=torch.device(type='cpu'), **kwargs):
        """

        :param lengthscale: 1xD, allows for a lengthscale per dim
        :param amplitude: scalar, controls amplitude of kernel
        :param alpha: scalar, controls lengthscale mixing and is shared over all dims
        """
        super().__init__(constraint_mapping)
        self.constraints["amplitude"] = constraints.positive
        self.constraints["lengthscale"] = constraints.positive
        self.constraints["alpha"] = constraints.positive

        lengthscale = validate_ard_parameter(lengthscale, 'lengthscale', dtype, device)
        assert is_postive_tensor(
            lengthscale), f"expected positive initialization for lengthscale, but found a negative or zero number: {lengthscale}"
        self._lengthscale_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["lengthscale"]).inv(lengthscale)
        )
        alpha = validate_scalar_parameter(alpha, 'alpha', dtype, device)

        assert is_postive_tensor(
            alpha), f"expected positive initialization for alpha, but found a negative or zero number: {alpha}"
        self._alpha_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["alpha"]).inv(alpha)
        )

        self.post_process = lambda x: x
        amplitude = validate_scalar_parameter(amplitude, 'amplitude', dtype, device)
        if amplitude is not None:
            assert is_postive_tensor(
                amplitude), f"expected positive initialization for amplitude, but found a negative or zero number {amplitude}"
            self._amplitude_storage = torch.nn.Parameter(
                self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
            )
            attach_dyn_prop(self, 'amplitude',
                            lambda self: self.constraint_mapping(self.constraints["amplitude"])(
                                self._amplitude_storage))
            self.post_process = lambda x: self.amplitude * x

    @staticmethod
    def is_ard():
        return True

    @staticmethod
    def ard_params():
        return ['lengthscale']

    @property
    def lengthscale(self):
        transform = self.constraint_mapping(self.constraints["lengthscale"])
        return transform(self._lengthscale_storage)

    @property
    def alpha(self):
        transform = self.constraint_mapping(self.constraints["alpha"])
        return transform(self._alpha_storage)

    def extra_repr(self):
        return f"lengthscale={self.lengthscale.data}, alpha={self.alpha.data}"

    def forward(self, x1, x2, diag=False, **params):
        x1_ = x1.div(self.lengthscale)
        x2_ = x2.div(self.lengthscale)
        r = self.covar_dist(x1_, x2_, square_dist=True)
        return self.post_process((1. + r.div(2 * self.alpha)).pow(-self.alpha))

    def diagonal(self, x):
        if x.dim() == 1:
            x = x.unsqueeze(1)
        assert x.dim() in {2, 3}, "axis must be of (n, d) shape or (b, n, d) shape"
        if x.dim() == 3:
            o = torch.ones(x.shape[-3], x.shape[-2], device=x.device, dtype=x.dtype)
            return self.post_process(o.unsqueeze(-1))
        else:
            o = torch.ones(x.shape[-2], device=x.device, dtype=x.dtype)
            return self.post_process(o.unsqueeze(-1))


class PeriodicKernel(BatchedKernel):
    def __init__(self, lengthscale, period, amplitude=None, constraint_mapping=None, dtype=torch.get_default_dtype(),
                 device=torch.device(type='cpu'), **kwargs):
        """

        :param lengthscale: scalar, does NOT allow for a lengthscale per dim
        :param amplitude: scalar, controls amplitude of kernel
        :param period: scalar
        """
        super().__init__(constraint_mapping)
        self.constraints["amplitude"] = constraints.positive
        self.constraints["lengthscale"] = constraints.positive
        self.constraints["period"] = constraints.positive

        lengthscale = validate_scalar_parameter(lengthscale, 'lengthscale',
                                                dtype, device)
        assert is_postive_tensor(
            lengthscale), f"expected positive initialization for lengthscale, but found a negative or zero number: {lengthscale}"
        self._lengthscale_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["lengthscale"]).inv(lengthscale)
        )

        period = validate_scalar_parameter(period, 'period', dtype, device)
        assert is_postive_tensor(
            period), f"expected positive initialization for period, but found a negative or zero number: {period}"
        self._period_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["period"]).inv(period)
        )

        self.post_process = lambda x: x
        amplitude = validate_scalar_parameter(amplitude, 'amplitude', dtype, device)
        if amplitude is not None:
            assert is_postive_tensor(
                amplitude), f"expected positive initialization for amplitude, but found a negative or zero number {amplitude}"
            self._amplitude_storage = torch.nn.Parameter(
                self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
            )
            attach_dyn_prop(self, 'amplitude',
                            lambda self: self.constraint_mapping(self.constraints["amplitude"])(
                                self._amplitude_storage))
            self.post_process = lambda x: self.amplitude * x

    @staticmethod
    def is_ard():
        return False

    @property
    def lengthscale(self):
        transform = self.constraint_mapping(self.constraints["lengthscale"])
        return transform(self._lengthscale_storage)

    @property
    def period(self):
        transform = self.constraint_mapping(self.constraints["period"])
        return transform(self._period_storage)

    def extra_repr(self):
        return f"lengthscale={self.lengthscale.data}, period={self.period.data}"

    def forward(self, x1, x2, diag=False, **params):
        x1_ = x1.div(self.period)
        x2_ = x2.div(self.period)
        r = self.covar_dist(x1_, x2_, square_dist=False) * math.pi
        return self.post_process(r.sin().div(self.lengthscale).pow(2).mul(-2).exp())

    def diagonal(self, x):
        if x.dim() == 1:
            x = x.unsqueeze(1)
        assert x.dim() in {2, 3}, "axis must be of (n, d) shape or (b, n, d) shape"
        if x.dim() == 3:
            o = torch.ones(x.shape[-3], x.shape[-2], device=x.device, dtype=x.dtype)
            return self.post_process(o.unsqueeze(-1))
        else:
            o = torch.ones(x.shape[-2], device=x.device, dtype=x.dtype)
            return self.post_process(o.unsqueeze(-1))


class ConstantKernel(BatchedKernel):
    def __init__(self, constant, amplitude=None, constraint_mapping=None, dtype=torch.get_default_dtype(),
                 device=torch.device(type='cpu'), **kwargs):
        """

        :param lengthscale: scalar, does NOT allow for a lengthscale per dim
        :param amplitude: scalar, controls amplitude of kernel
        :param period: scalar
        """
        super().__init__(constraint_mapping)
        self.constraints["amplitude"] = constraints.positive
        self.constraints["constant"] = constraints.positive

        constant = validate_scalar_parameter(constant, 'constant',
                                             dtype, device)
        assert is_postive_tensor(
            constant), f"expected positive initialization for constant, but found a negative or zero number: {constant}"
        self._constant_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["constant"]).inv(constant)
        )

        self.post_process = lambda x: x
        amplitude = validate_scalar_parameter(amplitude, 'amplitude', dtype, device)
        if amplitude is not None:
            assert is_postive_tensor(
                amplitude), f"expected positive initialization for amplitude, but found a negative or zero number {amplitude}"
            self._amplitude_storage = torch.nn.Parameter(
                self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
            )
            attach_dyn_prop(self, 'amplitude',
                            lambda self: self.constraint_mapping(self.constraints["amplitude"])(
                                self._amplitude_storage))
            self.post_process = lambda x: self.amplitude * x

    @staticmethod
    def is_ard():
        return False

    @property
    def constant(self):
        transform = self.constraint_mapping(self.constraints["constant"])
        return transform(self._constant_storage)

    def extra_repr(self):
        return f"constant={self.constant.data}"

    def forward(self, x1, x2, diag=False, **params):
        assert x1.dim() > 2, "axis must be of (n, d) shape or (..., n, d) shape"
        assert x1.dim() == x2.dim(), "dimensionality of x1 and x2 must be the same"
        o = torch.ones(list(x1.shape[:-2]) + [x1.shape[-2], x2.shape[-2]], dtype=x1.dtype, device=x1.device)
        r = self.constant * o
        return self.post_process(r)

    def diagonal(self, x):
        assert x.dim() in {2, 3}, "axis must be of (n, d) shape or (b, n, d) shape"
        if x.dim() == 3:
            o = torch.ones(x.shape[-3], x.shape[-2], 1, dtype=x.dtype, device=x.device)
        else:
            o = torch.ones(x.shape[-2], 1, dtype=x.dtype, device=x.device)
        r = self.constant * o
        return self.post_process(r)


class NSMaternKernel(BatchedKernel):
    """
    A quirky hack kernel; might delete if it doesn't work out. The idea is that it takes N+1 dimensional inputs, with
    the first N dims being passed to a MaternKernel and the last dim passed to a linear kernel. Then we take the prod-
    uct of these two kernels to form a non-stationary kernel.
    """
    def __init__(self, lengthscale, nu=2.5, amplitude=None, constraint_mapping=None, dtype=torch.get_default_dtype(),
                 device=torch.device(type='cpu'), **kwargs):
        super().__init__(constraint_mapping)
        if nu not in {0.5, 1.5, 2.5}:
            raise RuntimeError("nu expected to be 0.5, 1.5, or 2.5")
        super().__init__(constraint_mapping)
        self.nu = nu
        self.constraints["amplitude"] = constraints.positive
        self.constraints["lengthscale"] = constraints.positive

        lengthscale = validate_ard_parameter(lengthscale, 'lengthscale', dtype, device)
        assert is_postive_tensor(
            lengthscale), f"expected positive initialization for lengthscale, but found a negative or zero number: {lengthscale}"
        self._lengthscale_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["lengthscale"]).inv(lengthscale)
        )

        self.post_process = lambda x: x
        amplitude = validate_scalar_parameter(amplitude, 'amplitude', dtype, device)
        if amplitude is not None:
            assert is_postive_tensor(
                amplitude), f"expected positive initialization for amplitude, but found a negative or zero number {amplitude}"
            self._amplitude_storage = torch.nn.Parameter(
                self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
            )
            attach_dyn_prop(self, 'amplitude',
                            lambda self: self.constraint_mapping(self.constraints["amplitude"])(
                                self._amplitude_storage))
            self.post_process = lambda x: self.amplitude * x

    @staticmethod
    def is_ard():
        return True

    @staticmethod
    def ard_params():
        return ['lengthscale']

    @property
    def lengthscale(self):
        transform = self.constraint_mapping(self.constraints["lengthscale"])
        return transform(self._lengthscale_storage)

    def extra_repr(self):
        return f"lengthscale={self.lengthscale.data}"

    def forward(self, x1, x2):
        assert x1.dim() >= 2
        assert x1.shape[-1] >= 2, f"expecting at least 2 dims in final dim, got {x1.shape[-1]}"

        x1L_ = x1[...,-1].unsqueeze(-1)
        x2L_ = x2[...,-1].unsqueeze(-1)
        if x1L_.dim() == 3 and x2L_.dim() == 3:
            NS_component = torch.bmm(x1L_, x2L_.transpose(-2, -1))
        elif x1L_.dim() == 2 and x2L_.dim() == 2:
            NS_component = torch.mm(x1L_, x2L_.t())
        elif x1L_.dim() >= 2 and x2L_.dim() >= 2: #== x1L_.dim():
            NS_component = x1L_ @ x2L_.transpose(-2, -1)
        else:
            raise ValueError(
                f'Unsupported dimensionality of args, got x1.dim() = {x1L_.dim()} and x2.dim() = {x2L_.dim()}, for x1.shape {x1.shape}')
        x1_ = x1[...,:-1]
        x2_ = x2[..., :-1]
        mean = x1_.reshape(-1, x1_.size(-1)).mean(0)[(None,) * (x1_.dim() - 1)]
        x1_ = (x1_ - mean).div(self.lengthscale)
        x2_ = (x2_ - mean).div(self.lengthscale)
        distance = self.covar_dist(x1_, x2_)
        exp_component = torch.exp(-math.sqrt(self.nu * 2) * distance)
        if self.nu == 0.5:
            constant_component = 1
        elif self.nu == 1.5:
            constant_component = (math.sqrt(3) * distance).add(1)
        elif self.nu == 2.5:
            constant_component = (math.sqrt(5) * distance).add(1).add(5.0 / 3.0 * distance ** 2)
        return self.post_process(constant_component * exp_component * NS_component)

    def diagonal(self, x):
        assert x.dim() >= 2
        assert x.shape[-1] >= 2, f"expecting at least 2 dims in final dim, got {x.shape[-1]}"
        assert x.dim() in {2, 3}, "axis must be of (n, d) shape or (b, n, d) shape"
        xL_ = x[...,-1].unsqueeze(-1)
        NS_component = xL_.pow(2)
        x_ = x[...,:-1]
        if x_.dim() == 3:
            o = torch.ones(x_.shape[-3], x_.shape[-2], device=x_.device, dtype=x_.dtype)
            return self.post_process(o.unsqueeze(-1) * NS_component)
        else:
            o = torch.ones(x_.shape[-2], device=x_.device, dtype=x_.dtype)
            return self.post_process(o.unsqueeze(-1) * NS_component)


class ScaledGibbsKernel(BatchedKernel):
    """
    Based on https://papers.nips.cc/paper/7050-non-stationary-spectral-kernels.pdf, except that I fix it to a single,
    zero-frequency component, resulting in an amplitude scaled GibbsKernel (hence the name)
    """
    def __init__(self, constraint_mapping=None, dtype=torch.get_default_dtype(),
                 device=torch.device(type='cpu'), **kwargs):
        super().__init__(constraint_mapping)

    @staticmethod
    def is_ard():
        return False

    def extra_repr(self):
        return f"a kernel defined by auxillary processes"

    def forward(self, x1, x2, diag=False, **params):
        assert x1.shape[-1] >= 3, f"expected final dim to be 3 at least, got {x1.shape[-1]}"
        assert x2.shape[-1] >= 3, f"expected final dim to be 3 at least, got {x2.shape[-1]}"
        # we interpret input as [input_dims, w, l], where w,l are single dimensional
        lx1 = x1[...,-1].unsqueeze(-1).exp()
        lx2 = x2[...,-1].unsqueeze(-1).exp()
        wx1 = x1[...,-2].unsqueeze(-1).exp()
        wx2 = x2[...,-2].unsqueeze(-1).exp()
        x1_ = x1[..., slice(0,x1.shape[-1]-2)]
        x2_ = x2[..., slice(0,x2.shape[-1]-2)]
        denom = lx1.pow(2) + lx2.pow(2).transpose(-2,-1)
        num = 2.0*lx1*lx2.transpose(-2,-1)
        norm_part = (num/denom).sqrt()
        amp_part = wx1*wx2.transpose(-2,-1)
        r = self.covar_dist(x1_, x2_, square_dist=True)
        exp_part = r.div(denom).mul(-1).exp()
        return amp_part * norm_part * exp_part

    def diagonal(self, x):
        assert x.shape[-1] >= 3, f"expected final dim to be 3 at least, got {x.shape[-1]}"
        # lx = x[:,-1].exp()
        wx = x[...,-2].unsqueeze(-1).exp()
        amp_part = wx.pow(2)
        exp_part = torch.ones_like(x[...,-1]).unsqueeze(-1)
        return amp_part * exp_part


class BatchedGibbsKernel(BatchedKernel):
    """
    Batched version of classic Gibbs Kernel
    """
    def __init__(self, amplitude=None, constraint_mapping=None, dtype=torch.get_default_dtype(),
                 device=torch.device(type='cpu'), **kwargs):
        super().__init__(constraint_mapping)
        self.constraints["amplitude"] = constraints.positive
        self.post_process = lambda x: x
        amplitude = validate_scalar_parameter(amplitude, 'amplitude', dtype, device)
        if amplitude is not None:
            assert is_postive_tensor(
                amplitude), f"expected positive initialization for amplitude, but found a negative or zero number {amplitude}"
            self._amplitude_storage = torch.nn.Parameter(
                self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
            )
            attach_dyn_prop(self, 'amplitude',
                            lambda self: self.constraint_mapping(self.constraints["amplitude"])(
                                self._amplitude_storage))
            self.post_process = lambda x: self.amplitude * x

    @staticmethod
    def is_ard():
        return False

    def extra_repr(self):
        return f"a kernel defined by auxillary processes"

    def forward(self, x1, x2, diag=False, **params):
        assert x1.shape[-1] >= 2, f"expected final dim to be 2 at least, got {x1.shape[-1]}"
        assert x2.shape[-1] >= 2, f"expected final dim to be 2 at least, got {x2.shape[-1]}"
        # we interpret input as [input_dims, l], where l are single dimensional
        lx1 = x1[...,-1].unsqueeze(-1).exp()
        lx2 = x2[...,-1].unsqueeze(-1).exp()
        x1_ = x1[..., slice(0,x1.shape[-1]-1)]
        x2_ = x2[..., slice(0,x2.shape[-1]-1)]
        denom = lx1.pow(2) + lx2.pow(2).transpose(-2,-1)
        num = 2.0*lx1*lx2.transpose(-2,-1)
        norm_part = (num/denom).sqrt()
        r = self.covar_dist(x1_, x2_, square_dist=True)
        exp_part = r.div(denom).mul(-1).exp()
        return self.post_process(norm_part * exp_part)

    def diagonal(self, x):
        assert x.shape[-1] >= 2, f"expected final dim to be 2 at least, got {x.shape[-1]}"
        exp_part = torch.ones_like(x[...,-1]).unsqueeze(-1)
        return self.post_process(exp_part)


def kernel_factory(dict_in, **kwargs):
    """
    This is a simple factory function for BatchedKernels. Using a specific format for dict_in, specified below, it returns
    initialized kernel modules.
    :param dict_in: Expects a dictionary like {'Kernel Name': {'kernel_param_0': ___, 'kernel_param_1': ___, ...}}
    :return: a kernel module

    example to make an 2D RBF: dict_in {'RBFKernel': {'lengthscale': [0.1, 0.2], 'amplitude': 1.2}}
    """
    registry = {'LinearKernel': LinearKernel,
                'RBFKernel': RBFKernel,
                'RationalQuadraticKernel': RationalQuadraticKernel,
                'PeriodicKernel': PeriodicKernel,
                'ConstantKernel': ConstantKernel,
                'MaternKernel': MaternKernel,
                'NSMaternKernel': NSMaternKernel,
                'ScaledGibbsKernel': ScaledGibbsKernel,
                'BatchedGibbsKernel': BatchedGibbsKernel,
                'CombinationKernel': CombinationKernel,
                'CombinationSharedKernel': CombinationSharedKernel}
    num_gps = None if 'num_gps' not in kwargs else kwargs['num_gps']
    dtype = None if not 'dtype' in kwargs else kwargs['dtype']
    in_features = None if 'in_features' not in kwargs else kwargs['in_features']
    overwrite_ard = None if 'overwrite_ard' not in kwargs else kwargs['overwrite_ard']
    if not overwrite_ard:
        overwrite_ard = None
    dry_run = None if 'dry_run' not in kwargs else kwargs['dry_run']
    rep_kernels = None if 'rep_kernels' not in kwargs else kwargs['rep_kernels']
    kwargs = {'num_gps': num_gps, 'dtype': dtype, 'in_features': in_features, 'overwrite_ard': overwrite_ard,
              'dry_run': dry_run, 'rep_kernels': rep_kernels}
    for key in dict_in.keys():
        if key in registry.keys():
            if issubclass(registry[key], AbstractMOKernel):
                d = {}
                if dtype is not None:
                    d['dtype'] = dtype
                d['kernel_dict'] = dict_in[key]
                assert num_gps is not None, f"expected kwarg num_gps when requesting an MOKernel"
                d['num_gps'] = num_gps
                if registry[key] == CombinationKernel:
                    assert isinstance(dict_in[key], list), f"expected a list of kernel dicts, got {type(dict_in[key])}"
                    if not rep_kernels:
                        assert len(dict_in[key]) == num_gps, \
                            f"number of kernel dicts inconsistent with num_gps, got {len(dict_in[key])}, expected {num_gps}"
                    else:
                        d['kernel_dict'] = num_gps*[dict_in[key][0]]
                    for i,dkern in enumerate(d['kernel_dict']):
                        # this will check assertions on all kernels and apply corrections to dict
                        d['kernel_dict'][i] = kernel_factory(dkern, **{**kwargs, 'dry_run': True})
                else:
                    # this will check assertions on the kernel dict and apply corrections if needed
                    d['kernel_dict'] = kernel_factory(d['kernel_dict'], **{**kwargs, 'dry_run': True})
                if dry_run is None:
                    return registry[key](**d)
                else:
                    return {key: d}
            else:
                d = {**dict_in[key]}
                if dtype is not None:
                    d['dtype'] = dtype
                if registry[key].is_ard() and in_features is not None:
                    for p in registry[key].ard_params():
                        if not np.isscalar(d[p]):
                        # if its scalar, then its shared and no problem
                            if overwrite_ard is None: # don't over-write, just check
                                    assert len(d[p]) == in_features, \
                                        f"length of ard params ({len(d[p])}) does not match in_features ({in_features})"
                            else:
                                #then we over-write, we will over-write by repeating the first element, but only if
                                #over-writing is necessary
                                ard_p = list(d[p])
                                if not len(ard_p) == in_features:
                                    new_ard_p = in_features*[ard_p[0]]
                                    d[p] = new_ard_p
                if dry_run is None:
                    return registry[key](**d)
                else:
                    return {key: d}


class AbstractMOKernel(BatchedKernel):
    def __init__(self, num_gps: int, dtype=torch.get_default_dtype(), device=torch.device(type='cpu'), constraint_mapping=None):
        super().__init__(constraint_mapping)
        self.dtype = dtype
        self.device = device
        num_gps = validate_scalar_parameter(num_gps, 'num_gps', torch.int64, self.device)
        self.register_buffer('num_gps', num_gps)

    def _validate_dual_inputs(self, x1, x2):
        # assert x1.dim() == x2.dim(), \
        #     f"x1 and x2 must have same dimensionality, got x1.dim() = {x1.dim()} and x2.dim() = {x2.dim()}"
        # x1.dim() may not be same as x2.dim() when x1 has batch dims and x2 is an inducing input
        assert x1.dim() >= 3, f"expecting x1.dim() >= 3, got {x1.dim()}"
        assert x1.shape[-3] == self.num_gps, \
            f"expected batch dim (-3) to have length {self.num_gps}, got {x1.shape[-3]}"
        assert x1.shape[-3] == x2.shape[
            -3], f"x1.shape[-3] must match x2.shape[-3], got {x1.shape[-3]} and {x2.shape[-3]} respectively"
        return x1, x2

    def _validate_input(self, x1):
        assert x1.dim() >= 3, f"expecting x1.dim() >= 3, got {x1.dim()}"
        assert x1.shape[-3] == self.num_gps, \
            f"expected batch dim (-3) to have length {len(self.kernels)}, got {x1.shape[-3]}"
        return x1

    def forward(self, x1, x2):
        raise NotImplementedError()

    def diagonal(self, x):
        raise NotImplementedError()

class CombinationKernel(AbstractMOKernel):
    def __init__(self, num_gps: int, kernel_dict: list, dtype=torch.get_default_dtype(), device=torch.device(type='cpu'), constraint_mapping=None, **kwargs):
        super().__init__(num_gps, dtype, device, constraint_mapping)
        """
        Builds a combination of kernels, where each kernel gets evaluated into a separate batch dimension.

        kernel_dicts is a list of dictionaries that will be passed to kernel_factory to instantiate
        the base kernels.

        input axis must have a batch dim (dim = -3) corresponding to number of kernels 
        """
        kernel_list = []
        for k in kernel_dict:
            kernel_list.append(kernel_factory(k, dtype=dtype))
        for k in kernel_list:
            if k is None:
                raise ValueError('malformed kernel dict passed, failed to construct')
        assert len(kernel_list) == self.num_gps, f"expected {self.num_gps} kernels, only got {len(kernel_list)}"
        self.kernels = torch.nn.ModuleList(kernel_list)

    def forward(self, x1, x2):
        x1, x2 = self._validate_dual_inputs(x1, x2)
        result = torch.stack([k(x1[...,i,:,:], x2[...,i,:,:]) for i,k in enumerate(self.kernels)], dim=-3)
        return result

    def diagonal(self, x1):
        x1 = self._validate_input(x1)
        result = torch.stack([k.diagonal(x1[...,i,:,:]) for i,k in enumerate(self.kernels)], dim=-3)
        return result

class CombinationSharedKernel(AbstractMOKernel):
    def __init__(self, num_gps: int, kernel_dict: dict, dtype=torch.get_default_dtype(), device=torch.device(type='cpu'), constraint_mapping=None, **kwargs):
        super().__init__(num_gps, dtype, device, constraint_mapping)
        """
        This is a wrapper for a single kernel that allows it to be created similarly to a combination kernel. Only a single kernel is used
        and consequently, all it does is add assertions to the forward of the single underlying batch kernel. To make the assertions
        work, we also store the expected size of the input batch dim as a buffer parameter of this wrapper.

        input axis must have a batch dim (dim = -3) corresponding to number of kernels 
        """
        self.kernel = kernel_factory(kernel_dict, dtype=dtype)
        if self.kernel is None:
            raise ValueError('malformed kernel dict passed, failed to construct')

    def forward(self, x1, x2):
        x1, x2 = self._validate_dual_inputs(x1, x2)
        result = self.kernel(x1,x2)
        return result

    def diagonal(self, x1):
        x1 = self._validate_input(x1)
        result = self.kernel(x1)
        return result


class KernelMixer(torch.nn.Module):
    def __init__(self, n_kern_in, n_kern_out, dtype=torch.get_default_dtype()):
        super().__init__()
        # this initialization scheme taken from
        # https://github.com/ssydasheng/GPflow-Slim/blob/master/gpflowSlim/neural_kernel_network/neural_kernel_network_wrapper.py
        min_w, max_w = 1. / (2 * n_kern_in), 3. / (2 * n_kern_in)
        t = SoftplusTransform()
        _weights = t.inv(torch.empty(int(n_kern_out) * 2, int(n_kern_in), dtype=dtype).uniform_(min_w, max_w))
        self._weights = torch.nn.Parameter(_weights)
        _bias = t.inv(torch.ones(2 * int(n_kern_out), dtype=dtype) * 0.01)
        self._bias = torch.nn.Parameter(_bias)

    @property
    def weights(self):
        return F.softplus(self._weights)

    @property
    def bias(self):
        return F.softplus(self._bias)

    def forward(self, Ks):
        assert len(Ks) == self.weights.shape[1]
        if Ks.dim() == 4:
            o = torch.einsum('ab,bcde->acde', self.weights, Ks) + self.bias.unsqueeze(-1).unsqueeze(-1).unsqueeze(-1)
        elif Ks.dim() == 3:
            o = torch.einsum('ab,bcd->acd', self.weights, Ks) + self.bias.unsqueeze(-1).unsqueeze(-1)
        else:
            raise ValueError(f'expected 3 or 4 dims of inputs, got {Ks.dim()}')

        return o[0::2, ...] * o[1::2, ...]


class NeuralKernelNetwork(BatchedKernel):
    def __init__(self, kernel_features, arch=[8, 4, 2, 1],
                 dtype=torch.get_default_dtype(), device=torch.device(type='cpu'),
                 constraint_mapping=None):
        super().__init__(constraint_mapping)
        """
        Builds a neural kernel network like published in https://arxiv.org/abs/1806.04326

        kernel_features is a list of dictionaries that will be passed to kernel_factory to instantiate
        the base kernels.

        arch is a list of integers indicating the # of output kernels at each layer. Last element must be 1.
        """
        assert int(arch[-1]) == 1, f"final integer in arch must be 1, got {arch[-1]}"
        kernel_list = []
        for k in kernel_features:
            kernel_list.append(kernel_factory(k))
        self.features = torch.nn.ModuleList(kernel_list)
        mixer_list = []
        for i, m in enumerate(arch):
            if i == 0:
                n_in = len(kernel_list)
            else:
                n_in = int(arch[i - 1])
            n_out = int(m)
            mixer_list.append(KernelMixer(n_in, n_out))
        self.mixers = torch.nn.ModuleList(mixer_list)

    def forward(self, x1, x2):
        X = torch.stack([k(x1, x2) for k in self.features], dim=0)
        for m in self.mixers:
            X = m(X)
        return X.squeeze(0)

    def diagonal(self, x1):
        X = torch.stack([k.diagonal(x1) for k in self.features], dim=0)
        for m in self.mixers:
            X = m(X)
        return X.squeeze(0)


##### OLD STYLE KERNELS FOLLOW #######


# class ARDRBFKernel(Kernel):
#     def __init__(self, lengthscale, amplitude=1., transforms=None):
#         super().__init__(transforms)
#
#         amplitude = torch.tensor(amplitude, dtype=lengthscale.dtype, device=lengthscale.device)
#
#         if transforms is None:
#             self._lengthscale_storage = torch.nn.Parameter(lengthscale)
#             self._amplitude_storage = torch.nn.Parameter(amplitude)
#         else:
#             assert len(transforms) == 2, "too few transforms passed"
#             self._lengthscale_storage = torch.nn.Parameter(self._transforms[0].inv(lengthscale))
#             self._amplitude_storage = torch.nn.Parameter(self._transforms[1].inv(amplitude))
#
#     @property
#     def lengthscale(self):
#         if self._transforms is None:
#             return self._lengthscale_storage
#
#         return self._transforms[0](self._lengthscale_storage)
#
#     @property
#     def amplitude(self):
#         if self._transforms is None:
#             return self._amplitude_storage
#
#         return self._transforms[1](self._amplitude_storage)
#
#     def extra_repr(self):
#         return f"lengthscale={self.lengthscale.data}, amplitude={self.amplitude.data}"
#
#     def forward(self, x, xp):
#         assert x.shape[-1] == xp.shape[-1], "dimensionality mismatch"
#
#         n, d = x.shape
#         p, d = xp.shape
#
#         X = x.unsqueeze(-2).expand((n, p, d)).reshape(-1, d)
#         Xp = xp.unsqueeze(-3).expand((n, p, d)).reshape(-1, d)
#
#         K = self.amplitude*ard_rbf_kernel(X, Xp, self.lengthscale)
#         return K.reshape(n, p)
#
#     def diagonal(self, x):
#         assert x.dim() == 2, "axis must be of (n, d) shape"
#         return self.amplitude*ard_rbf_kernel(x, x, self.lengthscale).reshape(-1, 1)


class ARDRBFKernel(Kernel):
    def __init__(self, lengthscale, amplitude=1.0, constraint_mapping=None):
        super().__init__(constraint_mapping)
        self.constraints["amplitude"] = constraints.positive
        self.constraints["lengthscale"] = constraints.positive

        amplitude = torch.tensor(
            amplitude, dtype=lengthscale.dtype, device=lengthscale.device
        )

        self._amplitude_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
        )
        self._lengthscale_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["lengthscale"]).inv(lengthscale)
        )

    @property
    def lengthscale(self):
        transform = self.constraint_mapping(self.constraints["lengthscale"])
        return transform(self._lengthscale_storage)

    @property
    def amplitude(self):
        transform = self.constraint_mapping(self.constraints["amplitude"])
        return transform(self._amplitude_storage)

    def extra_repr(self):
        return f"lengthscale={self.lengthscale.data}, amplitude={self.amplitude.data}"

    def forward(self, x, y):
        lengthscale = self.lengthscale
        return self.amplitude * my_rbf_kernel(x, y, lengthscale)

    def diagonal(self, x):
        if x.dim() == 1:
            x = x.unsqueeze(1)
        assert x.dim() == 2, "axis must be of (n, d) shape"
        return self.amplitude * torch.ones(
            x.shape[0], device=x.device, dtype=x.dtype
        ).reshape(-1, 1)


class SafeARDRBFKernel(Kernel):
    def __init__(
        self, lengthscale, amplitude=1.0, jitter=1e-4, constraint_mapping=None
    ):
        super().__init__(constraint_mapping)

        self.constraints["amplitude"] = constraints.positive
        self.constraints["lengthscale"] = constraints.positive

        amplitude = torch.tensor(
            amplitude, dtype=lengthscale.dtype, device=lengthscale.device
        )
        if jitter < 0.0:
            raise ValueError("Need a positive jitter value")
        jitter = torch.tensor(
            jitter, dtype=lengthscale.dtype, device=lengthscale.device
        )
        self.register_buffer("jitter", jitter)
        self.register_buffer(
            "delta",
            torch.tensor(1e-8, dtype=lengthscale.dtype, device=lengthscale.device),
        )

        self._amplitude_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
        )
        self._lengthscale_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["lengthscale"]).inv(lengthscale)
        )

    @property
    def lengthscale(self):
        transform = self.constraint_mapping(self.constraints["lengthscale"])
        return transform(self._lengthscale_storage)

    @property
    def amplitude(self):
        transform = self.constraint_mapping(self.constraints["amplitude"])
        return transform(self._amplitude_storage)

    def extra_repr(self):
        return f"lengthscale={self.lengthscale.data}, amplitude={self.amplitude.data}"

    def forward(self, x, y):
        lengthscale = self.lengthscale
        return self.amplitude * my_rbf_kernel(
            x, y, lengthscale
        ) + self.amplitude * self.jitter * my_rbf_kernel(x, y, self.delta)

    def diagonal(self, x):
        if x.dim() == 1:
            x = x.unsqueeze(1)
        assert x.dim() == 2, "axis must be of (n, d) shape"
        return (
            self.amplitude
            * (1 + self.jitter)
            * torch.ones(x.shape[0], device=x.device, dtype=x.dtype).reshape(-1, 1)
        )


class GibbsKernel(Kernel):
    def __init__(self, lengthscale_fun, amplitude=1.0, constraint_mapping=None):
        super().__init__(constraint_mapping)
        self.constraints["amplitude"] = constraints.positive
        self.constraints["lengthscale"] = constraints.positive

        self._lengthscale_fun = lengthscale_fun

        amplitude = torch.tensor([amplitude])
        self._amplitude_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
        )

    def lengthscale(self, ax):
        transform = self.constraint_mapping(self.constraints["lengthscale"])
        return transform(self._lengthscale_fun(ax))

    @property
    def amplitude(self):
        transform = self.constraint_mapping(self.constraints["amplitude"])
        return transform(self._amplitude_storage)

    def forward(self, x, xp):
        assert x.shape[-1] == xp.shape[-1], "dimensionality mismatch"

        n, d = x.shape
        p, d = xp.shape

        X = x.unsqueeze(-2).expand((n, p, d)).reshape(-1, d)
        Xp = xp.unsqueeze(-3).expand((n, p, d)).reshape(-1, d)

        delta = (X - Xp).pow(2).sum(dim=-1).clamp(min=1.0e-36).sqrt()

        length = self.lengthscale(X).sum(dim=-1)  # constrain size to 1D
        lengthp = self.lengthscale(Xp).sum(dim=-1)

        num = 2.0 * length * lengthp
        denom = length * length + lengthp * lengthp
        prefactor = self.amplitude * torch.sqrt(num / denom)
        K = prefactor * (-delta.pow(2) / denom).exp()

        return K.reshape(n, p)

    def diagonal(self, x):
        assert x.dim() == 2, "axis must be of (n, d) shape"
        return self.amplitude * x.new_ones(x.shape)


class Matern32(Kernel):
    def __init__(self, lengthscale, amplitude=1.0, constraint_mapping=None):
        super().__init__(constraint_mapping)

        self.constraints["amplitude"] = constraints.positive
        self.constraints["lengthscale"] = constraints.positive

        amplitude = torch.tensor(
            amplitude, dtype=lengthscale.dtype, device=lengthscale.device
        )

        self._amplitude_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
        )
        self._lengthscale_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["lengthscale"]).inv(lengthscale)
        )

    @property
    def lengthscale(self):
        transform = self.constraint_mapping(self.constraints["lengthscale"])
        return transform(self._lengthscale_storage)

    @property
    def amplitude(self):
        transform = self.constraint_mapping(self.constraints["amplitude"])
        return transform(self._amplitude_storage)

    def extra_repr(self):
        return f"lengthscale={self.lengthscale.data}, amplitude={self.amplitude.data}"

    def forward(self, x, xp):
        assert x.shape[-1] == xp.shape[-1], "dimensionality mismatch"

        n, d = x.shape
        p, d = xp.shape

        X = x.unsqueeze(-2).expand((n, p, d)).reshape(-1, d)
        Xp = xp.unsqueeze(-3).expand((n, p, d)).reshape(-1, d)

        delta = (X - Xp).pow(2).sum(dim=-1).clamp(min=1.0e-36).sqrt()
        distance = delta * math.sqrt(3.0) * self.lengthscale.reciprocal()
        K = (1 + distance) * (-distance).exp()
        return self.amplitude * K.reshape(n, p)

    def diagonal(self, x):
        assert x.dim() == 2, "axis must be of (n, d) shape"
        return self.amplitude * torch.ones(x.shape[0], device=x.device).reshape(-1, 1)


class Matern52(Kernel):
    def __init__(self, lengthscale, amplitude=1.0, constraint_mapping=None):
        super().__init__(constraint_mapping)

        self.constraints["amplitude"] = constraints.positive
        self.constraints["lengthscale"] = constraints.positive

        amplitude = torch.tensor(
            amplitude, dtype=lengthscale.dtype, device=lengthscale.device
        )

        self._amplitude_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
        )
        self._lengthscale_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["lengthscale"]).inv(lengthscale)
        )

    @property
    def lengthscale(self):
        transform = self.constraint_mapping(self.constraints["lengthscale"])
        return transform(self._lengthscale_storage)

    @property
    def amplitude(self):
        transform = self.constraint_mapping(self.constraints["amplitude"])
        return transform(self._amplitude_storage)

    def extra_repr(self):
        return f"lengthscale={self.lengthscale.data}, amplitude={self.amplitude.data}"

    def forward(self, x, xp):
        assert x.shape[-1] == xp.shape[-1], "dimensionality mismatch"

        n, d = x.shape
        p, d = xp.shape

        X = x.unsqueeze(-2).expand((n, p, d)).reshape(-1, d)
        Xp = xp.unsqueeze(-3).expand((n, p, d)).reshape(-1, d)

        # delta = (X - Xp).clamp(min=1.0e-36)
        delta = (X - Xp).pow(2).sum(dim=-1).clamp(min=1.0e-36).sqrt()
        distance = math.sqrt(5.0) * delta * self.lengthscale.reciprocal()

        K = (1 + distance + distance * distance / 3.0) * (-distance).exp()
        return self.amplitude * K.reshape(n, p)

    def diagonal(self, x):
        assert x.dim() == 2, "axis must be of (n, d) shape"
        return self.amplitude * torch.ones(x.shape[0], device=x.device).reshape(-1, 1)


class Linear(Kernel):
    def __init__(self, amplitude):
        super().__init__()

        amplitude = torch.tensor([amplitude])
        self.constraints["amplitude"] = constraints.positive
        self._amplitude_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
        )

    @property
    def amplitude(self):
        transform = self.constraint_mapping(self.constraints["amplitude"])
        return transform(self._amplitude_storage)

    def extra_repr(self):
        return f"amplitude={self.amplitude.data}"

    def forward(self, x, xp):
        assert x.shape[-1] == xp.shape[-1], "dimensionality mismatch"
        assert x.dim() == xp.dim()
        assert x.dim() == 2
        return self.amplitude * (x @ xp.t())

    def diagonal(self, x):
        assert x.dim() == 2, "axis must be of (n, d) shape"
        return (x * x).sum(dim=1).unsqueeze(1).mul(self.amplitude)


class Quadratic(Kernel):
    def __init__(self, amplitude):
        super().__init__()
        self._secret_linear_kernel = Linear(amplitude)

    @property
    def amplitude(self):
        return self._secret_linear_kernel.amplitude

    def extra_repr(self):
        return "I am secretly a product of linear kernels"

    def forward(self, x, xp):
        return (
            self._secret_linear_kernel(x, xp)
            * self._secret_linear_kernel(x, xp)
            / self.amplitude
        )

    def diagonal(self, x):
        return (
            self._secret_linear_kernel.diagonal(x)
            * self._secret_linear_kernel.diagonal(x)
            / self.amplitude
        )


class HadamardKernel(Kernel):
    """
    Represents a Hadamard kernel composed of several kernels. You must specify active dimensions for each kernel;
    This will select the appropriate dimensions to pass to each subkernel.
    """

    def __init__(self, kernels: List[Kernel], active_dims: List[Tuple[int, ...]]):
        super().__init__()

        assert len(kernels) == len(active_dims), "active dims must have the same length as kernels"

        self.components = torch.nn.ModuleList(kernels)
        self.active_dims = active_dims

    def forward(self, x1, x2):
        # accumulate kernels
        data = torch.ones((x1.shape[0], x2.shape[0]), dtype=x1.dtype)

        for kernel, dims in zip(self.components, self.active_dims):
            _x1 = x1.index_select(1, torch.tensor(dims))
            _x2 = x2.index_select(1, torch.tensor(dims))

            data = data * kernel(_x1, _x2)

        return data

    def diagonal(self, x):
        data = torch.ones((x.shape[0], 1), dtype=x.dtype)

        for kernel, dims in zip(self.components, self.active_dims):
            _x = x.index_select(1, torch.tensor(dims))
            data = data * kernel.diagonal(_x)

        return data

    def symmetric(self, x):
        return self.forward(x, x)


class KroneckerKernel(Kernel):
    """
    Represents a Kronecker-structured composition of kernels. You must pass in an appropriately structured axis.
    """

    def __init__(self, kernels: List[Kernel]):
        super().__init__()
        self.components = torch.nn.ModuleList(kernels)

    def forward(self, x1, x2):
        # accumulate kernels

        assert len(x1) == len(self.components), "input axes must be properly structured"

        data = []

        for _kernel, _x1, _x2 in zip(self.components, x1, x2):
            data.append(_kernel(_x1, _x2))

        return data

    def diagonal(self, x):
        data = []

        for _kernel, _x in zip(self.components, x):
            data.append(_kernel.diagonal(_x))

        return data

    def symmetric(self, x):
        return self.forward(x, x)


class SpectralMixtureKernel(Kernel):
    def __init__(self, weights, means, variances, amplitude=1., constraint_mapping=None):
        super().__init__(constraint_mapping)

        assert weights.dim() == 1, "weights must be 1d"
        assert all((means.dim() == 2, variances.dim() == 2)), "means and variances must be 2d"

        Q, D = weights.shape[0], means.shape[1]
        self.register_buffer('sizes', torch.tensor([Q, D]))

        assert all([means.shape[0] == Q, means.shape[1] == D,
                    variances.shape[0] == Q, variances.shape[1] == D]), \
            "must have identical shapes for weights, means, and variances"

        self.constraints["weights"] = constraints.simplex
        self.constraints["means"] = constraints.greater_than_eq(0.)
        self.constraints["variances"] = constraints.positive
        self.constraints["amplitude"] = constraints.positive

        amplitude = torch.tensor(
            amplitude, dtype=weights.dtype, device=weights.device
        )

        self._amplitude_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["amplitude"]).inv(amplitude)
        )

        self._weights_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["weights"]).inv(weights)
        )
        self._means_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["means"]).inv(means)
        )
        self._variances_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["variances"]).inv(variances)
        )

    @property
    def amplitude(self):
        transform = self.constraint_mapping(self.constraints["amplitude"])
        return transform(self._amplitude_storage)

    @property
    def weights(self):
        transform = self.constraint_mapping(self.constraints["weights"])
        return transform(self._weights_storage)

    @property
    def means(self):
        transform = self.constraint_mapping(self.constraints["means"])
        return transform(self._means_storage)

    @property
    def variances(self):
        transform = self.constraint_mapping(self.constraints["variances"])
        return transform(self._variances_storage)

    def extra_repr(self):
        return f"weights={self.weights.data}, means={self.means.data}, variances={self.variances.data}"

    def forward(self, x, xp):
        a, d = x.shape
        b, d = xp.shape
        Q, D = self.sizes

        assert x.shape[-1] == xp.shape[-1], "dimensionality mismatch"
        assert d == D, "dimensionality mismatch"

        X = x.unsqueeze(-2)
        Xp = xp.unsqueeze(-3)

        tau = (X - Xp).reshape(a * b, 1, D)
        means = self.means.reshape(1, Q, D)  # N, Q, D
        variances = self.variances.reshape(1, Q, D)
        weights = self.weights.reshape(1, Q)
        result = weights * torch.cos(2 * math.pi * (means * tau).sum(dim=-1)) * torch.exp(
            -2 * math.pi * math.pi * (variances * (tau * tau)).sum(dim=-1))
        result = result.sum(dim=1).reshape(a, b)
        return self.amplitude * result

    def diagonal(self, x):
        if x.dim() == 1:
            x = x.unsqueeze(1)
        assert x.dim() == 2, "axis must be of (n, d) shape"
        return self.amplitude * torch.ones(
            x.shape[0], device=x.device, dtype=x.dtype
        ).reshape(-1, 1)
