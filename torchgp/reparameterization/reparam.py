from ..symbolic import *
from ..functional import loc_scale_skew_st2, loc_scale_skew_st3, loc_scale_skew_st4
from matchpy import Arity
import torch
import math
from ast import parse

__all__ = [
    "StdNormalSample",
    "StdNormalSampleVarSize",
    "gaussian_likelihood",
    "poisson_likelihood",
    "analytic_gaussian_likelihood",
    "analytic_gaussian_likelihood_unscaled",
    "multivariate_normal_logprob",
    "multivariate_poisson_logprob",
    "poisson_logprob",
    "titsias_normal_logprob",
    "SkewStudentT2LogDensity",
    "SkewStudentT3LogDensity",
    "SkewStudentT4LogDensity",
]


class StdNormalSample(TensorOperation):
    name = "StdNormalSample"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        v, Nmc = [op.evaluated for op in self.operands]
        return torch.randn(v.shape[0], Nmc, dtype=v.dtype, device=v.device)

    @property
    def shape(self):
        return tuple(
            Select(Constant(0), Shape(self.operands[0])),
            Constant(self.operands[1].data),
        )

    def to_ast(self, label, *arglabels, source=False):
        v = arglabels[0]
        Nmc = self.operands[1].data

        src = "{label} = torch.randn({v}.shape[0], {Nmc}, dtype={v}.dtype, device={v}.device)".format(
            label=label, v=v, Nmc=Nmc
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class StdNormalSampleVarSize(TensorOperation):
    name = "StdNormalSampleVarSize"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        v, Nmc = [op.evaluated for op in self.operands]
        return torch.randn(v.shape[0], Nmc, dtype=v.dtype, device=v.device)

    # @property
    # def shape(self):
    #     return tuple(Select(Constant(0), Shape(self.operands[0])), Constant(self.operands[1].data))

    def to_ast(self, label, *arglabels, source=False):
        v = arglabels[0]
        Nmc = arglabels[1]

        src = "{label} = torch.randn({v}.shape[0], {Nmc}, dtype={v}.dtype, device={v}.device)".format(
            label=label, v=v, Nmc=Nmc
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class SkewStudentT2LogDensity(TensorOperation):
    name = "SkewStudentT2LogDensity"
    arity = Arity(4, True)

    @property
    def evaluated(self):
        x, loc, scale, lam = [op.evaluated for op in self.operands]
        return loc_scale_skew_st2(x, loc, scale, lam)

    # @property
    # def shape(self):
    #     return tuple(Select(Constant(0), Shape(self.operands[0])), Constant(self.operands[1].data))

    def to_ast(self, label, *arglabels, source=False):
        y = arglabels[0]
        loc = arglabels[1]
        scale = arglabels[2]
        lam = arglabels[3]

        src = f"{label} = loc_scale_skew_st2({y}, {loc}, {scale}, {lam})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

class SkewStudentT3LogDensity(TensorOperation):
    name = "SkewStudentT3LogDensity"
    arity = Arity(4, True)

    @property
    def evaluated(self):
        x, loc, scale, lam = [op.evaluated for op in self.operands]
        return loc_scale_skew_st3(x, loc, scale, lam)

    # @property
    # def shape(self):
    #     return tuple(Select(Constant(0), Shape(self.operands[0])), Constant(self.operands[1].data))

    def to_ast(self, label, *arglabels, source=False):
        y = arglabels[0]
        loc = arglabels[1]
        scale = arglabels[2]
        lam = arglabels[3]

        src = f"{label} = loc_scale_skew_st3({y}, {loc}, {scale}, {lam})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

class SkewStudentT4LogDensity(TensorOperation):
    name = "SkewStudentT4LogDensity"
    arity = Arity(4, True)

    @property
    def evaluated(self):
        x, loc, scale, lam = [op.evaluated for op in self.operands]
        return loc_scale_skew_st4(x, loc, scale, lam)

    # @property
    # def shape(self):
    #     return tuple(Select(Constant(0), Shape(self.operands[0])), Constant(self.operands[1].data))

    def to_ast(self, label, *arglabels, source=False):
        y = arglabels[0]
        loc = arglabels[1]
        scale = arglabels[2]
        lam = arglabels[3]

        src = f"{label} = loc_scale_skew_st4({y}, {loc}, {scale}, {lam})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

def gaussian_likelihood(y, sample, std_lik, Ntotal):
    """

    :param y: the data, N x 1
    :param sample: a sample from the posterior distribution, N x Nmc
    :param std_lik: a scalar giving the std dev of likelihood
    :param Ntotal: total number of observatons in dataset
    :return: a scalar
    """
    q = y - sample  # broadcasts over column dim
    t1 = Constant(0.5) * Constant(1.8378770664093453)  # Nlog(2pi)/2
    t2 = Log(std_lik)
    r = Ntotal * Mean(
        -t1 - t2 - Constant(0.5) * Reciprocal(std_lik * std_lik) * (q * q)
    )
    return r


def poisson_likelihood(y, sample, Ntotal):
    """

    :param y: the data, N x 1
    :param sample: a sample from the posterior distribution, N x Nmc. Must be positive
    :param Ntotal: total number of observations in dataset
    :return: a scalar
    """
    return Ntotal * Mean(-LGamma(y + Constant(1.0)) + y * Log(sample) - sample)


def analytic_gaussian_likelihood_unscaled(y, mf, varf, std_lik):
    """
    Returns the average likelihood (per observation). To get the loss for the whole dataset scale this up by Ntotal

    :param y: the data, N x 1
    :param mf:  mean of posterior, N x 1
    :param varf: diagonal of variance of posterior, N x 1
    :param std_lik: std deviation of likelihood, scalar
    :return: scalar
    """
    sigma2 = std_lik*std_lik
    log2pi = Constant(math.log(2 * math.pi))
    t1 = Constant(-0.5)*(log2pi + Log(sigma2))
    t2 = Constant(-0.5)*Reciprocal(sigma2)*(Transpose(y - mf) @ (y - mf) + Sum(varf))
    return t1 + Reciprocal(Select(Constant(0), Shape(y)))*t2

def analytic_gaussian_likelihood(y, mf, varf, std_lik, Ntotal):
    """
    Use case: this is the analytic answer for the gaussian likelihood case, given a gaussian posterior.
    It's intended as a means of providing a test case for reparameterization gradient solutions that
    also are using gaussian likelihoods with gaussian posteriors.

    :param y: the data, N x 1
    :param mf:  mean of posterior, N x 1
    :param varf: diagonal of variance of posterior, N x 1
    :param std_lik: std deviation of likelihood, scalar
    :param Ntotal: total number of measurements, integer
    :return: scalar
    """
    sigma2 = std_lik * std_lik
    log2pi = Constant(math.log(2 * math.pi))
    N = Select(Constant(0), Shape(y))
    t1 = Constant(0.5) * Ntotal * log2pi
    t2 = Ntotal * Log(std_lik)
    t3 = (
        Divide(Ntotal, N)
        * Constant(0.5)
        * Reciprocal(sigma2)
        * (Transpose(y - mf) @ (y - mf))
    )
    t4 = Divide(Ntotal, N) * Constant(0.5) * Reciprocal(sigma2) * Sum(varf)
    ve_loss = -(t1 + t2 + t3 + t4)
    return ve_loss

def multivariate_normal_logprob(z, m, L):
    """

    :param m: mean (symbolic vector)
    :param L: lower triangular root factorization of the covariance
    :param z: samples (of size m) arranged with batch dim = -1
    :return: vector of log probabilities (length = batch)
    """
    D = z - m #this will broadcast across columns
    #t1 =  batched squared Mahalanobis distance diag(D.T @ L^-T @ L^-1 @ D)
    t1 = Transpose(PartialSum(D * CholeskySolveT(L,CholeskySolve(L,D)),Constant(0)))
    half_log_det = SumLogDiagonal(ExtractDiagonal(L))
    k = Select(Constant(0),Shape(m))
    return -Constant(0.5) * (k * Constant(math.log(2 * math.pi)) + t1) - half_log_det

def multivariate_poisson_logprob(y, z):
    """
    Implements Sum(LogPoisson(y_i, z_i)) with batching over multiple estimates of the rate z_i
    all observations (indexed by i) are presumed conditionally independent
    :param y: data samples of size Nobs x 1
    :param z: latent rate of size Nobs x batch
    :return: vector of log probabilities (length = batch)
    """
    return Transpose(PartialSum(-LGamma(y + Constant(1.0)) + (y * Log(z)) - z, Constant(0)))

def poisson_logprob(y, z):
    """
    Implements LogPoisson(y_i, z_i) with batching over multiple estimates of the rate z_i
    all observations (indexed by i) are presumed conditionally independent
    :param y: data samples of size Nobs x 1
    :param z: latent rate of size Nobs x batch
    :return: Matrix of log probabilities Nobs x batch
    """
    return -LGamma(y + Constant(1.0)) + (y * Log(z)) - z

def meanfield_normal_logprob(z, m, logv):
    """

    :param m: mean (symbolic vector)
    :param logv: log of the variance
    :param z: samples (of size m) arranged with batch dim = -1
    :return: vector of log probabilities (length = batch)
    """
    D = z - m #this will broadcast across columns
    #t1 =  batched squared Mahalanobis distance diag(D.T @ L^-T @ L^-1 @ D)
    v = Exp(logv)
    t1 = Transpose(PartialSum(D * Reciprocal(v) * D,Constant(0)))
    half_log_det = Sum(logv)
    k = Select(Constant(0),Shape(m))
    return -Constant(0.5) * (k * Constant(math.log(2 * math.pi)) + t1) - half_log_det

def titsias_normal_logprob(z, m, K0, lam, jitter):
    """
    The titsias parameterization of a normal distribution is:
    mean = m
    covariance = S = K0 @ (K0 + diag(λ))^-1 @ diag(λ)
    for the determinant term I use QR: S = QR(K0 - K0 @ (K0 + diag(λ))^-1 @ K0)
    """
    Lp = CholeskyRoot(K0 + Diagonal(lam))
    L = CholeskyRoot(AutoNugget(K0 - K0 @ CholeskySolveT(Lp, CholeskySolve(Lp, K0)), jitter))
    D = z - m
    # t1 = (z - m).T @ S^-1 @ (z - m); note that z is treated as a vector
    t1 = Transpose(PartialSum(D * CholeskySolveT(L, CholeskySolve(L, D)),Constant(0)))
    half_log_det = SumLogDiagonal(ExtractDiagonal(L))
    k = Select(Constant(0),Shape(m))
    return -Constant(0.5) * (k * Constant(math.log(2 * math.pi)) + t1) - half_log_det
