import torch
from ast import parse
from ..symbolic.core import TensorOperation, Constant, Shape, Select, Insert
from matchpy import Arity
from ..functional import prod
from ..structure import kron_mvprod, rkr_mv_2d

__all__ = [
    "batch_mvprod",
    "BatchMVProd",
    "batch_kron_mvprod",
    "BatchKronMVProd",
    "batch_rkr_mvprod_2d",
    "BatchRKRMVProd2D",
    "Unsqueeze",
]


def batch_mvprod(a, b):
    """
    :param a: some linear operator supporting @ operator of shape M x N
    :param b: a batched vector of shape batch x N x 1
    :return: batch x M x 1
    """
    assert a.dim() == 2
    assert b.dim() == 3
    assert a.shape[1] == b.shape[1]
    assert b.shape[2] == 1
    return a.unsqueeze(0) @ b


def batch_kron_mvprod(ops, v):
    """

    :param ops: list of linear operators supporting @ operator all of shape Mi x Ni
    :param v: a batched vector of shape batch x prod(Ni) x 1
    :return: batch x prod(Mi) x 1
    """
    for op in ops:
        assert op.dim() == 2
    assert v.dim() == 3
    assert v.shape[2] == 1
    opsu = [op.unsqueeze(0) for op in ops]
    return kron_mvprod(opsu, v)


def batch_rkr_mvprod_2d(op1, op2, v):
    """

    :param op1: a linear operator supporting @ operator of shape R x N1
    :param op2: a linear operator supporting @ operator of shape R x N2
    :param v: a batched vector of shape batch x N1*N2 x 1
    :return: batch x R x 1
    """
    assert op1.dim() == 2
    assert op2.dim() == 2
    assert op1.shape[0] == op2.shape[0]
    assert v.dim() == 3
    return rkr_mv_2d(op1.unsqueeze(0), op2.unsqueeze(0), v)


class BatchMVProd(TensorOperation):
    name = "BatchMVProd"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        args = [op.evaluated for op in self.operands]
        return batch_mvprod(args[0], args[1])

    @property
    def shape(self):
        return tuple(
            Select(Constant(0), Shape(self.operands[1])),
            Select(Constant(0), Shape(self.operands[0])),
            Constant(1),
        )

    def to_ast(self, label, *arglabels, source=False):
        A = arglabels[0]
        v = arglabels[1]

        src = "{label} = batch_mvprod({A}, {v})".format(label=label, A=A, v=v)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class BatchKronMVProd(TensorOperation):
    name = "BatchKronMVProd"
    arity = Arity(2, False)

    @property
    def evaluated(self):
        ops = [op.evaluated for op in self.operands[:-1]]
        v = self.operands[-1].evaluated
        return batch_kron_mvprod(ops, v)

    @property
    def shape(self):
        return tuple(
            Select(Constant(0), Shape(self.operands[-1])),
            prod([Select(Constant(0), Shape(op)) for op in self.operands[:-1]]),
            Constant(1),
        )

    def to_ast(self, label, *arglabels, source=False):
        ops, v = arglabels[:-1], arglabels[-1]
        oplst = "[" + ", ".join(ops) + "]"

        src = "{label} = batch_kron_mvprod({oplst}, {v})".format(
            label=label, oplst=oplst, v=v
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class BatchRKRMVProd2D(TensorOperation):
    name = "BatchRKRMVProd2D"
    arity = Arity(3, True)

    @property
    def evaluated(self):
        ops = [op.evaluated for op in self.operands[:-1]]
        v = self.operands[-1].evaluated
        return batch_kron_mvprod(ops, v)

    @property
    def shape(self):
        return tuple(
            Select(Constant(0), Shape(self.operands[-1])),
            Select(Constant(0), Shape(self.operands[0])),
            Constant(1),
        )

    def to_ast(self, label, *arglabels, source=False):
        op1, op2, v = arglabels[0], arglabels[1], arglabels[2]

        src = "{label} = batch_rkr_mvprod_2d({op1}, {op2}, {v})".format(
            label=label, op1=op1, op2=op2, v=v
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class Unsqueeze(TensorOperation):
    name = "Unsqueeze"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        dim = self.operands[1].data
        return op.unsqueeze(dim)

    @property
    def shape(self):
        return Insert(
            Shape(self.operands[0]), Constant(self.operands[1].data), Constant(1)
        )

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        dim = self.operands[1].data

        src = "{label} = {op}.unsqueeze({dim})".format(label=label, op=op, dim=dim)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result
