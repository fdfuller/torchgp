import torch
import numpy as np
from functools import reduce
from operator import mul, add
import math

__all__ = [
    "add_diagonal",
    "add_jitter",
    "add_jitter_v2",
    "ard_rbf_kernel",
    "my_rbf_kernel",
    "potrf",
    "potrs",
    "trtrs",
    "trifact",
    "trisolve",
    "triunpack",
    "pivoted_cholesky_solve",
    "prod",
    "concat",
    "uniq",
    "expand_posdef_lowertriangular",
    "numpyify",
    "qr_plus",
    "copy_ltu",
    "batched_vec_identity",
    "general_reciprocal",
    "loc_scale_skew_st2",
    "loc_scale_skew_st3",
    "loc_scale_skew_st4",
    "eye_like",
    "eye_like_square"
]


def prod(seq):
    return reduce(mul, seq)


def concat(iter):
    return reduce(add, iter)


def uniq(seq):
    s = set()
    for item in seq:
        if item not in s:
            s.add(item)
            yield item


def numpyify(a):
    return a.detach().cpu().numpy()


class CopyLtu(torch.autograd.Function):
    @staticmethod
    def forward(ctx, L):
        A = L.clone()
        nR, nC = L.shape[-2:]
        R, C = torch.triu_indices(nR, nC, offset=1)
        A[..., R, C] = L[..., C, R]
        return A

    @staticmethod
    def backward(ctx, aA):
        if not ctx.needs_input_grad[0]:
            return (None,)

        tmp = aA + aA.transpose(-2, -1)
        tmp.diagonal()[:] *= 0.5

        return torch.tril(tmp)


copy_ltu = lambda L: CopyLtu.apply(L)


class QrWithGrad(torch.autograd.Function):
    @staticmethod
    def forward(ctx, A):
        assert A.dim() == 2, "batched case not supported"
        assert A.shape[0] >= A.shape[1], "case with n < m not supported"
        Q, R = torch.qr(A)
        ctx.save_for_backward(Q, R)
        return Q, R

    @staticmethod
    def backward(ctx, dQ, dR):
        Q, R = ctx.saved_tensors
        M = R @ dR.t() - dQ.t() @ Q
        tmp = dQ + Q @ copy_ltu(M)
        result = torch.triangular_solve(tmp.t(), R, upper=True).solution.t()
        return result


qr_plus = lambda A: QrWithGrad.apply(A)


class AddDiagonal(torch.autograd.Function):
    @staticmethod
    def forward(ctx, M, diagflat):
        M = M.clone()
        diag = M.diagonal(dim1=-2, dim2=-1)
        diag += diagflat.view(-1)
        result = M
        return result

    @staticmethod
    def backward(ctx, dresult):
        results = [None] * 2

        if ctx.needs_input_grad[0]:
            results[0] = dresult

        if ctx.needs_input_grad[1]:
            results[1] = dresult.diagonal(dim1=-2, dim2=-1)

        return tuple(results)


def add_diagonal(M, diagflat):
    return AddDiagonal.apply(M, diagflat)


def add_jitter(M, delta):
    return AddDiagonal.apply(
        M, delta * torch.ones(M.shape[-1], dtype=M.dtype, device=M.device)
    )

def add_jitter_v2(M, x1, x2, jitter, tol=1e-4):
    diff = x1.unsqueeze(1) - x2
    diff = diff.norm(dim=-1)
    ones = torch.ones(size=(1,), dtype=M.dtype, device=M.device)
    zeros = torch.zeros(size=(1,), dtype=M.dtype, device=M.device)
    result = M + torch.where((diff - tol).abs() < tol, jitter*ones, zeros)
    return result

def inv_softplus(op):
    return torch.expm1(op).log()


def _vech_index(I, J, N):
    """computes the location of values of a lower-triangular matrix in compressed storage

    Args:
        I, J: vector of indices of lower-triangular matrix
        N: dimension of square lower-triangular matrix

    Returns:
        a vector of indices corresponding to the locations of I,J in compressed storage

    """
    return I + (2 * N - J - 1) * J // 2


def batched_vec_identity(A, B, V):
    M1, N1 = A.shape
    M2, N2 = B.shape
    P = V.shape[1]
    return (
        torch.einsum("ij,ljk,mk->lim", A, V.t().reshape(P, N1, N2), B)
        .reshape(P, M1 * M2)
        .t()
    )


class ExpandPosDefLowerTriangular(torch.autograd.Function):
    @staticmethod
    def forward(ctx, vech, size):
        assert vech.numel() == (
            size * (size + 1) // 2
        ), "incorrect number of elements passed"
        out = torch.zeros((size, size), dtype=vech.dtype, device=vech.device)

        # compute indices
        mapfun = lambda arr: torch.from_numpy(arr).to(device=vech.device)

        tril_ij = tuple(map(mapfun, np.tril_indices(size, k=-1)))
        diag_ij = tuple(map(mapfun, np.diag_indices(size)))
        tril_vech = _vech_index(*tril_ij, size)
        diag_vech = _vech_index(*diag_ij, size)

        # fill lower triangular directly
        out.index_put_(tril_ij, vech.index_select(0, tril_vech))

        diagonal = vech.index_select(0, diag_vech).exp()
        out.index_put_(diag_ij, diagonal)

        ctx.save_for_backward(diagonal, *tril_ij, *diag_ij, tril_vech, diag_vech)
        return out

    @staticmethod
    def backward(ctx, dresult):
        diagonal = ctx.saved_tensors[0]
        tril_ij = ctx.saved_tensors[1:3]
        diag_ij = ctx.saved_tensors[3:5]
        tril_vech = ctx.saved_tensors[5]
        diag_vech = ctx.saved_tensors[6]
        size = diagonal.shape[0]
        result = [None] * 2
        if ctx.needs_input_grad[0]:
            grad = torch.zeros(
                (size * (size + 1) // 2), dtype=dresult.dtype, device=dresult.device
            )
            grad.index_put_((tril_vech,), dresult[tril_ij])
            grad.index_put_((diag_vech,), dresult[diag_ij] * diagonal)
            result[0] = grad

        return tuple(result)


def expand_posdef_lowertriangular(vech, size):
    """expands a compressed-storage version of a positive-definite lower triangular matrix

    Args:
        vech: compressed storage
        size: full size of the matrix

    Returns:
        a matrix of (size, size) shape

    """
    return ExpandPosDefLowerTriangular.apply(vech, size)


def my_rbf_kernel(x, y, lengthscale):
    # expect x, y to be batch x D, but want to convert to D x batch.
    # First, handle the vector case (x -> 1 x batch)
    if x.dim() == 2:
        x = x.t()  # batch now last
    else:
        raise ValueError("expected x to be 2D")
    if y.dim() == 2:
        y = y.t()  # batch now last
    else:
        raise ValueError("expected y to be 2D")
    if lengthscale.dim() == 1:
        lengthscale = lengthscale.unsqueeze(
            0
        )  # covers case of vector-valued lengthscale
    elif lengthscale.dim() == 0:
        lengthscale = lengthscale.unsqueeze(0).unsqueeze(1)
    elif lengthscale.dim() > 2:
        raise ValueError("Expected lengthscale to be 0D, 1D, or 2D")
    else:
        pass
    # now we want to add x -> D x batch x 1 and y -> D x 1 x batch
    # this way when we broadcast add or mul, they get broadcast to D x batch x batch
    # then we'll reduce across D
    x = x.unsqueeze(2)
    y = y.unsqueeze(1)
    # to compute the kronecker delta efficiently, we use xx = x
    # stationary part: ϕ(x,y) = Σ((x_d - y_d)/L_d)^2
    denom = lengthscale.mul(math.sqrt(2))
    return torch.sum((x - y).div(denom).pow(2), 0).mul_(-1).exp()


class RBFKernelFunction(torch.autograd.Function):
    """evaluates the RBF kernel function k(x, x') using the ARD parameterization.

    This is a differentiable function computing the d-dimensional ARD RBF kernel
    k(x, x') where x and x' are tensors of dimension (n, d). The parameters of
    the kernel are the log variance, and log lengthscale.
    """

    @staticmethod
    def forward(ctx, x, xp, lengthscale):
        """evaluate the ARD RBF function k(x, x') for input tensors of size
        (n, d) with d being the ARD kernel dimension

        Args:
            ctx: required parameter
            x: tensor of size (n, d)
            xp: tensor of size (n, d)
            lengthscale: lengthscale of the kernel function, size (1, d)

        Returns:
            evaluated tensor of size (n,) containing the kernel values.

        """
        assert x.shape == xp.shape, "input shapes must be the same"
        assert (
            x.shape[-1] == lengthscale.shape[-1]
        ), "variance vector must be same length as input dimension x"

        d = x - xp
        dlinv = d / lengthscale  # elementwise broadcast
        A = (d * dlinv).sum(1)
        result = torch.exp(-0.5 * A)

        ctx.save_for_backward(result, d, dlinv)
        return result

    @staticmethod
    def backward(ctx, dresult):
        result, d, dlinv = ctx.saved_tensors
        grads = [None] * 3

        maskedr = (dresult * result).unsqueeze(1)

        if ctx.needs_input_grad[0]:
            grads[0] = -(maskedr * dlinv)

        if ctx.needs_input_grad[1]:
            grads[1] = maskedr * dlinv

        if ctx.needs_input_grad[2]:
            grads[2] = (0.5 * maskedr * dlinv * dlinv).sum(0, keepdim=True)

        return tuple(grads)


def ard_rbf_kernel(x1, x2, lengthscale):
    return RBFKernelFunction.apply(x1, x2, lengthscale)


def potrf(A):
    """computes the Cholesky factorization of a positive definite matrix A

    Args:
        A: matrix to factorize

    Returns:
        the lower-triangular matrix L, such that L L.t() == A
    """
    return cholesky(A, upper=False)


def trtrs(L, B, transpose=False):
    """computes the solution to L X = B or L.t() X = B given L

    Args:
        L: lower-triangular matrix
        B: right-hand side of equation
        transpose: run on transformed matrix L

    Returns:
        a matrix X representing the solution
    """
    return torch.triangular_solve(B, L, upper=False, transpose=transpose)[0]


def potrs(L, B):
    """solves the system A X = B given a lower-triangular Cholesky factorization of A

    Args:
        L: lower-triangular factorization of A = L L.t()
        B: right-hand side

    Returns:
        a matrix X representing the solution
    """
    if B.dim() < 2:
        B = B.unsqueeze(-1)

    return torch.potrs(B, L, upper=False)


def trifact(A):
    if A.dim() < 3:
        A = A.unsqueeze(-3)

    return torch.btrifact(A)


def trisolve(y, LU_data, LU_pivots):
    if y.dim() < 2:
        y = y.unsqueeze(-2)
    if y.dim() < 3:
        y = y.unsqueeze(-3)

    result = torch.btrisolve(y, LU_data, LU_pivots)
    return result.squeeze()


def triunpack(LU_data, LU_pivots):
    """unpacks LU data"""
    _, L, U = torch.btriunpack(
        LU_data, LU_pivots, unpack_data=True, unpack_pivots=False
    )
    # P = P.squeeze()
    L = L.squeeze()
    U = U.squeeze()
    return L, U


def pivoted_cholesky_solve(y, L_data, L_pivots):
    """computes the pivots"""
    p = L_pivots.long()
    pinv = p.argsort()

    L1 = trtrs(L_data, y.index_select(dim=0, index=p))
    L2 = trtrs(L_data, L1, transpose=True)

    return L2.index_select(dim=0, index=pinv)


cholesky = getattr(torch, "cholesky", potrf)


def auto_nugget(A, cond_limit=1e6, jitter=1e-4):
    """
    using qr, we estimate the rank of the square matrix A and then use that estimate to compute an adjusted nugget
    """
    with torch.no_grad():
        Q, _ = A.qr()
        Z = Q.t() @ A @ Q
        Zvals = Z.diag().abs().sort(descending=True)
        s = Zvals.indices
        try:
            rank = (
                (Zvals.values > ((1 / cond_limit) * Zvals.values[0]))
                .nonzero()
                .reshape(-1)[-1]
            )
        except IndexError:
            print((Zvals.values > ((1 / cond_limit) * Zvals.values[0])))
            raise IndexError('fuck')
        adjusted_jitter = (
            Zvals.values[:rank].mean() * jitter
        ).clamp(min=jitter)
#         print(f"auto_nugget's jitter value: {adjusted_jitter}")
    # return A + torch.eye(A.shape[0], dtype=A.dtype, device=A.device)*adjusted_jitter
    return add_jitter(A, adjusted_jitter)

def general_reciprocal(op):
    if torch.is_tensor(op):
        return op.reciprocal()
    else:
        return 1/op

def regrid_fun(fsamples, axis0, axis1=None, padding='border'):
    """
    This takes discrete samples (fsamples) of an atmost 2D function that are evaluated on a grid. We ASSUME
    that the samples provided are taken on a linearly spaced grid with axis values -1 to 1. The values of
    the interpolated function are evaluated at the out_axis positions, which if they exceed -1 to 1 follow
    the padding behavior, which defaults to 'border', i.e. repeats the nearest border pixel value.

    This function uses grid_sample, which expects 4D inputs NxCxHxW. The first 2 are for filter & color
    in image applications. As we assume fsamples is 2D at most, the N & C dims are added within the function.


    """
    if axis1 is None:
        axis1 = torch.zeros(1, dtype=axis0.dtype, device=axis0.device)
    G = torch.cartesian_prod(axis1, axis0).reshape(axis1.shape[0], axis0.shape[0], 2).unsqueeze(0)
    if fsamples.dim() == 1:
        F = fsamples.unsqueeze(0).unsqueeze(0).unsqueeze(0)
    elif fsamples.dim() == 2:
        F = fsamples.unsqueeze(0).unsqueeze(0)
    else:
        raise ValueError("expected fvals to be at most 2D")
    r = torch.nn.functional.grid_sample(F, G, padding_mode=padding)
    # grid_sample returns in NxCxHxW. We trim NxC axes, but leave it in 2D form
    return r.squeeze(0).squeeze(0).t()



def loc_scale_skew_st2(x, loc, scale, lam):
    return (-(math.pi*scale).log() - (3/2)*(2 + (loc - x).pow(2)/scale.pow(2)).log() +
      ((2*math.pi*scale.pow(2) - 2*loc*scale*(2 + (loc - x).pow(2)/scale.pow(2)).sqrt()*lam + 2*scale*(2 + (loc - x).pow(2)/scale.pow(2)).sqrt()*x*lam + math.pi*(loc - x).pow(2)*(1 + lam.pow(2)))/
         (2*scale.pow(2) + (loc - x).pow(2)*(1 + lam.pow(2))) - 2*torch.atan(((loc - x)*lam)/(scale*(2 + (loc - x).pow(2)/scale.pow(2)).sqrt()))).log())


def loc_scale_skew_st3(x, loc, scale, lam):
    return ((3 * math.log(3)) / 2 - (math.pi * scale).log() - 2 * (3 + (loc - x).pow(2) / scale.pow(2)).log() +
    (2 + (-3 * (3 * scale.pow(2) + (loc - x).pow(2)) * (loc - x)*lam - 2 * (loc - x).pow(3) *lam.pow(3)) / (
            scale.pow(3) * (3 + (loc - x).pow(2) / scale.pow(2)).pow(3 / 2) *
            (1 + ((loc - x).pow(2)*lam.pow(2)) / (3 * scale.pow(2) + (loc - x).pow(2))).pow(3 / 2))).log())


def loc_scale_skew_st4(x, loc, scale, lam):
    return (-scale.log() - (5 / 2) * (4 + (loc - x).pow(2) / scale.pow(2)).log() +
    (12 + (8 * (-loc + x) *lam * (((4 * scale.pow(2) + (loc - x).pow(2))*(
            20 * scale.pow(2) + (loc - x).pow(2) * (5 + 3 *lam.pow(2)))) / (4 * scale.pow(2) + (loc - x).pow(2) * (1 +
                                                                                                           lam.pow(2))).pow(2) +
                                                                        (3 * scale * (4 + (loc - x).pow(2) / scale.pow(2)).sqrt() *
                                                                         torch.atan(((loc - x) *lam) / (scale * (4 + (loc - x).pow(2) / scale.pow(2)).sqrt()))) / (
                                                                                (loc - x) *lam))) /
    (math.pi * scale * (4 + (loc - x).pow(2) / scale.pow(2)).sqrt())).log())

def eye_like(x):
    with torch.no_grad():
        z = torch.zeros_like(x)
        z.diagonal(dim1=-1, dim2=-2).add_(1.)
    return z

def eye_like_square(x):
    with torch.no_grad():
        z = torch.eye(x.shape[0], dtype=x.dtype, device=x.device)
    return z
