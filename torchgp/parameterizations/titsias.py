from ..symbolic import *
from .structured import *


# for these implementations I try to maintain the contract that K_uu will always contain a jitter parameter
# i.e. K_uu -> K_uu + Jitter
# the default value for the jitter is 0. This means that inverse(K_uu + Jitter) and K_uu are consistent wrt
# eachother. I do not, however, strive for consistence between K_fu, K_ff and K_uu


def kl_1D(K, u, L, m, jitter):
    """
    Assumes 0 prior mean

    :param K: Kernel symbol
    :param u: inducing points
    :param L: parameter vector
    :param m: mean vector
    :param jitter: [optional] jitter to add to diagonal of K_uu
    :return: scalar (expression)
    """

    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    KpD = AddDiagonal(K_uu, L)
    Lp = CholeskyRoot(KpD)
    t1 = SumLogDiagonal(ExtractDiagonal(Lp))
    t2 = Constant(0.5) * SumLogDiagonal(L)
    t3 = Constant(0.5) * Trace(CholeskySolveT(Lp, CholeskySolve(Lp, K_uu)))
    t4 = Constant(0.5) * (Transpose(m) @ K_uu @ m)
    kl_loss = t1 - t2 - t3 + t4
    return kl_loss


def kl_2D(K0, K1, u0, u1, L0, L1, mu, jitter):
    """
    Assumes 0 prior mean

    :param K0: Kernel symbol for 0th dim
    :param K1: Kernel symbol for 1st dim
    :param u0: inducing points for 0th dim
    :param u1: inducing points for 1st dim
    :param L0: parameter vector for 0th dim
    :param L1: parameter vector for 1st dim
    :param mu: vector for the mean
    :return: scalar (expression)

    Notes: in the trace term, we obtain a positive sign since we do not
    use the same identity as titsias does for 1D. (see 1D code, where the sign of
    t3 matches literature). We use K0^-1 S = K0^-1 @ K0 @ (K0 + L)^-1 @ L
    """
    K0_uu = EvaluateKernelSymmetric(K0, u0)
    K0_uu = AddJitter(K0_uu, jitter)
    K1_uu = EvaluateKernelSymmetric(K1, u1)
    K1_uu = AddJitter(K1_uu, jitter)
    K0pD = AddDiagonal(K0_uu, L0)
    K1pD = AddDiagonal(K1_uu, L1)
    L0p = CholeskyRoot(K0pD)
    L1p = CholeskyRoot(K1pD)
    total_rank = Rank(L0p) * Rank(L1p)
    t1 = Divide(total_rank, Rank(L0p)) * SumLogDiagonal(ExtractDiagonal(L0p)) + Divide(
        total_rank, Rank(L1p)
    ) * SumLogDiagonal(ExtractDiagonal(L1p))
    t2 = Constant(0.5) * Divide(total_rank, Rank(L0p)) * SumLogDiagonal(L0) + Constant(
        0.5
    ) * Divide(total_rank, Rank(L1p)) * SumLogDiagonal(L1)
    #     t3 = Constant(0.5) * \
    #             Trace(CholeskySolveT(L0p, CholeskySolve(L0p, Diagonal(L0)))) * \
    #             Trace(CholeskySolveT(L1p, CholeskySolve(L1p, Diagonal(L1))))
    t3 = (
        Constant(0.5)
        * (Rank(L0p) - Trace(CholeskySolveT(L0p, CholeskySolve(L0p, K0_uu))))
        * (Rank(L1p) - Trace(CholeskySolveT(L1p, CholeskySolve(L1p, K1_uu))))
    )
    t4 = Constant(0.5) * (Transpose(mu) @ KronMVProd(K0_uu, K1_uu, mu))
    t5 = Constant(0.5) * total_rank
    kl_loss = t1 - t2 + t3 + t4 - t5
    kl_loss = replace_all(kl_loss, replace_rank)
    return kl_loss


# Note: experimentation found that using:
# t3 = Constant(0.5) * \
#         (Rank(L0p) - Trace(CholeskySolveT(L0p, CholeskySolve(L0p, K0_uu)))) * \
#         (Rank(L1p) - Trace(CholeskySolveT(L1p, CholeskySolve(L1p, K1_uu))))

# is much better than
#     t3 = Constant(0.5) * \
#             Trace(CholeskySolveT(L0p, CholeskySolve(L0p, Diagonal(L0)))) * \
#             Trace(CholeskySolveT(L1p, CholeskySolve(L1p, Diagonal(L1))))

# reasons unknown, but perhaps Cholesky solve does not work well on Diagonal matrices. Both minimized
# the kl, but "works better" to me means "gets the mean closer to zero in a fixed # of steps"


def kl_2D_low_kruskal_rank(K0, K1, u0, u1, L0, L1, M0, M1, jitter):
    """
    Assumes 0 prior mean.
    Mean is parameterized as Sum(CKR{M0, M1}, dim=1)

    :param K0: Kernel symbol for 0th dim
    :param K1: Kernel symbol for 1st dim
    :param u0: inducing points for 0th dim
    :param u1: inducing points for 1st dim
    :param L0: parameter vector for 0th dim
    :param L1: parameter vector for 1st dim
    :param M0: component matrix of CKR mean for 0th dim
    :param M1: component matrix of CKR mean for 1st dim
    :return: scalar (expression)

    """
    K0_uu = EvaluateKernelSymmetric(K0, u0)
    K0_uu = AddJitter(K0_uu, jitter)
    K1_uu = EvaluateKernelSymmetric(K1, u1)
    K1_uu = AddJitter(K1_uu, jitter)
    K0pD = AddDiagonal(K0_uu, L0)
    K1pD = AddDiagonal(K1_uu, L1)
    L0p = CholeskyRoot(K0pD)
    L1p = CholeskyRoot(K1pD)
    total_rank = Rank(L0p) * Rank(L1p)
    t1 = Divide(total_rank, Rank(L0p)) * SumLogDiagonal(ExtractDiagonal(L0p)) + Divide(
        total_rank, Rank(L1p)
    ) * SumLogDiagonal(ExtractDiagonal(L1p))
    t2 = Constant(0.5) * Divide(total_rank, Rank(L0p)) * SumLogDiagonal(L0) + Constant(
        0.5
    ) * Divide(total_rank, Rank(L1p)) * SumLogDiagonal(L1)
    t3 = (
        Constant(0.5)
        * (Rank(L0p) - Trace(CholeskySolveT(L0p, CholeskySolve(L0p, K0_uu))))
        * (Rank(L1p) - Trace(CholeskySolveT(L1p, CholeskySolve(L1p, K1_uu))))
    )
    t4 = Constant(0.5) * Sum(
        (Transpose(M0) @ K0_uu @ M0) * (Transpose(M1) @ K1_uu @ M1)
    )
    t5 = Constant(0.5) * total_rank
    kl_loss = t1 - t2 + t3 + t4 - t5
    kl_loss = replace_all(kl_loss, replace_rank)
    return kl_loss


def kl_2D_low_kruskal_rank_symmetric(K0, K1, u0, u1, L0, L1, M0, M1, jitter):
    """
    Assumes 0 prior mean.
    Mean is parameterized as Sum(CKR{M0, M1}, dim=1)

    Additionally, we impose symmetry on the mean

    :param K0: Kernel symbol for 0th dim
    :param K1: Kernel symbol for 1st dim
    :param u0: inducing points for 0th dim
    :param u1: inducing points for 1st dim
    :param L0: parameter vector for 0th dim
    :param L1: parameter vector for 1st dim
    :param M0: component matrix of CKR mean for 0th dim
    :param M1: component matrix of CKR mean for 1st dim
    :return: scalar (expression)
    """

    K0_uu = EvaluateKernelSymmetric(K0, u0)
    K0_uu = AddJitter(K0_uu, jitter)
    K1_uu = EvaluateKernelSymmetric(K1, u1)
    K1_uu = AddJitter(K1_uu, jitter)
    K0pD = AddDiagonal(K0_uu, L0)
    K1pD = AddDiagonal(K1_uu, L1)
    L0p = CholeskyRoot(K0pD)
    L1p = CholeskyRoot(K1pD)
    total_rank = Rank(L0p) * Rank(L1p)
    t1 = Divide(total_rank, Rank(L0p)) * SumLogDiagonal(ExtractDiagonal(L0p)) + Divide(
        total_rank, Rank(L1p)
    ) * SumLogDiagonal(ExtractDiagonal(L1p))
    t2 = Constant(0.5) * Divide(total_rank, Rank(L0p)) * SumLogDiagonal(L0) + Constant(
        0.5
    ) * Divide(total_rank, Rank(L1p)) * SumLogDiagonal(L1)
    t3 = (
        Constant(0.5)
        * (Rank(L0p) - Trace(CholeskySolveT(L0p, CholeskySolve(L0p, K0_uu))))
        * (Rank(L1p) - Trace(CholeskySolveT(L1p, CholeskySolve(L1p, K1_uu))))
    )
    t4 = Constant(0.5) * (
        Constant(0.5) * Sum((Transpose(M0) @ K0_uu @ M0) * (Transpose(M1) @ K1_uu @ M1))
        + Constant(0.5)
        * Sum((Transpose(M1) @ K0_uu @ M0) * (Transpose(M0) @ K1_uu @ M1))
    )
    t5 = Constant(0.5) * total_rank
    kl_loss = t1 - t2 + t3 + t4 - t5
    kl_loss = replace_all(kl_loss, replace_rank)
    return kl_loss


def weighted_1D_mean(K, u, f, mu, X):
    """
    1D mean prediction when the mean is parameterized as m = K_uu @ mu
    (so as to avoid need to invert Kuu)

    :param K: Kernel Symbol
    :param u: inducing points, U x 1
    :param f: evaluation points, F x 1
    :param mu: mean vector, U x 1
    :param X: weighing matrix, N x F
    :return: weighted vector N x 1
    """
    K_fu = EvaluateKernel(K, f, u)
    w_mf = X @ K_fu @ mu
    return w_mf


def mean_1D(K, u, f, mu):
    K_fu = EvaluateKernel(K, f, u)
    mf = K_fu @ mu
    return mf


def fully_weighted_2D_mean(K0, K1, u0, u1, f0, f1, mu, X0, X1):
    """
    2D mean prediction when the mean is parameterized as
    m = KronMV(K0_uu, K1_uu, mu)

    Further, we require that the weight apply completely to both axes (0, 1). Such
    that the there is a single output pixel

    We allow for different weights to apply to each axis (0,1). Usually, though,
    we use degenerate weights, i.e. X0 = X1
    """
    K0_fu = EvaluateKernel(K0, f0, u0)
    K1_fu = EvaluateKernel(K1, f1, u1)
    W1 = X0 @ K0_fu
    W2 = X1 @ K1_fu
    w_mf = RKRMVProd(W1, W2, mu)
    return w_mf


def mean_2D(K0, K1, u0, u1, f0, f1, mu):
    K0_fu = EvaluateKernel(K0, f0, u0)
    K1_fu = EvaluateKernel(K1, f1, u1)
    mf = KronMVProd(K0_fu, K1_fu, mu)
    return mf


def mean_2D_low_kruskal_rank(K0, K1, u0, u1, f0, f1, M0, M1):
    K0_fu = EvaluateKernel(K0, f0, u0)
    K1_fu = EvaluateKernel(K1, f1, u1)
    mf = PartialSum(CKR(K0_fu @ M0, K1_fu @ M1), Constant(1))
    return mf


def mean_2D_low_kruskal_rank_symmetric(K0, K1, u0, u1, f0, f1, M0, M1):
    K0_fu = EvaluateKernel(K0, f0, u0)
    K1_fu = EvaluateKernel(K1, f1, u1)
    mf = PartialSum(CKR(K0_fu @ M0, K1_fu @ M1), Constant(1)) + PartialSum(
        CKR(K0_fu @ M1, K1_fu @ M0), Constant(1)
    )
    return mf


def weighted_1D_var_diag(K, u, f, L, X, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis U x 1
    :param f: evaluation axis F x 1
    :param L: variance parameter vector U x 1
    :param X: weighing matrix N x F
    :return: diagonal of variance N x 1

    Note: in the 1D case we have cancellation of low rank term (t2) (as seen in the titsias paper)
    """
    K_fu = EvaluateKernel(K, f, u)
    K_ff = EvaluateKernelSymmetric(K, f)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    KpD = AddDiagonal(K_uu, L)
    Lp = CholeskyRoot(KpD)
    t1 = PartialSum((X @ K_ff) * X, Constant(1))
    t3_a = CholeskySolve(Lp, Transpose(K_fu)) @ Transpose(X)
    t3 = Transpose(PartialSum(t3_a * t3_a, Constant(0)))
    w_var_f = t1 - t3
    return w_var_f


def var_1D_diag(K, u, f, L, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis U x 1
    :param f: evaluation axis F x 1
    :param L: variance parameter vector U x 1
    :param jitter: amount of jitter to add to diagonal of K_uu to force pos def-ness
    :return: diagonal of variance F x 1

    Note: use only for 1D data
    """
    K_fu = EvaluateKernel(K, f, u)
    K_ff_diag = EvaluateKernelDiagonal(K, f)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    KpD = AddDiagonal(K_uu, L)
    Lp = CholeskyRoot(KpD)
    t1 = K_ff_diag
    t3_a = CholeskySolve(Lp, Transpose(K_fu))
    t3 = Transpose(PartialSum(t3_a * t3_a, Constant(0)))
    varf = t1 - t3
    return varf


def weighted_var_1D_diag(K, u, f, L, X, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing axis, U x 1
    :param f: evaluation axis, F x 1
    :param L: variance parameter vector U x 1
    :param X: weighing matrix N x F
    :param jitter: amount of jitter to add to diagonal of K_uu to force pos def-ness
    :return: diagonal variance N x 1

    Note: use only for 1D data
    """
    K_fu = EvaluateKernel(K, f, u)
    K_ff = EvaluateKernelSymmetric(K, f)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    KpD = AddDiagonal(K_uu, L)
    Lp = CholeskyRoot(KpD)
    t1 = PartialSum((X @ K_ff) * X, Constant(1))
    t3_a = CholeskySolve(Lp, Transpose(K_fu)) @ Transpose(X)
    t3 = Transpose(PartialSum(t3_a * t3_a, Constant(0)))
    varf = t1 - t3
    return varf


def weighted_var_fullspace_diag(K, f, X):
    """
    implements the diagonal of the full-rank term in the variance expression

    aka t1 in the docs (weighted variation)
    """
    K_ff = EvaluateKernelSymmetric(K, f)
    return PartialSum((X @ K_ff) * X, Constant(1))


def var_fullspace_diag(K, f):
    """
    implements the diagonal of the full-rank term in the variance expression

    aka t1 in the docs
    """
    K_ff = EvaluateKernelDiagonal(K, f)
    return K_ff


def weighted_var_subspace_diag(K, u, f, X, jitter):
    """
    implements the diagonal of the low-rank term of the variance expression.
    normally does not appear in the titsias parameterization, but
    does when we go to multi-dimensional. Involves cholesky of prior

    aka t2 in the docs (weighted variation)
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    t2_a = CholeskySolve(L_uu, Transpose(K_fu)) @ Transpose(X)
    return Transpose(PartialSum(t2_a * t2_a, Constant(0)))


def var_subspace_diag(K, u, f, jitter):
    """
    implements the diagonal of the low-rank term of the variance expression.
    normally does not appear in the titsias parameterization, but
    does when we go to multi-dimensional. Involves cholesky of prior (hence the jitter term)

    aka t2 in the docs
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    L = CholeskyRoot(K_uu)
    t2_a = CholeskySolve(L, Transpose(K_fu))
    return Transpose(PartialSum(t2_a * t2_a, Constant(0)))


def weighted_var_posterior_diag(K, u, f, L, X, jitter):
    """
    implements the diagonal of the low-rank term of the variance expression.
    normally does not appear in the titsias parameterization, but
    does when we go to multi-dimensional. Involves cholesky of prior (hence the jitter term delta)

    aka t3 in the docs (weighted variation)
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    KpD = AddDiagonal(K_uu, L)
    Lp = CholeskyRoot(KpD)
    L_uu = CholeskyRoot(K_uu)
    # term 1
    t3_a_1 = CholeskySolve(L_uu, Transpose(K_fu)) @ Transpose(X)
    t3_a = Transpose(PartialSum(t3_a_1 * t3_a_1, Constant(0)))
    # term 2
    t3_b_1 = CholeskySolve(Lp, Transpose(K_fu)) @ Transpose(X)
    t3_b = Transpose(PartialSum(t3_b_1 * t3_b_1, Constant(0)))

    return t3_a - t3_b


def var_posterior_diag(K, u, f, L, jitter):
    """
    implements the diagonal of the low-rank term of the variance expression.
    normally does not appear in the titsias parameterization, but
    does when we go to multi-dimensional. Involves cholesky of prior (hence the jitter term delta)

    aka t3 in the docs
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    KpD = AddDiagonal(K_uu, L)
    Lp = CholeskyRoot(KpD)
    L_uu = CholeskyRoot(K_uu)
    # first term
    t3_a_1 = CholeskySolve(L_uu, Transpose(K_fu))
    t3_a = Transpose(PartialSum(t3_a_1 * t3_a_1, Constant(0)))
    # second term
    t3_b_1 = CholeskySolve(Lp, Transpose(K_fu))
    t3_b = Transpose(PartialSum(t3_b_1 * t3_b_1, Constant(0)))
    return t3_a - t3_b


### Full covariance stuff here


def weighted_var_fullspace_fullcov(K, f, X):
    """
    implements the full-rank term in the variance expression

    aka t1 in the docs (weighted variation)
    """
    K_ff = EvaluateKernelSymmetric(K, f)
    return (X @ K_ff) @ X


def var_fullspace_fullcov(K, f):
    """
    implements the full-rank term in the variance expression

    aka t1 in the docs
    """
    K_ff = EvaluateKernelSymmetric(K, f)
    return K_ff


def weighted_var_subspace_fullcov(K, u, f, X, jitter):
    """
    implements the low-rank term of the variance expression.

    aka t2 in the docs (weighted variation)
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    t2_a = CholeskySolve(L_uu, Transpose(K_fu)) @ Transpose(X)
    return Transpose(t2_a) @ t2_a


def var_subspace_fullcov(K, u, f, jitter):
    """
    implements the low-rank term of the variance expression.

    aka t2 in the docs
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    L = CholeskyRoot(K_uu)
    t2_a = CholeskySolve(L, Transpose(K_fu))
    return Transpose(t2_a) @ t2_a


def weighted_var_posterior_fullcov(K, u, f, L, X, jitter):
    """
    implements the posterior term of the variance expression

    aka t3 in the docs (weighted variation)
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    KpD = AddDiagonal(K_uu, L)
    Lp = CholeskyRoot(KpD)
    L_uu = CholeskyRoot(K_uu)
    # term 1
    t3_a_1 = CholeskySolve(L_uu, Transpose(K_fu)) @ Transpose(X)
    t3_a = Transpose(t3_a_1) @ t3_a_1
    # term 2
    t3_b_1 = CholeskySolve(Lp, Transpose(K_fu)) @ Transpose(X)
    t3_b = Transpose(t3_b_1) @ t3_b_1
    return t3_a - t3_b


def var_posterior_fullcov(K, u, f, L, jitter):
    """
    implements the posterior term of the variance expression

    aka t3 in the docs
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    KpD = AddDiagonal(K_uu, L)
    Lp = CholeskyRoot(KpD)
    L_uu = CholeskyRoot(K_uu)
    # first term
    t3_a_1 = CholeskySolve(L_uu, Transpose(K_fu))
    t3_a = Transpose(t3_a_1) @ t3_a_1
    # second term
    t3_b_1 = CholeskySolve(Lp, Transpose(K_fu))
    t3_b = Transpose(t3_b_1) @ t3_b_1
    return t3_a - t3_b


def weighted_var_posterior_fullcov_fullrank(K, f, L, X, jitter):
    """
    implements the posterior term of the variance expression where u = f
    so some terms cancel out

    aka t3 in the docs (weighted variation)
    """
    K_ff = EvaluateKernelSymmetric(K, f)
    K_ff = AddJitter(K_ff, jitter)
    KpD = AddDiagonal(K_ff, L)
    Lp = CholeskyRoot(KpD)
    # term 1
    t3_a = X @ K_ff @ Transpose(X)
    # term 2
    t3_b_1 = CholeskySolve(Lp, K_ff) @ Transpose(X)
    t3_b = Transpose(t3_b_1) @ t3_b_1
    return t3_a - t3_b


def var_posterior_fullcov_fullrank(K, f, L, jitter):
    """
    implements the posterior term of the variance expression where u = f
    so some terms cancel out

    aka t3 in the docs
    """
    K_ff = EvaluateKernelSymmetric(K, f)
    K_ff = AddJitter(K_ff, jitter)
    KpD = AddDiagonal(K_ff, L)
    Lp = CholeskyRoot(KpD)
    t3_b_1 = CholeskySolve(Lp, K_ff)
    t3_b = Transpose(t3_b_1) @ t3_b_1
    return K_ff - t3_b
