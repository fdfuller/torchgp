from ..symbolic import *
from ..means import *
import torch
import numpy as np
import math

# for these implementations I try to maintain the contract that K_uu will always contain a jitter parameter
# i.e. K_uu -> K_uu + Jitter
# the default value for the jitter is 0. This means that inverse(K_uu + Jitter) and K_uu are consistent wrt
# eachother. I do not, however, strive for consistence between K_fu, K_ff and K_uu. I assume that these things are not
# being inverted


def kl(K, u, m0, T, t, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param m0: prior mean (MeanSymbol)
    :param T: unpacked precision matrix (Matrix Symbol)
    :param t: unpacked mean vector (Vector Symbol)
    :param jitter: lower bound eigen value for K
    :return: scalar (expression)
    """
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    # Tscale = Mean(ExtractDiagonal(T))
    # T = AddJitter(T,jitter*Tscale)
    L_uu = CholeskyRoot(K_uu)
    Lnat = CholeskyRoot(T)
    t0 = SumLogDiagonal(ExtractDiagonal(L_uu))
    t1 = SumLogDiagonal(ExtractDiagonal(Lnat))

    A = CholeskySolve(Lnat, CholeskySolveT(L_uu, IdentityLike(K_uu)))
    t2 = Constant(0.25) * Sum(A * A)

    source_mean = Constant(0.5) * CholeskySolveT(Lnat, CholeskySolve(Lnat, t))
    m0_u = EvaluateMean(m0, u)
    b = m0_u - source_mean
    t3 = Constant(0.5) * (Transpose(b) @ (CholeskySolveT(L_uu, CholeskySolve(L_uu, b))))

    d = Select(Constant(0), Shape(K_uu))
    t4 = (
        Constant(0.5) + Constant(0.5) * Constant(-0.6931471805599453)
    ) * d  # (1/2*log(1/2) + 1/2)d

    kl_loss = t0 + t1 + t2 + t3 - t4
    return kl_loss


def mean(K, u, f, m0, T, t, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param m0: prior mean (MeanSymbol)
    :param T: unpacked precision matrix (Matrix Symbol)
    :param t: unpacked mean vector (Vector Symbol)
    :param jitter: lower bound eigen value for K
    :return: scalar (expression)
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    # Tscale = Mean(ExtractDiagonal(T))
    # T = AddJitter(T,Tscale*jitter)
    Lnat = CholeskyRoot(T)
    m0_u = EvaluateMean(m0, u)
    m0_f = EvaluateMean(m0, f)

    b = Constant(0.5) * CholeskySolveT(Lnat, CholeskySolve(Lnat, t)) - m0_u
    mf = m0_f + (K_fu @ CholeskySolveT(L_uu, CholeskySolve(L_uu, b)))
    return mf


def weighted_mean(K, u, f, m0, T, t, X, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param m0: prior mean (MeanSymbol)
    :param T: unpacked precision matrix (Matrix Symbol)
    :param t: unpacked mean vector (Vector Symbol)
    :param jitter: lower bound eigen value for K
    :return: scalar (expression)
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    # Tscale = Mean(ExtractDiagonal(T))
    # T = AddJitter(T,Tscale*jitter)
    Lnat = CholeskyRoot(T)
    m0_u = EvaluateMean(m0, u)
    m0_f = EvaluateMean(m0, f)

    b = Constant(0.5) * CholeskySolveT(Lnat, CholeskySolve(Lnat, t)) - m0_u
    mf = (X @ m0_f) + (X @ (K_fu @ CholeskySolveT(L_uu, CholeskySolve(L_uu, b))))
    return mf


def var_fullspace(K, f):
    """

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :return: the full Kernel Kff
    """
    return EvaluateKernelSymmetric(K, f)


def weighted_var_fullspace(K, f, X):
    """

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :return: the full Kernel Kff
    """
    return X @ EvaluateKernelSymmetric(K, f) @ Transpose(X)


def var_fullspace_diag(K, f):
    """

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :return: the diagonal of the full Kernel Kff
    """
    return EvaluateKernelDiagonal(K, f)


def weighted_var_fullspace_diag(K, f, X):
    """

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :return: the diagonal of the full Kernel Kff
    """
    K_ff = EvaluateKernelSymmetric(K, f)
    return PartialSum((X @ K_ff) * X, Constant(1))


def var_subspace(K, u, f, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    A = CholeskySolve(L_uu, Transpose(K_fu))
    return Transpose(A) @ A


def weighted_var_subspace(K, u, f, X, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    A = CholeskySolve(L_uu, Transpose(K_fu)) @ Transpose(X)
    return Transpose(A) @ A


def var_subspace_diag(K, u, f, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: The diagonal of the low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    A = CholeskySolve(L_uu, Transpose(K_fu))
    return Transpose(PartialSum(A * A, Constant(0)))


def var_subspace_root(K, u, f, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    A = CholeskySolve(L_uu, Transpose(K_fu))
    return Transpose(A)


def weighted_var_subspace_diag(K, u, f, X, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: The diagonal of the low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    A = CholeskySolve(L_uu, Transpose(K_fu)) @ Transpose(X)
    return Transpose(PartialSum(A * A, Constant(0)))


def var_posterior(K, u, f, T, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param T: unpacked precision matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    # Tscale = Mean(ExtractDiagonal(T))
    # T = AddJitter(T,Tscale*jitter)
    Lnat = CholeskyRoot(T)
    B = CholeskySolve(Lnat, CholeskySolveT(L_uu, CholeskySolve(L_uu, Transpose(K_fu))))
    return Constant(0.5) * (Transpose(B) @ B)


def weighted_var_posterior(K, u, f, T, X, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param T: unpacked precision matrix (Matrix Symbol)
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    # Tscale = Mean(ExtractDiagonal(T))
    # T = AddJitter(T,Tscale*jitter)
    Lnat = CholeskyRoot(T)
    D = CholeskySolve(
        Lnat, CholeskySolveT(L_uu, CholeskySolve(L_uu, Transpose(X @ K_fu)))
    )
    return Constant(0.5) * (Transpose(D) @ D)


def var_posterior_diag(K, u, f, T, jitter):
    """

        :param K: Kernel symbol
        :param u: inducing points (Vector Symbol)
        :param f: data axis (Vector Symbol)
        :param T: unpacked precision matrix (Matrix Symbol)
        :param jitter: lower bound eigen value for K
        :return: The entire posterior covariance, as opposed to just the diag of it
        """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    # Tscale = Mean(ExtractDiagonal(T))
    # T = AddJitter(T,Tscale*jitter)
    Lnat = CholeskyRoot(T)
    B = CholeskySolve(Lnat, CholeskySolveT(L_uu, CholeskySolve(L_uu, Transpose(K_fu))))
    return Constant(0.5) * Transpose(PartialSum(B * B, Constant(0)))


def weighted_var_posterior_diag(K, u, f, T, X, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param T: unpacked precision matrix (Matrix Symbol)
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    # Tscale = Mean(ExtractDiagonal(T))
    # T = AddJitter(T,Tscale*jitter)
    Lnat = CholeskyRoot(T)
    D = CholeskySolve(
        Lnat, CholeskySolveT(L_uu, CholeskySolve(L_uu, Transpose(X @ K_fu)))
    )
    return Constant(0.5) * Transpose(PartialSum(D * D, Constant(0)))


def var_posterior_root(K, u, f, T, jitter):
    """

        :param K: Kernel symbol
        :param u: inducing points (Vector Symbol)
        :param f: data axis (Vector Symbol)
        :param T: unpacked precision matrix (Matrix Symbol)
        :param jitter: lower bound eigen value for K
        :return: The entire posterior covariance, as opposed to just the diag of it
        """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    # Tscale = Mean(ExtractDiagonal(T))
    # T = AddJitter(T,Tscale*jitter)
    Lnat = CholeskyRoot(T)
    B = CholeskySolve(Lnat, CholeskySolveT(L_uu, CholeskySolve(L_uu, Transpose(K_fu))))
    return Constant(0.70710678118) * Transpose(B)


####### The following are useful for streaming GP ###############


def kl_to_oldposterior(K, u_new, u_old, mprior, T_new, T_old, t_new, t_old, jitter):
    """
    projects the "new" posterior onto old inducing points, computing:
    KL[q_new( u_old ) || q_old( u_old )], the kl between the new and old posterior

    :param K: Kernel symbol
    :param u_new: inducing points for new posterior (Vector Symbol)
    :param u_old: inducing points for old posterior (Vector Symbol)
    :param mprior: prior mean (MeanSymbol)
    :param L_new: lower triangular (unpacked) sqrt of new posterior variance (Matrix Symbol)
    :param L_old: lower triangular (unpacked) sqrt of old posterior variance (Matrix Symbol)
    :param m_new: new posterior mean (Vector Symbol)
    :param m_old: old posterior mean (Vector Symbol)
    :param jitter: lower bound eigen value for K
    :return: scalar (expression)
    """

    proj_mean = mean(K, u_new, u_old, mprior, T_new, t_new, jitter)
    proj_var = (
        var_fullspace(K, u_old)
        - var_subspace(K, u_new, u_old, jitter)
        + var_posterior(K, u_new, u_old, T_new, jitter)
    )
    # proj_var_scale = Mean(ExtractDiagonal(proj_var))
    proj_var = AddJitter(proj_var, jitter)
    L_q_proj = CholeskyRoot(proj_var)

    L_old = CholeskyRoot(T_old)
    m_old = Constant(0.5) * CholeskySolveT(L_old, CholeskySolve(L_old, t_old))
    mdiff = m_old - proj_mean
    t0 = SumLogDiagonal(ExtractDiagonal(L_old))
    t1 = SumLogDiagonal(ExtractDiagonal(L_q_proj))
    t2 = Sum(T_old * Transpose(proj_var))
    t3 = Transpose(mdiff) @ (T_old @ mdiff)
    d = Select(Constant(0), Shape(T_old))
    t4 = Constant(0.5) * d * Constant(1.69314718056)  # 1/2 * d * (1 - log(1/2))

    kl = -t0 - t1 + t2 + t3 - t4
    return kl


def kl_to_oldposterior_sameinducing(T_new, T_old, t_new, t_old):
    """
    Computes: KL[q_new( u_old ) || q_old( u_old )], the kl between the new and old posterior, assuming
    they both share the same inducing point locations

    :param K: Kernel symbol
    :param L_new: lower triangular (unpacked) sqrt of new posterior variance (Matrix Symbol)
    :param L_old: lower triangular (unpacked) sqrt of old posterior variance (Matrix Symbol)
    :param m_new: new posterior mean (Vector Symbol)
    :param m_old: old posterior mean (Vector Symbol)
    :param jitter: lower bound eigen value for K
    :return: scalar (expression)
    """

    L_new = CholeskyRoot(T_new)
    L_old = CholeskyRoot(T_old)
    m_new = Constant(0.5) * CholeskySolveT(L_new, CholeskySolve(L_new, t_new))
    m_old = Constant(0.5) * CholeskySolveT(L_old, CholeskySolve(L_old, t_old))
    mdiff = m_old - m_new
    t0 = SumLogDiagonal(ExtractDiagonal(L_old))
    t1 = SumLogDiagonal(ExtractDiagonal(L_new))
    A = CholeskySolve(L_new, L_old)
    t2 = Constant(0.5) * Sum(A * A)
    t3 = Transpose(mdiff) @ (T_old @ mdiff)
    d = Select(Constant(0), Shape(T_old))
    t4 = Constant(0.5) * d

    kl = -t0 + t1 + t2 + t3 - t4
    return kl


def general_kl(L_left, L_right, m_left, m_right):
    """
    computes KL between two normal distributions: KL[q_left || q_right]

    :param L_left: lower triangular (unpacked) sqrt of left variance
    :param L_right: lower triangular (unpacked) sqrt of right variance
    :param m_left: mean of left
    :param m_right: mean of right
    :return: scalar (expression)
    """
    t0 = SumLogDiagonal(ExtractDiagonal(L_right))
    t1 = SumLogDiagonal(ExtractDiagonal(L_left))
    A = CholeskySolve(L_right, L_left)
    t2 = Constant(0.5) * Sum(A * A)
    mdiff = m_right - m_left
    b = CholeskySolveT(L_right, CholeskySolve(L_right, mdiff))
    t3 = Constant(0.5) * (Transpose(mdiff) @ b)
    d = Select(Constant(0), Shape(L_right))
    t4 = Constant(0.5) * d
    r = t0 - t1 + t2 + t3 - t4
    return r


def kl_to_oldprior(
    K, u_new, u_old, mprior, T_new, Lprior_old, t_new, mprior_old, jitter
):
    """
    projects the "new" posterior onto old inducing points, computing:
    KL[q_new( u_old ) || p( u_old )], where p ( . ) is the prior. We assume
    that the old prior was cached as a matrix and is passed in via K_old. The prior
    mean is also to be cached as a vector and passed in via m_old. Unlike the kl to
    old posterior, the prior is held in source form.

    :param K: Kernel symbol
    :param u_new: inducing points for new posterior (Vector Symbol)
    :param u_old: inducing points for old posterior (Vector Symbol)
    :param mprior: prior mean (MeanSymbol)
    :param T_new: Precision (unpacked) of new posterior variance (Matrix Symbol)
    :param K_old: Covariance (unpacked) of old prior (Matrix Symbol)
    :param t_new: natural mean new posterior mean (Vector Symbol)
    :param m_old: mean of old prior (Vector Symbol)
    :param jitter: lower bound eigen value for K
    :return: scalar (expression)
    """
    proj_mean = mean(K, u_new, u_old, mprior, T_new, t_new, jitter)
    proj_var = (
        var_fullspace(K, u_old)
        - var_subspace(K, u_new, u_old, jitter)
        + var_posterior(K, u_new, u_old, T_new, jitter)
    )
    # proj_var_scale = Mean(ExtractDiagonal(proj_var))
    proj_var = AddJitter(proj_var, jitter)
    L_q_proj = CholeskyRoot(proj_var)

    kl = general_kl(L_q_proj, Lprior_old, proj_mean, mprior_old)
    return kl


def kl_to_oldprior_sameinducing(T_new, Lprior_old, t_new, mprior_old):
    """
    computes KL[q_new( u_old ) || p( u_old )], where p ( . ) is the prior. We assume
    that the old prior was cached as a matrix and is passed in via K_old. The prior
    mean is also to be cached as a vector and passed in via m_old. Unlike the kl to
    old posterior, the prior is held in source form. We assume in this variant that the
    inducing points did not change between prior and posterior

    :param K: Kernel symbol
    :param u_new: inducing points for new posterior (Vector Symbol)
    :param u_old: inducing points for old posterior (Vector Symbol)
    :param mprior: prior mean (MeanSymbol)
    :param T_new: Precision (unpacked) of new posterior variance (Matrix Symbol)
    :param K_old: Covariance (unpacked) of old prior (Matrix Symbol)
    :param t_new: natural mean new posterior mean (Vector Symbol)
    :param m_old: mean of old prior (Vector Symbol)
    :param jitter: lower bound eigen value for K
    :return: scalar (expression)
    """
    L_new = CholeskyRoot(T_new)
    source_var_posterior = Constant(0.5) * CholeskySolveT(
        L_new, CholeskySolve(L_new, IdentityLike(T_new))
    )
    L_new_source = CholeskyRoot(source_var_posterior)
    source_mean_posterior = Constant(0.5) * CholeskySolveT(
        L_new, CholeskySolve(L_new, t_new)
    )

    kl = general_kl(L_new_source, Lprior_old, source_mean_posterior, mprior_old)
    return kl
