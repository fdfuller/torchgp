import attr
from typing import Union, Optional
from ..symbolic import *
from ..transforms import *
from ..means import *
from ..kernels import *
from ..functional import numpyify
import math
import torch
from torchgp.constraints import constraints, default_mapping, ConstraintRegistry


@attr.s(slots=True, auto_attribs=True)
class VarGP1DGaussHypers:
    kernel_type: str = "ARDRBF"
    """kernel type to use"""

    constraint_registry: Optional[ConstraintRegistry] = None
    """which constraint mapping to use"""

    Kr_lengthscale: float = 0.01
    """lengthscale for the input axis kernel"""

    Kr_amplitude: float = 1.0
    """amplitude for the input axis kernel"""

    Kr_jitter: float = 1e-6
    """jitter to add to Kr kernel"""

    Kr_inducing_count: int = 10
    """number of inducing points to use for the Kr kernel"""

    sigma2: float = 1.0
    """Gaussian noise variance estimate"""


def titsias_kl_1d(Kuu: Union[TensorOperation, Matrix], mparam: Vector, vparam: Vector):
    """
    Compute the Titsias KL loss expression given an evaluated kernel.

    Args:
        Kuu: kernel Kuu evaluated at axis points axu
        mparam, vparam: vectors containing the variational parameters for the mean and variance

    Returns:
        symbolic expression for the loss
    """
    # compute the Cholesky factorization f the term L = (Kuu + diag^-1(v))

    L = CholeskyRoot(AddDiagonal(Kuu, vparam))

    t1 = Constant(0.5) * (Transpose(mparam) @ Kuu @ mparam)
    t2 = -Constant(0.5) * Trace(trisolve(L, trisolve(L, Kuu), transpose=True))
    t3 = SumLogDiagonal(ExtractDiagonal(L))
    t4 = -Constant(0.5) * SumLogDiagonal(vparam)
    kl_loss = t1 + t2 + t3 + t4

    """
    Lp = CholeskyRoot(AddDiagonal(Kuu, vparam))
    t1 = SumLogDiagonal(ExtractDiagonal(Lp))
    t2 = Constant(0.5) * SumLogDiagonal(vparam)
    t3 = Constant(0.5) * Trace(CholeskySolveT(Lp, CholeskySolve(Lp, Kuu)))
    t4 = Constant(0.5) * (Transpose(mparam) @ Kuu @ mparam)
    kl_loss = t1 - t2 - t3 + t4
    """

    return kl_loss


def titsias_1d_mean_covariance(Kuu, Kfu, Kff, mparam: Vector, vparam: Vector):
    """
    Evaluate the full space mean and covariance in the Titsias parameterization.

    Args:
        Kuu: evaluated Kuu kernel at points axu
        Kuf: evaluated cross kernel at points axf and axu
        Kff: evaluated Kff kernel at points axf
        mparam, vparam: stored variational parameters

    Returns:
        mf, Vff: symbolic expressions for mean and covariance in the Titsias parameterization
    """
    L = CholeskyRoot(AddDiagonal(Kuu, vparam))

    mf = Kfu @ mparam
    tmp = trisolve(L, Transpose(Kfu))
    Vff = Kff - Transpose(tmp) @ tmp

    return mf, Vff


def titsias_1d_mean_variance(Kuu, Kfu, diagKff, mparam: Vector, vparam: Vector):
    """
    Evaluate the full space mean and diagonal of covariance in the Titsias parameterization.

    Args:
        Kuu: evaluated Kuu kernel at points axu
        Kuf: evaluated cross kernel at points axf and axu
        diagKff: evaluated Kff kernel at points axf
        mparam, vparam: stored variational parameters

    Returns:
        mf, vf: symbolic expressions for mean and covariance in the Titsias parameterization
    """
    L = CholeskyRoot(AddDiagonal(Kuu, vparam))

    mf = Kfu @ mparam
    tmp = trisolve(L, Transpose(Kfu))
    vf = diagKff - Transpose(PartialSum(tmp * tmp, Constant(0)))

    return mf, vf


def gaussian_loglikelihood(
    observed: Union[Vector, Matrix],
    noise_variance: Scalar,
    latent_mean: Union[Vector, Matrix, TensorOperation],
    latent_variance: Union[Vector, Matrix, TensorOperation],
):
    """
    Compute analytic Gaussian log-likelihood given observations. It is computed elementwise, so must be
    reduced along all dimensions

    Args:
        observed (Vector ~ (M, 1)): given observations
        noise_variance (Scalar): noise variance (sigma2) of noise
        latent_mean, latent_variance (same shape as observed): mean and variance of Gaussian prior

    Returns:
        a symbolic expression for computing the log likelihood. You probably want to sum it.
    """
    tmp = observed - latent_mean
    t1 = Reciprocal(Constant(2.0) * noise_variance) * tmp * tmp
    t2 = Constant(0.5) * Log(Constant(2 * math.pi) * noise_variance)
    t3 = Reciprocal(Constant(2.0) * noise_variance) * latent_variance
    return -(t1 + t2 + t3)


class VarGP1DGauss(torch.nn.Module):
    def __init__(self):
        super().__init__()
        ## define schema
        self._create_buffers()

        ## create symbolic expressions
        # make symbols of the existing buffers
        axur = Vector("axur")  # inducing point axis
        axsr = Vector("axsr")  # prediction axis

        Kr = Kernel("Kr")  # reduced kernel
        m = Vector("m")  # variational mean
        v = Vector("v")  # variational variance
        sigma2 = Scalar("sigma2")  # noise variance for likelihood

        axfr = Vector("axfr")  # data axis
        W = Matrix("W")  # weight matrix
        y = Vector("y")  # measurement matrix

        M = Scalar("M")  # full size of repeats
        jitter_r = Scalar("jitter_r")  # jitter of Kr kernel

        kl_scale = Scalar("kl_scale")

        # Define loss and prediction functions
        Kuur = AddJitter(EvaluateKernelSymmetric(Kr, axur), jitter_r)
        Kfur = EvaluateKernel(Kr, axfr, axur)
        Ksur = EvaluateKernel(Kr, axsr, axur)
        Kffr = AddJitter(EvaluateKernelSymmetric(Kr, axfr), jitter_r)
        diagKssr = EvaluateKernelDiagonal(Kr, axsr)

        # 2(a)
        #  compute KL term
        kl_loss = titsias_kl_1d(Kuu=Kuur, mparam=m, vparam=v)

        # 2(b)
        #  compute mean and full covariance in this parameterization
        mf, Vff = titsias_1d_mean_covariance(
            Kuu=Kuur, Kff=Kffr, Kfu=Kfur, mparam=m, vparam=v
        )

        # 2(c)
        #  compute expectation term using exact formula for Gaussian likelihood
        # compute weighed mean
        obs_mf = W @ mf
        obs_vf = PartialSum((W @ Vff) * W, Constant(1))

        loglik = gaussian_loglikelihood(
            observed=y,
            latent_mean=obs_mf,
            latent_variance=obs_vf,
            noise_variance=sigma2,
        )

        miniM = Select(Constant(0), Shape(W))

        elbo_expr = -Divide(M, miniM) * Sum(loglik) + kl_scale * kl_loss

        graph = expr2graph(Label(String("elbo"), elbo_expr))
        self._elbo_fun = graph2ast(graph, compiled=True, debug=False)

        kl_loss_expr = kl_scale * kl_loss
        graph = expr2graph(Label(String("kl_loss"), kl_loss_expr))
        self._kl_loss_fun = graph2ast(graph, compiled=True, debug=False)

        expect_loss_expr = -Divide(M, miniM) * Sum(loglik)
        graph = expr2graph(Label(String("expect_loss"), expect_loss_expr))
        self._expect_loss_fun = graph2ast(graph, compiled=True, debug=False)

        # 3 (a)
        #  Compute mean and variance
        ms, vs = titsias_1d_mean_variance(
            Kuu=Kuur, diagKff=diagKssr, Kfu=Ksur, mparam=m, vparam=v
        )

        graph_mean = expr2graph(Label(String("mean"), ms))
        graph_var = expr2graph(Label(String("var"), vs))
        graph = compose_all([graph_mean, graph_var])
        self._predict = graph2ast(graph, compiled=True, debug=False)

        # Compute predicted L2 loss
        diagKffr = EvaluateKernelDiagonal(Kr, axsr)
        mf, vf = titsias_1d_mean_variance(
            Kuu=Kuur, diagKff=diagKffr, Kfu=Kfur, mparam=m, vparam=v
        )

        tmp = W @ mf - y
        l2_loss = Divide(M, miniM) * Sum(tmp * tmp)
        graph_l2_loss = expr2graph(Label(String("l2_loss"), l2_loss))
        self._l2_loss_fun = graph2ast(graph_l2_loss, compiled=True, debug=False)

        # 4 (a)
        #  Match KL to target using numerical optimization
        """
        Lv = Matrix('Lv') # Cholesky Root of Vhat
        Lk = CholeskyRoot(Kuur)
        
        tmp = trisolve(Lv, Lk)
        tmp2 = Transpose(trisolve(Lv, Transpose(trisolve(L, Kuur, upper=False)), upper=False))
        t1 = Constant(0.5)*(Sum(tmp*tmp) - Sum(tmp2*tmp2)) # trace terms
        t2 = SumLogDiagonal(ExtractDiagonal(tmp)) - SumLogDiagonal(ExtractDiagonal(tmp2))
        tmp3 = trisolve(Lv, Kuur @ m - mhat)
        t3 = Constant(0.5)*Sum(tmp3*tmp3)
        t4 = -Constant(0.5)*M
        kl_match_expr = t1 - t2 + t3 + t4
        graph = expr2graph(Label(String("kl_match"), kl_match_expr))
        self._kl_match_fun = graph2ast(graph, compiled=True, debug=False)
        """

    def _create_buffers(self):
        ## define buffers/modules that will exist later, after model initialization
        # currently the following lines don't do anything in pytorch; eventually they will register
        # stuff to state_dict so that I don't have to use the custom from_checkpoint method

        # standardization parameters
        self.register_buffer("jitter_r", None)
        self.register_buffer("axfr", None)

        self.stored_K = None  # holds kernels Kr, Kq
        self.stored_params = None  # holds sigma2

        # Register constraints on parameters
        self.constraints = {
            "stored_params.sigma2": constraints.positive,
            "stored_params.v": constraints.positive,
            "stored_params.m": constraints.real,
            "stored_params.axur": constraints.interval(-1.1, 1.1),
        }

    @classmethod
    def from_checkpoint(cls, checkpoint, hypers):
        model = cls()
        hypers = VarGP1DGaussHypers(**hypers)

        if hypers.kernel_type == "Matern52":
            kernels = {
                "Kr": Matern52(lengthscale=torch.tensor([[hypers.Kr_lengthscale]]))
            }
        elif hypers.kernel_type == "ARDRBF":
            kernels = {
                "Kr": ARDRBFKernel(lengthscale=torch.tensor([[hypers.Kr_lengthscale]]))
            }
        else:
            raise NotImplementedError("unknown kernel type")

        print(f"Using kernel type {hypers.kernel_type}")
        model.stored_K = torch.nn.ModuleDict(kernels)

        model.constraint_registry = (
            default_mapping
            if hypers.constraint_registry is None
            else hypers.constraint_registry
        )
        if hypers.constraint_registry is None:
            print(f"Using default constraint registry")
        else:
            print(f"Using custom constraint registry")

        model.stored_params = torch.nn.ParameterDict(
            {
                "sigma2": torch.nn.Parameter(torch.empty_like(checkpoint["sigma2"])),
                "v": torch.nn.Parameter(torch.empty_like(checkpoint["v"])),
                "m": torch.nn.Parameter(torch.empty_like(checkpoint["m"])),
                "axur": torch.nn.Parameter(torch.empty_like(checkpoint["axur"])),
            }
        )
        model.register_buffer("jitter_r", torch.empty_like(checkpoint["jitter_r"]))

        model.load_state_dict(checkpoint)
        return model

    @classmethod
    def from_buffers(cls, axfr, hypers=None):
        """create model from input buffers
        """
        model = cls()

        hypers = (
            VarGP1DGaussHypers() if hypers is None else VarGP1DGaussHypers(**hypers)
        )

        # precompute some stuff
        model.axfr = axfr
        model.jitter_r = torch.tensor(hypers.Kr_jitter)

        # initialize parameters
        if hypers.kernel_type == "Matern52":
            kernels = {
                "Kr": Matern52(lengthscale=torch.tensor([[hypers.Kr_lengthscale]]))
            }
        elif hypers.kernel_type == "ARDRBF":
            kernels = {
                "Kr": ARDRBFKernel(lengthscale=torch.tensor([[hypers.Kr_lengthscale]]))
            }
        else:
            raise NotImplementedError("unknown kernel type")

        print(f"Using kernel type {hypers.kernel_type}")
        model.stored_K = torch.nn.ModuleDict(kernels)

        if hypers.constraint_registry is None:
            print(f"Using default constraint registry")
            model.constraint_registry = default_mapping
        else:
            model.constraint_registry = default_mapping
            print(f"Using custom constraint registry")

        sigma2_transform = model.constraint_registry(
            model.constraints["stored_params.sigma2"]
        )
        v_transform = model.constraint_registry(model.constraints["stored_params.v"])
        m_transform = model.constraint_registry(model.constraints["stored_params.m"])
        axur_transform = model.constraint_registry(
            model.constraints["stored_params.axur"]
        )

        sigma2 = sigma2_transform.inv(torch.tensor([hypers.sigma2]))
        v = v_transform.inv(torch.ones(hypers.Kr_inducing_count).view(-1, 1))
        m = m_transform.inv(torch.empty(hypers.Kr_inducing_count).normal_().view(-1, 1))
        axur = axur_transform.inv(
            torch.linspace(-1, 1, hypers.Kr_inducing_count).view(-1, 1)
        )

        model.stored_params = torch.nn.ParameterDict(
            {
                "sigma2": torch.nn.Parameter(sigma2),
                "v": torch.nn.Parameter(v),
                "m": torch.nn.Parameter(m),
                "axur": torch.nn.Parameter(axur),
            }
        )

        return model

    @property
    def metrics(self):
        with torch.no_grad():
            sigma2_transform = self.constraint_registry(
                self.constraints["stored_params.sigma2"]
            )
            sigma2 = sigma2_transform(self.stored_params["sigma2"]).item()
            Kr_lengthscale = self.stored_K["Kr"].lengthscale.item()
            Kr_amplitude = self.stored_K["Kr"].amplitude.item()

            v_transform = self.constraint_registry(self.constraints["stored_params.v"])
            m_transform = self.constraint_registry(self.constraints["stored_params.m"])
            axur_transform = self.constraint_registry(
                self.constraints["stored_params.axur"]
            )
            v = v_transform(self.stored_params.v)
            m = m_transform(self.stored_params.m)
            axur = axur_transform(self.stored_params.axur)

        return {
            "sigma2": float(sigma2),
            "Kr_lengthscale": float(Kr_lengthscale),
            "Kr_amplitude": float(Kr_amplitude),
            "v": v,
            "m": m,
            "axur": axur,
        }

    def expect_metrics(self, W, y, M):
        sigma2_transform = self.constraint_registry(
            self.constraints["stored_params.sigma2"]
        )
        sigma2 = sigma2_transform(self.stored_params["sigma2"]).unsqueeze(-1)

        v_transform = self.constraint_registry(self.constraints["stored_params.v"])
        v = v_transform(self.stored_params["v"])
        m_transform = self.constraint_registry(self.constraints["stored_params.m"])
        m = m_transform(self.stored_params["m"])
        axur_transform = self.constraint_registry(
            self.constraints["stored_params.axur"]
        )
        axur = axur_transform(self.stored_params["axur"])

        miniM = W.shape[0]

        params = {
            "W": W,
            "y": y.view(-1, 1),
            "M": M,
            "miniM": miniM,
            "axfr": self.axfr,
            "axur": axur,
            "sigma2": sigma2,
            "jitter_r": self.jitter_r,
            "v": v,
            "m": m,
            **self.stored_K,
        }
        result = self._l2_loss_fun(**params).l2_loss
        return result

    def forward(self, W, y, M, kl_scale=1.0):
        sigma2_transform = self.constraint_registry(
            self.constraints["stored_params.sigma2"]
        )
        sigma2 = sigma2_transform(self.stored_params["sigma2"]).unsqueeze(-1)

        v_transform = self.constraint_registry(self.constraints["stored_params.v"])
        v = v_transform(self.stored_params["v"])
        m_transform = self.constraint_registry(self.constraints["stored_params.m"])
        m = m_transform(self.stored_params["m"])
        axur_transform = self.constraint_registry(
            self.constraints["stored_params.axur"]
        )
        axur = axur_transform(self.stored_params["axur"])

        miniM = W.shape[0]

        params = {
            "W": W,
            "y": y.view(-1, 1),
            "M": M,
            "miniM": miniM,
            "kl_scale": kl_scale,
            "axfr": self.axfr,
            "sigma2": sigma2,
            "jitter_r": self.jitter_r,
            "v": v,
            "m": m,
            "axur": axur,
            **self.stored_K,
        }
        return self._elbo_fun(**params).elbo

    def expect_loss(self, W, y, M, kl_scale):
        sigma2_transform = self.constraint_registry(
            self.constraints["stored_params.sigma2"]
        )
        sigma2 = sigma2_transform(self.stored_params["sigma2"]).unsqueeze(-1)

        v_transform = self.constraint_registry(self.constraints["stored_params.v"])
        v = v_transform(self.stored_params["v"])
        m_transform = self.constraint_registry(self.constraints["stored_params.m"])
        m = m_transform(self.stored_params["m"])
        axur_transform = self.constraint_registry(
            self.constraints["stored_params.axur"]
        )
        axur = axur_transform(self.stored_params["axur"])

        miniM = W.shape[0]

        params = {
            "W": W,
            "y": y.view(-1, 1),
            "M": M,
            "miniM": miniM,
            "kl_scale": kl_scale,
            "axfr": self.axfr,
            "sigma2": sigma2,
            "jitter_r": self.jitter_r,
            "v": v,
            "m": m,
            "axur": axur,
            **self.stored_K,
        }
        return self._expect_loss_fun(**params).expect_loss

    def kl_loss(self, W, y, M, kl_scale):
        sigma2_transform = self.constraint_registry(
            self.constraints["stored_params.sigma2"]
        )
        sigma2 = sigma2_transform(self.stored_params["sigma2"]).unsqueeze(-1)

        v_transform = self.constraint_registry(self.constraints["stored_params.v"])
        v = v_transform(self.stored_params["v"])
        m_transform = self.constraint_registry(self.constraints["stored_params.m"])
        m = m_transform(self.stored_params["m"])
        axur_transform = self.constraint_registry(
            self.constraints["stored_params.axur"]
        )
        axur = axur_transform(self.stored_params["axur"])

        miniM = W.shape[0]

        params = {
            "W": W,
            "y": y.view(-1, 1),
            "M": M,
            "miniM": miniM,
            "kl_scale": kl_scale,
            "axfr": self.axfr,
            "sigma2": sigma2,
            "jitter_r": self.jitter_r,
            "v": v,
            "m": m,
            "axur": axur,
            **self.stored_K,
        }
        return self._kl_loss_fun(**params).kl_loss

    def predict(self, axsr):
        sigma2_transform = self.constraint_registry(
            self.constraints["stored_params.sigma2"]
        )
        sigma2 = sigma2_transform(self.stored_params["sigma2"]).unsqueeze(-1)
        v_transform = self.constraint_registry(self.constraints["stored_params.v"])
        v = v_transform(self.stored_params["v"])
        m_transform = self.constraint_registry(self.constraints["stored_params.m"])
        m = m_transform(self.stored_params["m"])
        axur_transform = self.constraint_registry(
            self.constraints["stored_params.axur"]
        )
        axur = axur_transform(self.stored_params["axur"])

        params = {
            "axfr": self.axfr,
            "axur": axur,
            "axsr": axsr,
            "sigma2": sigma2,
            "jitter_r": self.jitter_r,
            "v": v,
            "m": m,
            **self.stored_K,
        }
        result = self._predict(**params)
        return result.mean, result.var

    """
    def match_posterior(self, mhat, Lvhat):
        # set the mean to mhat
        m_transform = biject_to(self.constraints['stored_params.m'])
        m = m_transform(self.stored_params['m'])
        v_transform = biject_to(self.constraints['stored_params.v'])
        v = v_transform(self.stored_params['v'])
        axur_transform = biject_to(self.constraints['stored_params.axur'])

        with torch.no_grad():
            axur = axur_transform(self.stored_params['axur'])

        M = axur.shape[0]

        params = {
            'axur': axur,
            'jitter_r': self.jitter_r,
            'v': v,
            'm': m,
            'M': M,
            'mhat': mhat,
            'Lv': Lvhat,
            **self.stored_K,
        }
        result = self._kl_match_fun(**params)
        return result.kl_match
    """

    def match_posterior(self, mhat, Lvhat):
        # set the mean to mhat
        m_transform = self.constraint_registry(self.constraints["stored_params.m"])

        # ensure inducing points don't move
        with torch.no_grad():
            axur_transform = self.constraint_registry(
                self.constraints["stored_params.axur"]
            )
            axur = axur_transform(self.stored_params["axur"])

            Kuur = self.stored_K["Kr"].symmetric(axur)
            Q, R = Kuur.qr()
            m_target = torch.triangular_solve(
                Q.t() @ mhat, R, upper=True, transpose=False
            ).solution
            m_target = m_transform.inv(m_target)

            self.stored_params["m"][:] = m_target

        return m_target
