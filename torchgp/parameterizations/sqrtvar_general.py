from ..symbolic import *
from ..means import *
import torch
import numpy as np
import math
from matchpy import (
    CustomConstraint,
    Wildcard,
    ReplacementRule,
    Pattern,
    replace_all,
    is_match,
    ManyToOneMatcher,
    Operation,
    Arity,
)

### In this parameterization file, we write functions in terms of evaluated tensors only. This allows
### for some more general use cases, like hadamard kernels and sum kernels.

x_ = Wildcard.dot("x")
is_transpose = CustomConstraint(lambda x: x.head == Transpose)
simp_transpose = (
    ReplacementRule(Pattern(Transpose(x_), is_transpose), lambda x: x.operands[0]),
)


def kl(K, mprior, Lp, mp, jitter):
    """
    This function computes:
    KL( N(mp, Lp) || N(mprior, K) ), where Lp is the Cholesky roots of the covariance
    all matrices/vectors assumed evaluated on the same axis (usually inducing axis)

    :param K: Kernel
    :param mprior: prior mean
    :param Lp: lower triangular (unpacked) of posterior variance
    :param mp: posterior mean
    :param jitter: lower bound eigen value for K (Scalar)
    :return: scalar (expression)
    """

    Kj = AutoNugget(K, jitter)
    L0 = CholeskyRoot(Kj)
    t1 = SumLogDiagonal(ExtractDiagonal(L0))
    t2 = SumLogDiagonal(ExtractDiagonal(Lp))
    A = CholeskySolve(L0, Lp)
    t3 = Sum(A * A)
    mdiff = mprior - mp
    b = CholeskySolve(L0, mdiff)
    t4 = Transpose(b) @ b
    t5 = Select(Constant(0), Shape(K))
    r = t1 - t2 + Constant(0.5) * t3 + Constant(0.5) * t4 - Constant(0.5) * t5
    return r

def kl_alt(L0, m0, L1, m1):
    """
    This function computes:
    KL( N(m0, L0) || N(m1, L1) ), where {L0,L1} are Cholesky roots of the covariance
    (i.e. their diagonal is constrained to be positive)

    all matrices/vectors assumed evaluated on the same axis (usually inducing axis)

    :param L1: lower triangular (unpacked) of covariance
    :param m1: mean vector
    :param L0: lower triangular (unpacked) of covariance
    :param m0: mean vector
    :return: scalar (expression)
    """

    t1 = SumLogDiagonal(ExtractDiagonal(L1))
    t2 = SumLogDiagonal(ExtractDiagonal(L0))
    A = CholeskySolve(L1, L0)
    t3 = Sum(A * A)
    mdiff = m1 - m0
    b = CholeskySolve(L1, mdiff)
    t4 = Transpose(b) @ b
    t5 = Select(Constant(0), Shape(L1))
    r = t1 - t2 + Constant(0.5) * t3 + Constant(0.5) * t4 - Constant(0.5) * t5
    return r

def kl_unconstrained(L0, m0, L1, m1):
    """
    This function computes:
    KL( N(m0, L0) || N(m1, L1) ), where {L0,L1} are unconstrained square roots of the covariance
    (i.e. their diagonals are not necessarily positive)

    all matrices/vectors assumed evaluated on the same axis (usually inducing axis)

    :param L1: lower triangular (unpacked) of covariance
    :param m1: mean vector
    :param L0: lower triangular (unpacked) of covariance
    :param m0: mean vector
    :return: scalar (expression)
    """

    t1 = SumLogAbs(ExtractDiagonal(L1))
    t2 = SumLogAbs(ExtractDiagonal(L0))
    A = CholeskySolve(L1, L0)
    t3 = Sum(A * A)
    mdiff = m1 - m0
    b = CholeskySolve(L1, mdiff)
    t4 = Transpose(b) @ b
    t5 = Select(Constant(0), Shape(L1))
    r = t1 - t2 + Constant(0.5) * t3 + Constant(0.5) * t4 - Constant(0.5) * t5
    return r

def kl_diag(v0, m0, v1, m1):
    """
    This function computes:
    KL( N(m0, diag(v0)) || N(m1, diag(v1) ), i.e the KL between two multivariate gaussians with diagonal cov

    useful for mean-field situations.

    :param v0: the variance of first gaussian (assumed positive); assumed a vector
    :param m0: the mean of the first gaussian
    :param v1: the variance of the second gaussian (assumed positive); assumed a vector
    :param m1: the mean of the second gaussian
    :return: scalar (expression)
    """
    t1 = SumLogDiagonal(v1)
    t2 = SumLogDiagonal(v0)
    t3 = Sum(Reciprocal(v1)*v0)
    mdiff = m1 - m0
    t4 = Transpose(mdiff) @ (Reciprocal(v1)*(mdiff))
    t5 = Select(Constant(0), Shape(v1))
    r = t1 - t2 + t3 + t4 - t5
    return Constant(0.5)*r



def mean(Kuu, Kfu, mprior_u, mprior_f, mp, jitter):
    """
    :param Kuu: Inducing Kernel
    :param Kfu: Cross Kernel
    :param mprior_u: prior mean evaluated at inducing points
    :param mprior_f: prior mean evaluated at f
    :param mp: posterior mean
    :param jitter: lower bound eigen value for K (Scalar)
    :return: scalar expression
    """
    Kuuj = AutoNugget(Kuu, jitter)
    L0 = CholeskyRoot(Kuuj)
    mf = mprior_f + Kfu @ CholeskySolveT(L0, CholeskySolve(L0, (mp - mprior_u)))
    return mf


def var_subspace_root(Kuu, Kfu, jitter):
    """

    :param Kuu: Inducing Kernel
    :param Kfu: Cross Kernel
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    Kuuj = AutoNugget(Kuu, jitter)
    L0 = CholeskyRoot(Kuuj)
    A = CholeskySolve(L0, Transpose(Kfu))
    return Transpose(A)


def var_subspace(Kuu, Kfu, jitter):
    """

    :param Kuu: Inducing Kernel
    :param Kfu: Cross Kernel
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    Kuuj = AutoNugget(Kuu, jitter)
    L0 = CholeskyRoot(Kuuj)
    A = CholeskySolve(L0, Transpose(Kfu))
    r = Transpose(A) @ A
    return r


def var_subspace_diag(Kuu, Kfu, jitter):
    """

    :param Kuu: Inducing Kernel
    :param Kfu: Cross Kernel
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    A = var_subspace_root(Kuu, Kfu, jitter)
    return PartialSum(A * A, Constant(1))


def var_posterior_root(Kuu, Kfu, Lp, jitter):
    """

    :param Kuu: Inducing Kernel
    :param Kfu: Cross Kernel
    :param Lp: lower triangular (unpacked) of posterior variance (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: a root factorization of the posterior term
    """
    Kuuj = AutoNugget(Kuu, jitter)
    L0 = CholeskyRoot(Kuuj)
    B = Kfu @ CholeskySolveT(L0, CholeskySolve(L0, Lp))
    return B


def var_posterior(Kuu, Kfu, Lp, jitter):
    """

    :param Kuu: Inducing Kernel
    :param Kfu: Cross Kernel
    :param Lp: lower triangular (unpacked) of posterior variance (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    B = var_posterior_root(Kuu, Kfu, Lp, jitter)
    return B @ Transpose(B)


def var_posterior_diag(Kuu, Kfu, Lp, jitter):
    """

    :param Kuu: Inducing Kernel
    :param Kfu: Cross Kernel
    :param Lp: lower triangular (unpacked) of posterior variance (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The diagonal of the posterior covariance
    """
    B = var_posterior_root(Kuu, Kfu, Lp, jitter)
    return PartialSum(B * B, Constant(1))
