from ..symbolic import *
from ..means import *
import torch
import numpy as np
import math


def kl_pspace(Z, B, u, m0, L, m, jitter):
    """
    :param Z: prior in pspace (Matrix Symbol)
    :param B: basis that translates u-space to p-space
    :param u: inducing point axis
    :param m0: prior mean (MeanSymbol)
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param m: posterior mean (Vector Symbol)
    :param jitter: lower bound eigen value for K
    :return: scalar (expression)
    """

    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    t1 = SumLogDiagonal(ExtractDiagonal(L0))
    t2 = SumLogDiagonal(ExtractDiagonal(L))
    A = CholeskySolve(L0, L)
    t3 = Sum(A * A)
    m0_p = Transpose(B) @ EvaluateMean(m0, u)
    mdiff = m0_p - m
    b = CholeskySolve(L0, mdiff)
    t4 = Transpose(b) @ b
    t5 = Select(Constant(0), Shape(Z0))
    r = t1 - t2 + Constant(0.5) * t3 + Constant(0.5) * t4 - Constant(0.5) * t5
    return r


def kl_pspace_2D(Z, B, f0, f1, m0, L, m):
    """
    We assume that Z is ready for Cholesky (jitter is added)

    :param Z: prior in pspace (Matrix Symbol)
    :param B: basis that translates u-space to p-space
    :param f0: data axis for K0
    :param f1: data axis for K1
    :param m0: prior mean (MeanSymbol)
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param m: posterior mean (Vector Symbol)
    :param jitter: lower bound eigen value for K
    :return: scalar (expression)
    """

    L0 = CholeskyRoot(Z)
    t1 = SumLogDiagonal(ExtractDiagonal(L0))
    t2 = SumLogDiagonal(ExtractDiagonal(L))
    A = CholeskySolve(L0, L)
    t3 = Sum(A * A)
    m0_p = Transpose(B) @ KroneckerProduct(EvaluateMean(m0, f0), EvaluateMean(m0, f1))
    mdiff = m0_p - m
    b = CholeskySolve(L0, mdiff)
    t4 = Transpose(b) @ b
    t5 = Select(Constant(0), Shape(Z))
    r = t1 - t2 + Constant(0.5) * t3 + Constant(0.5) * t4 - Constant(0.5) * t5
    return r


def mean_pspace(K, u, f, Z, B, m0, m, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param m0: prior mean (MeanSymbol)
    :param m: posterior mean (Vector Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: scalar expression
    """
    K_fu = EvaluateKernel(K, f, u)
    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    m0_p = Transpose(B) @ EvaluateMean(m0, u)
    m0_f = EvaluateMean(m0, f)
    mf = m0_f + (K_fu @ (B @ CholeskySolveT(L0, CholeskySolve(L0, (m - m0_p)))))
    return mf


def mean_pspace_2D(K0, K1, f0, f1, Z, B, m0, m):
    """
    No inducing points; we go straight from f-space to p-space. Further we assume
    that Z is ready for Cholesky (no jitter added)

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param m0: prior mean (MeanSymbol)
    :param m: posterior mean (Vector Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: scalar expression
    """
    K0_ff = EvaluateKernelSymmetric(K0, f0)
    K1_ff = EvaluateKernelSymmetric(K1, f1)
    L0 = CholeskyRoot(Z)
    m0_f = KroneckerProduct(EvaluateMean(m0, f0), EvaluateMean(m0, f1))
    m0_p = Transpose(B) @ m0_f
    mf = m0_f + KronMVProd2D(
        K0_ff, K1_ff, (B @ CholeskySolveT(L0, CholeskySolve(L0, (m - m0_p))))
    )
    return mf


def mean_pspace_2D_alt(f0, f1, B, m0, m):
    """
    No inducing points; we go straight from f-space to p-space. Further we assume
    that Z is ready for Cholesky (no jitter added)

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param m0: prior mean (MeanSymbol)
    :param m: posterior mean (Vector Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: scalar expression
    """
    m0_f = KroneckerProduct(EvaluateMean(m0, f0), EvaluateMean(m0, f1))
    m0_p = B @ Transpose(B) @ m0_f
    mf = m0_f - m0_p + B @ m
    return mf


def weighted_mean_pspace(K, u, f, Z, B, m0, m, X, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param m0: prior mean (MeanSymbol)
    :param m: posterior mean (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: scalar expression
    """
    K_fu = EvaluateKernel(K, f, u)
    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    m0_p = Transpose(B) @ EvaluateMean(m0, u)
    m0_f = EvaluateMean(m0, f)
    mf = (X @ m0_f) + X @ (K_fu @ CholeskySolveT(L0, CholeskySolve(L0, (m - m0_p))))
    return mf


def weighted_mean_pspace_2D(K0, K1, f0, f1, Z, B, m0, m, X):
    """
    No inducing points; we go straight from f-space to p-space. Further we assume
    that Z is ready for Cholesky (no jitter added)

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param m0: prior mean (MeanSymbol)
    :param m: posterior mean (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :return: scalar expression
    """
    K0_ff = EvaluateKernelSymmetric(K0, f0)
    K1_ff = EvaluateKernelSymmetric(K1, f1)
    L0 = CholeskyRoot(Z)
    m0_f = KroneckerProduct(EvaluateMean(m0, f0), EvaluateMean(m0, f1))
    m0_p = Transpose(B) @ m0_f
    mf = m0_f + KronMVProd2D(
        K0_ff, K1_ff, (B @ CholeskySolveT(L0, CholeskySolve(L0, (m - m0_p))))
    )
    wmf = KronMVProd2D(IdentityLike(K0_ff), X, mf)
    return wmf


def var_fullspace(K, f):
    """

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :return: the full Kernel Kff
    """
    return EvaluateKernelSymmetric(K, f)


def weighted_var_fullspace(K, f, X):
    """

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :return: the full Kernel Kff
    """
    return X @ EvaluateKernelSymmetric(K, f) @ Transpose(X)


def var_fullspace_diag(K, f):
    """

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :return: the diagonal of the full Kernel Kff
    """
    return EvaluateKernelDiagonal(K, f)


def var_fullspace_diag_2D(K0, K1, f0, f1):
    """

    :param K0: Kernel Symbol
    :param K1: Kernel Symbol
    :param f0: data axis (Vector Symbol)
    :param f1: data axis (Vector Symbol)
    :return: the diagonal of the full Kernel Kron(K0,K1) evaluated on f0, f1
    """
    return KroneckerProduct(
        EvaluateKernelDiagonal(K0, f0), EvaluateKernelDiagonal(K1, f1)
    )


def weighted_var_fullspace_diag(K, f, X):
    """

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :return: the diagonal of the full Kernel Kff
    """
    K_ff = EvaluateKernelSymmetric(K, f)
    return PartialSum((X @ K_ff) * X, Constant(1))


def var_subspace_pspace(K, u, f, Z, B, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    K_fu = EvaluateKernel(K, f, u)
    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    A = CholeskySolve(L0, Transpose(B)) @ Transpose(K_fu)
    return Transpose(A) @ A


def weighted_var_subspace_pspace(K, u, f, Z, B, X, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    K_fu = EvaluateKernel(K, f, u)
    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    A = CholeskySolve(L0, Transpose(B)) @ Transpose(K_fu) @ Transpose(X)
    return Transpose(A) @ A


def var_subspace_pspace_diag(K, u, f, Z, B, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    K_fu = EvaluateKernel(K, f, u)
    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    A = CholeskySolve(L0, Transpose(B)) @ Transpose(K_fu)
    return Transpose(PartialSum(A * A, Constant(0)))


# def var_subspace_pspace_diag_2D(K0, K1, f0, f1, Z, B):
#     """
#     We assume that Z has jitter added already
#
#     :param K0: Kernel Symbol
#     :param K1: Kernel Symbol
#     :param f0: data axis (Vector Symbol) for K0
#     :param f1: data axis (Vector Symbol) for K1
#     :param Z: prior cov in pspace
#     :param B: basis to translate from u-space to p-space
#     :return: the diagonal of the 2D subspace kernel, needed for FITC correction.
#     """
#     K0_ff = EvaluateKernelSymmetric(K0, f0)
#     K1_ff = EvaluateKernelSymmetric(K1, f1)
#     L0 = CholeskyRoot(Z)
#     C = KronMVProd2D(K0_ff, K1_ff, Transpose(CholeskySolve(L0, Transpose(B))))
#     return PartialSum(C * C, Constant(1))


def var_subspace_pspace_diag_2D(Z, B):
    """
    We assume that Z has jitter added already

    :param K0: Kernel Symbol
    :param K1: Kernel Symbol
    :param f0: data axis (Vector Symbol) for K0
    :param f1: data axis (Vector Symbol) for K1
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :return: the diagonal of the 2D subspace kernel, needed for FITC correction.
    """
    L0 = CholeskyRoot(Z)
    C = B @ L0
    return PartialSum(C * C, Constant(1))


def var_subspace_pspace_root(K, u, f, Z, B, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    K_fu = EvaluateKernel(K, f, u)
    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    A = CholeskySolve(L0, Transpose(B)) @ Transpose(K_fu)
    return Transpose(A)


def weighted_var_subspace_pspace_diag(K, u, f, Z, B, X, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    K_fu = EvaluateKernel(K, f, u)
    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    A = CholeskySolve(L0, Transpose(B)) @ Transpose(K_fu) @ Transpose(X)
    return Transpose(PartialSum(A * A, Constant(0)))


def var_posterior_pspace(K, u, f, Z, B, L, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    K_fu = EvaluateKernel(K, f, u)
    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    D = K_fu @ (B @ CholeskySolveT(L0, CholeskySolve(L0, L)))
    return D @ Transpose(D)


def var_posterior_pspace_diag(K, u, f, Z, B, L, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    K_fu = EvaluateKernel(K, f, u)
    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    D = K_fu @ (B @ CholeskySolveT(L0, CholeskySolve(L0, L)))
    return PartialSum(D * D, Constant(1))


def var_posterior_pspace_diag_2D(K0, K1, f0, f1, Z, B, L):
    """
    We assume Z has jitter added and is good to be cholesky'd

    :param K0: Kernel symbol
    :param K1: Kernel symbol
    :param f0: data axis (Vector Symbol) for K0
    :param f1: data axis (Vector Symbol) for K1
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    K0_ff = EvaluateKernelSymmetric(K0, f0)
    K1_ff = EvaluateKernelSymmetric(K1, f1)
    L0 = CholeskyRoot(Z)
    D = KronMVProd2D(K0_ff, K1_ff, (B @ CholeskySolveT(L0, CholeskySolve(L0, L))))
    return PartialSum(D * D, Constant(1))


def var_posterior_pspace_diag_2D_alt(B, L):
    """
    We assume Z has jitter added and is good to be cholesky'd

    :param K0: Kernel symbol
    :param K1: Kernel symbol
    :param f0: data axis (Vector Symbol) for K0
    :param f1: data axis (Vector Symbol) for K1
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    D = B @ L
    return PartialSum(D * D, Constant(1))


def var_posterior_root_pspace(K, u, f, Z, B, L, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    D = K_fu @ (B @ CholeskySolveT(L0, CholeskySolve(L0, L)))
    return D


def var_posterior_root_pspace_2D(K0, K1, f0, f1, Z, B, L):
    """
    We assume Z has jitter added and is good to be cholesky'd

    :param K0: Kernel symbol
    :param K1: Kernel symbol
    :param f0: data axis (Vector Symbol) for K0
    :param f1: data axis (Vector Symbol) for K1
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    K0_ff = EvaluateKernelSymmetric(K0, f0)
    K1_ff = EvaluateKernelSymmetric(K1, f1)
    L0 = CholeskyRoot(Z)
    D = KronMVProd2D(K0_ff, K1_ff, (B @ CholeskySolveT(L0, CholeskySolve(L0, L))))
    return D


def var_posterior_root_pspace_2D_alt(B, L):
    """
    We assume Z has jitter added and is good to be cholesky'd

    :param K0: Kernel symbol
    :param K1: Kernel symbol
    :param f0: data axis (Vector Symbol) for K0
    :param f1: data axis (Vector Symbol) for K1
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    D = B @ L
    return D


def weighted_var_posterior_pspace(K, u, f, Z, B, L, X, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    D = X @ (K_fu @ (B @ CholeskySolveT(L0, CholeskySolve(L0, L))))
    return D @ Transpose(D)


def weighted_var_posterior_pspace_diag(K, u, f, Z, B, L, X, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param Z: prior cov in pspace
    :param B: basis to translate from u-space to p-space
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    Z0 = AddJitter(Z, jitter)
    L0 = CholeskyRoot(Z0)
    D = X @ (K_fu @ (B @ CholeskySolveT(L0, CholeskySolve(L0, L))))
    return PartialSum(D * D, Constant(1))
