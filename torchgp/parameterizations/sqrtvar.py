from ..symbolic import *
from ..means import *
import torch
import numpy as np
import math

# for these implementations I try to maintain the contract that K_uu will always contain a jitter parameter
# i.e. K_uu -> K_uu + Jitter
# the default value for the jitter is 0. This means that inverse(K_uu + Jitter) and K_uu are consistent wrt
# eachother. I do not, however, strive for consistence between K_fu, K_ff and K_uu. I assume that these things are not
# being inverted


def kl(K, u, m0, L, m, jitter):
    """
    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param m0: prior mean (MeanSymbol)
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param m: posterior mean (Vector Symbol)
    :param jitter: lower bound eigen value for K
    :return: scalar (expression)
    """

    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    t1 = SumLogDiagonal(ExtractDiagonal(L_uu))
    t2 = SumLogDiagonal(ExtractDiagonal(L))
    A = CholeskySolve(L_uu, L)
    t3 = Sum(A * A)
    m0_u = EvaluateMean(m0, u)
    mdiff = m0_u - m
    b = CholeskySolve(L_uu, mdiff)
    t4 = Transpose(b) @ b
    t5 = Select(Constant(0), Shape(K_uu))
    r = t1 - t2 + Constant(0.5) * t3 + Constant(0.5) * t4 - Constant(0.5) * t5
    return r


def mean(K, u, f, m0, m, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param m0: prior mean (MeanSymbol)
    :param m: posterior mean (Vector Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: scalar expression
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    m0_u = EvaluateMean(m0, u)
    m0_f = EvaluateMean(m0, f)
    mf = m0_f + K_fu @ CholeskySolveT(L_uu, CholeskySolve(L_uu, (m - m0_u)))
    return mf


def weighted_mean(K, u, f, m0, m, X, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param m0: prior mean (MeanSymbol)
    :param m: posterior mean (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: scalar expression
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    m0_u = EvaluateMean(m0, u)
    m0_f = EvaluateMean(m0, f)
    mf = (X @ m0_f) + X @ (K_fu @ CholeskySolveT(L_uu, CholeskySolve(L_uu, (m - m0_u))))
    return mf


def var_fullspace(K, f):
    """

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :return: the full Kernel Kff
    """
    return EvaluateKernelSymmetric(K, f)


def weighted_var_fullspace(K, f, X):
    """

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :return: the full Kernel Kff
    """
    return X @ EvaluateKernelSymmetric(K, f) @ Transpose(X)


def var_fullspace_diag(K, f):
    """

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :return: the diagonal of the full Kernel Kff
    """
    return EvaluateKernelDiagonal(K, f)


def weighted_var_fullspace_diag(K, f, X):
    """

    :param K: Kernel Symbol
    :param f: data axis (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :return: the diagonal of the full Kernel Kff
    """
    K_ff = EvaluateKernelSymmetric(K, f)
    return PartialSum((X @ K_ff) * X, Constant(1))


def var_subspace(K, u, f, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    A = CholeskySolve(L_uu, Transpose(K_fu))
    return Transpose(A) @ A


def weighted_var_subspace(K, u, f, X, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    A = CholeskySolve(L_uu, Transpose(K_fu)) @ Transpose(X)
    return Transpose(A) @ A


def var_subspace_diag(K, u, f, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: The diagonal of the low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    A = CholeskySolve(L_uu, Transpose(K_fu))
    return Transpose(PartialSum(A * A, Constant(0)))


def var_subspace_root(K, u, f, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: The entire low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf, as opposed to just the diagonal of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    A = CholeskySolve(L_uu, Transpose(K_fu))
    return Transpose(A)


def weighted_var_subspace_diag(K, u, f, X, jitter):
    """

    :param K: Kernel Symbol
    :param u: inducing axis (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K (Constant)
    :return: The diagonal of the low rank or "subspace" kernel, i.e. Kfu Kuu^-1 Kuf
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    A = CholeskySolve(L_uu, Transpose(K_fu)) @ Transpose(X)
    return Transpose(PartialSum(A * A, Constant(0)))


def var_posterior(K, u, f, L, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    B = K_fu @ CholeskySolveT(L_uu, CholeskySolve(L_uu, L))
    return B @ Transpose(B)


def weighted_var_posterior(K, u, f, L, X, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The entire posterior covariance, as opposed to just the diag of it
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    B = X @ K_fu @ CholeskySolveT(L_uu, CholeskySolve(L_uu, L))
    return B @ Transpose(B)


def var_posterior_diag(K, u, f, L, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The diagonal of the posterior covariance
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    B = K_fu @ CholeskySolveT(L_uu, CholeskySolve(L_uu, L))
    return PartialSum(B * B, Constant(1))


def var_posterior_root(K, u, f, L, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: a root factorization of the posterior term
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    B = K_fu @ CholeskySolveT(L_uu, CholeskySolve(L_uu, L))
    return B


def weighted_var_posterior_diag(K, u, f, L, X, jitter):
    """

    :param K: Kernel symbol
    :param u: inducing points (Vector Symbol)
    :param f: data axis (Vector Symbol)
    :param L: lower triangular (unpacked) of variance (Matrix Symbol)
    :param X: weight matrix (Matrix Symbol)
    :param jitter: lower bound eigen value for K
    :return: The diagonal of the posterior covariance
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    # Kscale = Mean(ExtractDiagonal(K_uu))
    K_uu = AddJitter(K_uu, jitter)
    L_uu = CholeskyRoot(K_uu)
    B = X @ K_fu @ CholeskySolveT(L_uu, CholeskySolve(L_uu, L))
    return PartialSum(B * B, Constant(1))


###### The following are useful for streaming GP #######


def general_kl(L_left, L_right, m_left, m_right):
    """
    computes KL between two normal distributions: KL[q_left || q_right]

    :param L_left: lower triangular (unpacked) sqrt of left variance
    :param L_right: lower triangular (unpacked) sqrt of right variance
    :param m_left: mean of left
    :param m_right: mean of right
    :return: scalar (expression)
    """
    t0 = SumLogDiagonal(ExtractDiagonal(L_right))
    t1 = SumLogDiagonal(ExtractDiagonal(L_left))
    A = CholeskySolve(L_right, L_left)
    t2 = Constant(0.5) * Sum(A * A)
    mdiff = m_right - m_left
    b = CholeskySolveT(L_right, CholeskySolve(L_right, mdiff))
    t3 = Constant(0.5) * (Transpose(mdiff) @ b)
    d = Select(Constant(0), Shape(L_right))
    t4 = Constant(0.5) * d
    r = t0 - t1 + t2 + t3 - t4
    return r


def kl_to_oldposterior(K, u_new, u_old, mprior, L_new, L_old, m_new, m_old, jitter):
    """
    projects the "new" posterior onto old inducing points, computing:
    KL[q_new( u_old ) || q_old( u_old )], the kl between the new and old posterior

    :param K: Kernel symbol
    :param u_new: inducing points for new posterior (Vector Symbol)
    :param u_old: inducing points for old posterior (Vector Symbol)
    :param mprior: prior mean (MeanSymbol)
    :param L_new: lower triangular (unpacked) sqrt of new posterior variance (Matrix Symbol)
    :param L_old: lower triangular (unpacked) sqrt of old posterior variance (Matrix Symbol)
    :param m_new: new posterior mean (Vector Symbol)
    :param m_old: old posterior mean (Vector Symbol)
    :param jitter: lower bound eigen value for K
    :return: scalar (expression)
    """

    proj_mean = mean(K, u_new, u_old, mprior, m_new, jitter)
    proj_var = (
        var_fullspace(K, u_old)
        - var_subspace(K, u_new, u_old, jitter)
        + var_posterior(K, u_new, u_old, L_new, jitter)
    )
    # proj_var_scale = Mean(ExtractDiagonal(proj_var))
    proj_var = AddJitter(proj_var, jitter)
    L_proj = CholeskyRoot(proj_var)

    # var_old = L_old @ Transpose(L_old)
    # L_old_with_jitter = CholeskyRoot(AddJitter(var_old, jitter))

    kl = general_kl(L_proj, L_old, proj_mean, m_old)
    return kl


def kl_to_oldprior(K, u_new, u_old, mprior, L_new, Lprior_old, m_new, m_old, jitter):
    """
    projects the "new" posterior onto old inducing points, computing:
    KL[q_new( u_old ) || p( u_old )], where p ( . ) is the prior. We assume
    that the old prior cholesky factor was cached as a matrix and is passed in via K_old. The prior
    mean is also to be cached as a vector and passed in via m_old. Unlike the kl_to_oldposterior
    we store the prior covariance rather than cholesky factor and so have to compute it here. Otherwise,
    it's the same as kl_to_oldposterior

    :param K: Kernel symbol
    :param u_new: inducing points for new posterior (Vector Symbol)
    :param u_old: inducing points for old posterior (Vector Symbol)
    :param mprior: prior mean (MeanSymbol)
    :param L_new: lower triangular (unpacked) sqrt of new posterior variance (Matrix Symbol)
    :param m_new: new posterior mean (Vector Symbol)
    :param jitter: lower bound eigen value for K
    :return: scalar (expression)
    """
    proj_mean = mean(K, u_new, u_old, mprior, m_new, jitter)
    proj_var = (
        var_fullspace(K, u_old)
        - var_subspace(K, u_new, u_old, jitter)
        + var_posterior(K, u_new, u_old, L_new, jitter)
    )
    proj_var = AddJitter(proj_var, jitter)
    L_proj = CholeskyRoot(proj_var)

    kl = general_kl(L_proj, Lprior_old, proj_mean, m_old)
    return kl
