from ..symbolic import *
import torch
import numpy as np

# for these implementations I try to maintain the contract that K_uu will always contain a jitter parameter
# i.e. K_uu -> K_uu + Jitter
# the default value for the jitter is 0. This means that inverse(K_uu + Jitter) and K_uu are consistent wrt
# eachother. I do not, however, strive for consistence between K_fu, K_ff and K_uu. I assume that these things are not
# being inverted


def kl_1D(K, u, Lp, m, jitter):
    """
    Assumes 0 prior mean

    :param K: Kernel symbol
    :param u: inducing points
    :param Lp: lower triangular parameter matrix
    :param m: mean vector
    :return: scalar (expression)
    """

    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    t1 = SumLogAbs(
        ExtractDiagonal(Lp)
    )  # absolute to allow unconstrained parameterization of Lp
    t3 = Constant(0.5) * Sum(Lp * Lp)
    t4 = Constant(0.5) * (Transpose(m) @ K_uu @ m)
    d = Rank(K_uu)
    t5 = Constant(0.5) * d
    kl_loss = -t1 + t3 + t4 - t5
    kl_loss = replace_all(kl_loss, replace_rank)
    return kl_loss


def kl_2D(K0, K1, u0, u1, L0p, L1p, mu, jitter):
    """
    Assumes 0 prior mean

    :param K0: Kernel symbol for 0th dim
    :param K1: Kernel symbol for 1st dim
    :param u0: inducing points for 0th dim
    :param u1: inducing points for 1st dim
    :param L0p: lower triangular parameter matrix for 0th dim
    :param L1p: lower triangular parameter matrix for 1st dim
    :param mu: vector for the mean
    :return: scalar (expression)

    """
    K0_uu = EvaluateKernelSymmetric(K0, u0)
    K0_uu = AddJitter(K0_uu, jitter)
    K1_uu = EvaluateKernelSymmetric(K1, u1)
    K1_uu = AddJitter(K1_uu, jitter)
    total_rank = Rank(K0_uu) * Rank(K1_uu)
    t1 = Divide(total_rank, Rank(L0p)) * SumLogAbs(ExtractDiagonal(L0p)) + Divide(
        total_rank, Rank(L1p)
    ) * SumLogAbs(ExtractDiagonal(L1p))
    t3 = Constant(0.5) * Sum(L0p * L0p) * Sum(L1p * L1p)
    t4 = Constant(0.5) * (Transpose(mu) @ KronMVProd(K0_uu, K1_uu, mu))
    t5 = Constant(0.5) * total_rank
    kl_loss = -t1 + t3 + t4 - t5
    kl_loss = replace_all(kl_loss, replace_rank)
    return kl_loss


def kl_2D_low_kruskal_rank(K0, K1, u0, u1, L0p, L1p, M0, M1, jitter):
    """
    Assumes 0 prior mean.
    Mean is parameterized as Sum(CKR{M0, M1}, dim=1)

    :param K0: Kernel symbol for 0th dim
    :param K1: Kernel symbol for 1st dim
    :param u0: inducing points for 0th dim
    :param u1: inducing points for 1st dim
    :param L0: lower triangular parameter matrix for 0th dim
    :param L1: lower triangular parameter matrix for 1st dim
    :param M0: component matrix of CKR mean for 0th dim
    :param M1: component matrix of CKR mean for 1st dim
    :return: scalar (expression)

    """
    K0_uu = EvaluateKernelSymmetric(K0, u0)
    K0_uu = AddJitter(K0_uu, jitter)
    K1_uu = EvaluateKernelSymmetric(K1, u1)
    K1_uu = AddJitter(K1_uu, jitter)
    total_rank = Rank(K0_uu) * Rank(K1_uu)
    t1 = Divide(total_rank, Rank(L0p)) * SumLogAbs(ExtractDiagonal(L0p)) + Divide(
        total_rank, Rank(L1p)
    ) * SumLogAbs(ExtractDiagonal(L1p))
    t3 = Constant(0.5) * Sum(L0p * L0p) * Sum(L1p * L1p)
    t4 = Constant(0.5) * Sum(
        (Transpose(M0) @ K0_uu @ M0) * (Transpose(M1) @ K1_uu @ M1)
    )
    t5 = Constant(0.5) * total_rank
    kl_loss = -t1 + t3 + t4 - t5
    kl_loss = replace_all(kl_loss, replace_rank)
    return kl_loss


def kl_2D_low_kruskal_rank_symmetric(K0, K1, u0, u1, L0p, L1p, M0, M1, jitter):
    """
        Assumes 0 prior mean.
        Mean is parameterized as Sum(CKR{M0, M1}, dim=1)

        :param K0: Kernel symbol for 0th dim
        :param K1: Kernel symbol for 1st dim
        :param u0: inducing points for 0th dim
        :param u1: inducing points for 1st dim
        :param L0p: lower triangular parameter matrix for 0th dim
        :param L1p: lower triangular parameter matrix for 1st dim
        :param M0: component matrix of CKR mean for 0th dim
        :param M1: component matrix of CKR mean for 1st dim
        :return: scalar (expression)

        """
    K0_uu = EvaluateKernelSymmetric(K0, u0)
    K0_uu = AddJitter(K0_uu, jitter)
    K1_uu = EvaluateKernelSymmetric(K1, u1)
    K1_uu = AddJitter(K1_uu, jitter)
    total_rank = Rank(K0_uu) * Rank(K1_uu)
    t1 = Divide(total_rank, Rank(L0p)) * SumLogAbs(ExtractDiagonal(L0p)) + Divide(
        total_rank, Rank(L1p)
    ) * SumLogAbs(ExtractDiagonal(L1p))
    t3 = Constant(0.5) * Sum(L0p * L0p) * Sum(L1p * L1p)
    t4 = Constant(0.5) * (
        Constant(0.5) * Sum((Transpose(M0) @ K0_uu @ M0) * (Transpose(M1) @ K1_uu @ M1))
        + Constant(0.5)
        * Sum((Transpose(M1) @ K0_uu @ M0) * (Transpose(M0) @ K1_uu @ M1))
    )
    t5 = Constant(0.5) * total_rank
    kl_loss = -t1 + t3 + t4 - t5
    kl_loss = replace_all(kl_loss, replace_rank)
    return kl_loss


def weighted_1D_mean(K, u, f, mu, X):
    """
    1D mean prediction when the mean is parameterized as m = K_uu @ mu
    (so as to avoid need to invert Kuu)

    :param K: Kernel Symbol
    :param u: inducing points, U x 1
    :param f: evaluation points, F x 1
    :param m: mean vector, U x 1
    :param X: weighing matrix, N x F
    :return: weighted vector N x 1
    """
    K_fu = EvaluateKernel(K, f, u)
    w_mf = X @ K_fu @ mu
    return w_mf


def mean_1D(K, u, f, mu):
    K_fu = EvaluateKernel(K, f, u)
    mf = K_fu @ mu
    return mf


def fully_weighted_2D_mean(K0, K1, u0, u1, f0, f1, mu, X0, X1):
    """
    2D mean prediction when the mean is parameterized as
    m = KronMV(K0_uu, K1_uu, mu)

    Further, we require that the weight apply completely to both axes (0, 1). Such
    that the there is a single output pixel

    We allow for different weights to apply to each axis (0,1). Usually, though,
    we use degenerate weights, i.e. X0 = X1
    """
    K0_fu = EvaluateKernel(K0, f0, u0)
    K1_fu = EvaluateKernel(K1, f1, u1)
    W1 = X0 @ K0_fu
    W2 = X1 @ K1_fu
    w_mf = RKRMVProd(W1, W2, mu)
    return w_mf


def mean_2D(K0, K1, u0, u1, f0, f1, mu):
    K0_fu = EvaluateKernel(K0, f0, u0)
    K1_fu = EvaluateKernel(K1, f1, u1)
    mf = KronMVProd(K0_fu, K1_fu, mu)
    return mf


def mean_2D_low_kruskal_rank(K0, K1, u0, u1, f0, f1, M0, M1):
    K0_fu = EvaluateKernel(K0, f0, u0)
    K1_fu = EvaluateKernel(K1, f1, u1)
    mf = PartialSum(CKR(K0_fu @ M0, K1_fu @ M1), Constant(1))
    return mf


def mean_2D_low_kruskal_rank_symmetric(K0, K1, u0, u1, f0, f1, M0, M1):
    K0_fu = EvaluateKernel(K0, f0, u0)
    K1_fu = EvaluateKernel(K1, f1, u1)
    mf = PartialSum(CKR(K0_fu @ M0, K1_fu @ M1), Constant(1)) + PartialSum(
        CKR(K0_fu @ M1, K1_fu @ M0), Constant(1)
    )
    return mf


def weighted_var_fullspace_diag(K, f, X):
    """
    implements the diagonal of the full-rank term in the variance expression

    aka t1 in the docs (weighted variation)
    """
    K_ff = EvaluateKernelSymmetric(K, f)
    return PartialSum((X @ K_ff) * X, Constant(1))


def var_fullspace_diag(K, f):
    """
    implements the diagonal of the full-rank term in the variance expression

    aka t1 in the docs
    """
    K_ff = EvaluateKernelDiagonal(K, f)
    return K_ff


def weighted_var_subspace_diag(K, u, f, X, jitter):
    """
    implements the diagonal of the low-rank term of the variance expression.
    normally does not appear in the titsias parameterization, but
    does when we go to multi-dimensional. Involves cholesky of prior (hence the jitter term delta)

    aka t2 in the docs (weighted variation)
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    L = CholeskyRoot(K_uu)
    t2_a = CholeskySolve(L, Transpose(K_fu)) @ Transpose(X)
    return Transpose(PartialSum(t2_a * t2_a, Constant(0)))


def var_subspace_diag(K, u, f, jitter):
    """
    implements the diagonal of the low-rank term of the variance expression.
    normally does not appear in the titsias parameterization, but
    does when we go to multi-dimensional. Involves cholesky of prior (hence the jitter term delta)

    aka t2 in the docs (weighted variation)
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    L = CholeskyRoot(K_uu)
    t2_a = CholeskySolve(L, Transpose(K_fu))
    return Transpose(PartialSum(t2_a * t2_a, Constant(0)))


def weighted_var_posterior_diag(K, u, f, Lp, X, jitter):
    """
    implements the diagonal of the low-rank term of the variance expression.
    normally does not appear in the titsias parameterization, but
    does when we go to multi-dimensional. Involves cholesky of prior (hence the jitter term delta)

    aka t3 in the docs (weighted variation)
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    L = CholeskyRoot(K_uu)
    t3_a = X @ K_fu @ CholeskySolveT(L, Lp)
    t3 = PartialSum(t3_a * t3_a, Constant(1))
    return t3


def var_posterior_diag(K, u, f, Lp, jitter):
    """
    implements the diagonal of the low-rank term of the variance expression.
    normally does not appear in the titsias parameterization, but
    does when we go to multi-dimensional. Involves cholesky of prior (hence the jitter term delta)

    aka t3 in the docs (weighted variation)
    """
    K_fu = EvaluateKernel(K, f, u)
    K_uu = EvaluateKernelSymmetric(K, u)
    K_uu = AddJitter(K_uu, jitter)
    L = CholeskyRoot(K_uu)
    t3_a = K_fu @ CholeskySolveT(L, Lp)
    t3 = PartialSum(t3_a * t3_a, Constant(1))
    return t3


def var_1D_diag(K, u, f, Lp, jitter):
    # mainly for convenience
    t1 = var_fullspace_diag(K, f)
    t2 = var_subspace_diag(K, u, f, jitter)
    t3 = var_posterior_diag(K, u, f, Lp, jitter)
    return t1 - t2 + t3


def weighted_var_1D_diag(K, u, f, Lp, X, jitter):
    """
    provided mainly for convenience

    :param K: Kernel Symbol
    :param u: inducing axis U x 1
    :param f: evaluation axis F x 1
    :param Lp: variance lower triangular parameter matrix U x U
    :param X: weighing matrix N x F
    :return: diagonal of variance N x 1

    Note: in the 1D case we have cancellation of low rank term (t2) (as seen in the titsias paper)
    """
    t1 = weighted_var_fullspace_diag(K, f, X)
    t2 = weighted_var_subspace_diag(K, u, f, X, jitter)
    t3 = weighted_var_posterior_diag(K, u, f, Lp, X, jitter)
    return t1 - t2 + t3
