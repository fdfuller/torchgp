import torch
from ..transforms import SoftplusTransform


class Symmetric2DVector(torch.nn.Module):
    def __init__(self, n: int, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        self.n = n
        ti = torch.tril_indices(n, n, offset=-1)
        rL, cL = ti[0], ti[1]
        rD, cD = torch.arange(n), torch.arange(n)
        self.register_buffer("rL", rL)
        self.register_buffer("cL", cL)
        self.register_buffer("rD", rD)
        self.register_buffer("cD", cD)
        self.lower_storage = torch.nn.Parameter(
            torch.empty(len(rL), dtype=self.dtype).normal_(std=1 / self.n)
        )
        self.diag_storage = torch.nn.Parameter(
            torch.empty(len(rD), dtype=self.dtype).normal_(std=1 / self.n)
        )

    def forward(self):
        allocated = torch.zeros(
            (self.n, self.n), dtype=self.dtype, device=self.lower_storage.device
        )
        allocated[self.rL, self.cL] = self.lower_storage
        allocated[self.cL, self.rL] = self.lower_storage
        allocated[self.rD, self.cD] = self.diag_storage
        return allocated.reshape(-1, 1)


class UnconstrainedLowerTriangular(torch.nn.Module):
    def __init__(self, n: int, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        self.n = n
        ti = torch.tril_indices(n, n, offset=-1)
        rL, cL = ti[0], ti[1]
        rD, cD = torch.arange(n), torch.arange(n)
        self.register_buffer("rL", rL)
        self.register_buffer("cL", cL)
        self.register_buffer("rD", rD)
        self.register_buffer("cD", cD)
        self.lower_storage = torch.nn.Parameter(torch.zeros(len(rL), dtype=self.dtype))
        self.diag_storage = torch.nn.Parameter(torch.ones(len(rD), dtype=self.dtype))

    def forward(self):
        allocated = torch.zeros(
            (self.n, self.n), dtype=self.dtype, device=self.lower_storage.device
        )
        allocated[self.rL, self.cL] = self.lower_storage
        allocated[self.rD, self.cD] = self.diag_storage
        return allocated


class PackedLowerTriangular(torch.nn.Module):
    def __init__(self, n: int, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        self.n = n
        ti = torch.tril_indices(n, n, offset=0)
        rL, cL = ti[0], ti[1]
        self.register_buffer("rL", rL)
        self.register_buffer("cL", cL)
        with torch.no_grad():
            S = torch.randn(self.n, self.n, dtype=self.dtype) / self.n
            pos_def = S @ S.t()
            Linit = pos_def[rL, cL]
        self.lower_storage = torch.nn.Parameter(Linit)

    def forward(self):
        allocated = torch.zeros(
            (self.n, self.n), dtype=self.dtype, device=self.lower_storage.device
        )
        allocated[self.rL, self.cL] = self.lower_storage
        return allocated
