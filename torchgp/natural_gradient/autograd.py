import torch

__all__ = ["vjp", "jvp", "hvp"]


def vjp(y, x, v=None, **kwargs):
    return torch.autograd.grad(y, x, grad_outputs=v, **kwargs)


def jvp(y, x, v, **kwargs):
    if torch.is_tensor(y):
        outputs = [y]
    else:
        outputs = list(y)

    if torch.is_tensor(x):
        inputs = [x]
    else:
        inputs = list(x)
    u = [torch.ones_like(yi).requires_grad_(True) for yi in outputs]
    kw = {**kwargs, "create_graph": True}
    t = vjp(outputs, inputs, u, **kw)
    return vjp(t, u, v, **kwargs)


def hvp(y, x, vectors, **kwargs):
    """Computes the Hessian vector product, where the Hessian is formed from a scalar-valued function
        y = f(params). params may be a list of tensors. vectors should be a list of tensors identical in
        size to params.
    """
    for p, v in zip(x, vectors):
        if not p.squeeze().shape == v.squeeze().shape:
            raise ValueError(f"expected two tensors of the same size, but got {p.shape} and {v.shape}")
    if torch.is_tensor(y):
        output = [y]
    else:
        output = list(y)
        if len(output) > 1:
            raise ValueError("Expected only a single tensor for y")

    if torch.is_tensor(vectors):
        vectors = [vectors]
    else:
        vectors = list(vectors)

    if not output[0].nelement() == 1:
        raise ValueError("Expected a scalar-valued output")

    if torch.is_tensor(x):
        inputs = [x]
    else:
        inputs = list(x)
    g = torch.autograd.grad(output, inputs, **kwargs, create_graph=True)
    out = torch.autograd.grad([x.squeeze() for x in g], inputs, grad_outputs=[v.squeeze() for v in vectors], **kwargs)
    return out
