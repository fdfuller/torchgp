from torchgp.symbolic import *
from torchgp.means import *
import torch
import numpy as np
import math

__all__ = [
    "sqrtvar_to_source",
    "source_to_sqrtvar",
    "source_to_natural",
    "natural_to_source",
]

"""

Here we are specifying the relationships between various parameterizations of the posterior (termed xi) and
the source parameterization. In the conversions package module we establish relationships between source and
expectation or source and natural parameterization. Note: due to programmatic advantages, we're abusing the 
definition of "source" parameterization here to indicate what is more commonly known as sqrt-var parameterization,
wherein the covariance matrix S = L @ L.T and stored as L.
In total we need to establish establish:

xi <-> source <-> natural
        ^
        |
        v
    expectation
    
Where <-> indicates we can reversibly go from one parameterization to the other. We will implement only the 
introconversion between xi and source using the symbolic framework. This allows us to have full access to the
model when formulating xi. 

Conversion between source and natural or expectation is independent of the model and can be left as pure torch.
 The use of a bunch of autograd methods within the natural gradient methodology means we're going to 
 drop out of the symbolic framework anyhow.
"""


def sqrtvar_to_source(m, L):
    """
    With the definition of source as sqrtvar, this becomes a no-op

    :param m: posterior mean (Vector Symbol)
    :param L: posterior unpacked lower triangular (Matrix Symbol)
    :return: tuple of mean and variance in source form
    """
    return TupleExpr(m, L)


def source_to_sqrtvar(m, S, jitter):
    """

    :param m: posterior mean (Vector Symbol)
    :param S: posterior covariance (Matrix Symbol)
    :param jitter: left in for historic reasons
    :return: tuple of mean and unpacked lower triangular
    """
    return TupleExpr(m, S)


def natural_to_source(t, T, jitter):
    """

    :param m: posterior mean (Vector Symbol)
    :param L: posterior unpacked lower triangular (Matrix Symbol)
    :return: tuple of mean and variance in source form
    """
    # Tscale = Mean(ExtractDiagonal(T))
    # Lnat = CholeskyRoot(AddJitter(T,jitter*Tscale))
    Lnat = CholeskyRoot(T)
    source_mean = Constant(0.5) * CholeskySolveT(Lnat, CholeskySolve(Lnat, t))
    # source_var = Constant(0.5) * CholeskySolveT(
    #     Lnat, CholeskySolve(Lnat, IdentityLike(T))
    # )
    # The following is the strategy used in GPFlow rather than the above commented out version
    Lnat_inv = CholeskySolve(Lnat, IdentityLike(T))
    S = Constant(0.5) * Transpose(Lnat_inv) @ Lnat_inv
    L = CholeskyRoot(S)
    return TupleExpr(source_mean, L)


def source_to_natural(m, L, jitter):
    """

    :param m: posterior mean (Vector Symbol)
    :param L: unpacked lower triangular of posterior covariance (Matrix Symbol)
    :param jitter: left in for historic reasons
    :return: tuple of natural mean and unpacked precision
    """
    # don't add jitter under assumption that if we formed this from natural
    # then the source covariance will be full rank
    natural_mean = CholeskySolveT(L, CholeskySolve(L, m))
    # natural_var = Constant(0.5) * CholeskySolveT(
    #     L_source, CholeskySolve(L_source, IdentityLike(S))
    # )
    # using the following instead: S^-1 = L^-T @ L^-1, so only need to compute Linv
    Linv = CholeskySolve(L, IdentityLike(L))
    natural_var = Constant(0.5) * (Transpose(Linv) @ Linv)
    return TupleExpr(natural_mean, natural_var)
