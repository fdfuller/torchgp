import torch
from .conversions import *
from .autograd import vjp, jvp
from torch.optim.optimizer import Optimizer
from functools import reduce
import math
from typing import Optional, Callable

__all__ = [
    "NaturalGradient",
    "L2NormLimitedNaturalGradient",
    "FisherNormLimitedNaturalGradient",
    "LogSpaceScheduler",
    "BigBatchScheduler",
    "general_fisher_norm_limited_ng_update",
    "natural_fisher_norm_limited_ng_update",
    "ClippedNaturalGradient"
]


def blank_fun(*args, **kwargs):
    print("Adding param_groups after initialization is disabled")


class NaturalGradient(Optimizer):
    def __init__(self, params, lr=1.0, natural_parameterization=False):
        if lr < 0:
            raise ValueError(f"Invalid learning rate: {lr}")
        defaults = {"lr": lr, "natural_parameterization": natural_parameterization}
        super().__init__(params, defaults)
        if len(self.param_groups) != 1:
            raise ValueError(
                "Natural Gradient doesn't support per-parameter options "
                "(parameter groups)"
            )
        self.add_param_group = blank_fun
        self._params = self.param_groups[0]["params"]

    def step(self, xi_to_source, source_to_xi, closure=None):
        """

        :param closure: a callable to compute the loss
        :param xi_to_source: a callable to convert packed model parameterization to
                unpacked source parameterization (mean, variance). Expected to take a single list-valued argument and
                return a list of [mean, variance]
        :param source_to_xi: a callable to convert unpacked source parameterization to packed model parameterization
                Expected to accept a list of inputs [mean, variance] and return a list of parameters corresponding to
                how xi is stored in the model

        performs a step along the natural gradient on the MVN distribution this optimizer was initialized for.
        """
        lr = self.param_groups[0]["lr"]
        nat_param = self.param_groups[0]["natural_parameterization"]
        loss = None
        if closure is not None:
            loss = closure()
            dL_dxi = vjp(loss, self._params)
        else:
            dL_dxi = [p.grad.data for p in self._params]
        xi = self._params  # always a list, even when containing 1 element
        with torch.no_grad():
            expectation = [
                x.clone().detach().requires_grad_(True)
                for x in source2expectation(*xi_to_source(xi))
            ]
        xi_from_expectation = source_to_xi(*expectation2source(*expectation))
        if nat_param:
            nat_grad = vjp(xi_from_expectation, expectation, dL_dxi)
        else:
            with torch.no_grad():
                natural = [
                    x.clone().detach().requires_grad_(True)
                    for x in source2natural(*xi_to_source(xi))
                ]
            xi_from_natural = source_to_xi(*natural2source(*natural))
            nat_grad = jvp(
                xi_from_natural, natural, vjp(xi_from_expectation, expectation, dL_dxi)
            )
        for g, p in zip(nat_grad, self._params):
            p.data.add_(-lr, g)
        return loss


class L2NormLimitedNaturalGradient(Optimizer):
    def __init__(self, params, lr=1.0, natural_parameterization=False, norm_ratio=0.01):
        if lr < 0:
            raise ValueError(f"Invalid learning rate: {lr}")
        if norm_ratio <= 0:
            raise ValueError(
                f"must use a positive non-zero norm_ratio, but got {norm_ratio}"
            )
        defaults = {
            "lr": lr,
            "natural_parameterization": natural_parameterization,
            "norm_ratio": norm_ratio,
        }
        super().__init__(params, defaults)
        if len(self.param_groups) != 1:
            raise ValueError(
                "Natural Gradient doesn't support per-parameter options "
                "(parameter groups)"
            )
        self.add_param_group = blank_fun
        self._params = self.param_groups[0]["params"]

    def step(self, xi_to_source, source_to_xi, batch_fraction=1, closure=None):
        """

        :param closure: a callable to compute the loss
        :param xi_to_source: a callable to convert packed model parameterization to
                unpacked source parameterization (mean, variance). Expected to take a single list-valued argument and
                return a list of [mean, variance]
        :param source_to_xi: a callable to convert unpacked source parameterization to packed model parameterization
                Expected to accept a list of inputs [mean, variance] and return a list of parameters corresponding to
                how xi is stored in the model

        performs a step along the natural gradient on the MVN distribution this optimizer was initialized for.
        """
        lr = self.param_groups[0]["lr"]
        norm_ratio = self.param_groups[0]["norm_ratio"] * batch_fraction
        nat_param = self.param_groups[0]["natural_parameterization"]
        loss = None
        if closure is not None:
            loss = closure()
            dL_dxi = vjp(loss, self._params)
        else:
            dL_dxi = [p.grad.data for p in self._params]
        xi = self._params  # always a list, even when containing 1 element
        with torch.no_grad():
            expectation = [
                x.clone().detach().requires_grad_(True)
                for x in source2expectation(*xi_to_source(xi))
            ]
        xi_from_expectation = source_to_xi(*expectation2source(*expectation))
        if nat_param:
            nat_grad = vjp(xi_from_expectation, expectation, dL_dxi)
        else:
            with torch.no_grad():
                natural = [
                    x.clone().detach().requires_grad_(True)
                    for x in source2natural(*xi_to_source(xi))
                ]
            xi_from_natural = source_to_xi(*natural2source(*natural))
            nat_grad = jvp(
                xi_from_natural, natural, vjp(xi_from_expectation, expectation, dL_dxi)
            )
        T_norm = torch.norm(torch.cat([x.reshape(-1) for x in xi]))
        ng_norm = torch.norm(torch.cat([x.reshape(-1) for x in nat_grad]))
        if ((lr * ng_norm) / T_norm) > norm_ratio:
            lr = lr * norm_ratio / ((lr * ng_norm) / T_norm)
        candidates = [p.data - lr * g for p, g in zip(self._params, nat_grad)]
        for p, c in zip(self._params, candidates):
            p.data[...] = c.data
        return lr


class FisherNormLimitedNaturalGradient(Optimizer):
    def __init__(self, params, lr=1.0, norm_limit=0.3, natural_parameterization=False):
        if lr < 0:
            raise ValueError(f"Invalid learning rate: {lr}")
        if norm_limit <= 0:
            raise ValueError(
                f"must use a positive non-zero norm_ratio, but got {norm_limit}"
            )
        defaults = {
            "lr": lr,
            "natural_parameterization": natural_parameterization,
            "norm_limit": norm_limit,
        }
        super().__init__(params, defaults)
        if len(self.param_groups) != 1:
            raise ValueError(
                "Natural Gradient doesn't support per-parameter options "
                "(parameter groups)"
            )
        self.add_param_group = blank_fun
        self._params = self.param_groups[0]["params"]

    def step(
        self, xi_to_source, source_to_xi, closure=None, pack_xi=None, unpack_xi=None
    ):
        """

        :param closure: a callable to compute the loss
        :param pack_xi: a callable to convert unpacked xi to packed xi. Needed only if
                                natural_parameterization = True. If not passed in, then setting natural_parameterization
                                will do nothing and we'll treat xi as any other parameterization which is not optimal
        :param unpack_xi: a callable to convert packed xi to unpacked xi. Like pack_xi, needed only if
                            natural_parameterization = True
        :param xi_to_source: a callable to convert packed model parameterization to
                unpacked source parameterization (mean, variance). Expected to take a single list-valued argument and
                return a list of [mean, variance]
        :param source_to_xi: a callable to convert unpacked source parameterization to packed model parameterization
                Expected to accept a list of inputs [mean, variance] and return a list of parameters corresponding to
                how xi is stored in the model

        performs a step along the natural gradient on the MVN distribution this optimizer was initialized for.
        """
        lr = self.param_groups[0]["lr"]
        norm_limit = self.param_groups[0]["norm_limit"]
        nat_param = self.param_groups[0]["natural_parameterization"]
        if nat_param and pack_xi is None:
            nat_param = False
        loss = None
        if closure is not None:
            loss = closure()
            dL_dxi = vjp(loss, self._params)
        else:
            dL_dxi = [p.grad.data for p in self._params]
        xi = self._params  # always a list, even when containing 1 element
        with torch.no_grad():
            expectation = [
                x.clone().detach().requires_grad_(True)
                for x in source2expectation(*xi_to_source(xi))
            ]
        xi_from_expectation = source_to_xi(*expectation2source(*expectation))
        if nat_param:
            with torch.no_grad():
                natural = [
                    x.clone().detach().requires_grad_(True) for x in unpack_xi(xi)
                ]
            nat_grad = jvp(
                pack_xi(natural), natural, vjp(xi_from_expectation, expectation, dL_dxi)
            )
        else:
            with torch.no_grad():
                natural = [
                    x.clone().detach().requires_grad_(True)
                    for x in source2natural(*xi_to_source(xi))
                ]
            xi_from_natural = source_to_xi(*natural2source(*natural))
            nat_grad = jvp(
                xi_from_natural, natural, vjp(xi_from_expectation, expectation, dL_dxi)
            )
        ng = torch.cat([x.reshape(-1) for x in nat_grad])
        vg = torch.cat([x.reshape(-1) for x in dL_dxi])
        update_fisher_norm = (lr * lr * torch.dot(vg, ng)).sqrt()
        if update_fisher_norm > norm_limit:
            lr = lr * norm_limit / update_fisher_norm
        candidates = [p.data - lr * g for p, g in zip(self._params, nat_grad)]
        for p, c in zip(self._params, candidates):
            p.data[...] = c.data
        return lr

def general_fisher_norm_limited_ng_update(lr, xi, xi_to_source, source_to_xi, norm_limit = 0.3):
    """
    a different kind of closure which expects you to have called the loss and backward on it.
    this function will then perform a natural gradient given the vanilla gradients stored
    for xi.
    :param xi: parameters to optimize
    :return:
    """
    lr = float(lr)
    dL_dxi = [p.grad.data for p in xi]
    with torch.no_grad():
        expectation = [x.clone().detach().requires_grad_(True) for x in source2expectation(*xi_to_source(xi))]
    xi_from_expectation = source_to_xi(*expectation2source(*expectation))
    with torch.no_grad():
        natural = [x.clone().detach().requires_grad_(True) for x in source2natural(*xi_to_source(xi))]
    xi_from_natural = source_to_xi(*natural2source(*natural))
    nat_grad = jvp(xi_from_natural, natural, vjp(xi_from_expectation, expectation, dL_dxi))
    ng = torch.cat([x.reshape(-1) for x in nat_grad])
    vg = torch.cat([x.reshape(-1) for x in dL_dxi])
    update_fisher_norm = (lr * lr * torch.dot(vg, ng)).sqrt()
    if update_fisher_norm > norm_limit:
        lr = lr * norm_limit / float(update_fisher_norm)
    candidates = [p.data - lr * g for p, g in zip(xi, nat_grad)]
    for p, c in zip(xi, candidates):
        p.data[...] = c.data
    return lr

def natural_fisher_norm_limited_ng_update(lr, xi, xi_to_source, source_to_xi, pack_xi, unpack_xi, norm_limit = 0.3):
    """
    a different kind of closure which expects you to have called the loss and backward on it.
    this function will then perform a natural gradient given the vanilla gradients stored
    for xi.
    :param xi: parameters to optimize
    :return:
    """
    lr = float(lr)
    dL_dxi = [p.grad.data for p in xi]
    with torch.no_grad():
        expectation = [x.clone().detach().requires_grad_(True) for x in source2expectation(*xi_to_source(xi))]
    xi_from_expectation = source_to_xi(*expectation2source(*expectation))
    with torch.no_grad():
        natural = [x.clone().detach().requires_grad_(True) for x in unpack_xi(xi)]
    nat_grad = jvp(pack_xi(natural), natural, vjp(xi_from_expectation, expectation, dL_dxi))
    ng = torch.cat([x.reshape(-1) for x in nat_grad])
    vg = torch.cat([x.reshape(-1) for x in dL_dxi])
    update_fisher_norm = (lr * lr * torch.dot(vg, ng)).sqrt()
    if update_fisher_norm > norm_limit:
        lr = lr * norm_limit / float(update_fisher_norm)
    candidates = [p.data - lr * g for p, g in zip(xi, nat_grad)]
    for p, c in zip(xi, candidates):
        p.data[...] = c.data
    return lr


class ClippedNaturalGradient(Optimizer):
    def __init__(self, params, stepper: Callable, lr=1.0, norm_limit=0.3):
        if lr < 0:
            raise ValueError(f"Invalid learning rate: {lr}")
        if norm_limit <= 0:
            raise ValueError(
                f"must use a positive non-zero norm_ratio, but got {norm_limit}"
            )
        defaults = {
            "lr": lr,
            "norm_limit": norm_limit,
        }
        super().__init__(params, defaults)
        if len(self.param_groups) != 1:
            raise ValueError(
                "Natural Gradient doesn't support per-parameter options "
                "(parameter groups)"
            )
        self.add_param_group = blank_fun
        self.xi = self.param_groups[0]["params"]
        self.stepper = stepper

    def step(self, closure: Optional[Callable[[], float]]=None) -> float:
        lr = self.param_groups[0]["lr"]
        norm_limit = self.param_groups[0]["norm_limit"]
        lr = self.stepper(lr, self.xi, norm_limit=norm_limit)
        return lr



class GGT(Optimizer):
    """
    Implements a version of the algorithm presented in:
    https://arxiv.org/pdf/1806.02958.pdf
    """

    def __init__(self, params, lr=1e-3, beta_2=0.999, eps=1e-4, window=100):
        if lr < 0:
            raise ValueError(f"Invalid learning rate: {lr}")
        if beta_2 <= 0 or beta_2 > 1:
            raise ValueError(f"must use a beta_2 in (0,1], got {beta_2}")
        if eps <= 0:
            raise ValueError(f"must use eps in (0,inf), got {eps}")
        if int(window) < 1:
            raise ValueError(f"expected a window length at least 1")
        gradient_history = []
        defaults = {
            "lr": lr,
            "beta_2": beta_2,
            "eps": eps,
            "gradient_history": gradient_history,
            "window": int(window),
        }
        super().__init__(params, defaults)
        if len(self.param_groups) != 1:
            raise ValueError(
                "GGT doesn't support per-parameter options " "(parameter groups)"
            )
        self.add_param_group = blank_fun
        self._params = self.param_groups[0]["params"]
        self._numel_cache = None

    def _numel(self):
        if self._numel_cache is None:
            self._numel_cache = reduce(
                lambda total, p: total + p.numel(), self._params, 0
            )
        return self._numel_cache

    def _gather_flat_grad(self):
        views = []
        for p in self._params:
            if p.grad is None:
                view = p.data.new(p.data.numel()).zero_()
            elif p.grad.data.is_sparse:
                view = p.grad.data.to_dense().view(-1)
            else:
                view = p.grad.data.view(-1)
            views.append(view)
        return torch.cat(views, 0)

    def _add_grad(self, step_size, update):
        offset = 0
        for p in self._params:
            numel = p.numel()
            # view as to avoid deprecated pointwise semantics
            p.data.add_(step_size, update[offset : offset + numel].view_as(p.data))
            offset += numel
        assert offset == self._numel()

    def step(self, closure=None):
        """

        :param closure: a callable to compute the loss
        """
        lr = self.param_groups[0]["lr"]
        beta_2 = self.param_groups[0]["beta_2"]
        window = self.param_groups[0]["window"]
        eps = self.param_groups[0]["eps"]
        gradient_history = self.param_groups[0]["gradient_history"]
        loss = None
        if closure is not None:
            loss = closure()
            loss.backward()
        xi = self._params  # always a list, even when containing 1 element
        v = self._gather_flat_grad()
        if len(gradient_history) < window:
            gradient_history.append(v)
        elif len(gradient_history) == window:
            gradient_history.pop(0)
            gradient_history.append(v)
        else:
            raise ValueError("how did we get here?")
        sgd_update = (1 / eps) * v
        if len(gradient_history) == window:
            G = torch.stack(
                [(beta_2 ** k) * g for k, g in enumerate(gradient_history)], 1
            )
            GTG = G.t() @ G
            GTG = GTG + 1e-6 * torch.eye(GTG.shape[0], dtype=G.dtype, device=G.device)
            _, S, V = GTG.svd()
            U = G @ V @ S.sqrt().reciprocal().diag()
            t0 = U.t() @ v
            t1 = (
                (
                    S.sqrt() + eps * torch.ones(len(S), dtype=G.dtype, device=G.device)
                ).reciprocal()
                - (1 / eps) * torch.ones(len(S), dtype=G.dtype, device=G.device)
            ).diag()
            t2 = t1 @ t0
            adaptive_update = U @ t2
            update = sgd_update + adaptive_update
        else:
            update = sgd_update  # require window burn in steps
        self._add_grad(-lr, update)
        return loss


class LogSpaceScheduler(object):
    def __init__(
        self, optimizer, starting_power, final_power, N: int, decimation_factor=1
    ):
        """
        This is a scheduler aimed at Natural Gradient optimization. It will do a log-linear (base 10) ramp in learning
        rate from the starting_power to the final power and then stay there until the scheduler is reset via the reset
        method. To get repeated values of the learning rate use decimation_factor > 1.

        :param optimizer: the optimizer whose learning rate we are going schedule
        :param starting_power: learning rate starts at 10**starting_power (generally -5 to -3 is safe)
        :param final_power: learning rate ends at 10**final_power (generally -1 to 0). Should not be greater than 0, but
                            this is not enforced. I'm not your mom.
        :param N: how many steps to ramp over to the final value
        :param decimation_factor: the same value will be repeated decimation factor times
        """
        if int(N) <= 1:
            raise ValueError(f"Expecting N >= 2, got {N}")
        self.N = N
        self.optimizer = optimizer
        if final_power < starting_power:
            raise ValueError(
                f"starting power is greater than final power: {starting_power} > {final_power}. Boo!"
            )
        self.starting_power = starting_power
        self.final_power = final_power
        self.step_size = (final_power - starting_power) / (self.N - 1)
        self.decimation_factor = (
            int(decimation_factor) if int(decimation_factor) >= 1 else 1
        )
        self.counter = -1
        self.k = -1

    def step(self):
        self.counter += 1
        if not self.counter % self.decimation_factor:
            self.k += 1
        lr = self.get_lr()
        self.optimizer.param_groups[0]["lr"] = torch.tensor(lr)

    def get_lr(self):
        power = (self.k) * self.step_size + self.starting_power
        if power > self.final_power:
            power = self.final_power
        return 10 ** power

    def reset(self):
        self.counter = -1
        self.k = -1


class BigBatchScheduler(object):
    def __init__(self, starting_size, final_size, N: int, decimation_factor=1):
        """
        This is a batch size scheduler, increasing batch size in linear step sizes from starting_size to final_size
        in N steps. After the final size is reached, it continues to return that size until reset.

        :param optimizer: the optimizer whose learning rate we are going schedule
        :param starting_size: batch size that we start with
        :param final_size: batch size that we end with
        :param N: how many steps to ramp over to the final value
        :param decimation_factor: the same value will be repeated decimation factor times
        """
        if int(N) <= 1:
            raise ValueError(f"Expecting N >= 2, got {N}")
        self.N = N
        if final_size < starting_size:
            raise ValueError(
                f"starting power is greater than final power: {starting_size} > {final_size}. Boo!"
            )
        self.starting_size = starting_size
        self.final_size = final_size
        self.step_size = (final_size - starting_size) / (self.N - 1)
        self.decimation_factor = (
            int(decimation_factor) if int(decimation_factor) >= 1 else 1
        )
        self.counter = -1
        self.k = -1

    def step(self):
        self.counter += 1
        if not self.counter % self.decimation_factor:
            self.k += 1
        size = self.get_size()
        return int(size)

    def get_size(self):
        size = (self.k) * self.step_size + self.starting_size
        if size > self.final_size:
            size = self.final_size
        return size

    def reset(self):
        self.counter = -1
        self.k = -1


class PatchAdam(Optimizer):
    r"""Implements Adam algorithm with a patch to allow for null steps that update optimizer state, but don't move
        the solution

    It has been proposed in `Adam: A Method for Stochastic Optimization`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 1e-3)
        betas (Tuple[float, float], optional): coefficients used for computing
            running averages of gradient and its square (default: (0.9, 0.999))
        eps (float, optional): term added to the denominator to improve
            numerical stability (default: 1e-8)
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
        amsgrad (boolean, optional): whether to use the AMSGrad variant of this
            algorithm from the paper `On the Convergence of Adam and Beyond`_
            (default: False)

    .. _Adam\: A Method for Stochastic Optimization:
        https://arxiv.org/abs/1412.6980
    .. _On the Convergence of Adam and Beyond:
        https://openreview.net/forum?id=ryQu7f-RZ
    """

    def __init__(
        self,
        params,
        lr=1e-3,
        betas=(0.9, 0.999),
        eps=1e-8,
        weight_decay=0,
        amsgrad=False,
    ):
        if not 0.0 <= lr:
            raise ValueError("Invalid learning rate: {}".format(lr))
        if not 0.0 <= eps:
            raise ValueError("Invalid epsilon value: {}".format(eps))
        if not 0.0 <= betas[0] < 1.0:
            raise ValueError("Invalid beta parameter at index 0: {}".format(betas[0]))
        if not 0.0 <= betas[1] < 1.0:
            raise ValueError("Invalid beta parameter at index 1: {}".format(betas[1]))
        defaults = dict(
            lr=lr, betas=betas, eps=eps, weight_decay=weight_decay, amsgrad=amsgrad
        )
        super().__init__(params, defaults)

    def __setstate__(self, state):
        super().__setstate__(state)
        for group in self.param_groups:
            group.setdefault("amsgrad", False)

    def step(self, closure=None, null_step=False):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group["params"]:
                if p.grad is None:
                    continue
                grad = p.grad.data
                if grad.is_sparse:
                    raise RuntimeError(
                        "Adam does not support sparse gradients, please consider SparseAdam instead"
                    )
                amsgrad = group["amsgrad"]

                state = self.state[p]

                # State initialization
                if len(state) == 0:
                    state["step"] = 0
                    # Exponential moving average of gradient values
                    state["exp_avg"] = torch.zeros_like(p.data)
                    # Exponential moving average of squared gradient values
                    state["exp_avg_sq"] = torch.zeros_like(p.data)
                    if amsgrad:
                        # Maintains max of all exp. moving avg. of sq. grad. values
                        state["max_exp_avg_sq"] = torch.zeros_like(p.data)

                exp_avg, exp_avg_sq = state["exp_avg"], state["exp_avg_sq"]
                if amsgrad:
                    max_exp_avg_sq = state["max_exp_avg_sq"]
                beta1, beta2 = group["betas"]

                state["step"] += 1

                if group["weight_decay"] != 0:
                    grad.add_(group["weight_decay"], p.data)

                # Decay the first and second moment running average coefficient
                exp_avg.mul_(beta1).add_(1 - beta1, grad)
                exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, grad, grad)
                if amsgrad:
                    # Maintains the maximum of all 2nd moment running avg. till now
                    torch.max(max_exp_avg_sq, exp_avg_sq, out=max_exp_avg_sq)
                    # Use the max. for normalizing running avg. of gradient
                    denom = max_exp_avg_sq.sqrt().add_(group["eps"])
                else:
                    denom = exp_avg_sq.sqrt().add_(group["eps"])

                bias_correction1 = 1 - beta1 ** state["step"]
                bias_correction2 = 1 - beta2 ** state["step"]
                step_size = group["lr"] * math.sqrt(bias_correction2) / bias_correction1

                if not null_step:
                    p.data.addcdiv_(-step_size, exp_avg, denom)

        return loss
