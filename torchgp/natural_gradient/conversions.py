import torch

__all__ = [
    "source2expectation",
    "source2natural",
    "expectation2source",
    "natural2source",
]

"""
Here we establish relationships between source and expectation or source and natural parameterization. In the
parameterizations module the connection between xi and source is made (these are model dependent). Note: due to 
programmatic advantages, we're abusing the definition of "source" parameterization here to indicate what is more 
commonly known as sqrt-var parameterization, wherein the covariance matrix S = L @ L.T and stored as L.
In total we need to establish establish: 

xi <-> source <-> natural
        ^
        |
        v
    expectation

Where <-> indicates we can reversibly go from one parameterization to the other. We will implement only the 
introconversion between xi and source using the symbolic framework. This allows us to have full access to the
model when formulating xi. 

Conversion between source and natural or expectation is independent of the model and can be left as pure torch.
 The use of a bunch of autograd methods within the natural gradient methodology means we're going to 
 drop out of the symbolic framework anyhow. Note, the conversions here assume that all parameters are fully "unpacked",
 which means that symmetric matrices are represented as full matrices and mean vectors are column vectors, i.e. shape = 
 mean has shape: [...,*,N,1] and cov has shape [...,*,N,N], where * represents a special batch dim, and ... represents further batches
 
 The * batch dim is usually used to represent independent latent processes, while ... is reserved for multi-sampling.
 
 
 Conversions we use follow GPFlow parameterization, rather than Statistical Flashcards
 
 S = H - e @ e.T  |      H = S + m @ m.T
 m = e            |      e = m
 
 S = -0.5*Tinv    |      T = -0.5*Sinv
 m = S @ t        |      t = Sinv @ m
 
 
  
"""


def cholesky_solve(L, M):
    #note: if L = has shape ...NN and M has shape ...NK
    #this will return a soln of shape ...NK
    return torch.triangular_solve(M, L, upper=False).solution

def cholesky_solve_T(L, M):
    # note: if L = has shape ...NN and M has shape ...NK
    # this will return a soln of shape ...NK
    return torch.triangular_solve(M, L, upper=False, transpose=True).solution

def eye_like(x):
    with torch.no_grad():
        y = torch.zeros_like(x)
        y.diagonal(dim1=-1,dim2=-2).add_(1)
    return y

def batch_ger(x,y):
    """
    expecting x to be ...,N,1 in shape, y to be ...,M,1 in shape
    """
    assert x.shape[:-2] == y.shape[:-2], \
        f"expected batch dims to match, got {x.shape[:-2]} and {y.shape[:-2]}"
    assert x.shape[-1] == 1 and y.shape[-1] == 1, \
        "final dim should be 1, got {x.shape[-1]} and {y.shape[-1]} respectively for x and y"
    return x @ x.transpose(-1,-2)


def natural2source(t, T):
    """
    t: natural mean of shape ...,J,N,1
    T: natural cov of shape ...,J,N,N
    """
    assert t.shape[-1] == 1, f"expected final dim of t to be 1, got {t.shape[-1]}"
    assert T.shape[-1] == T.shape[-2], f"expected final two dims of T to be the same, got {T.shape[-2:]}"
    assert t.shape[:-2] == T.shape[:-2], \
        f"expected batch dims of t to be identical to that of T, got {t.shape[:-2]} and {T.shape[:-2]} respectively"
    assert t.shape[-2] == T.shape[-1], "t is incompatible with T"
    L = (-2.*T).cholesky()
    Linv = cholesky_solve(L, eye_like(L))
    S = Linv.transpose(-2, -1) @ Linv
    m = S @ t
    L = S.cholesky()
    return m, L


def source2natural(m, L):
    assert m.shape[-1] == 1, f"expected final dim of m to be 1, got {m.shape[-1]}"
    assert L.shape[-1] == L.shape[-2], f"expected final two dims of S to be the same, got {L.shape[-2:]}"
    assert m.shape[:-2] == L.shape[:-2], \
        f"expected batch dims of m to be identical to that of S, got {m.shape[:-2]} and {S.shape[:-2]} respectively"
    assert m.shape[-2] == L.shape[-1], "m is incompatible with S"
    Linv = cholesky_solve(L, eye_like(L))
    Sinv = Linv.transpose(-2, -1) @ Linv
    t = Sinv @ m
    T = -0.5 * Sinv
    return t, T


def source2expectation(m, L):
    assert m.shape[-1] == 1, f"expected final dim of m to be 1, got {m.shape[-1]}"
    assert L.shape[-1] == L.shape[-2], f"expected final two dims of L to be the same, got {L.shape[-2:]}"
    assert m.shape[:-2] == L.shape[:-2], \
        f"expected batch dims of m to be identical to that of L, got {m.shape[:-2]} and {L.shape[:-2]} respectively"
    assert m.shape[-2] == L.shape[-1], "m is incompatible with L"
    S = L @ L.transpose(-2, -1)
    H = S + batch_ger(m, m)
    return m, H


def expectation2source(e, H):
    assert e.shape[-1] == 1, f"expected final dim of m to be 1, got {e.shape[-1]}"
    assert H.shape[-1] == H.shape[-2], f"expected final two dims of H to be the same, got {H.shape[-2:]}"
    assert e.shape[:-2] == H.shape[:-2], \
        f"expected batch dims of e to be identical to that of H, got {e.shape[:-2]} and {H.shape[:-2]} respectively"
    assert e.shape[-2] == H.shape[-1], "e is incompatible with H"
    S = H - batch_ger(e, e)
    L = S.cholesky()
    return e, L
