import torch
from collections import OrderedDict
from ..transforms import SoftplusTransform, IdentityTransform

__all__ = [
    # "BatchedVector",
    "ConstrainedLowerTriangular",
    "UnconstrainedLowerTriangular",
    # "ConstrainedBatchedLowerTriangular",
    # "UnconstrainedBatchedSymmetric",
    "WrappedVector",
    "MVNGaussianSqrtVar",
    "MVNGaussianMeanField",
    "MVNGaussianSqrtVarVarSize",
    "MVNGaussianNatural",
    # "BatchedMVNGaussianSqrtVar"
]

"""
Here I define a container to hold packed parameters for a MVN gaussian. I had hoped we wouldn't need to define custom
container classes to hold what amount to two tensors, but there's some annoying packaging details that make
it so I want to parcel these two tensors into a module. Elsewhere in the Natural Gradient package these tensors are
collectively referred to as "xi". The tensors collectively known as "xi" are not necessarily the mean and variance of a
MVN gaussian, just related to them. So a given parameterization (e.g. sqrtvar, whitened, etc) would like a "xi" to mean
different things. One might think we should define these containers within the parameterizations package, but this is a
detail particular to Natural Gradients, so we find it here. Anyway, the API is pretty basic. Every "xi" should have the
following methods:

1. unpack: (no arguments) returns a list of tensors that are unpacked into vectors and matrices, ready for 
                consumption by the symbolic framework.
2. pack: takes a list of tensors as its one argument representing unpacked_xi and returns their corresponding packed
            representation (as a list of tensors).
3. xi: (no arguments) returns the packed state of xi as a list of tensors
            
Also, by making these a module, we can target them through the inherited parameters method, which is certainly convenient.
"""


# class BatchedVector(torch.nn.Module):
#     """
#     This stores a collection of vectors as a matrix internally, provides initializers (default is zero_init), and allows for
#     the vector size to be resized (reduced only) from the original shape. Useful if you want to
#     allocate a big storage and then start off with a smaller size. Resizing works by masking the storage.
#
#     batch dim is 0th dim. So when unpacked, this thing has shape batch x N x 1
#     """
#
#     def __init__(self, N, batch=1, init=None, dtype=torch.get_default_dtype()):
#         super().__init__()
#         self.dtype = dtype
#         if init is None:
#             init = self.zero_init
#         self.storage = torch.nn.Parameter(torch.empty(batch, N, dtype=self.dtype))
#         self.register_buffer("inds", torch.arange(N))
#         self.register_buffer("batch", torch.tensor(batch, dtype=torch.int64))
#         init(self.storage)
#
#     def forward(self):
#         return self.unpack()
#
#     def pack(self, unpacked):
#         allocated = torch.zeros(
#             self.batch, len(self.inds), dtype=unpacked.dtype, device=unpacked.device
#         )
#         if unpacked.dim() == 3:
#             allocated[:, :len(self.inds)] = unpacked[:,:,0]
#         elif unpacked.dim() == 2:
#             allocated[:, :len(self.inds)] = unpacked
#         else:
#             raise ValueError(f"expected either unpacked to have dim = 2 or 3, got {unpacked.dim()}")
#         return allocated
#
#     def unpack(self, packed=None):
#         if packed is None:
#             packed = self.storage
#         allocated = torch.zeros(
#             self.batch, len(self.inds), 1, dtype=packed.dtype, device=packed.device
#         )
#         allocated[:, :, 0] = packed[:, self.inds]
#         return allocated
#
#     @staticmethod
#     def random_init(x):
#         f = torch.randn_like(x)
#         x.data[...] = (f / x.shape[1]).data
#         return x
#
#     @staticmethod
#     def zero_init(x):
#         f = torch.zeros_like(x)
#         x.data[...] = f.data
#         return x
#
#     def custom_initialization(self, init_vals):
#         packed_init_vals = self.pack(init_vals)
#         self.storage.data[...] = packed_init_vals.data
#         return self.unpack()
#
#     def resize(self, n):
#         assert n<=self.storage.shape[1], "we only allow downsizing from instantiation"
#         self.inds.resize_(n)
#         self.inds.copy_(
#             torch.arange(n, dtype=self.inds.dtype, device=self.inds.device).data
#         )


class WrappedVector(torch.nn.Module):
    """
    Simply stores a matrix of the requested dimension and supplies some initializers, with
    the default being random_init
    """

    def __init__(self, nelem, init=None, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        if init is None:
            init = self.random_init
        self.storage = torch.nn.Parameter(torch.empty(nelem, dtype=self.dtype))
        self.register_buffer("inds", torch.arange(nelem))
        init(self.storage)

    def forward(self):
        return self.unpack()

    def pack(self, mpacked):
        if mpacked.dim() > 1:
            mpacked = mpacked.squeeze(1)
        allocated = torch.zeros(
            self.inds.shape[0], dtype=mpacked.dtype, device=mpacked.device
        )
        allocated[: self.inds.shape[0]] = mpacked[self.inds]
        return allocated

    def unpack(self, mpacked=None):
        if mpacked is None:
            mpacked = self.storage
        if mpacked.dim() > 1:
            mpacked = mpacked.squeeze(1)
        allocated = torch.zeros(
            self.inds.shape[0], dtype=mpacked.dtype, device=mpacked.device
        )
        allocated[: self.inds.shape[0]] = mpacked[self.inds]
        return allocated.reshape(-1, 1)

    def resize(self, n):
        self.inds.resize_(n)
        self.inds.copy_(
            torch.arange(n, dtype=self.inds.dtype, device=self.inds.device).data
        )

    @staticmethod
    def random_init(x):
        f = torch.randn_like(x)
        x.data[...] = (f / x.shape[0]).data
        return x

    @staticmethod
    def zero_init(x):
        f = torch.zeros_like(x)
        x.data[...] = f.data
        return x

    @staticmethod
    def one_init(x):
        f = torch.ones_like(x)
        x.data[...] = f.data
        return x

    def custom_initialization(self, v):
        self.storage.data[: v.shape[0]] = v.data
        return self.storage


class PositiveWrappedVector(torch.nn.Module):
    """
    Stores a positive vector
    """

    def __init__(self, nelem, init=None, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        if init is None:
            init = self.random_init
        self.transform=SoftplusTransform()
        self.storage = torch.nn.Parameter(torch.empty(nelem, dtype=self.dtype))
        self.register_buffer("inds", torch.arange(nelem))
        init(self.storage)

    def forward(self):
        return self.unpack()

    def pack(self, mpacked):
        if mpacked.dim() > 1:
            mpacked = mpacked.squeeze(1)
        allocated = torch.zeros(
            self.inds.shape[0], dtype=mpacked.dtype, device=mpacked.device
        )
        allocated[: self.inds.shape[0]] = self.transform.inv(mpacked[self.inds].clamp(min=1e-6))
        return allocated

    def unpack(self, mpacked=None):
        if mpacked is None:
            mpacked = self.storage
        if mpacked.dim() > 1:
            mpacked = mpacked.squeeze(1)
        allocated = torch.zeros(
            self.inds.shape[0], dtype=mpacked.dtype, device=mpacked.device
        )
        allocated[: self.inds.shape[0]] = self.transform(mpacked[self.inds])
        return allocated.reshape(-1, 1)

    def resize(self, n):
        self.inds.resize_(n)
        self.inds.copy_(
            torch.arange(n, dtype=self.inds.dtype, device=self.inds.device).data
        )

    @staticmethod
    def random_init(x):
        f = SoftplusTransform().inv((torch.randn_like(x)/x.shape[0]).clamp(min=1e-6))
        x.data[...] = f.data
        return x

    @staticmethod
    def zero_init(x):
        f = SoftplusTransform().inv(torch.zeros_like(x) + 1e-6)
        x.data[...] = f.data
        return x

    @staticmethod
    def one_init(x):
        f =  SoftplusTransform().inv(torch.ones_like(x))
        x.data[...] = f.data
        return x

    def custom_initialization(self, v):
        self.storage.data[: v.shape[0]] =  SoftplusTransform().inv(v.clamp(min=1e-6)).data
        return self.storage


class ConstrainedLowerTriangular(torch.nn.Module):
    """
    Initializes to the identity matrix. You can change that via custom_initializer
    """

    def __init__(
        self, n: int, transform=SoftplusTransform(), dtype=torch.get_default_dtype()
    ):
        super().__init__()
        self.dtype = dtype
        self.register_buffer("n", torch.tensor(n, dtype=torch.int64))
        ti = torch.tril_indices(n, n, offset=-1)
        rL, cL = ti[0], ti[1]
        rD, cD = torch.arange(n), torch.arange(n)
        rI = torch.cat([rD, rL])
        cI = torch.cat([cD, cL])
        self.register_buffer("rI", rI)
        self.register_buffer("cI", cI)
        self.diag_transform = transform
        packed = torch.zeros(len(rI), dtype=self.dtype)
        packed[:n] = self.diag_transform.inv(torch.ones(n, dtype=self.dtype))
        self.packed = torch.nn.Parameter(packed)

    def unpack(self, Lpacked=None):
        if Lpacked is None:
            Lpacked = self.packed
        allocated = torch.zeros(
            (self.n, self.n), dtype=Lpacked.dtype, device=Lpacked.device
        )
        allocated[self.rI[self.n :], self.cI[self.n :]] = Lpacked[self.n : len(self.rI)]
        allocated[self.rI[: self.n], self.cI[: self.n]] = self.diag_transform(
            Lpacked[: self.n]
        )
        return allocated

    def pack(self, tril, clamp=False):
        allocated = torch.zeros(len(self.rI), dtype=tril.dtype, device=tril.device)
        if not clamp:
            allocated[: self.n] = self.diag_transform.inv(
                tril[self.rI[: self.n], self.cI[: self.n]]
            )
        else:
            allocated[: self.n] = self.diag_transform.inv(
                tril[self.rI[: self.n], self.cI[: self.n]].clamp(1e-4)
            )
        allocated[self.n :] = tril[self.rI[self.n :], self.cI[self.n :]]
        return allocated

    def forward(self):
        allocated = torch.zeros(
            (self.n, self.n), dtype=self.packed.dtype, device=self.packed.device
        )
        allocated[self.rI[self.n :], self.cI[self.n :]] = self.packed[self.n :]
        allocated[self.rI[: self.n], self.cI[: self.n]] = self.diag_transform(
            self.packed[: self.n]
        )
        return allocated

    def custom_initialization(self, tril):
        with torch.no_grad():
            v = self.pack(tril)
            self.packed.data[: v.shape[0]] = v.data

    def resize(self, n):
        """

        :param n: int, specifies the new shape.
        :return: this function just updates the buffers that hold the mapping indices
        """

        ti = torch.tril_indices(n, n, offset=-1)
        rL, cL = ti[0], ti[1]
        rD, cD = torch.arange(n), torch.arange(n)
        rI = torch.cat([rD, rL])
        cI = torch.cat([cD, cL])
        self.rI.resize_(rI.shape)
        self.cI.resize_(cI.shape)
        self.rI.copy_(rI.data)
        self.cI.copy_(cI.data)
        self.n.copy_(torch.tensor(n, dtype=self.n.dtype, device=self.n.device))


# class ConstrainedBatchedLowerTriangular(torch.nn.Module):
#     """
#     Initializes to the batched identity matrix. You can change that via custom_initializer.
#
#     select transform=None if you don't want the diagonal to be transformed in storage/unpacking
#     """
#
#     def __init__(
#         self, batch: int, n: int, transform=SoftplusTransform(), dtype=torch.get_default_dtype()
#     ):
#         super().__init__()
#         self.dtype = dtype
#         self.register_buffer("n", torch.tensor(int(n), dtype=torch.int64))
#         self.register_buffer("_n", torch.tensor(int(n), dtype=torch.int64))
#         self.register_buffer("batch", torch.tensor(int(batch), dtype=torch.int64))
#         ti = torch.tril_indices(n, n, offset=-1)
#         rL, cL = ti[0], ti[1]
#         rD, cD = torch.arange(n), torch.arange(n)
#         rI = torch.cat([rD, rL])
#         cI = torch.cat([cD, cL])
#         self.register_buffer("rI", rI)
#         self.register_buffer("cI", cI)
#         if transform is None:
#             # hack where for some reason we don't want an instance to get access to .inv method
#             # I think that's a bug in the definition of IdentityTransform
#             transform = IdentityTransform
#         self.diag_transform = transform
#         packed = torch.zeros(batch, len(rI), dtype=self.dtype)
#         packed[:, :n] = self.diag_transform.inv(torch.ones(n, dtype=self.dtype)).expand(self.batch, -1)
#         self.packed = torch.nn.Parameter(packed)
#
#     def unpack(self, packed=None):
#         if packed is None:
#             packed = self.packed
#         allocated = torch.zeros(
#             (self.batch, self.n, self.n), dtype=packed.dtype, device=packed.device
#         )
#         allocated[:, self.rI[self.n:], self.cI[self.n:]] = packed[:, self.n: len(self.rI)]
#         allocated[:, self.rI[: self.n], self.cI[: self.n]] = self.diag_transform(
#             packed[:, : self.n]
#         )
#         return allocated
#
#     def pack(self, tril, clamp=False):
#         allocated = torch.zeros(self.batch, len(self.rI), dtype=tril.dtype, device=tril.device)
#         if not clamp:
#             allocated[:, : self.n] = self.diag_transform.inv(
#                 tril[:, self.rI[: self.n], self.cI[: self.n]]
#             )
#         else:
#             allocated[:, : self.n] = self.diag_transform.inv(
#                 tril[:, self.rI[: self.n], self.cI[: self.n]].clamp(1e-4)
#             )
#         allocated[:, self.n:] = tril[:, self.rI[self.n:], self.cI[self.n:]]
#         return allocated
#
#     def forward(self):
#         return self.unpack()
#
#     def custom_initialization(self, tril):
#         if tril.dim() == 2:
#             tril = tril.expand(self.batch, -1, -1)
#         with torch.no_grad():
#             v = self.pack(tril)
#             self.packed.data[:, : v.shape[1]] = v.data
#
#     def resize(self, n):
#         """
#
#         :param n: int, specifies the new shape. no change is made to batch dim
#         :return: this function just updates the buffers that hold the mapping indices
#
#         note that what's stored in the object probably no longer makes sense and you'll need
#         to have a way to fix that.
#         """
#         assert n <= self._n, "we only allow downsizing from instantiation"
#         ti = torch.tril_indices(n, n, offset=-1)
#         rL, cL = ti[0], ti[1]
#         rD, cD = torch.arange(n), torch.arange(n)
#         rI = torch.cat([rD, rL])
#         cI = torch.cat([cD, cL])
#         self.rI.resize_(rI.shape)
#         self.cI.resize_(cI.shape)
#         self.rI.copy_(rI.data)
#         self.cI.copy_(cI.data)
#         self.n.copy_(torch.tensor(n, dtype=self.n.dtype, device=self.n.device))
#
#
# class UnconstrainedBatchedSymmetric(torch.nn.Module):
#     """
#     Initializes to the batched identity matrix. You can change that via custom_initializer.
#
#     select transform=None if you don't want the diagonal to be transformed in storage/unpacking
#     """
#
#     def __init__(
#         self, batch: int, n: int, dtype=torch.get_default_dtype()
#     ):
#         super().__init__()
#         self.dtype = dtype
#         self.register_buffer("n", torch.tensor(int(n), dtype=torch.int64))
#         self.register_buffer("_n", torch.tensor(int(n), dtype=torch.int64))
#         self.register_buffer("batch", torch.tensor(int(batch), dtype=torch.int64))
#         ti = torch.tril_indices(n, n, offset=-1)
#         rL, cL = ti[0], ti[1]
#         rD, cD = torch.arange(n), torch.arange(n)
#         rI = torch.cat([rD, rL])
#         cI = torch.cat([cD, cL])
#         self.register_buffer("rI", rI)
#         self.register_buffer("cI", cI)
#         packed = torch.zeros(batch, len(rI), dtype=self.dtype)
#         packed[:, :n] = torch.ones(n, dtype=self.dtype).expand(self.batch, -1)
#         self.packed = torch.nn.Parameter(packed)
#
#     def unpack(self, packed=None):
#         if packed is None:
#             packed = self.packed
#         allocated = torch.zeros(
#             (self.batch, self.n, self.n), dtype=packed.dtype, device=packed.device
#         )
#         allocated[:, self.rI[self.n:], self.cI[self.n:]] = packed[:, self.n: len(self.rI)]
#         allocated[:, self.cI[self.n:], self.rI[self.n:]] = packed[:, self.n: len(self.rI)]
#         allocated[:, self.rI[: self.n], self.cI[: self.n]] = packed[:, : self.n]
#         return allocated
#
#     def pack(self, tril):
#         allocated = torch.zeros(self.batch, len(self.rI), dtype=tril.dtype, device=tril.device)
#         allocated[:, : self.n] = tril[:, self.rI[: self.n], self.cI[: self.n]]
#         allocated[:, self.n:] = tril[:, self.rI[self.n:], self.cI[self.n:]]
#         return allocated
#
#     def forward(self):
#         return self.unpack()
#
#     def custom_initialization(self, tril):
#         if tril.dim() == 2:
#             tril = tril.expand(self.batch, -1, -1)
#         with torch.no_grad():
#             v = self.pack(tril)
#             self.packed.data[:, : v.shape[1]] = v.data
#
#     def resize(self, n):
#         """
#
#         :param n: int, specifies the new shape. no change is made to batch dim
#         :return: this function just updates the buffers that hold the mapping indices
#         """
#         assert n <= self._n, "we only allow downsizing from instantiation"
#         ti = torch.tril_indices(n, n, offset=-1)
#         rL, cL = ti[0], ti[1]
#         rD, cD = torch.arange(n), torch.arange(n)
#         rI = torch.cat([rD, rL])
#         cI = torch.cat([cD, cL])
#         self.rI.resize_(rI.shape)
#         self.cI.resize_(cI.shape)
#         self.rI.copy_(rI.data)
#         self.cI.copy_(cI.data)
#         self.n.copy_(torch.tensor(n, dtype=self.n.dtype, device=self.n.device))

class UnconstrainedLowerTriangular(torch.nn.Module):
    """
    Initializes to the identity matrix. You can change that via custom_initializer
    """

    def __init__(
        self, n: int, dtype=torch.get_default_dtype()
    ):
        super().__init__()
        self.dtype = dtype
        self.register_buffer("n", torch.tensor(n, dtype=torch.int64))
        ti = torch.tril_indices(n, n, offset=-1)
        rL, cL = ti[0], ti[1]
        rD, cD = torch.arange(n), torch.arange(n)
        rI = torch.cat([rD, rL])
        cI = torch.cat([cD, cL])
        self.register_buffer("rI", rI)
        self.register_buffer("cI", cI)
        packed = torch.zeros(len(rI), dtype=self.dtype)
        packed[:n] = torch.ones(n, dtype=self.dtype)
        self.packed = torch.nn.Parameter(packed)

    def unpack(self, Lpacked=None):
        if Lpacked is None:
            Lpacked = self.packed
        allocated = torch.zeros(
            (self.n, self.n), dtype=Lpacked.dtype, device=Lpacked.device
        )
        allocated[self.rI[self.n :], self.cI[self.n :]] = Lpacked[self.n : len(self.rI)]
        allocated[self.rI[: self.n], self.cI[: self.n]] = Lpacked[: self.n]
        return allocated

    def pack(self, tril):
        allocated = torch.zeros(len(self.rI), dtype=tril.dtype, device=tril.device)
        allocated[: self.n] = tril[self.rI[: self.n], self.cI[: self.n]]
        allocated[self.n :] = tril[self.rI[self.n :], self.cI[self.n :]]
        return allocated

    def forward(self):
        allocated = torch.zeros(
            (self.n, self.n), dtype=self.packed.dtype, device=self.packed.device
        )
        allocated[self.rI[self.n :], self.cI[self.n :]] = self.packed[self.n :]
        allocated[self.rI[: self.n], self.cI[: self.n]] = self.packed[: self.n]
        return allocated

    def custom_initialization(self, tril):
        with torch.no_grad():
            v = self.pack(tril)
            self.packed.data[: v.shape[0]] = v.data

    def resize(self, n):
        """

        :param n: int, specifies the new shape.
        :return: this function just updates the buffers that hold the mapping indices
        """

        ti = torch.tril_indices(n, n, offset=-1)
        rL, cL = ti[0], ti[1]
        rD, cD = torch.arange(n), torch.arange(n)
        rI = torch.cat([rD, rL])
        cI = torch.cat([cD, cL])
        self.rI.resize_(rI.shape)
        self.cI.resize_(cI.shape)
        self.rI.copy_(rI.data)
        self.cI.copy_(cI.data)
        self.n.copy_(torch.tensor(n, dtype=self.n.dtype, device=self.n.device))


class UnconstrainedSymmetricMatrix(torch.nn.Module):
    """
    Initializes to the identity matrix. You can change that via custom_initializer
    """

    def __init__(self, n: int, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        self.n = n
        ti = torch.tril_indices(n, n, offset=-1)
        rL, cL = ti[0], ti[1]
        rD, cD = torch.arange(n), torch.arange(n)
        rI = torch.cat([rD, rL])
        cI = torch.cat([cD, cL])
        self.register_buffer("rI", rI)
        self.register_buffer("cI", cI)
        packed = torch.zeros(len(rI), dtype=self.dtype)
        packed[:n] = torch.ones(n, dtype=self.dtype)
        self.packed = torch.nn.Parameter(packed)

    def unpack(self, packed=None):
        if packed is None:
            packed = self.packed
        allocated = torch.zeros(
            (self.n, self.n), dtype=packed.dtype, device=packed.device
        )
        allocated[self.rI[self.n :], self.cI[self.n :]] = packed[self.n :]
        allocated[self.cI[self.n :], self.rI[self.n :]] = packed[
            self.n :
        ]  # fill in upper triangular
        allocated[self.rI[: self.n], self.cI[: self.n]] = packed[
            : self.n
        ]  # fill the diagonal
        return allocated

    def pack(self, A):
        allocated = torch.zeros(len(self.rI), dtype=A.dtype, device=A.device)
        allocated[: self.n] = A[self.rI[: self.n], self.cI[: self.n]]  # fill diagonal
        allocated[self.n :] = A[self.rI[self.n :], self.cI[self.n :]]
        return allocated

    def forward(self):
        return self.unpack()

    def custom_initialization(self, A):
        with torch.no_grad():
            v = self.pack(A)
            self.packed.data[...] = v.data


class MVNGaussianSqrtVar(torch.nn.Module):
    def __init__(self, N, constrained=True, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        if constrained:
            L = ConstrainedLowerTriangular(N, dtype=self.dtype)
        else:
            L = UnconstrainedLowerTriangular(N, dtype=self.dtype)
        self.packed_params = torch.nn.ModuleDict(
            OrderedDict(
                m=WrappedVector(N, dtype=self.dtype, init=WrappedVector.zero_init),
                L=ConstrainedLowerTriangular(N, dtype=self.dtype),
            )
        )

    def xi(self):
        m_packed = self.packed_params["m"].storage
        L_packed = self.packed_params["L"].packed
        return [m_packed, L_packed]

    def unpack(self, xi_packed=None):
        if xi_packed is None:
            m_packed = self.packed_params["m"].storage
            L_packed = self.packed_params["L"].packed
        else:
            m_packed, L_packed = xi_packed
        m_unpacked = self.packed_params["m"].unpack(mpacked=m_packed)
        L_unpacked = self.packed_params["L"].unpack(Lpacked=L_packed)
        return [m_unpacked, L_unpacked]

    def pack(self, xi):
        m_unpacked, L_unpacked = xi
        m_packed = self.packed_params["m"].pack(m_unpacked)
        L_packed = self.packed_params["L"].pack(L_unpacked, clamp=False)
        return [m_packed, L_packed]

    def custom_L_init(self, lower):
        self.packed_params["L"].custom_initialization(lower)

    def custom_m_init(self, v):
        if v.dim() == 2:
            if v.shape[1] == 1:
                v = v.squeeze(1)
            else:
                raise ValueError("expected a vector or column vector")
        elif v.dim() > 2:
            raise ValueError("expected a vector or column vector")
        self.packed_params["m"].custom_initialization(v)

    def update(self, m, S):
        """

        :param m: unpacked and valid mean vector
        :param S: unpacked and valid cov matrix
        :return: nothing, updates the packed state to now hold the passed in mean/cov
        """
        with torch.no_grad():
            L = S.cholesky()
            m_packed, L_packed = self.pack([m, L])
            self.packed_params["m"].storage.copy_(m_packed.data)
            self.packed_params["L"].packed.copy_(L_packed.data)

class MVNGaussianMeanField(torch.nn.Module):
    def __init__(self, N, dtype=torch.get_default_dtype()):
        """
        represents a multidimensional normal variate with mean m, and covariance = diag(V)

        note: that the "unpacked" state of the covariance is not actually a diagonal matrix, but just a column vector
        with column shape = 1.

        :param N: size of the gaussian
        :param dtype: tensor type
        """
        super().__init__()
        self.dtype = dtype
        self.packed_params = torch.nn.ModuleDict(
            OrderedDict(
                m=WrappedVector(N, dtype=self.dtype, init=WrappedVector.zero_init),
                V=PositiveWrappedVector(N, dtype=self.dtype, init=PositiveWrappedVector.one_init),
            )
        )

    def xi(self):
        m_packed = self.packed_params["m"].storage
        V_packed = self.packed_params["V"].storage
        return [m_packed, V_packed]

    def unpack(self, xi_packed=None):
        if xi_packed is None:
            m_packed = self.packed_params["m"].storage
            V_packed = self.packed_params["V"].storage
        else:
            m_packed, V_packed = xi_packed
        m_unpacked = self.packed_params["m"].unpack(mpacked=m_packed)
        V_unpacked = self.packed_params["V"].unpack(mpacked=V_packed)
        return [m_unpacked, V_unpacked]

    def pack(self, xi):
        m_unpacked, V_unpacked = xi
        m_packed = self.packed_params["m"].pack(m_unpacked)
        V_packed = self.packed_params["V"].pack(V_unpacked)
        return [m_packed, V_packed]

    def custom_V_init(self, v):
        if v.dim() == 2:
            if v.shape[1] == 1:
                v = v.squeeze(1)
            else:
                raise ValueError("expected a vector or column vector")
        elif v.dim() > 2:
            raise ValueError("expected a vector or column vector")
        self.packed_params["V"].custom_initialization(v)

    def custom_m_init(self, v):
        if v.dim() == 2:
            if v.shape[1] == 1:
                v = v.squeeze(1)
            else:
                raise ValueError("expected a vector or column vector")
        elif v.dim() > 2:
            raise ValueError("expected a vector or column vector")
        self.packed_params["m"].custom_initialization(v)

    def update(self, m, S):
        """

        :param m: unpacked and valid mean vector
        :param S: unpacked and valid var vector (or a full matrix, we'll extract the diagonal only)
        :return: nothing, updates the packed state to now hold the passed in mean/cov
        """
        with torch.no_grad():
            if S.shape[1] > 1:
                S = S.diag().unsqueeze(1)
            m_packed, V_packed = self.pack([m, S])
            self.packed_params["m"].storage.copy_(m_packed.data)
            self.packed_params["V"].packed.copy_(V_packed.data)


class MVNGaussianSqrtVarVarSize(torch.nn.Module):
    """
    This abortion of a thing can change shape. Neat trick.
    """

    def __init__(self, N, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        self.packed_params = torch.nn.ModuleDict(
            OrderedDict(
                m=WrappedVector(N, dtype=self.dtype, init=WrappedVector.zero_init),
                L=ConstrainedLowerTriangular(N, dtype=self.dtype),
            )
        )

    def xi(self):
        m_packed = self.packed_params["m"].storage
        L_packed = self.packed_params["L"].packed
        return [m_packed, L_packed]

    def unpack(self, xi_packed=None):
        if xi_packed is None:
            m_packed = self.packed_params["m"].storage
            L_packed = self.packed_params["L"].packed
        else:
            m_packed, L_packed = xi_packed
        m_unpacked = self.packed_params["m"].unpack(mpacked=m_packed)
        L_unpacked = self.packed_params["L"].unpack(Lpacked=L_packed)
        return [m_unpacked, L_unpacked]

    def pack(self, xi):
        m_unpacked, L_unpacked = xi
        m_packed = self.packed_params["m"].pack(m_unpacked)
        L_packed = self.packed_params["L"].pack(L_unpacked, clamp=False)
        return [m_packed, L_packed]

    def custom_L_init(self, lower):
        self.packed_params["L"].custom_initialization(lower)

    def custom_m_init(self, v):
        if v.dim() == 2:
            if v.shape[1] == 1:
                v = v.squeeze(1)
            else:
                raise ValueError("expected a vector or column vector")
        elif v.dim() > 2:
            raise ValueError("expected a vector or column vector")
        self.packed_params["m"].custom_initialization(v)

    def update(self, m, S):
        """

        :param m: unpacked and valid mean vector
        :param S: unpacked and valid cov matrix
        :return: nothing, updates the packed state to now hold the passed in mean/cov
        """
        with torch.no_grad():
            L = S.cholesky()
            n = L.shape[0]
            self.packed_params["L"].resize(n)
            self.packed_params["m"].resize(n)
            m_packed, L_packed = self.pack([m, L])
            self.packed_params["m"].storage[: m_packed.shape[0]] = m_packed.data
            self.packed_params["L"].packed[: L_packed.shape[0]] = L_packed.data

    def pad(self, xi_packed):
        """ For the niche use-case that we need to provide a packed version of xi, but
            padded out to the size of the underlying storage. Such use case arises in
            the natural gradient optimizer. We don't actually resize the storage of the
            xi parameters, just mask it, but the optimizer is oblivious to this and expects
            a gradient of full size.
        """
        m_packed, L_packed = xi_packed
        Lallocated = torch.zeros_like(self.packed_params["L"].packed)
        Lallocated[: L_packed.shape[0]] = L_packed
        mallocated = torch.zeros_like(self.packed_params["m"].storage)
        mallocated[: m_packed.shape[0]] = m_packed
        return (mallocated, Lallocated)


class MVNGaussianNatural(torch.nn.Module):
    def __init__(self, N, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        self.packed_params = torch.nn.ModuleDict(
            OrderedDict(
                t=WrappedVector(N, dtype=self.dtype, init=WrappedVector.zero_init),
                T=UnconstrainedSymmetricMatrix(N, dtype=self.dtype),
            )
        )

    def xi(self):
        t_packed = self.packed_params["t"].storage
        T_packed = self.packed_params["T"].packed
        return [t_packed, T_packed]

    def unpack(self, xi_packed=None):
        if xi_packed is None:
            t_packed = self.packed_params["t"].storage
            T_packed = self.packed_params["T"].packed
        else:
            t_packed, T_packed = xi_packed
        t_unpacked = t_packed.unsqueeze(1)
        T_unpacked = self.packed_params["T"].unpack(packed=T_packed)
        return [t_unpacked, T_unpacked]

    def pack(self, xi):
        t_unpacked, T_unpacked = xi
        t_packed = t_unpacked.squeeze(1)
        T_packed = self.packed_params["T"].pack(T_unpacked)
        return [t_packed, T_packed]

    def custom_T_init(self, A):
        self.packed_params["T"].custom_initialization(A)

    def custom_t_init(self, v):
        if v.dim() == 2:
            if v.shape[1] == 1:
                v = v.squeeze(1)
            else:
                raise ValueError("expected a vector or column vector")
        elif v.dim() > 2:
            raise ValueError("expected a vector or column vector")
        self.packed_params["t"].custom_initialization(v)


# class BatchedMVNGaussianSqrtVar(torch.nn.Module):
#     def __init__(self, batch, N, constrained=True, dtype=torch.get_default_dtype()):
#         super().__init__()
#         self.dtype = dtype
#         if constrained:
#             L = ConstrainedBatchedLowerTriangular(batch, N)
#         else:
#             L = ConstrainedBatchedLowerTriangular(batch, N, transform=None)
#         self.packed_params = torch.nn.ModuleDict(
#             OrderedDict(
#                 m=BatchedVector(batch=batch, N=N, dtype=self.dtype, init=BatchedVector.zero_init),
#                 L=L,
#             )
#         )
#
#     def xi(self):
#         m_packed = self.packed_params["m"].storage
#         L_packed = self.packed_params["L"].packed
#         return [m_packed, L_packed]
#
#     def unpack(self, xi_packed=None):
#         if xi_packed is None:
#             xi_packed = self.xi()
#         m_packed, L_packed = xi_packed
#         m_unpacked = self.packed_params["m"].unpack(packed=m_packed)
#         L_unpacked = self.packed_params["L"].unpack(packed=L_packed)
#         return [m_unpacked, L_unpacked]
#
#     def pack(self, xi):
#         m_unpacked, L_unpacked = xi
#         m_packed = self.packed_params["m"].pack(m_unpacked)
#         L_packed = self.packed_params["L"].pack(L_unpacked)
#         return [m_packed, L_packed]
#
#     def custom_L_init(self, lower):
#         self.packed_params["L"].custom_initialization(lower)
#
#     def custom_m_init(self, v):
#         if v.dim() == 3:
#             if not v.shape[-1] == 1:
#                 raise ValueError("expected a batched column vector or matrix")
#         elif v.dim() == 2:
#             v = v.unsqueeze(-1)
#         else:
#             raise ValueError("expected a batched column vector or matrix")
#         self.packed_params["m"].custom_initialization(v)
#
#     def update(self, m, S):
#         """
#
#         :param m: unpacked and valid batched mean vector
#         :param S: unpacked and valid (full rank) batched cov matrix
#         :return: nothing, updates the packed state to now hold the passed in mean/cov
#
#         note that if diagonal constraint is None, this update will still give a purely
#         positive diagonal, but this would still be a "valid" update, depending on how
#         it is used...
#         """
#         with torch.no_grad():
#             L = S.cholesky()
#             m_packed, L_packed = self.pack([m, L])
#             self.packed_params["m"].storage.copy_(m_packed.data)
#             self.packed_params["L"].packed.copy_(L_packed.data)
