import torch
from matchpy import Operation, Arity, cached_property, Symbol
from numbers import Number
from ast import parse
from ..symbolic.evalmixin import EvaluateMixin
from ..functional import prod, uniq, general_reciprocal
from ..structure import kron
from itertools import zip_longest
import itertools
from functools import reduce
from operator import matmul

__all__ = [
    "Select",
    "IndexSelect",
    "index_select",
    "Reshape",
    "PermuteDims",
    "reshape",
    "Slice",
    "Replace",
    "Transpose",
    "TransposeInverse",
    "Dot",
    "Hadamard",
    "Plus",
    "Negate",
    "Trace",
    "Diagonal",
    "ExtractDiagonal",
    "Inverse",
    "Reciprocal",
    "KroneckerProduct",
    "RootFactorize",
    "LogDet",
    "Rank",
    "Shape",
    "Sum",
    "HoldForm",
    "Apply",
    "Label",
    "Divide",
    "EvaluateKernel",
    "TensorOperation",
    "Mean",
    "TupleExpr",
    "Insert",
    "Remove",

]

symbols = [
    "Lambda",
    "String",
    "Constant",
    "Scalar",
    "Vector",
    "Matrix",
    "Tensor",
    "Kernel",
]

__all__ += symbols


class AstMixin:
    def to_ast(self, label, source=False):
        raise NotImplementedError(
            "implement to_ast for type {type}".format(type=type(self))
        )


class NumericMixin:
    def __pos__(self):
        return self

    def __neg__(self):
        return Negate(self)

    def __invert__(self):
        return Inverse(self)

    def __add__(self, other):
        return Plus(self, other)

    def __radd__(self, other):
        return Plus(other, self)

    def __sub__(self, other):
        return Plus(self, -other)

    def __rsub__(self, other):
        return Plus(other, -self)

    def __matmul__(self, other):
        return Dot(self, other)

    def __rmatmul__(self, other):
        return Dot(other, self)

    def __mul__(self, other):
        return Hadamard(self, other)

    def __rmul__(self, other):
        return Hadamard(other, self)


class Lambda(AstMixin, Symbol):
    default_function = lambda obj, *args: print(
        obj.name + " did not receive an implementation"
    )
    default_shape_function = lambda obj, *args: print(
        obj.name + " did not receive a shape implementation"
    )

    def __init__(self, name, function=None, shape_function=None):
        super().__init__(name)
        self.function = self.default_function if not function else function
        self.shape_function = (
            self.default_shape_function if not shape_function else shape_function
        )

    @property
    def evaluated(self):
        return self.function

    def to_ast(self, label, source=False):
        # have to pass the lambda in as a parameter
        src = "{label} = {self_name}".format(label=label, self_name=self.name)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class String(AstMixin, Symbol):
    def __init__(self, str):
        super().__init__(str)

    def to_ast(self, label, source=False):
        return None


class Tensor(AstMixin, NumericMixin, Symbol):
    def __init__(self, name, data=None, variable_name=None):
        super().__init__(name, variable_name=variable_name)
        self.data = data

    @property
    def evaluated(self):
        if self.data is None:
            raise ValueError("cannot evaluate fully symbolic type {}".format(self.name))
        return self.data

    def __repr__(self):
        return "{type}('{name}', data={data})".format(
            type=type(self).__name__, name=self.name, data=self.data
        )

    def __hash__(self):
        # ignore torch data when computing hash
        return super().__hash__()

    def to_ast(self, label, source=False):
        # construct a rename operation
        src = "{label} = {self_name}".format(label=label, self_name=self.name)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class Constant(Tensor):
    def __init__(self, data, variable_name=None):
        super().__init__(name=str(data), variable_name=variable_name)
        self.data = data

    @property
    def evaluated(self):
        return self.data

    def to_ast(self, label, source=False):
        # construct a rename operation
        src = "{label} = {data}".format(label=label, data=self.data)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class Scalar(Tensor):
    pass


class Vector(Tensor):
    pass


class Matrix(Tensor):
    pass


class Kernel(AstMixin, Symbol):
    default_function = lambda obj, *args: print(
        obj.name + " did not receive an implementation"
    )

    def __init__(self, name, data=None, variable_name=None):
        self.data = self.default_function if not data else data

        super().__init__(name, variable_name=variable_name)

    @property
    def evaluated(self):
        return self.data

    def to_ast(self, label, source=False):
        # construct a rename operation
        src = "{label} = {self_name}".format(label=label, self_name=self.name)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

    def __repr__(self):
        return "{type}('{name}')".format(type=type(self).__name__, name=self.name)


class TensorOperation(NumericMixin, EvaluateMixin, Operation):
    @property
    def shape(self):
        raise NotImplementedError(
            "implement shape operation for type {type}".format(type=type(self))
        )

    def dim(self):
        raise NotImplementedError(
            "implement dim operation for type {type}".format(type=type(self))
        )

    @property
    def dtype(self):
        return self.operands[0].dtype

    def to_ast(self, label, *arglabels, source=False):
        """convert the op to an AST representation

        Args:
            label: the resulting name to assign to
            *arglabels: the names assigned to the input nodes
        Keyword Args:
            source: whether to return the source for the ast at the same time

        Returns:
            an ast representation of the op and optionally the source that generated it if the source flag is set.

        """
        raise NotImplementedError(
            "implement to_ast conversion for type {type}".format(
                type=type(self).__name__
            )
        )


class Select(TensorOperation):
    name = "Select"
    associative = False
    commutative = False
    arity = Arity(2, False)

    @property
    def shape(self):
        return (len(self.operands[1:]),)

    @property
    def evaluated(self):
        idx, rest = self.operands[0], self.operands[1:]
        if len(rest) > 1:
            # select the tuple
            return self.operands[idx.evaluated]
        else:
            # collapse tuple and select on object
            return rest[0].evaluated[idx.evaluated]

    def to_ast(self, label, *arglabels, source=False):
        idx, rest = self.operands[0].data, arglabels[1:]

        if len(rest) > 1:
            # select the appropriate node label
            src = "{label} = {selected}".format(label=label, selected=rest[idx])
        else:
            src = "{label} = {op}[{idx}]".format(label=label, op=rest[0], idx=idx)

        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class IndexSelect(TensorOperation):
    name = "IndexSelect"
    associative = False
    commutative = False
    arity = Arity(3, True)

    @property
    def evaluated(self):
        op, dim, indices = self.operands

        # collapse tuple and select on object
        return torch.index_select(op.evaluated, dim.evaluated, torch.tensor(indices.evaluated, device=op.evaluated.device, dtype=torch.int64))

    def to_ast(self, label, *arglabels, source=False):
        op, dim, indices = arglabels

        src = "{label} = torch.index_select({op}, {dim}, torch.tensor({indices}, dtype=torch.int64, device={op}.device))".format(
            label=label, op=op, dim=dim, indices=indices)

        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


def index_select(op, dim, indices):
    return IndexSelect(op, Constant(dim), Constant(indices))


class Slice(TensorOperation):
    name = "Slice"
    arity = Arity(2, False)

    @property
    def evaluated(self):
        index = [x for x in (self.operands[1:] + 2 * [None])]
        s = slice(index[0], index[1], index[2])
        return self.operands[0].evaluated[s]

    def to_ast(self, label, *arglabels, source=False):
        index = [x for x in (self.operands[1:].data + 2 * [None])]
        s = "slice({},{},{})".format(index[0], index[1], index[2])
        op = arglabels[0]
        src = "{label} = {op}[{s}]".format(label=label, op=op, s=s)

        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Replace(TensorOperation):
    name = "Replace"
    arity = Arity(3, True)
    # args: op, index to replace, val to insert

    @property
    def evaluated(self):
        op, idx, val = self.operands[0], self.operands[1], self.operands[2]
        return tuple([val if i == idx else v for i, v in enumerate(op)])

    def to_ast(self, label, *arglabels, source=False):
        idx, val = self.operands[1].data, self.operands[2].data
        op = arglabels[0]
        src = "{label} = tuple([{val} if i == {idx} else v for i, v in enumerate({op})])".format(
            label=label, op=op, idx=idx, val=val
        )

        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Insert(TensorOperation):
    name = "Insert"
    arity = Arity(3, True)
    # args: op, index to insert, val to insert

    @property
    def evaluated(self):
        op, idx, val = self.operands[0], self.operands[1], self.operands[2]
        return tuple(
            itertools.chain.from_iterable(
                [[val, o] if i == idx else [o] for i, o in enumerate(op)]
            )
        )

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        idx = arglabels[1]
        val = arglabels[2]

        src = f"{label} = tuple(itertools.chain.from_iterable([[{val},o] if i=={idx} else [o] for i,o in enumerate({op})]))"

        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class Remove(TensorOperation):
    name = "Remove"
    arity = Arity(2, True)
    # args: op, index to delete

    @property
    def evaluated(self):
        op, idx = self.operands[0], self.operands[1]
        return tuple(
            itertools.chain.from_iterable(
                [[] if i == idx else [o] for i, o in enumerate(op)]
            )
        )

    def to_ast(self, label, *arglabels, source=False):
        idx = self.operands[1].data
        op = arglabels[0]
        src = "{label} = tuple(itertools.chain.from_iterable([[] if i=={idx} else [o] for i,o in enumerate({op})]))".format(
            label=label, op=op, idx=idx
        )


        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Shape(TensorOperation):
    name = "Shape"
    arity = Arity.unary

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = {op_name}.shape".format(label=label, op_name=op_name)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

    @property
    def evaluated(self):
        return self.operands[0].evaluated.shape


class TupleExpr(TensorOperation):
    name = "TupleExpr"
    arity = Arity(2, False)

    @property
    def evaluated(self):
        return tuple([op for op in self.operands])

    def to_ast(self, label, *arglabels, source=False):
        op_list = ", ".join(arglabels)
        src = f"{label} = tuple([{op_list}])"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class Reshape(TensorOperation):
    name = "Reshape"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        op, shape = (x.evaluated for x in self.operands)
        return op.reshape(shape)

    def to_ast(self, label, *arglabels, source=False):
        op, shape = arglabels
        src = f"{label} = torch.reshape({op}, {shape})"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class PermuteDims(TensorOperation):
    name = "PermuteDims"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        op, dims = (x.evaluated for x in self.operands)
        return op.permute(dims)

    def to_ast(self, label, *arglabels, source=False):
        op, dims = arglabels
        src = f"{label} = torch.reshape({op}, {dims})"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

def reshape(expr, shape):
    shape = TupleExpr(*shape)
    return Reshape(expr, shape)

class Transpose(TensorOperation):
    name = "ᵀ"
    arity = Arity.unary

    def __str__(self):
        return "({})ᵀ".format(self.operands[0])

    @property
    def shape(self):
        op = self.operands[0]
        shape = Shape(op)
        return Select(Constant(1), shape), Select(Constant(0), shape)

    @classmethod
    def _evaluate(cls, left, right=None):
        return torch.transpose(left.evaluated, -1, -2)

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        # src = f"{label} = torch.transpose({op_name}, 1, 0)"
        src = f"{label} = torch.transpose({op_name}, -1, -2)" #permits transpose to work on batched tensors (batch in dim = 0).
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class TransposeInverse(TensorOperation):
    name = "⁻ᵀ"
    arity = Arity.unary

    def __str__(self):
        return "({})⁻ᵀ".format(self.operands[0])

    @property
    def shape(self):
        shape = Shape(self.operands[0])
        return Select(Constant(1), shape), Select(Constant(0), shape)


class Dot(TensorOperation):
    name = "·"
    arity = Arity.variadic
    associative = True
    one_identity = True
    infix = True

    @property
    def shape(self):
        return (
            Select(Constant(0), Shape(self.operands[0])),
            Select(Constant(1), Shape(self.operands[-1])),
        )

    @property
    def evaluated(self):
        return reduce(matmul, *(x.evaluated for x in self.operands))
        #unfortunately in order to allow for batch operations, we cannot use chain_matmul
        # return torch.chain_matmul(*(x.evaluated for x in self.operands))

    def to_ast(self, label, *arglabels, source=False):
        #we must also disable use of chain_matmul here as well
        # arglabels = ", ".join(arglabels)
        # src = "{label} = torch.chain_matmul({arglabels})".format(
        #     label=label, arglabels=arglabels
        # )
        cmd = '(' + ', '.join(arglabels) + ')'
        src = f"{label} = reduce(matmul, {cmd})"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class _Max(TensorOperation):
    name = "Max"
    arity = Arity.variadic
    one_identity = True
    commutative = True


class Hadamard(TensorOperation):
    name = "￮"
    arity = Arity.variadic
    associative = False
    one_identity = True
    commutative = False
    infix = True

    @property
    def shape(self):
        # assume no broadcasting happens; use hack that assumes that scalars will be put in front
        # by canonical ordering, so we use the shape of the last operand
        return Shape(self.operands[-1])

    @classmethod
    def _evaluate(cls, left, right):
        return left.evaluated * right.evaluated

    def to_ast(self, label, *arglabels, source=False):
        arglabels = ", ".join(arglabels)
        src = "{label} = reduce(mul, [{arglabels}])".format(
            label=label, arglabels=arglabels
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Plus(TensorOperation):
    name = "+"
    arity = Arity.variadic
    associative = True
    commutative = True
    one_identity = True
    infix = True

    @property
    def shape(self):
        # assume no broadcasting happens; use hack that assumes that scalars will be put in front
        return Shape(self.operands[-1])

    @classmethod
    def _evaluate(cls, left, right):
        return left.evaluated + right.evaluated

    def to_ast(self, label, *arglabels, source=False):
        arglabels = ", ".join(arglabels)
        src = "{label} = reduce(add, [{arglabels}])".format(
            label=label, arglabels=arglabels
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Trace(TensorOperation):
    name = "Trace"
    arity = Arity.unary

    def dim(self):
        return self.operands[0].dim() - 1

    @property
    def shape(self):
        return (Constant(1),)

    @classmethod
    def _evaluate(cls, left, right):
        return torch.trace(left.evaluated)

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = torch.trace({op_name})".format(label=label, op_name=op_name)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Diagonal(TensorOperation):
    name = "diag"
    arity = Arity.unary

    @property
    def shape(self):
        shape0 = Select(Constant(0), Shape(self.operands[0]))
        return shape0, shape0

    @classmethod
    def _evaluate(cls, left, right=None):
        assert left.shape[-1] == 1, "doesn't make sense for matrices"
        return torch.diagflat(left.evaluated)

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = torch.diagflat({op_name})".format(label=label, op_name=op_name)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class ExtractDiagonal(TensorOperation):
    name = "idiag"
    arity = Arity.unary

    @property
    def shape(self):
        return Select(Constant(0), self.operands[0]), Constant(1)

    @classmethod
    def _evaluate(cls, left, right=None):
        #the following impl allows for batch dims
        return torch.diagonal(left.evaluated, dim1=-1, dim2=-2).unsqueeze(-1)

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = torch.diagonal({op_name}, dim1=-1, dim2=-2).unsqueeze(-1)".format(
            label=label, op_name=op_name
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Inverse(TensorOperation):
    name = "⁻¹"
    arity = Arity.unary

    def __str__(self):
        return "({})⁻¹".format(self.operands[0])

    @classmethod
    def _evaluate(cls, op, _unused=None):
        return torch.inverse(op.evaluated)

    @property
    def shape(self):
        return Shape(self.operands[0])


class Reciprocal(TensorOperation):
    name = "⁻¹"
    arity = Arity.unary

    def __str__(self):
        return "({})⁻¹".format(self.operands[0])

    @classmethod
    def _evaluate(cls, op, _unused=None):
        return general_reciprocal(op.evaluated)

    @property
    def shape(self):
        op = self.operands[0]
        return Shape(op)

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = general_reciprocal({op_name})".format(label=label, op_name=op_name)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class KroneckerProduct(TensorOperation):
    name = "⊗"
    arity = Arity.variadic
    associative = True
    one_identity = True
    infix = True

    @classmethod
    def _evaluate(cls, left, right):
        return kron(left.evaluated, right.evaluated, batched=False)

    @property
    def shape(self):
        SelectShape0 = lambda op: Select(Constant(0), Shape(op))
        SelectShape1 = lambda op: Select(Constant(1), Shape(op))

        rowsizes = [SelectShape0(op) for op in self.operands]
        colsizes = [SelectShape1(op) for op in self.operands]

        return prod(rowsizes), prod(colsizes)

    def to_ast(self, label, *arglabels, source=False):
        left = arglabels[0]
        right = arglabels[1]
        src = "{label} = kron({left}, {right}, batched=False)".format(
            label=label, left=left, right=right
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Negate(TensorOperation):
    name = "-"
    arity = Arity.unary

    @classmethod
    def _evaluate(cls, left, right=None):
        return -left.evaluated

    @property
    def shape(self):
        return Shape(self.operands[0])

    def __str__(self):
        return "(-{})".format(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = -{op_name}".format(label=label, op_name=op_name)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class RootFactorize(TensorOperation):
    name = "RootFactorize"
    arity = Arity.unary

    @property
    def shape(self):
        return self.operands[0].shape

    def dim(self):
        return self.operands[0].dim()


class LogDet(TensorOperation):
    name = "LogDet"
    arity = Arity.unary

    @property
    def shape(self):
        return (Constant(1),)


class Rank(TensorOperation):
    name = "Rank"
    arity = Arity.unary

    @property
    def shape(self):
        return Constant(1)

    def _evaluate(cls, left, right=None):
        t = left.evaluated
        assert t.dim() == 2, "must be matrix"
        assert t.shape[0] == t.shape[1], "must be square"
        return t.shape[0]


class Apply(TensorOperation):
    name = "Apply"
    arity = Arity(2, False)

    @cached_property
    def shape(self):
        fun = self.operands[0]
        opshapes = [Shape(op) for op in self.operands[1:]]
        shapefun = Lambda(fun.name + "_shape", function=fun.shape_function)
        # print('Apply shape: ', fun._shape_map_fun(*opshapes))
        return Apply(shapefun, *opshapes)

    @property
    def evaluated(self):
        fun = self.operands[0]
        if type(fun).__name__ != "Lambda":
            raise RuntimeError(
                "wrap bare lambdas in the Lambda symbol"
                + "\nGot {}".format(type(fun).__name__)
            )
        return fun.evaluated(*[o.evaluated for o in self.operands[1:]])

    def to_ast(self, label, *arglabels, source=False):
        callable = arglabels[0]
        arguments = ",".join(arglabels[1:])
        src = "{label} = {callable}({arguments})".format(
            label=label, callable=callable, arguments=arguments
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Label(TensorOperation):
    name = "Label"
    arity = Arity.binary

    @property
    def evaluated(self):
        # a no-op just for tagging expressions
        return self.operands[1].evaluated

    def to_ast(self, label, *arglabels, source=False):
        # convert to return statement
        if source:
            return None, None

        return None


class Sum(TensorOperation):
    name = "Sum"
    arity = Arity.unary

    @property
    def shape(self):
        return tuple()

    @classmethod
    def _evaluate(cls, left, right=None):
        return (left.evaluated).sum()

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = {op_name}.sum()".format(label=label, op_name=op_name)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Mean(TensorOperation):
    name = "Mean"
    arity = Arity.unary

    @property
    def shape(self):
        return tuple()

    @classmethod
    def _evaluate(cls, left, right=None):
        return (left.evaluated).mean()

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = {op_name}.mean()".format(label=label, op_name=op_name)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Divide(TensorOperation):
    name = "/"
    arity = Arity.binary
    infix = True

    @property
    def shape(self):
        return (Constant(1),)

    @classmethod
    def _evaluate(cls, left, right=None):
        op1, op2 = left.evaluated, right.evaluated
        assert all(
            [isinstance(op1, Number), isinstance(op2, Number)]
        ), "can only divide two numbers"
        return op1 / op2

    def to_ast(self, label, *arglabels, source=False):
        left = arglabels[0]
        right = arglabels[1]
        src = "{label} = {left}/{right}".format(label=label, left=left, right=right)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class HoldForm(TensorOperation):
    name = "HoldForm"
    arity = Arity.unary

    @property
    def shape(self):
        return Shape(self.operands[0])

    @classmethod
    def _evaluate(cls, left, right=None):
        return left.evaluated

    def __str__(self):
        return "{" + str(self.operands[0]) + "}"

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        src = "{label} = {op}".format(label=label, op=op)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class EvaluateKernel(TensorOperation):
    name = "EvaluateKernel"
    arity = Arity(3, True)

    def __str__(self):
        return "{kernel}[{opstr}]".format(
            kernel=self.operands[0], opstr=", ".join(map(str, self.operands[1:]))
        )

    @property
    def shape(self):
        Shape0 = lambda op: Select(Constant(0), Shape(op))
        return Shape0(self.operands[1]), Shape0(self.operands[2])

    @property
    def evaluated(self):
        if not self._dirty:
            return self._cache

        kernel = self.operands[0]
        assert type(kernel).__name__ == "Kernel", "can only evaluate kernel symbol"
        axis1, axis2 = [a.evaluated for a in self.operands[1:]]

        self._cache = kernel.data(axis1, axis2)
        self._dirty = False
        return self._cache

    def to_ast(self, label, *arglabels, source=False):
        kfun = arglabels[0]
        laxis = arglabels[1]
        raxis = arglabels[2]

        src = "{label} = {kfun}({laxis}, {raxis})".format(
            label=label, kfun=kfun, laxis=laxis, raxis=raxis
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result
