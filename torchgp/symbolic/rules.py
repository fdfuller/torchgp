from collections.abc import Iterable
from matchpy import (
    CustomConstraint,
    Wildcard,
    ReplacementRule,
    Pattern,
    replace_all,
    is_match,
    ManyToOneMatcher,
    Operation,
    Arity,
)
from itertools import zip_longest
from . import *

__all__ = [
    "kronecker_simplify_rules",
    "misc_simplify_rules",
    "all_simplify_rules",
    "enable_cholesky_factorization_rules",
    "enable_aat_rules",
    "replace_all",
    "replace_rank",
]


def _root2cholesky(cdot):
    L = CholeskyRoot(cdot)
    return L @ Transpose(L)


id_registry = {}


def make_unique_id(expr):
    nextid = len(id_registry)
    id = id_registry.get(str(expr))
    if id is None:
        id = "id#{}".format(nextid)
        id_registry[str(expr)] = id

    return id


x_ = Wildcard.dot("x")
x2_ = Wildcard.dot("x2")
seq_ = Wildcard.plus("seq")
seq2_ = Wildcard.plus("seq2")
anyseq_ = Wildcard.star("anyseq")
anyseq2_ = Wildcard.star("anyseq2")

y_vec = Wildcard.symbol("y", Vector)

is_torchoperation = CustomConstraint(lambda x: isinstance(x, TensorOperation))
is_kron_prod = CustomConstraint(lambda x: x.head == KroneckerProduct)
is_plus = CustomConstraint(lambda x: x.head == Plus)
is_negate = CustomConstraint(lambda x: x.head == Negate)
is_dot = CustomConstraint(lambda x: x.head == Dot)
is_inverse = CustomConstraint(lambda x: x.head == Inverse)
is_transpose = CustomConstraint(lambda x: x.head == Transpose)
is_onedimensional = CustomConstraint(
    lambda x: type(x).__name__ in ["Constant", "Scalar"]
)
is_cholesky_lambda = lambda x: any(
    [x.head == CholeskyRoot, type(x).__name__ == "LowerCholeskyMatrix"]
)
is_diagonal_lambda = lambda x: any(
    [x.head == Diagonal, type(x).__name__ == "DiagonalMatrix"]
)

is_cholesky = CustomConstraint(is_cholesky_lambda)
is_diagonal = CustomConstraint(is_diagonal_lambda)


def is_triangular_lambda(x):
    # check if cholesky roots, or cholesky matrix
    cholesky = is_cholesky_lambda(x)

    # check if diagonal
    diagonal = is_diagonal_lambda(x)

    return any([cholesky, diagonal])


is_triangular = CustomConstraint(is_triangular_lambda)


def log_det_kron_lambda(x):
    args = []
    allrank = Hadamard(*[Rank(op) for op in x])
    for op in x:
        term = Divide(allrank, Rank(op)) * LogDet(op)
        args.append(term)
    return Plus(*args)


def kron_kron_constraint_lambda(x, x2, anyseq, anyseq2):
    # check that seq and seq2 are Kronecker Products
    if not all([x.head == KroneckerProduct, x2.head == KroneckerProduct]):
        return False
    # print('x: ', x)
    # print('x2: ', x2)

    if len(x.operands) != len(x2.operands):
        return False

    # check that column space of seq and rowspace of seq2 are compatible
    # for left, right in zip(x, x2):
    # print('left = {}, right = {}'.format(left, right))
    # print('left shape = ', left.shape)
    # print('right shape = ', right.shape)
    #    if len(left.operands) != len(right.operands):
    # print('Mismatch at {left} vs {right}'.format(left=left, right=right))
    #        return False

    return True


make_transpose_pattern = lambda x: Pattern(Transpose(x))

# utility op to compare a collection against several patterns
Collect = Operation.new("Collect", Arity.polyadic)


def dot_aat_lambda(x):
    # checks that x is Dot with an even number of elements, that are transposes of each other
    if x.head != Dot:
        return False

    seq = x.operands

    if len(seq) % 2 != 0:
        return False  # fast exit for odd lengths

    # move through the sequence from both ends, comparing
    for a, b in zip(
        seq, reversed(seq)
    ):  # technically only need to go through half of the sequence
        # fast exit for shape mismatch
        # if a.shape != b.shape[::-1]:
        # print(f"shape mismatch for a={a} and b={b}, {a.shape}, {b.shape[::-1]}, {a.shape == b.shape[::-1]}")
        #    return False

        matcher = ManyToOneMatcher(
            Pattern(Collect(Transpose(x_), x2_)),
            Pattern(Collect(x_, Transpose(x2_))),
            Pattern(Collect(TransposeInverse(x_), Inverse(x2_))),
            Pattern(Collect(Inverse(x_), TransposeInverse(x2_))),
        )

        _matched = False
        for m, s in matcher.match(Collect(a, b)):
            # print('Found match', m, s)
            _matched = True
            break

        if not _matched:
            # print('Mismatch operands {}, {}'.format(a, b))
            return False
    return True


is_dot_aat = CustomConstraint(dot_aat_lambda)


def trace_aat_replacement_lambda(x):
    # get the sequence of Dot
    seq = x.operands

    # select half of it
    seq = seq[: len(seq) // 2]
    return TraceAAt(Dot(*seq))


def diag_aat_replacement_lambda(x):
    # get the sequence of Dot
    seq = x.operands

    # select half of it
    seq = seq[: len(seq) // 2]
    return DiagAAt(*seq)


def contains_kron_prods_lambda(x):
    # checks that the first term of this variadic term contains kronecker products
    return all([op.head == KroneckerProduct for op in x.operands])


is_kron_kron = CustomConstraint(kron_kron_constraint_lambda)


def kron_kron_replacement_lambda(kron1, kron2):
    args = []

    for left, right in zip(kron1, kron2):
        term = left @ right
        args.append(term)
    result = KroneckerProduct(*args)
    return result


def kron_cholesky_replacement_lambda(kron1, kron2):
    args = []
    kron1 = kron1 if isinstance(kron1, Iterable) else [kron1]
    kron2 = kron2 if isinstance(kron2, Iterable) else [kron2]

    for left, right in zip(kron1, kron2):
        term = CholeskySolve(left, right)
        args.append(term)
    result = KroneckerProduct(*args)
    return result


kronecker_simplify_rules = [
    # kron log det rule
    ReplacementRule(Pattern(LogDet(x_), is_kron_prod), log_det_kron_lambda),
    # kron transpose rule
    ReplacementRule(
        Pattern(Transpose(x_), is_kron_prod),
        lambda x: KroneckerProduct(*(Transpose(op) for op in x)),
    ),
    # Kron inverse factorization Rule
    ReplacementRule(
        Pattern(Inverse(x_), is_kron_prod),
        lambda x: KroneckerProduct(*[Inverse(op) for op in x]),
    ),
    # Kron transpose inverse factorization Rule
    ReplacementRule(
        Pattern(TransposeInverse(x_), is_kron_prod),
        lambda x: KroneckerProduct(*[TransposeInverse(op) for op in x]),
    ),
    # Kron trace Rule
    ReplacementRule(
        Pattern(Trace(x_), is_kron_prod), lambda x: Hadamard(*[Trace(op) for op in x])
    ),
    # Kron root factorization Rule
    ReplacementRule(
        Pattern(RootFactorize(x_), is_kron_prod),
        lambda x: KroneckerProduct(*[RootFactorize(op) for op in x]),
    ),
    # Diag AAt rule
    ReplacementRule(
        Pattern(DiagAAt(x_), is_kron_prod),
        lambda x: KroneckerProduct(*(DiagAAt(op) for op in x)),
    ),
    # Sum rule
    ReplacementRule(
        Pattern(Sum(x_), is_kron_prod), lambda x: Hadamard(*(Sum(op) for op in x))
    ),
    # Diagonal (makes matrix out of diagonal)
    ReplacementRule(
        Pattern(Diagonal(x_), is_kron_prod),
        lambda x: KroneckerProduct(*(Diagonal(op) for op in x)),
    ),
    # ExtractDiagonal (takes diagonal from matrix)
    ReplacementRule(
        Pattern(ExtractDiagonal(x_), is_kron_prod),
        lambda x: KroneckerProduct(*(ExtractDiagonal(op) for op in x)),
    ),
    # cholesky root kron rule
    ReplacementRule(
        Pattern(CholeskyRoot(x_), is_kron_prod),
        lambda x: KroneckerProduct(*(CholeskyRoot(op) for op in x)),
    ),
    # cholesky solve kron rule
    ReplacementRule(
        Pattern(Dot(anyseq_, CholeskySolve(x_, x2_), anyseq2_), is_kron_kron),
        lambda x, x2, anyseq, anyseq2: Dot(
            *anyseq, kron_cholesky_replacement_lambda(x, x2), *anyseq2
        ),
    ),
    # kron_mvprod dot rules
    ReplacementRule(
        Pattern(Dot(anyseq_, x_, y_vec), is_kron_prod),
        lambda y, x, anyseq: Dot(*anyseq, KronMVProd(*x, y)),
    ),
    ReplacementRule(
        Pattern(Dot(x_, y_vec, anyseq_), is_kron_prod),
        lambda y, x, anyseq: Dot(KronMVProd(*x, y), *anyseq),
    ),
    # kron kron rules
    ReplacementRule(
        Pattern(Dot(anyseq_, x_, x2_, anyseq2_), is_kron_kron),
        lambda x, x2, anyseq, anyseq2: Dot(
            *anyseq, kron_kron_replacement_lambda(x, x2), *anyseq2
        ),
    ),
]

misc_simplify_rules = [
    # Trace distributes over addition
    ReplacementRule(
        Pattern(Trace(x_), is_plus), lambda x: Plus(*[Trace(op) for op in x])
    ),
    # Trace of a transpose is the trace
    ReplacementRule(Pattern(Trace(x_), is_transpose), lambda x: Trace(x.operands[0])),
    # Sum distributes over addition
    ReplacementRule(Pattern(Sum(x_), is_plus), lambda x: Plus(*[Sum(op) for op in x])),
    # Diagonal distributes over addition
    ReplacementRule(
        Pattern(Diagonal(x_), is_plus), lambda x: Plus(*[Diagonal(op) for op in x])
    ),
    # And so does idiag
    ReplacementRule(
        Pattern(ExtractDiagonal(x_), is_plus),
        lambda x: Plus(*[ExtractDiagonal(op) for op in x]),
    ),
    # idiag distributes over negate
    ReplacementRule(
        Pattern(ExtractDiagonal(x_), is_negate),
        lambda x: Negate(*[ExtractDiagonal(op) for op in x]),
    ),
    # Log det transpose rule
    ReplacementRule(Pattern(LogDet(Transpose(x_))), lambda x: LogDet(x)),
    # Diagonal log det rule
    ReplacementRule(
        Pattern(LogDet(x_), is_triangular), lambda x: SumLogDiagonal(ExtractDiagonal(x))
    ),
    # Inverse distributes over Dot
    ReplacementRule(
        Pattern(Inverse(x_), is_dot),
        lambda x: Dot(*[Inverse(op) for op in reversed(x)]),
    ),
    # Transpose distributes over Dot
    ReplacementRule(
        Pattern(Transpose(x_), is_dot),
        lambda x: Dot(*[Transpose(op) for op in reversed(x)]),
    ),
    # Replace transpose inverse with single op
    ReplacementRule(
        Pattern(Transpose(x_), is_inverse), lambda x: TransposeInverse(x.operands[0])
    ),
    ReplacementRule(
        Pattern(Inverse(x_), is_transpose), lambda x: TransposeInverse(x.operands[0])
    ),
    # Remove dual transposes and inverses
    ReplacementRule(Pattern(Transpose(x_), is_transpose), lambda x: x.operands[0]),
    ReplacementRule(Pattern(Inverse(x_), is_inverse), lambda x: x.operands[0]),
    # Partition transposeinverse
    ReplacementRule(
        Pattern(TransposeInverse(x_), is_inverse), lambda x: Transpose(x.operands[0])
    ),
    ReplacementRule(
        Pattern(TransposeInverse(x_), is_transpose), lambda x: Inverse(x.operands[0])
    ),
    # Replace rank with size
    ReplacementRule(Pattern(Rank(x_)), lambda x: Select(Constant(0), Shape(x))),
    # Replace Plus(Diagonal, *mats) with AddDiagonal(Plus(*mats), vector)
    ReplacementRule(
        Pattern(Plus(x_, seq_), is_diagonal),
        lambda x, seq: AddDiagonal(Plus(*seq), ExtractDiagonal(x)),
    ),
    # Replace ExtractDiagonal(Diagonal(v)) -> v
    ReplacementRule(
        Pattern(ExtractDiagonal(x_), CustomConstraint(lambda x: x.head == Diagonal)),
        lambda x: x.operands[0],
    ),
    # Replace ExtractDiagonal of EvaluateKernelSymmetric -> EvaluateKernelDiagonal
    ReplacementRule(
        Pattern(
            ExtractDiagonal(x_),
            CustomConstraint(lambda x: x.head == EvaluateKernelSymmetric),
        ),
        lambda x: EvaluateKernelDiagonal(x.operands[0], x.operands[1]),
    ),
    # Replace Symmetric EvaluateKernel -> EvaluateKernelSymmetric
    ReplacementRule(
        Pattern(EvaluateKernel(x2_, x_, x_)),
        lambda x, x2: EvaluateKernelSymmetric(x2, x),
    ),
    # Extract Diagonal commutes with Hadamard
    ReplacementRule(
        Pattern(ExtractDiagonal(x_), CustomConstraint(lambda x: x.head == Hadamard)),
        lambda x: Hadamard(*[ExtractDiagonal(op) for op in x.operands]),
    ),
    # ExtractDiagonal of scalar or constant is no-op
    ReplacementRule(Pattern(ExtractDiagonal(x_), is_onedimensional), lambda x: x),
    # Remove labels to enable simplification
    ReplacementRule(Pattern(Label(x2_, x_)), lambda x, x2: x),
]

enable_cholesky_factorization_rules = [
    # replace root factorize tag with Cholesky
    ReplacementRule(Pattern(RootFactorize(x_)), lambda x: _root2cholesky(x)),
    # Replace inverses of cholesky roots with cholesky solves
    ReplacementRule(
        Pattern(
            Dot(anyseq_, Inverse(x_), x2_, anyseq2_),
            CustomConstraint(lambda x: is_cholesky_lambda(x)),
        ),
        lambda anyseq, x, x2, anyseq2: Dot(*anyseq, CholeskySolve(x, x2), *anyseq2),
    ),
    ReplacementRule(
        Pattern(
            Dot(anyseq_, x2_, TransposeInverse(x_), anyseq2_),
            CustomConstraint(lambda x: is_cholesky_lambda(x)),
        ),
        lambda anyseq, x, x2, anyseq2: Dot(
            *anyseq, Transpose(CholeskySolveT(x, x2)), *anyseq2
        ),
    ),
    # Replace CholeskySolveT transposed with choleskysolve t
    ReplacementRule(
        Pattern(
            Dot(anyseq_, Transpose(CholeskySolveT(x_, x2_)), anyseq2_),
            CustomConstraint(lambda x: is_cholesky_lambda(x)),
        ),
        lambda anyseq, x, x2, anyseq2: Dot(
            *anyseq, Transpose(CholeskySolve(x, Transpose(x2))), *anyseq2
        ),
    ),
    # Special rule for Trace(L^-T A)
    ReplacementRule(
        Pattern(
            Trace(Dot(TransposeInverse(x_), x2_)),
            CustomConstraint(lambda x: is_cholesky_lambda(x)),
        ),
        lambda x, x2: Trace(Dot(x2, TransposeInverse(x))),
    ),
]

enable_aat_rules = [
    # Diag AAt rule
    ReplacementRule(
        Pattern(ExtractDiagonal(x_), is_dot_aat),
        lambda x: diag_aat_replacement_lambda(x),
    ),
    # Log det AAt rule
    ReplacementRule(
        Pattern(LogDet(x_ @ Transpose(x_))), lambda x: Constant(2) * LogDet(x)
    ),
    # Trace AAt rule
    ReplacementRule(
        Pattern(Trace(x_), is_dot_aat), lambda x: trace_aat_replacement_lambda(x)
    ),
]

shape_rules = [
    # symbolic shape processing
    ReplacementRule(Pattern(Shape(x_), is_torchoperation), lambda x: x.shape),
    # nested selects
    ReplacementRule(
        Pattern(Select(x_, seq_), CustomConstraint(lambda x, seq: len(seq) > 1)),
        lambda x, seq: seq[x.evaluated],
    ),
]

replace_rank = [
    ReplacementRule(Pattern(Rank(x_)), lambda x: Select(Constant(0), Shape(x)))
]
all_simplify_rules = kronecker_simplify_rules + misc_simplify_rules + shape_rules
