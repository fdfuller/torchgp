from ast import parse
from typing import Union
from matchpy import Arity
from ..symbolic.core import (
    TensorOperation,
    Shape,
    Select,
    Constant,
    Replace,
    Matrix,
    Vector,
    Slice,
    Remove
)
from ..functional import (
    potrf,
    trtrs,
    add_diagonal,
    prod,
    inv_softplus,
    reduce,
    mul,
    # trifact,
    # trisolve,
    # triunpack,
    add_jitter,
    add_jitter_v2,
    pivoted_cholesky_solve,
    qr_plus,
    batched_vec_identity,
    auto_nugget,
    eye_like,
    eye_like_square
)
from torch.nn.functional import softplus
from ..structure import kron_mvprod, ckr, rkr, rkr_mv_2d, rkr_mv, fast_2D_ckr_mm
import torch
import math
from operator import add

__all__ = [
    "CholeskyRoot",
    "CholeskySolve",
    "CholeskySolveT",
    "SumLogDiagonal",
    "Log",
    "Abs",
    "Exp",
    "Pow",
    "Softplus",
    "InvSoftplus",
    "DiagAAt",
    "TraceAAt",
    "TraceDiagonal",
    "FVec",
    "CVec",
    "AddDiagonal",
    "KronMVProd",
    "KronMVProd2D",
    "CKR",
    "RKR",
    "Relu",
    "Sigmoid",
    "Elu",
    "PartialSum",
    "PartialSumNoKeepDims",
    "partial_sum",
    "PartialMean",
    "EvaluateKernelDiagonal",
    "EvaluateKernelSymmetric",
    "RKR2DMVProd",
    "RKRMVProd",
    "AddJitter",
    "AddJitterV2",
    "SumLogAbs",
    "LGamma",
    "Erf",
    "Erfc",
    "Log1p",
    "RepeatVector",
    "ExpandColumns",
    "Softhinge",
    "trisolve",
    "TriangularSolve",
    "CKRMM2D",
    "QR",
    "PivotedCholeskyRoot",
    "PivotedCholeskySolve",
    "IdentityLike",
    "SquareIdentityLike",
    "OnesLike",
    "ZerosLike",
    "ExtractColumn",
    "ExtractColumns",
    "AutoNugget",
    "ConcatenateColumns",
    "BMM",
    "ExpandBatchDim",
    "Matrix2BatchedVector",
    "SqueezeDim",
    "UnsqueezeDim",
    "Sqrt"
]

class BMM(TensorOperation):
    name = "BMM"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        mat1 = self.operands[0].evaluated
        mat2 = self.operands[1].evaluated
        result = torch.bmm(mat1,mat2)
        return result

    def to_ast(self, label, *arglabels, source=False):
        mat1 = arglabels[0]
        mat2 = arglabels[1]

        src = f"{label} = {mat1}.bmm({mat2})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

class ExpandBatchDim(TensorOperation):
    name = "ExpandBatchDim"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        const = self.operands[1].evaluated
        result = op.expand(const,*op.dim()*[-1])
        return result

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        const = arglabels[1]

        src = f"{label} = {op}.expand({const},*{op}.dim()*[-1])"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

class Matrix2BatchedVector(TensorOperation):
    name = "Matrix2BatchedVector"
    arity = Arity.unary

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        result = op.unsqueeze(-1)
        return result

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]

        src = f"{label} = {op}.unsqueeze(-1)"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class CholeskyRoot(TensorOperation):
    name = "CholeskyRoot"
    arity = Arity.unary

    @classmethod
    def _evaluate(cls, left, right=None):
        # return potrf(left.evaluated)
        return left.cholesky()

    def to_ast(self, label, *arglabels, source=False):
        LLt = arglabels[0]

        src = f"{label} = {LLt}.cholesky()"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class QR(TensorOperation):
    name = "QR"
    arity = Arity.unary

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        q, r = qr_plus(op)
        return q, r

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]

        src = "{label} = qr_plus({op})".format(label=label, op=op)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


def trisolve(
    A: Union[Matrix, TensorOperation],
    B: Union[Vector, Matrix, TensorOperation],
    upper=False,
    transpose=False,
    unitriangular=False,
):
    """
    Compute the solution of A @ X = B where A is triangular and B
    may be a vector or matrix.

    Args:
        A (Matrix): left hand side
        B (Vector or Matrix): right-hand side
        upper: assume A is upper triangular (default False)
        transpose: solve the system A.t() x = B instead (default False)
        unitriangular: assume ones on the diagonal

    Returns:
        a symbolic expression for the solution X.
    """
    return TriangularSolve(
        A, B, Constant(upper), Constant(transpose), Constant(unitriangular)
    )


class TriangularSolve(TensorOperation):
    name = "TriangularSolve"
    arity = Arity(5, True)

    @property
    def evaluated(self):
        A, y, upper, transpose, unitriangular = self.operands

        upper, transpose, unitriangular = tuple(
            map(lambda x: x.evaluated, [upper, transpose, unitriangular])
        )

        return torch.triangular_solve(
            y.evaluated,
            A.evaluated,
            upper=upper,
            transpose=transpose,
            unitriangular=unitriangular,
        )[0]

    def to_ast(self, label, *arglabels, source=False):
        A = arglabels[0]
        y = arglabels[1]

        upper, transpose, unitriangular = self.operands[2:]

        upper, transpose, unitriangular = tuple(
            map(lambda x: x.evaluated, [upper, transpose, unitriangular])
        )

        src = "{label} = torch.triangular_solve({y}, {A}, upper={upper}, transpose={transpose}, unitriangular={unitriangular})[0]".format(
            label=label,
            A=A,
            y=y,
            upper=upper,
            transpose=transpose,
            unitriangular=unitriangular,
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class PivotedCholeskyRoot(TensorOperation):
    name = "PivotedCholeskyRoot"
    arity = Arity.unary

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        result = op.pstrf(upper=False)
        return result

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]

        src = "{label} = {op}.pstrf(upper=False)".format(label=label, op=op)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class PivotedCholeskySolve(TensorOperation):
    """solves Ax = b given the pivoted cholesky factorization of A"""

    name = "PivotedCholeskySolve"
    arity = Arity.binary

    @property
    def evaluated(self):
        A = self.operands[0].evaluated
        b = self.operands[1].evaluated

        return pivoted_cholesky_solve(b, *A)

    def to_ast(self, label, *arglabels, source=False):
        A = arglabels[0]
        b = arglabels[1]

        src = "{label} = pivoted_cholesky_solve({b}, *{A})".format(
            label=label, A=A, b=b
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


"""
class LU(TensorOperation):
    name = "LU"
    arity = Arity.unary

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        result = trifact(op)
        return result

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]

        src = "{label} = trifact({op})".format(label=label, op=op)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class LUUnpack(TensorOperation):
    name = "LUUnpack"
    arity = Arity.unary

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        result = triunpack(*op)
        return result

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]

        src = "{label} = triunpack(*{op})".format(label=label, op=op)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class LUSolve(TensorOperation):
    name = "LUSolve"
    arity = Arity.binary

    @property
    def evaluated(self):
        A = self.operands[0].evaluated
        y = self.operands[1].evaluated
        result = trisolve(y, *A)
        return result

    def to_ast(self, label, *arglabels, source=False):
        A = arglabels[0]
        y = arglabels[1]

        src = "{label} = trisolve({y}, *{A})".format(label=label, A=A, y=y)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

"""


class CKRMM2D(TensorOperation):
    name = "CKRMM2D"
    arity = Arity(3, True)

    @property
    def evaluated(self):
        op0 = self.operands[0].evaluated
        op1 = self.operands[1].evaluated
        M = self.operands[-1].evaluated
        return fast_2D_ckr_mm(op0, op1, M)

    def to_ast(self, label, *arglabels, source=False):
        op0 = arglabels[0]
        op1 = arglabels[1]
        M = arglabels[-1]

        src = "{label} = fast_2D_ckr_mm({op0},{op1},{M})".format(
            label=label, op0=op0, op1=op1, M=M
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class CholeskySolve(TensorOperation):
    name = "CholeskySolve"
    arity = Arity.binary

    @classmethod
    def _evaluate(cls, A, y):
        return trtrs(A.evaluated, y.evaluated)

    def to_ast(self, label, *arglabels, source=False):
        L = arglabels[0]
        y = arglabels[1]

        src = "{label} = trtrs({L}, {y})".format(label=label, L=L, y=y)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class CholeskySolveT(TensorOperation):
    name = "CholeskySolveT"
    arity = Arity.binary

    @classmethod
    def _evaluate(cls, A, y):
        return trtrs(A.evaluated, y.evaluated, transpose=True)

    def to_ast(self, label, *arglabels, source=False):
        Lt = arglabels[0]
        y = arglabels[1]

        src = "{label} = trtrs({Lt}, {y}, transpose=True)".format(
            label=label, Lt=Lt, y=y
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class SumLogDiagonal(TensorOperation):
    name = "SumLogDiagonal"
    arity = Arity.unary

    @classmethod
    def _evaluate(cls, left, right=None):
        return left.evaluated.log().sum()

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = {op_name}.log().sum()".format(label=label, op_name=op_name)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class SumLogAbs(TensorOperation):
    name = "SumLogAbs"
    arity = Arity.unary

    @classmethod
    def _evaluate(cls, left, right=None):
        return left.evaluated.abs().log().sum()

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = {op_name}.abs().log().sum()".format(
            label=label, op_name=op_name
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Log(TensorOperation):
    name = "LogTransform"
    arity = Arity.unary

    def evaluated(self):
        op = self.operands[0].evaluated
        return op.log()

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = f"{label} = {op_name}.log()"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class Abs(TensorOperation):
    name = "Abs"
    arity = Arity.unary

    def evaluated(self):
        op = self.operands[0].evaluated
        return op.abs()

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = f"{label} = {op_name}.abs()"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Exp(TensorOperation):
    name = "Exp"
    arity = Arity.unary

    def evaluated(self):
        return self.operands[0].evaluated.exp()

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = {op_name}.exp()".format(label=label, op_name=op_name)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class Pow(TensorOperation):
    name = "Pow"
    arity = Arity(2,True)

    def evaluated(self):
        power = self.operands[1].evaluated
        return self.operands[0].evaluated.pow(power)

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        power = arglabels[1]
        op_name = arglabels[0]
        src = f"{label} = {op_name}.pow({power})"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Relu(TensorOperation):
    name = "Relu"
    arity = Arity.unary

    @property
    def evaluated(self):
        return torch.nn.functional.relu(self.operands[0].evaluated)

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        # softplus has to be fully qualified due to how graph2ast manages its import of torch
        src = "{label} = torch.nn.functional.relu({op})".format(label=label, op=op)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class Sigmoid(TensorOperation):
    name = "Sigmoid"
    arity = Arity.unary

    @property
    def evaluated(self):
        return torch.sigmoid(self.operands[0].evaluated)

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        # softplus has to be fully qualified due to how graph2ast manages its import of torch
        src = "{label} = torch.sigmoid({op})".format(label=label, op=op)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class Elu(TensorOperation):
    name = "Elu"
    arity = Arity.unary

    @property
    def evaluated(self):
        return torch.nn.functional.elu(self.operands[0].evaluated)

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        # softplus has to be fully qualified due to how graph2ast manages its import of torch
        src = "{label} = torch.nn.functional.elu({op})".format(label=label, op=op)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Softplus(TensorOperation):
    name = "Softplus"
    arity = Arity.unary

    @property
    def evaluated(self):
        return softplus(self.operands[0].evaluated)

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        # softplus has to be fully qualified due to how graph2ast manages its import of torch
        src = "{label} = torch.nn.functional.softplus({op})".format(label=label, op=op)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class Softhinge(TensorOperation):
    name = "Softhinge"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        return softplus(self.operands[0].evaluated, beta=self.operands[1].evaluated)

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        beta = self.operands[1].data
        # softplus has to be fully qualified due to how graph2ast manages its import of torch
        src = "{label} = torch.nn.functional.softplus({op}, beta={beta})".format(
            label=label, op=op, beta=beta
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class LGamma(TensorOperation):
    name = "LGamma"
    arity = Arity.unary

    @property
    def evaluated(self):
        return self.operands[0].evaluated.lgamma()

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        # softplus has to be fully qualified due to how graph2ast manages its import of torch
        src = "{label} = {op}.lgamma()".format(label=label, op=op)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class Erf(TensorOperation):
    name = "Erf"
    arity = Arity.unary

    @property
    def evaluated(self):
        return self.operands[0].evaluated.erf()

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        # softplus has to be fully qualified due to how graph2ast manages its import of torch
        src = "{label} = {op}.erf()".format(label=label, op=op)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class Erfc(TensorOperation):
    name = "Erfc"
    arity = Arity.unary

    @property
    def evaluated(self):
        return self.operands[0].evaluated.erfc()

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        # softplus has to be fully qualified due to how graph2ast manages its import of torch
        src = "{label} = {op}.erfc()".format(label=label, op=op)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class Log1p(TensorOperation):
    name = "Log1p"
    arity = Arity.unary

    @property
    def evaluated(self):
        return self.operands[0].evaluated.log1p()

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        # softplus has to be fully qualified due to how graph2ast manages its import of torch
        src = "{label} = {op}.log1p()".format(label=label, op=op)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result



class InvSoftplus(TensorOperation):
    name = "Softplus"
    arity = Arity.unary

    @property
    def evaluated(self):
        return inv_softplus(self.operands[0].evaluated)

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        # imported from torchgp.functional
        src = "{label} = inv_softplus({op})".format(label=label, op=op)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class DiagAAt(TensorOperation):
    name = "DiagAAt"
    arity = Arity.unary

    @classmethod
    def _evaluate(cls, left, right=None):
        return (left.evaluated * left.evaluated).sum(-1, keepdim=True)

    def to_ast(self, label, *arglabels, source=False):
        A = arglabels[0]

        src = "{label} = ({A}*{A}).sum(-1, keepdim=True)".format(label=label, A=A)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class TraceAAt(TensorOperation):
    name = "TraceAAt"
    arity = Arity.unary

    @classmethod
    def _evaluate(cls, left, right=None):
        return (left.evaluated * left.evaluated).sum()

    def to_ast(self, label, *arglabels, source=False):
        A = arglabels[0]

        src = "{label} = ({A}*{A}).sum()".format(label=label, A=A)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class TraceDiagonal(TensorOperation):
    name = "TraceDiagonal"
    arity = Arity.unary

    @classmethod
    def _evaluate(cls, left, right=None):
        if type(left).__name__ == "DiagonalMatrix":
            # extract the diagonal from a diagonal matrix
            left_eval = left.diagonal.evaluated
        else:
            left_eval = left.evaluated.diagonal()
        return left_eval.sum()


class FVec(TensorOperation):
    name = "FVec"
    arity = Arity.unary

    @property
    def evaluated(self):
        return (
            self.operands[0]
            .permute(tuple(range(self.operands[0].dim() - 1, -1, -1)))
            .reshape(-1)
            .unsqueeze(1)
        )

    def shape(self):
        return reduce(mul, Shape(self.operands[0]))

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]

        src = "{label} = {op}.permute(tuple(range({op}.dim()-1,-1,-1))).reshape(-1).unsqueeze(1)".format(
            label=label, op=op
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class CVec(TensorOperation):
    name = "CVec"
    arity = Arity.unary

    @property
    def evaluated(self):
        return self.operands[0].reshape(-1).unsqueeze(1)

    def shape(self):
        return reduce(mul, Shape(self.operands[0]))

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]

        src = "{label} = {op}.reshape(-1).unsqueeze(1)".format(label=label, op=op)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class AddDiagonal(TensorOperation):
    name = "AddDiagonal"
    arity = Arity.binary

    @property
    def evaluated(self):
        M, diag = self.operands
        return add_diagonal(M.evaluated, diag.evaluated.view(-1))

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        M = arglabels[0]
        diag = arglabels[1]

        src = "{label} = add_diagonal({M}, {diag}.view(-1))".format(
            label=label, M=M, diag=diag
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class AddJitter(TensorOperation):
    name = "AddJitter"
    arity = Arity.binary

    @property
    def evaluated(self):
        M, delta = self.operands
        return add_jitter(M.evaluated, delta.evaluated)

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        M = arglabels[0]
        delta = arglabels[1]

        src = "{label} = add_jitter({M}, {delta})".format(label=label, M=M, delta=delta)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class AddJitterV2(TensorOperation):
    name = "AddJitterV2"
    arity = Arity(4, True)

    @property
    def evaluated(self):
        M, x1, x2, jitter = self.operands
        return add_jitter_v2(M.evaluated, x1.evaluated, x2.evaluated, jitter.evaluated)

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        M = arglabels[0]
        x1 = arglabels[1]
        x2 = arglabels[2]
        jitter = arglabels[3]

        src = "{label} = add_jitter_v2({M}, {x1}, {x2}, {jitter})".format(label=label, M=M, x1=x1, x2=x2, jitter=jitter)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

class KronMVProd(TensorOperation):
    name = "KronMVProd"
    arity = Arity(2, False)

    @property
    def evaluated(self):
        ops = [o.evaluated for o in self.operands[:-1]]
        vec = self.operands[-1].evaluated

        return kron_mvprod(ops, vec)

    def to_ast(self, label, *arglabels, source=False):
        ops, vec = arglabels[:-1], arglabels[-1]
        oplst = "[" + ", ".join(ops) + "]"

        src = "{label} = kron_mvprod({oplst}, {vec})".format(
            label=label, oplst=oplst, vec=vec
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class KronMVProd2D(TensorOperation):
    """ A specialized version for 2 components only"""

    name = "KronMVProd2D"
    arity = Arity(3, True)

    @property
    def evaluated(self):
        ops = [o.evaluated for o in self.operands[:-1]]
        vec = self.operands[-1].evaluated
        op1 = ops[-3]
        op2 = ops[-2]

        return batched_vec_identity(op1, op2, vec)

    def to_ast(self, label, *arglabels, source=False):
        op1 = arglabels[0]
        op2 = arglabels[1]
        vec = arglabels[2]

        src = f"{label} = batched_vec_identity({op1}, {op2}, {vec})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class RKR2DMVProd(TensorOperation):
    name = "RKR2DMVProd"
    arity = Arity(3, True)

    ##TODO implement shape property. This requires cognizance of broadcasting semantics in the op

    @property
    def evaluated(self):
        ops = [o.evaluated for o in self.operands[:-1]]
        vec = self.operands[-1].evaluated

        return rkr_mv_2d(*ops, vec)

    def to_ast(self, label, *arglabels, source=False):
        A, B, vec = arglabels[0], arglabels[1], arglabels[2]

        src = "{label} = rkr_mv_2d({A}, {B}, {vec})".format(
            label=label, A=A, B=B, vec=vec
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class RKRMVProd(TensorOperation):
    name = "RKRMVProd"
    arity = Arity(2, False)

    ##TODO implement shape property. This requires cognizance of broadcasting semantics in the op

    @property
    def evaluated(self):
        ops = [o.evaluated for o in self.operands[:-1]]
        vec = self.operands[-1].evaluated

        return rkr_mv(ops, vec)

    def to_ast(self, label, *arglabels, source=False):
        ops = arglabels[:-1]
        vec = arglabels[-1]
        oplst = "[" + ", ".join(ops) + "]"

        src = "{label} = rkr_mv({oplst}, {vec})".format(
            label=label, oplst=oplst, vec=vec
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class CKR(TensorOperation):
    name = "CKR"
    arity = Arity(2, False)  # requires two arguments minimum, no maximum.

    def __str__(self):
        return "CKR({opstr})".format(opstr=", ".join(map(str, self.operands[:])))

    @property
    def evaluated(self):
        ops = [o.evaluated for o in self.operands]
        return ckr(*ops)

    @property
    def shape(self):
        return (
            prod([Select(Constant(0), Shape(op)) for op in self.operands]),
            Select(Constant(1), Shape(self.operands[0])),
        )

    def to_ast(self, label, *arglabels, source=False):
        ops = arglabels[:]
        oplst = "[" + ", ".join(ops) + "]"

        src = "{label} = ckr(*{oplst})".format(label=label, oplst=oplst)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract the assignment
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class RKR(TensorOperation):
    name = "RKR"
    arity = Arity(2, False)  # requires two arguments minimum, no maximum.

    def __str__(self):
        return "RKR({opstr})".format(opstr=", ".join(map(str, self.operands[:])))

    @property
    def evaluated(self):
        ops = [o.evaluated for o in self.operands]
        return rkr(*ops)

    @property
    def shape(self):
        return (
            Select(Constant(0), Shape(self.operands[0])),
            prod([Select(Constant(1), Shape(op)) for op in self.operands]),
        )

    def to_ast(self, label, *arglabels, source=False):
        ops = arglabels[:]
        oplst = "[" + ", ".join(ops) + "]"

        src = "{label} = rkr(*{oplst})".format(label=label, oplst=oplst)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract the assignment
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class PartialSum(TensorOperation):
    name = "PartialSum"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        dim = self.operands[1].evaluated
        return op.sum(dim=dim).unsqueeze(dim)

    @property
    def shape(self):
        op = self.operands[0]
        dim = self.operands[1].evaluated
        return Replace(Shape(op), Constant(dim), Constant(1))

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        dim = self.operands[1].data

        src = "{label} = {op}.sum(dim={dim}).unsqueeze({dim})".format(
            label=label, op=op, dim=dim
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract the assignment
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

class PartialSumNoKeepDims(TensorOperation):
    name = "PartialSum"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        dim = self.operands[1].evaluated
        return op.sum(dim=dim)

    @property
    def shape(self):
        op = self.operands[0]
        dim = self.operands[1].evaluated
        return Remove(Shape(op), Constant(dim))

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        dim = self.operands[1].data

        src = "{label} = {op}.sum(dim={dim})".format(
            label=label, op=op, dim=dim
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract the assignment
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

def partial_sum(op, dim):
    return PartialSum(op, Constant(dim))


class EvaluateKernelDiagonal(TensorOperation):
    name = "EvaluateKernelDiagonal"
    arity = Arity(2, True)

    def __str__(self):
        return "{kernel}[{opstr}]".format(
            kernel=self.operands[0], opstr=", ".join(map(str, self.operands[1:]))
        )

    @property
    def shape(self):
        return Select(Constant(0), Shape(self.operands[1])), Constant(1)

    @property
    def evaluated(self):
        if not self._dirty:
            return self._cache

        kernel = self.operands[0]
        axis1 = self.operands[1].evaluated

        self._cache = kernel.data.diagonal(axis1)
        self._dirty = False
        return self._cache

    def to_ast(self, label, *arglabels, source=False):
        kfun = arglabels[0]
        laxis = arglabels[1]

        src = "{label} = {kfun}.diagonal({laxis})".format(
            label=label, kfun=kfun, laxis=laxis
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class EvaluateKernelSymmetric(TensorOperation):
    name = "EvaluateKernelSymmetric"
    arity = Arity(2, True)

    def __str__(self):
        return "{kernel}[{opstr},{opstr}]".format(
            kernel=self.operands[0], opstr=", ".join(map(str, self.operands[1:]))
        )

    @property
    def shape(self):
        shape0 = Select(Constant(0), Shape(self.operands[1]))
        return shape0, shape0

    @property
    def evaluated(self):
        if not self._dirty:
            return self._cache

        kernel = self.operands[0]
        axis1 = self.operands[1].evaluated

        self._cache = kernel.data.symmetric(axis1)
        self._dirty = False
        return self._cache

    def to_ast(self, label, *arglabels, source=False):
        kfun = arglabels[0]
        laxis = arglabels[1]

        src = "{label} = {kfun}.symmetric({laxis})".format(
            label=label, kfun=kfun, laxis=laxis
        )
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class PartialMean(TensorOperation):
    name = "PartialMean"
    arity = Arity(2, True)

    @property
    def shape(self):
        op = self.operands[0]
        dim = self.operands[1].evaluated
        return Replace(Shape(op), Constant(dim), Constant(1))

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        dim = self.operands[1].evaluated
        return op.mean(dim=dim).unsqueeze(dim)

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        dim = self.operands[1].data
        src = "{label} = {op_name}.mean(dim={dim}).unsqueeze({dim})".format(
            label=label, op_name=op_name, dim=dim
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class RepeatVector(TensorOperation):
    name = "RepeatVector"
    arity = Arity(2, True)

    @property
    def shape(self):
        vec = self.operands[0]
        reps = self.operands[1].evaluated
        return Replace(Shape(vec), Constant(0), Select(Constant(0), Shape(vec)) * reps)

    @property
    def evaluated(self):
        vec = self.operands[0].evaluated
        reps = self.operands[1].evaluated
        # the following works for both N x 1 and N x None vectors.
        return vec.unsqueeze(0).expand(reps, *vec.dim() * [-1]).reshape(-1, 1)

    def to_ast(self, label, *arglabels, source=False):
        vec = arglabels[0]
        reps = arglabels[1]
        src = "{label} = {vec}.unsqueeze(0).expand({reps},*{vec}.dim()*[-1]).reshape(-1,1)".format(
            label=label, vec=vec, reps=reps
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class ExpandColumns(TensorOperation):
    name = "ExpandColumns"
    arity = Arity(2, True)

    @property
    def shape(self):
        vec = self.operands[0]
        reps = self.operands[1].evaluated
        return Replace(Shape(vec), Constant(1), reps)

    @property
    def evaluated(self):
        vec = self.operands[0].evaluated
        reps = self.operands[1].evaluated
        # the following works for both N x 1 and N x None vectors.
        return vec.expand(-1, reps)

    def to_ast(self, label, *arglabels, source=False):
        vec = arglabels[0]
        reps = arglabels[1]
        src = f"{label} = {vec}.expand(-1, {reps})"

        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class IdentityLike(TensorOperation):
    name = "Ilike"
    arity = Arity(1, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        return eye_like(op)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        src = (
            f"{label} = eye_like({op})"
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class SquareIdentityLike(TensorOperation):
    """ This variant is guaranteed to be square, which is desirable sometimes, but it cannot handle batching
        like IdentityLike can
    """
    name = "IlikeSquare"
    arity = Arity(1, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        return eye_like_square(op)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        src = (
            f"{label} = eye_like_square({op})"
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class OnesLike(TensorOperation):
    name = "Oneslike"
    arity = Arity(1, True)

    @property
    def shape(self):
        op = self.operands[0].evaluated
        return Shape(op)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        return torch.ones_like(op)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        src = (
            f"{label} = torch.ones_like({op})"
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class ZerosLike(TensorOperation):
    name = "Zeroslike"
    arity = Arity(1, True)

    @property
    def shape(self):
        op = self.operands[0].evaluated
        return Shape(op)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        return torch.zeros_like(op)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        src = (
            f"{label} = torch.zeros_like({op})"
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class ExtractColumn(TensorOperation):
    name = "ExtractColumn"
    arity = Arity(2, True)

    @property
    def shape(self):
        op = self.operands[0].evaluated
        return (Slice(Shape(op), Constant(0), Constant(-1)), Constant(1))

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        col = self.operands[1].evaluated
        return op[..., col].unsqueeze(-1)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        col = arglabels[1]
        src = f"{label} = {op}[...,{col}].unsqueeze(-1)"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class ExtractColumns(TensorOperation):
    name = "ExtractColumns"
    arity = Arity(2, True)

    @property
    def shape(self):
        op = self.operands[0].evaluated
        cols = self.operands[1].evaluated
        return (
            Slice(Shape(op), Constant(0), Constant(-1)),
            Select(Constant(0), Shape(cols)),
        )

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        cols = self.operands[1].evaluated
        return op[..., cols]

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        cols = arglabels[1]
        src = f"{label} = {op}[...,{cols}]"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class AutoNugget(TensorOperation):
    name = "AutoNugget"
    arity = Arity(2, True)

    @property
    def shape(self):
        op = self.operands[0].evaluated
        return Shape(op)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        jitter = self.operands[1].evaluated
        return auto_nugget(op, jitter=jitter)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        jitter = arglabels[1]
        src = f"{label} = auto_nugget({op},jitter={jitter})"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class ConcatenateColumns(TensorOperation):
    """basically torch.cat([ops],dim=-1). Assumes ops are 2D only"""
    name = "ConcatenateColumns"
    arity = Arity(2, False)

    @property
    def shape(self):
        ops = [op.evaluated for op in self.operands]
        return (Select(Constant(-2), Shape(ops[0])),
                reduce(add, [
                    Select(Constant(-1), Shape(o)) for o in self.operands
                ]))

    @property
    def evaluated(self):
        ops = [op.evaluated for op in self.operands]
        return torch.stack(ops, dim=-1)

    def to_ast(self, label, *arglabels, source=False):
        oplst = "[" + ", ".join(arglabels) + "]"
        src = f"{label} = torch.cat({oplst},dim=-1)"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class SqueezeDim(TensorOperation):
    """op.squeeze()"""
    name = "SqueezeDim"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        dim = self.operands[1].evaluated
        return op.squeeze(dim=dim)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        dim = arglabels[1]
        src = f"{label} = {op}.squeeze(dim={dim})"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class UnsqueezeDim(TensorOperation):
    """op.squeeze()"""
    name = "UnsqueezeDim"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        dim = self.operands[1].evaluated
        return op.unsqueeze(dim=dim)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        dim = arglabels[1]
        src = f"{label} = {op}.unsqueeze(dim={dim})"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class Sqrt(TensorOperation):
    # Useful for sampling from gaussians with diagonal covariance
    name = "Sqrt"
    arity = Arity.unary

    @property
    def evaluated(self):
        return self.operands[0].evaluated.sqrt()

    @property
    def shape(self):
        return Shape(self.operands[0])

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        # softplus has to be fully qualified due to how graph2ast manages its import of torch
        src = "{label} = {op}.sqrt()".format(label=label, op=op)
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result
