import importlib
import importlib.util
import tempfile
import ast
import astor
import pathlib
import hashlib
from textwrap import dedent
import networkx as nx
import pydot
from IPython.display import SVG

from networkx import compose_all
from . import *


def make_unique_id(expr, id_registry={}):
    nextid = len(id_registry)
    id = id_registry.get(str(expr))
    if id is None:
        id = "node{}".format(nextid)
        id_registry[str(expr)] = id
    return id


def expr2svg(expr, positions=False):
    # look at properties of the symbols in graph and make a nice repr for it
    P = pydot.Dot(graph_type="digraph", strict=True)

    for item, pos in expr.preorder_iter():

        props = make_node_properties(item)

        position = str(pos) if positions else ""
        label = (
            "{label}\n{position}".format(label=props.pop("label"), position=position)
            if positions
            else props.pop("label")
        )
        p = pydot.Node(str(pos), label=label, **props)
        P.add_node(p)
        if len(pos) > 0:
            style = props.get("style", "")
            e = pydot.Edge(str(pos), str(pos[:-1]), style=style)
            P.add_edge(e)

    return SVG(P.create_svg())


def make_node_properties(expr):
    name = type(expr).__name__
    props = {"style": "", "label": expr.name, "astskip": False, "kwarg": False}

    if isinstance(expr, Tensor) or isinstance(expr, Kernel):
        props["shape"] = "oval"

        if not isinstance(expr, Constant):  # Constants get represented directly
            props["kwarg"] = True  # include in kwargs

    elif isinstance(expr, TensorOperation):
        props["label"] = name
        props["shape"] = "box"
        props["color"] = "" if expr._dirty else "gray"

        if name == "Label":
            props["label"] = expr.operands[0].name  # show the string label
            props["shape"] = "doubleoctagon"
    elif name == "String":
        props["style"] = "invis"
        props["astskip"] = True

    return props


def expr2graph(expr, expr_label="expr"):
    N = nx.MultiDiGraph()

    expr_label = expr_label if expr.head != Label else expr.operands[0].name

    for item, pos in expr.preorder_iter():
        h = str(item)
        props = make_node_properties(item)
        style = props.get("style", "")

        if h not in N:  # add only unique nodes
            N.add_node(h, expr=item, **props)

        if len(pos) > 0:  # don't add self-edges
            N.add_edge(
                h,
                str(expr[pos[:-1]]),
                key=(expr_label, pos),
                tooltip=(expr_label, pos),
                style=style,
            )

    return N


def check_clean_hash(N, expr):
    for item, pos in expr.preorder_iter():
        # replace each (potentially unique) item with the cached version
        h = str(item)
        hashedexpr = N.nodes.data("expr")[h]
        if hashedexpr is not item:
            print(
                "objects in hash not identical for {}: {} vs {}".format(
                    N.nodes.data("expr")[h], id(N.nodes.data("expr")[h]), id(item)
                )
            )


def graph2compsvg(graph):
    return SVG(nx.drawing.nx_pydot.to_pydot(graph).create_svg())


def expr2compsvg(expr, key="expression"):
    g = expr2graph(expr, key)
    return graph2compsvg(g)


def graph2ast(graph, expr_label="expr", compiled=False, debug=False):
    N = graph
    id_registry = {}

    # create a wrapper function that takes each leaf node variable as a parameter
    leaves = [
        node
        for node in N
        if all([N.in_degree(node) == 0, not N.nodes.data("astskip")[node]])
    ]
    kwonlyargs = [
        ast.arg(arg=node, annotation=None)
        for node in leaves
        if not isinstance(N.nodes.data("expr")[node], Constant)
    ]

    returns = [
        node for node in N if all([isinstance(N.nodes.data("expr")[node], Label)])
    ]
    return_labels = [N.nodes.data("expr")[node].operands[0].name for node in returns]

    preamble = dedent(
        """
    import torch
    from collections import namedtuple
    from torchgp.functional import *
    from torchgp.structure import *
    from torchgp.reparameterization import *
    from torchgp.factorizations import *
    from functools import reduce
    import itertools
    from torchgp.deep.util import *
    from operator import add, mul, matmul
    
    return_type = namedtuple("return_type", {return_labels}) 
    """.format(
            expr_label=expr_label, return_labels=repr(return_labels)
        )
    )
    result = ast.parse(
        preamble, "<{expr_label}>".format(expr_label=expr_label), mode="exec"
    )
    body = result.body

    # add wrapper function to the module
    callable = ast.FunctionDef(
        name=expr_label,
        args=ast.arguments(
            args=[],
            vararg=None,
            kwonlyargs=kwonlyargs,
            kw_defaults=[None for _ in kwonlyargs],
            kwarg=ast.arg(arg="unused", annotation=None),
            defaults=[],
        ),
        decorator_list=[],
    )
    body.append(callable)

    # rename body to function def body
    callable.body = body = []

    # add all renames from leaf nodes
    if debug:
        print("=" * 80)

    for node in leaves:
        e = N.nodes.data("expr")[node]
        eid = make_unique_id(node, id_registry=id_registry)
        if debug:
            print("{node}: {eid}".format(node=node, eid=eid))
        body.append(e.to_ast(eid))

    if debug:
        print("\n" + "=" * 80)
    # now loop through the topologically-sorted nodes and add their asts to the function body
    for node in nx.topological_sort(N):
        # find an occurrence of this node in an expression and create it with the arguments given by the keys
        expr = N.nodes.data("expr")[node]
        if not isinstance(expr, TensorOperation):
            continue  # skip leaves, as those have been handled already

        arglabels = [make_unique_id(e, id_registry=id_registry) for e in expr.operands]
        label = make_unique_id(expr, id_registry=id_registry)
        _ast, src = expr.to_ast(label, *arglabels, source=True)
        if debug:
            # find first neighbor
            if len(N[node]) > 0:
                # if it has neighbors, it must have at least one edge
                u = node
                v = next(iter(N[u].values()))
                key, pos = next(iter(v))
            else:
                key, pos = (None, ())

            print(
                "{expr_name} in `{key}` at {pos}: {src}".format(
                    expr_name=type(expr).__name__, src=src, key=key, pos=pos
                )
            )
        if _ast is not None:
            body.append(_ast)

    # returns are encoded as Labels; construct a dictionary of them
    outputs = [node for node in returns]
    rkeys, rvalues = [], []
    for node in outputs:
        expr = N.nodes.data("expr")[node]
        rkeys.append(ast.Str(s=str(expr.operands[0])))
        rvalues.append(
            ast.Name(
                id=make_unique_id(expr.operands[1], id_registry=id_registry),
                ctx=ast.Load(),
            )
        )

    body.append(
        ast.Return(
            value=ast.Call(
                func=ast.Name(id="return_type", ctx=ast.Load()),
                args=[],
                keywords=[
                    ast.keyword(arg=None, value=ast.Dict(keys=rkeys, values=rvalues))
                ],
            )
        )
    )

    source = astor.to_source(result)

    if not compiled:
        return source

    # dump the resulting source code to file
    hash = hashlib.sha1(source.encode("utf-8")).hexdigest()[:16]
    compdir = pathlib.Path(".compiled").absolute()
    compdir.mkdir(parents=True, exist_ok=True)

    module_path = (compdir / hash).with_suffix(".py")
    # print("Module path: ", module_path)
    with module_path.open("w") as file:
        file.write(source)

    spec = importlib.util.spec_from_file_location(
        module_path.with_suffix("").name, str(module_path)
    )
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return getattr(module, expr_label)
