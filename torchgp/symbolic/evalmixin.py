from matchpy import Arity
from functools import reduce


class _EvaluateHelper:
    __slots__ = ("evaluated",)

    def __init__(self, evaluated):
        self.evaluated = evaluated


class EvaluateMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.invalidate()

    def invalidate(self):
        # re-evaluate this case
        # print(f'Invalidated {self}')
        self._dirty = True
        self._cache = None

    def cache(self, result):
        self._dirty = False
        self._cache = result
        return self._cache

    @property
    def evaluated(self):
        if not self._dirty:
            # print(f'Returning cache for {self}')
            return self._cache

        if self.arity == Arity.unary:
            try:
                result = self.__evaluate_helper(self.operands[0])
            except (RuntimeError, TypeError) as e:
                msg = "unable to evaluate {operation} with operand {operand}".format(
                    operation=str(self), operand=self.operands[0]
                )
                msg += "\n{emsg}".format(emsg=e)
                print(msg)
                raise

            # success
            return self.cache(result)

        # binary or variadic
        try:
            result = reduce(self.__evaluate_helper, self.operands)
        except (RuntimeError, TypeError) as e:
            msg = "unable to evaluate {operation} with operands {operands}".format(
                operation=str(self), operands=self.operands
            )
            msg += "\n{emsg}".format(emsg=e)
            print(msg)
            raise

        return self.cache(result)

    @classmethod
    def __evaluate_helper(cls, left, right=None):
        # take care of the multi-element reduce case, where after the first reduction,
        # left is tensor and right is Op/Matrix

        left = _EvaluateHelper(left) if "evaluated" not in dir(left) else left

        if right is None:  # fast exit for unary
            return cls._evaluate(left, right)

        right = _EvaluateHelper(right) if "evaluated" not in dir(right) else right
        return cls._evaluate(left, right)

    @classmethod
    def _evaluate(cls, left, right=None):
        raise NotImplementedError(
            "implement torch evaluation for {name}".format(name=cls.__name__)
        )
