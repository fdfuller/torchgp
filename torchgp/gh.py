import torch
import math
from scipy.special import roots_hermite
import numpy as np
import itertools


class GaussHermiteIntegrator(torch.nn.Module):
    def __init__(self, order):
        super().__init__()
        self.order = order
        mapfun = lambda a: torch.from_numpy(a).to(dtype=torch.get_default_dtype())
        points, weights = map(mapfun, roots_hermite(order))

        self.register_buffer("_points", points)
        self.register_buffer("_weights", weights / math.sqrt(math.pi))

    def forward(self, integrand, means, variances):
        shape = means.shape

        pstar = (
            means.unsqueeze(-1)
            + torch.sqrt(2.0 * variances).unsqueeze(-1) * self._points
        )
        pstar = pstar.view(-1, self.order)

        result = (self._weights * integrand(pstar)).sum(dim=-1)
        result = result.reshape(*shape)
        return result

    def extra_repr(self):
        return "order={}".format(self.order)


class GHVariationalExpectations(torch.nn.Module):
    def __init__(self, Likelihood, order, dimension, dtype=torch.get_default_dtype()):
        super(GHVariationalExpectations, self).__init__()
        self.register_buffer("order", torch.tensor(order, dtype=torch.int32))
        self.register_buffer("dimension", torch.tensor(dimension, dtype=torch.int32))
        self.dtype = dtype
        self.register_buffer(
            "const", torch.tensor(np.pi ** (-0.5 * self.dimension.item()), dtype=dtype)
        )
        xn, wn = self._mvhermgauss()
        self.register_buffer("xn", xn)
        self.register_buffer("wn", wn)
        self.Likelihood = Likelihood

    def _ndtype(self, T):
        if T is torch.float32:
            return np.float32
        elif T is torch.float64:
            return np.float64
        else:
            raise TypeError("Expected a floating type")

    def _hermgauss(self):
        x, w = np.polynomial.hermite.hermgauss(self.order.item())
        return x.astype(self._ndtype(self.dtype)), w.astype(self._ndtype(self.dtype))

    def _mvhermgauss(self):
        gh_x, gh_w = self._hermgauss()
        x = torch.from_numpy(
            np.array(
                list(itertools.product(*(gh_x,) * self.dimension.item())),
                dtype=self._ndtype(self.dtype),
            )
        )  # H**DxD
        w = torch.from_numpy(
            np.prod(
                np.array(list(itertools.product(*(gh_w,) * self.dimension.item()))),
                1,
                dtype=self._ndtype(self.dtype),
            )
        )  # H**D
        w.unsqueeze_(0).unsqueeze_(2)
        x.unsqueeze_(0)
        return x, w

    def forward(self, y, mus, sigmas):
        assert sigmas.shape == mus.shape
        # expect y of shape batch_len x D, so we broadcast to batch_len x 1 x D
        # expect sigmas/mus of shape batch_len x D, so broadcast as y
        # x is of shape 1 x order**D x D
        # w is of shape 1 x order**D x 1
        # zn is the transformed x broadcasted over batch: dim x order**dim x batch_len
        zn = math.sqrt(2) * (torch.sqrt(sigmas).unsqueeze(1) * self.xn) + mus.unsqueeze(
            1
        )
        yc = y.unsqueeze(1)  # dim x 1 x batch_len
        # contract over [1] axis (order**dim) to get batch_len x dim
        return torch.sum(self.wn * self.const * self.Likelihood.log_density(yc, zn), 1)
