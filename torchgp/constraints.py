from torch.distributions.constraint_registry import ConstraintRegistry
from torch.distributions import constraints, biject_to
from torch.distributions.transforms import ComposeTransform
from .transforms import SoftplusTransform

default_mapping = ConstraintRegistry()


@default_mapping.register(constraints.real)
def _make_real(constraint):
    assert isinstance(constraint, type(constraints.real))
    return ComposeTransform([])


@default_mapping.register(constraints.real_vector)
def _make_real_vector(constraint):
    assert isinstance(constraint, type(constraints.real_vector))
    return ComposeTransform([])


@default_mapping.register(constraints.positive)
def _make_positive(constraint):
    assert isinstance(constraint, type(constraints.positive))
    return SoftplusTransform()


@default_mapping.register(constraints.interval)
def _make_interval(constraint):
    return biject_to(constraint)


@default_mapping.register(constraints.simplex)
def _make_simplex(constraint):
    return biject_to(constraint)


@default_mapping.register(constraints.greater_than_eq)
def _make_gte(constraint):
    return biject_to(constraint)
