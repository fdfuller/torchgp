import torch
import math
from ..symbolic import *
from abc import ABC, abstractmethod
from itertools import chain
from torch.optim import Adam
from .layers import AbstractLayerWrapper
from .util import to_neg, merge_axis
from .symbolic_shape import recover_integral
import torch.nn.functional as F
from .layers import Regularizer, merge_regularizers
from .inducing import inducing_factory
from ..kernels import kernel_factory

__all__ = [
    'AbstractLatentLayer',
    'MeanFieldGaussianLatentLayer',
    'MeanFieldGaussianLatentLayerAxisDependent',
    'MeanFieldGaussianLatentLayerND',
    'CorrelateLatentLayer',
    'FixMarginalizationState',
    'AmortizedMeanFieldGaussianLatentLayer',
    # 'ImplicitMeanFieldGaussianLatentLayer'
]


def kl_to_std_normal(mu: torch.Tensor, log_var: torch.Tensor):
    """ computes KL( q(mu, log_var) || N(0, I) ), where q is a mean field gaussian """
    assert mu.shape == log_var.shape
    return (0.5 * (-log_var.sum(dim=-1) + log_var.exp().sum(dim=-1) + mu.pow(2).sum(dim=-1) - mu.shape[-1])).unsqueeze(
        -1)


def std_normal_log_density(z: torch.Tensor):
    """ z is a sample of shape ...D"""
    d = z.shape[-1]
    return -0.5 * d * (math.log(math.pi * 2)) - 0.5*z.pow(2).sum(dim=-1)


def mean_field_normal_log_density(z, mu: torch.Tensor, log_var: torch.Tensor):
    """ z is a sample of shape ...D and we assume mu/log_var are the same size"""
    assert mu.shape == log_var.shape
    d = mu.shape[-1]
    return (-0.5 * (d * math.log(math.pi * 2) + log_var.sum(dim=-1)) -
            0.5*(log_var.mul(-1).exp() * (z - mu).pow(2)).sum(dim=-1))


def alter_plan_for_batch(plan, batch_ind, observed_dim, expected_dim):
    if plan is not None:
        a = observed_dim - expected_dim
        ap = []
        for p in plan:
            if p < batch_ind:
                ap.append(p - a)  # works for negative plan indices only
            else:
                ap.append(p)
        return ap
    else:
        return None


class CorrelateLatentLayer(AbstractLayerWrapper):
    """
    When we use a latent layer, it's typically combined with an output concatenation layer or a skip layer to form
    an output that's partially latent and partially deterministic. When this happens, we want to have the resulting
    tensor obey certain properties that depend on the marginalization state of the model. For instance, the multiple
    latent samples for the nth sample should all be correlated with each other. If marginalize_input = True, then we
    simply need to transpose the 'W' index and the 'N' index, so that the 'W' index (corresponding to multiple latent
    samples) is in the special -2 position where it is interpreted to be correlated. If marginalize_input = False,
    however, we want to merge the 'W' and 'N' axis together.

    Anyway, this layer wrapper does that conditional logic. Use it like: CorrelateLatentLayer(SkipLayer(LatentLayer...

    We can't just wrap the latent layer because it does not have any true data dependence (auxiliary dependence doesn't
    count here)
    """

    def __init__(self, layer, name=''):
        super().__init__(layer, name)
        self._build_config['kind'] = type(self).__name__
        self._sample_shape = None
        self._merge_plan = None
        self._transpose_plan = None
        self._sample_batch_ind = None
        self.marginalize_input = None
        self.marginalize_output = None

    def _calc_sample_shape(self):
        base_sample_shape = self.layer.sample_shape
        transpose_plan = None
        merge_plan = None
        if self.marginalize_input:
            if 'W' in base_sample_shape:
                if 'N' in base_sample_shape:
                    n_ind = base_sample_shape.index('N')
                else:
                    n_ind = base_sample_shape.index(-2)
                w_ind = base_sample_shape.index('W')
                modified_sample_shape = [s for s in base_sample_shape]
                # a simple transpose
                modified_sample_shape[n_ind] = 'W'
                modified_sample_shape[w_ind] = 'N'  # forces -2 to become 'N'
                transpose_plan = recover_integral(list(to_neg([n_ind, w_ind], len(modified_sample_shape))))
            else:
                # no change required
                modified_sample_shape = [s for s in base_sample_shape]
        else:
            if 'W' in base_sample_shape:
                modified_sample_shape = [s for s in filter(lambda x: not x == 'W', base_sample_shape)]
                if 'N' in modified_sample_shape:
                    n_ind = modified_sample_shape.index('N')
                else:
                    n_ind = modified_sample_shape.index(-2)

                if 'N' in base_sample_shape:
                    bn_ind = base_sample_shape.index('N')
                else:
                    bn_ind = base_sample_shape.index(-2)
                # merge the axes
                merge_plan = recover_integral(list(to_neg([base_sample_shape.index('W'), bn_ind],
                                                          len(base_sample_shape))))
                modified_sample_shape[n_ind] = ['W', 'N']
            else:
                # no change required
                modified_sample_shape = [s for s in base_sample_shape]
        batch_ind = int(to_neg(base_sample_shape.index(None), len(base_sample_shape)))
        return modified_sample_shape, transpose_plan, merge_plan, batch_ind

    def update_marginalization(self, **kwargs):
        marginalize_input = True if not 'marginalize_input' in kwargs else kwargs['marginalize_input']
        marginalize_output = True if not 'marginalize_output' in kwargs else kwargs['marginalize_output']
        self._build_config['marginalization'] = {'arg': None,
                                                 'kwargs': {'marginalize_input': marginalize_input,
                                                            'marginalize_output': marginalize_output}}
        self.marginalize_input = marginalize_input
        self.marginalize_output = marginalize_output
        self.layer.update_marginalization(marginalize_input=marginalize_input, marginalize_output=marginalize_output)
        return self

    @property
    def ready_to_build(self):
        if self.marginalize_output is not None and \
            self.marginalize_input is not None:
            return True
        else:
            return False

    @property
    def out_features(self):
        return self.layer.out_features

    @property
    def in_features(self):
        return self.layer.in_features

    @property
    def mean_shape(self):
        raise NotImplementedError('CorrelateLatentLayer only intended for sampling')

    @property
    def sample_shape(self):
        return self._sample_shape

    @property
    def cov_shape(self):
        raise NotImplementedError('CorrelateLatentLayer only intended for sampling')

    def build(self):
        if self.ready_to_build:
            self.layer.build()
            self._sample_shape, self._transpose_plan, self._merge_plan, self._sample_batch_ind = \
                self._calc_sample_shape()
        else:
            raise ValueError('missing marginalization state, therefore cannot build')

    def sample(self, x, **kwargs):
        sample, kl = self.layer.sample(x, **kwargs)
        atp = alter_plan_for_batch(self._transpose_plan, self._sample_batch_ind, int(sample.dim()), len(self._sample_shape))
        amp = alter_plan_for_batch(self._merge_plan, self._sample_batch_ind, int(sample.dim()), len(self._sample_shape))
        if self._transpose_plan is not None:
            sample = sample.transpose(*atp)
        if self._merge_plan is not None:
            sample = merge_axis(sample, *amp)
        return sample, kl

    def split_sample(self, x, **kwargs):
        rpart, dpart = self.layer.split_sample(x, **kwargs)
        atp = alter_plan_for_batch(self._transpose_plan, self._sample_batch_ind, int(x.dim()), len(self._sample_shape))
        amp = alter_plan_for_batch(self._merge_plan, self._sample_batch_ind, int(x.dim()), len(self._sample_shape))
        if self._transpose_plan is not None:
            rpart = rpart.transpose(*atp)
            dpart = dpart.transpose(*atp)
        if self._merge_plan is not None:
            rpart = merge_axis(rpart, *amp)
            dpart = merge_axis(dpart, *amp)
        return rpart, dpart

    def components(self, x, **unused):
        raise NotImplementedError('CorrelateLatentLayer only intended for sampling')


class FixMarginalizationState(AbstractLayerWrapper):
    """
    This wrapper allows one to shield a layer against changes of marginalization state. You set the state in
    instantiation. If you want the state to remain mutable for either marginalize_input or output, set it
    to None on instantiation.
    """

    def __init__(self, layer, marginalize_input, marginalize_output, name=''):
        super().__init__(layer, name)
        self._build_config['kind'] = type(self).__name__
        self._marginalize_input = marginalize_input
        self._marginalize_output = marginalize_output

    def update_marginalization(self, **kwargs):
        marginalize_input = True if not 'marginalize_input' in kwargs else kwargs['marginalize_input']
        marginalize_output = True if not 'marginalize_output' in kwargs else kwargs['marginalize_output']
        self._build_config['marginalization'] = {'arg': None,
                                                 'kwargs': {'marginalize_input': marginalize_input,
                                                            'marginalize_output': marginalize_output}}
        # intercept the update
        if self._marginalize_input is not None:
            marginalize_input = self._marginalize_input
        if self._marginalize_output is not None:
            marginalize_output = self._marginalize_output
        self.layer.update_marginalization(marginalize_input=marginalize_input, marginalize_output=marginalize_output)
        return self

    @property
    def out_features(self):
        return self.layer.out_features

    @property
    def in_features(self):
        return self.layer.in_features

    @property
    def mean_shape(self):
        return self.layer.mean_shape

    @property
    def sample_shape(self):
        return self.layer.sample_shape

    @property
    def cov_shape(self):
        return self.layer.cov_shape

    def build(self):
        if self.ready_to_build:
            self.layer.build()
        else:
            raise ValueError('Wrapped layer is not ready to build')

    def sample(self, x, **kwargs):
        sample = self.layer.sample(x, **kwargs)
        return sample

    def split_sample(self, x, **kwargs):
        rpart, dpart = self.layer.split_sample(x, **kwargs)
        return rpart, dpart

    def components(self, x, **kwargs):
        mean, cov, kl = self.layer.components(x, **kwargs)
        return mean, cov, kl


class AbstractLatentLayer(torch.nn.Module, ABC):
    """ Represents the interface for an amortized VI latent layer. Amortized, in this context, means that the
        parameters for the variational distribution depend on an auxiliary input x. When the variational family is
        gaussian q(x) would be as below, which the typical use-case for VAEs (on the encoder side).
            q(x) ~ N( mu = encoder(x)[0], var = exp(encoder(x)[1]) )

        The envisioned use-case is for a gaussian family variational model. The interface should be general enough to be
         useful for any sort of variational family, though the KL might be obnoxious to compute unless you're using a
         a gaussian. For cases where the kl is hard to compute or when using importance weighted VI inference, I also
         request that the log_importance_ratio be computable. Requirements here are less mild than the KL, but still
         I implicitly assume that the ratio is differentiable (via reparameterization)
    """

    def __init__(self, name=''):
        super().__init__()
        # generates a list of parameters needed to sample the posterior from a given auxiliary input
        self.name = str(name)
        self._build_config = {'name': self.name}
        self.inference_method = 'VI'
        self.multisamples = None
        self.latent_multisamples = 1
        self._build_config['latent_multisamples'] = {'arg': 1, 'kwargs': {}}
        self.marginalize_input = None
        self.marginalize_output = None
        self._post_process_iw = None  # allows for modification of iw weights by model (e.g. transposing/reshaping)


    def update_inference_method(self, kind, **unused):
        if kind in ['VI', 'IVI']:
            self.inference_method = kind
        else:
            raise ValueError('unknown inference method, expected either VI or IVI')
        return self

    def update_multisamples(self, num: int, **unused):
        # NOTE: Latent layers do not use multisamples, they use latent multisamples. So updating this will not change
        # the behavior of the latent layer output
        assert int(num) >= 1, "must be a strictly positive integer"
        self.multisamples = int(num)
        self._build_config['multisamples'] = {'arg': int(num), 'kwargs': unused}
        return self

    def update_latent_multisamples(self, num: int, **unused):
        assert int(num) >= 1, "must be a strictly positive integer"
        self.latent_multisamples = int(num)
        self._build_config['latent_multisamples'] = {'arg': int(num), 'kwargs': unused}
        return self

    def update_marginalization(self, **kwargs):
        """
        I include marginalization state for compatibility with the Layer API, but I don't think it makes sense to
        change the behavior of the layer given a marginalization state. At present, these attributes do nothing.
        """
        marginalize_input = True if 'marginalize_input' not in kwargs else kwargs['marginalize_input']
        marginalize_output = True if 'marginalize_output' not in kwargs else kwargs['marginalize_output']
        self._build_config['marginalization'] = {'arg': None, 'kwargs': kwargs}
        self.marginalize_output = marginalize_output
        self.marginalize_input = marginalize_input
        return self

    def _compile(self):
        """ Yeah, this a $10 method to elide a couple conditionals, but whatever it's the pattern"""
        kl_method = Lambda(self.name + '_kl')
        iw_method = Lambda(self.name + '_iw')
        sample_method = Lambda(self.name + '_sampler')
        split_sample_rpart_method = Lambda(self.name + '_ssample_rpart')
        split_sample_dpart_method = Lambda(self.name + '_ssampel_dpart')
        variational_components = Lambda(self.name + '_vc')
        prior_variational_components = Lambda(self.name + '_pvc')
        iwreg = Lambda(self.name + '_iwreg')
        klreg = Lambda(self.name + '_klreg')
        latent_multisamples = Tensor(self.name + '_latent_multisamples')
        aux_in = Tensor(self.name + '_aux_in')
        x_in = Tensor(self.name + '_x_in')

        vc_out = Apply(variational_components, aux_in)
        pvc_out = Apply(prior_variational_components, x_in)
        sample = Apply(sample_method, aux_in, vc_out, latent_multisamples)
        prior_sample = Apply(sample_method, x_in, pvc_out, latent_multisamples)
        split_sample_rpart = Apply(split_sample_rpart_method, aux_in, vc_out, latent_multisamples)
        split_sample_dpart = Apply(split_sample_dpart_method, aux_in, vc_out, latent_multisamples)
        split_prior_sample_rpart = Apply(split_sample_rpart_method, x_in, pvc_out, latent_multisamples)
        split_prior_sample_dpart = Apply(split_sample_dpart_method, x_in, pvc_out, latent_multisamples)
        if self.inference_method == 'VI':
            reg = Apply(klreg, Apply(kl_method, aux_in, vc_out))
            preg = Apply(klreg, Apply(kl_method, x_in, pvc_out))
        elif self.inference_method == 'IVI':
            reg = Apply(iwreg, Apply(iw_method, sample, aux_in, vc_out, latent_multisamples))
            preg = Apply(iwreg, Apply(iw_method, prior_sample, x_in, pvc_out, latent_multisamples))
        else:
            raise ValueError(f"Unknown inference type, expected either VI or IVI, got {self.inference_method}")

        # obviously implies amortization, but that's not necessarily the case. Just a name, bro. Calm down
        vc_output_graph = expr2graph(Label(String('encoder_output'), vc_out))
        sample_graph = expr2graph(Label(String('sample'), sample))
        prior_sample_graph = expr2graph(Label(String('prior_sample'), prior_sample))
        split_sample_rpart_graph = expr2graph(Label(String('random_part'), split_sample_rpart))
        split_sample_dpart_graph = expr2graph(Label(String('deterministic_part'), split_sample_dpart))
        reg_graph = expr2graph(Label(String('regularizer'), reg))
        split_prior_sample_rpart_graph = expr2graph(Label(String('prior_random_part'), split_prior_sample_rpart))
        split_prior_sample_dpart_graph = expr2graph(Label(String('prior_deterministic_part'), split_prior_sample_dpart))
        preg_graph = expr2graph(Label(String('prior_regularizer'), preg))
        self._sample = graph2ast(compose_all([sample_graph, reg_graph]), compiled=True, debug=False)
        self._prior_sample = graph2ast(compose_all([prior_sample_graph, preg_graph]), compiled=True, debug=False)
        self._components = graph2ast(compose_all([vc_output_graph, reg_graph]), compiled=True, debug=False)
        self._split_sample = graph2ast(compose_all([split_sample_rpart_graph, split_sample_dpart_graph]),
                                       compiled=True, debug=False)
        self._split_prior_sample = graph2ast(compose_all([split_prior_sample_rpart_graph, split_prior_sample_dpart_graph]),
                                       compiled=True, debug=False)

    def _populate_symbols(self):
        leaves = {
            self.name + '_kl': self.kl,
            self.name + '_iw': self.log_importance_ratio,
            self.name + '_sampler': self.sample_posterior,
            self.name + '_ssample_rpart': self.split_sample_rpart,
            self.name + '_ssample_dpart': self.split_sample_dpart,
            self.name + '_vc': self.variational_components,
            self.name + '_pvc': self.prior_variational_components,
            self.name + '_iwreg': lambda x: Regularizer(None,x),
            self.name + '_klreg': lambda x: Regularizer(x,None)
        }
        return leaves

    def sample(self, x, **kwargs):
        latent_multisamples = self.latent_multisamples if 'latent_multisamples' not in kwargs else \
            kwargs['latent_multisamples']
        aux_in = None if 'aux_in' not in kwargs else kwargs['aux_in']
        if aux_in is None:
            result = self._prior_sample(**{self.name + '_x_in': x, **self._populate_symbols(),
                                           self.name + '_latent_multisamples': latent_multisamples})
            r = result.prior_sample, result.prior_regularizer
        else:
            result = self._sample(**{self.name + '_aux_in': aux_in, **self._populate_symbols(),
                                 self.name + '_latent_multisamples': latent_multisamples})
            r = result.sample, result.regularizer
        return r

    def forward(self, x, **kwargs):
        return self.sample(x, **kwargs)

    def split_sample(self, x, **kwargs):
        latent_multisamples = self.latent_multisamples if 'latent_multisamples' not in kwargs else \
            kwargs['latent_multisamples']
        aux_in = None if 'aux_in' not in kwargs else kwargs['aux_in']
        if aux_in is None:
            result = self._split_prior_sample(**{self.name + '_xstar': x, **self._populate_symbols(),
                                           self.name + '_latent_multisamples': latent_multisamples})
            r = result.prior_random_part, result.prior_deterministic_part
        else:
            result = self._split_sample(**{self.name + '_aux_in': aux_in, **self._populate_symbols(),
                                     self.name + '_latent_multisamples': latent_multisamples})
            r = result.random_part, result.deterministic_part
        return r

    def components(self, x, **kwargs):
        raise NotImplementedError('Latent layers should be used in sampling mode only')

    def build(self):
        if self.ready_to_build:
            self._compile()
        else:
            raise ValueError('Not yet ready to build')

    @property
    def build_config(self):
        return self._build_config

    @property
    @abstractmethod
    def in_features(self):
        pass

    @property
    @abstractmethod
    def out_features(self):
        pass

    @property
    def num_layers(self):
        return 1

    @property
    def iw_shape(self):
        if self.latent_multisamples > 1:
            shape = ['W', None, -2]
        else:
            shape = [None, -2]
        return shape

    @property
    def mean_shape(self):
        raise NotImplementedError('Latent layers should be used in sampling mode only')

    @property
    def cov_shape(self):
        raise NotImplementedError('Latent layers should be used in sampling mode only')

    @property
    @abstractmethod
    def sample_shape(self):
        pass

    @property
    @abstractmethod
    def ready_to_build(self):
        pass

    @abstractmethod
    def variational_components(self, x):
        """ Only intended to be used internally, not like the `components` method of the Layer API"""
        pass

    @abstractmethod
    def prior_variational_components(self, x):
        pass

    @abstractmethod
    def kl(self, x, variational_components):
        pass

    @abstractmethod
    def sample_posterior(self, x, variational_components, latent_multisamples):
        pass

    @abstractmethod
    def split_sample_rpart(self, x, variational_components, latent_multisamples):
        pass

    @abstractmethod
    def split_sample_dpart(self, x, variational_components, latent_multisamples):
        pass

    @abstractmethod
    def log_importance_ratio(self, s, x, variational_components, latent_multisamples):
        """ should compute log(p(s)) - log(q(s)), where s ~ q. where q is defined by parameters in
            variational components
        """
        pass

    @abstractmethod
    def variational_parameters(self):
        pass

    @abstractmethod
    def named_variational_parameters(self):
        pass

    @abstractmethod
    def hyper_parameters(self):
        pass

    @abstractmethod
    def named_hyper_parameters(self):
        pass

    def build_hyper_optimizer(self, default_lr=1e-3, **kwargs):
        dict = {'params': filter(lambda x: x.requires_grad, self.hyper_parameters()), 'lr': default_lr}
        return (Adam([dict]),)  # needs to be iterable so that I can chain them in layer containers

    def build_variational_optimizer(self, default_lr=1e-3, **kwargs):
        dict = {'params': filter(lambda x: x.requires_grad, self.variational_parameters()), 'lr': default_lr}
        return (Adam([dict]),)  # needs to be iterable so that I can chain them in layer containers


def nearest_points(probe_axis, reference_axis, values1, values2):
    """
    This is a hack trick to find the closest points on a reference axis to a requested probe_axis. Once
    those axis positions are found, we return the values1 and values2 which are tied to those reference
    axis positions. Intended use-case here is to imbue a mean-field gaussian with an axis interpretation, i.e
    a D dimensional mean-field gaussian can now be associated with a potentially multi-dimensional axis given
    by reference axis, such that prod(reference_axis.shape) = D.

    This nearest_point algorithm is differentiable so long as we do not differentiate with respect
    to probe_axis or reference_axis. In the envisioned use-case, probe_axis will be input axis points,
    and reference_axis will be a fixed parameter. values1 and values2 will be essentially masked, and
    thus we can freely differentiate them
    """
    D = reference_axis.shape[-1]
    assert reference_axis.shape[:-1] == values1.shape[:-1], \
        f"got ref shape: {reference_axis.shape} and val shape: {values1.shape}"
    assert reference_axis.shape[:-1] == values2.shape[:-1], \
        f"got ref shape: {reference_axis.shape} and val shape: {values2.shape}"
    assert values1.shape[-1] == values2.shape[-1]
    assert probe_axis.shape[-1] == D
    rprobe = probe_axis.reshape(-1, D)
    rref = reference_axis.reshape(-1, D).unsqueeze(-3)  # 1ND
    rvals1 = values1.reshape(-1, values1.shape[-1])
    rvals2 = values2.reshape(-1, values2.shape[-1])
    with torch.no_grad():
        uprobe = rprobe.unsqueeze(-2)  # B1D
        # compute squared euclidean distance
        dist = rref.add(-uprobe).pow(2).sum(-1)  # BN
        i = dist.topk(1, largest=False, sorted=False)[1].squeeze()
    r1 = rvals1[i, :].reshape(list(probe_axis.shape[:-1]) + [values1.shape[-1]])
    r2 = rvals2[i, :].reshape(list(probe_axis.shape[:-1]) + [values2.shape[-1]])
    return r1, r2


class MeanFieldGaussianLatentLayerAxisDependent(AbstractLatentLayer):
    """ A parametric mean field gaussian which depends on a data axis by mapping the requested axis position via nearest
     neighbor to a fixed internal axis. This allows us to have a relatively interpretable latent space (contrasted with
    more general amortized schemes) that has controlled size and the ability to handle random mini-batches more
    gracefully.

    The number of parameters held here is: prod(axis_shape) * num_features. For a 1D axis, it'd be ND. But we can
    tolerate multiple axis dimensions.
    """

    def __init__(self, in_features: int, out_features: int, axis_shape, name='', dtype=torch.get_default_dtype()):
        super().__init__(name)
        self._out_features = int(out_features)
        self._in_features = int(in_features)
        assert len(axis_shape) == self._in_features
        self.axis_shape = tuple([int(s) for s in axis_shape])
        self._build_config['kind'] = type(self).__name__
        self._build_config['in_features'] = self._in_features
        self._build_config['out_features'] = self._out_features
        self._build_config['axis_shape'] = self.axis_shape
        self.dtype = dtype
        axes = [torch.linspace(-3, 3, n, dtype=self.dtype) for n in self.axis_shape]
        ix = torch.cartesian_prod(*axes)
        if ix.dim() < 2:
            ix = ix.unsqueeze(-1)  # covers the case where in_features == 1, we want D = 1 for ix.shape[-1]
        self.register_buffer('_internal_axis', ix)  # ND

        self.mu = torch.nn.Parameter(torch.zeros(list(ix.shape[:-1]) + [self._out_features], dtype=self.dtype))
        self.log_var = torch.nn.Parameter(torch.zeros(list(ix.shape[:-1]) + [self._out_features], dtype=self.dtype))

    @property
    def ready_to_build(self):
        flag = False
        if self.multisamples is not None and \
            self.marginalize_input is not None and \
            self.marginalize_output is not None:
            flag = True
        else:
            print('Not yet ready to build')
            print('You must ensure that multisamples and marginalization state are set first')
        return flag

    def build(self):
        if self.ready_to_build:
            self._compile()
        else:
            raise ValueError('not yet ready to build')

    def initialize_mean(self, mean_init):
        assert mean_init.shape == self.mu.shape
        with torch.no_grad():
            self.mu.data[...] = mean_init

    def initialize_log_var(self, log_var_init):
        assert log_var_init.shape == self.log_var.shape
        with torch.no_grad():
            self.log_var.data[...] = log_var_init

    def variational_components(self, x):
        assert x.shape[-1] == self.in_features
        mu, log_var = nearest_points(x, self._internal_axis, self.mu, self.log_var)
        return mu, log_var  # ...D

    def prior_variational_components(self, x):
        return torch.randn(list(x.shape[:-1]) + [self._out_features], dtype=x.dtype, device=x.device)

    def kl(self, x, variational_components):
        """ We assume the prior is N(O,I). mu and log_var should have shape D """
        mu, log_var = variational_components
        return kl_to_std_normal(mu.reshape(-1), log_var.reshape(-1))

    def sample_posterior(self, x, variational_components, latent_multisamples):
        """ Implements basic reparameterization sampling for a mean-field gaussian."""
        return self.split_sample_rpart(x, variational_components, latent_multisamples) + \
               self.split_sample_dpart(x, variational_components, latent_multisamples)

    def split_sample_rpart(self, x, variational_components, latent_multisamples):
        """ Implements basic reparameterization sampling for a mean-field gaussian """
        assert x.dim() >= 2, f"we expect input to be at least 2D, got {x.dim()}"
        mu, log_var = variational_components  # ...D
        log_var = log_var.expand([latent_multisamples] + list(log_var.shape))  # W...D
        epsilon = torch.randn(log_var.shape, dtype=log_var.dtype, device=log_var.device)
        q = log_var.exp().sqrt() * epsilon
        return q.squeeze(0)  # W...D or ...D, if W == 1

    def split_sample_dpart(self, x, variational_components, latent_multisamples):
        assert x.dim() >= 2, f"we expect input to be at least 2D, got {x.dim()}"
        mu, _ = variational_components
        # expand mu, log_var to match size of x in all dims except last
        mu = mu.expand([latent_multisamples] + list(mu.shape))  # W...D
        return mu.squeeze(0)  # W...D or ...D, if W == 1

    def log_importance_ratio(self, sample, x, variational_components, latent_multisamples):
        """ should compute log(p(x)) - log(q(x)), where x ~ q."""
        # expecting sample to have shape ...SND, but components to just have shape D
        assert sample.dim() >= 2, f"we expect input to be at least 2D, got {sample.dim()}"
        mu, log_var = variational_components  # ...D
        if latent_multisamples > 1:
            mud = mu.dim()
            vard = log_var.dim()
            mu = mu.unsqueeze(0).expand([latent_multisamples] + mud * [-1])
            log_var = log_var.unsqueeze(0).expand([latent_multisamples] + vard * [-1])
        logp = std_normal_log_density(sample.reshape(-1))
        logq = mean_field_normal_log_density(sample.reshape(-1), mu.reshape(-1), log_var.reshape(-1))
        if self._post_process_iw is None:
            return logp - logq
        else:
            return self._post_process_iw(logp - logq)

    @property
    def in_features(self):
        return self._in_features

    @property
    def out_features(self):
        return self._out_features

    @property
    def sample_shape(self):
        if self.latent_multisamples > 1:
            shape = ['W', None, -2, 'D']
        else:
            shape = [None, -2, 'D']
        return shape

    def variational_parameters(self):
        # all parameters in this layer are `variational`
        return self.parameters()

    def named_variational_parameters(self):
        return self.named_parameters()

    def hyper_parameters(self):
        # all parameters in this layer are `variational`, so none are hyper
        return []

    def named_hyper_parameters(self):
        return []


class MeanFieldGaussianLatentLayer(AbstractLatentLayer):
    """ A parametric mean field gaussian which has no auxilliary data dependence. If you say there are 2 features
        then this layer only stores 4 numbers worth of parameters (2 mean, 2 log_variance). This is a basic, but
        probably not super useful layer. Good for testing.
    """

    def __init__(self, num_features: int, name='', dtype=torch.get_default_dtype()):
        super().__init__(name)
        self.num_features = int(num_features)
        self._build_config['kind'] = type(self).__name__
        self._build_config['num_features'] = self.num_features
        self.dtype = dtype
        self.mu = torch.nn.Parameter(torch.zeros(self.num_features, dtype=self.dtype))
        self.log_var = torch.nn.Parameter(torch.zeros(self.num_features, dtype=self.dtype))

    @property
    def ready_to_build(self):
        flag = False
        if self.multisamples is not None and \
            self.marginalize_input is not None and \
            self.marginalize_output is not None:
            flag = True
        else:
            print('Not yet ready to build')
            print('You must ensure that multisamples and marginalization state are set first')
        return flag

    def build(self):
        if self.ready_to_build:
            self._compile()
        else:
            raise ValueError('not yet ready to build')

    def initialize_mean(self, mean_init):
        assert mean_init.shape == self.mu.shape
        with torch.no_grad():
            self.mu.data[...] = mean_init

    def initialize_log_var(self, log_var_init):
        assert log_var_init.shape == self.log_var.shape
        with torch.no_grad():
            self.log_var.data[...] = log_var_init

    def variational_components(self, x):
        # x is unused in this layer, but generally would be
        mu, log_var = self.mu, self.log_var
        return mu, log_var

    def prior_variational_components(self, x):
        return torch.zeros_like(self.mu), torch.zeros_like(self.log_var)

    def kl(self, x, variational_components):
        """ We assume the prior is N(O,I). mu and log_var should have shape D """
        mu, log_var = variational_components
        return kl_to_std_normal(mu, log_var)

    def sample_posterior(self, x, variational_components, latent_multisamples):
        """ Implements basic reparameterization sampling for a mean-field gaussian."""
        return self.split_sample_rpart(x, variational_components, latent_multisamples) + \
               self.split_sample_dpart(x, variational_components, latent_multisamples)

    def split_sample_rpart(self, x, variational_components, latent_multisamples):
        """ Implements basic reparameterization sampling for a mean-field gaussian """
        assert x.dim() >= 2, f"we expect input to be at least 2D, got {x.dim()}"
        mu, log_var = variational_components  # D
        log_var = log_var.expand([latent_multisamples] + list(x.shape[:-1]) + list(log_var.shape))  # W...ND
        epsilon = torch.randn(log_var.shape, dtype=log_var.dtype, device=log_var.device)
        q = log_var.exp().sqrt() * epsilon
        return q.squeeze(0)  # remove multisampling if it was multisamples == 1

    def split_sample_dpart(self, x, variational_components, latent_multisamples):
        """ Implements basic reparameterization sampling for a mean-field gaussian """
        assert x.dim() >= 2, f"we expect input to be at least 2D, got {x.dim()}"
        mu = self.mu
        # expand mu, log_var to match size of x in all dims except last
        mu = mu.expand([latent_multisamples] + list(x.shape[:-1]) + list(mu.shape))  # W...ND
        return mu.squeeze(0)  # remove multisampling if it was multisamples == 1

    def log_importance_ratio(self, sample, x, variational_components, latent_multisamples):
        """ should compute log(p(x)) - log(q(x)), where x ~ q."""
        # expecting sample to have shape ...SND, but components to just have shape D
        assert sample.dim() >= 2, f"we expect input to be at least 2D, got {sample.dim()}"
        mu, log_var = self.mu, self.log_var  # D
        # expand mu, log_var to match size of x in all dims except last
        mu = mu.expand(list(sample.shape[:-1]) + list(mu.shape))  # ...ND
        log_var = log_var.expand(list(sample.shape[:-1]) + list(log_var.shape))  # ...ND
        logp = std_normal_log_density(sample)
        logq = mean_field_normal_log_density(sample, mu, log_var)
        if self._post_process_iw is None:
            return logp - logq
        else:
            return self._post_process_iw(logp - logq)

    @property
    def in_features(self):
        return 0

    @property
    def out_features(self):
        return self.num_features

    @property
    def sample_shape(self):
        if self.latent_multisamples > 1:
            shape = ['W', None, -2, 'D']
        else:
            shape = [None, -2, 'D']
        return shape

    def variational_parameters(self):
        # all parameters in this layer are `variational`
        return self.parameters()

    def named_variational_parameters(self):
        return self.named_parameters()

    def hyper_parameters(self):
        # all parameters in this layer are `variational`, so none are hyper
        return []

    def named_hyper_parameters(self):
        return []


class MeanFieldGaussianLatentLayerND(AbstractLatentLayer):
    """ A parametric mean field gaussian which has no auxilliary data dependence. This one has shape NxD, allowing
        one to add D dimensions to every measurement.
    """

    def __init__(self, num_obs: int, num_features: int, name='', dtype=torch.get_default_dtype()):
        super().__init__(name)
        self.num_features = int(num_features)
        self.num_obs = int(num_obs)
        self._build_config['kind'] = type(self).__name__
        self._build_config['num_features'] = self.num_features
        self._build_config['num_obs'] = self.num_obs
        self.dtype = dtype
        self.mu = torch.nn.Parameter(torch.zeros(self.num_obs, self.num_features, dtype=self.dtype))
        self.log_var = torch.nn.Parameter(torch.zeros(self.num_obs, self.num_features, dtype=self.dtype))

    @property
    def ready_to_build(self):
        flag = False
        if self.multisamples is not None and \
            self.marginalize_input is not None and \
            self.marginalize_output is not None:
            flag = True
        else:
            print('Not yet ready to build')
            print('You must ensure that multisamples and marginalization state are set first')
        return flag

    def build(self):
        if self.ready_to_build:
            self._compile()
        else:
            raise ValueError('not yet ready to build')

    def initialize_mean(self, mean_init):
        assert mean_init.shape == self.mu.shape
        with torch.no_grad():
            self.mu.data[...] = mean_init

    def initialize_log_var(self, log_var_init):
        assert log_var_init.shape == self.log_var.shape
        with torch.no_grad():
            self.log_var.data[...] = log_var_init

    def variational_components(self, x):
        # x is unused in this layer, but generally would be
        mu, log_var = self.mu, self.log_var
        return mu, log_var

    def prior_variational_components(self, x):
        return torch.zeros_like(self.mu), torch.zeros_like(self.log_var)

    def kl(self, x, variational_components):
        """ We assume the prior is N(O,I). mu and log_var should have shape D """
        mu, log_var = variational_components
        return kl_to_std_normal(mu.reshape(-1), log_var.reshape(-1))

    def sample_posterior(self, x, variational_components, latent_multisamples):
        """ Implements basic reparameterization sampling for a mean-field gaussian."""
        return self.split_sample_rpart(x, variational_components, latent_multisamples) + \
               self.split_sample_dpart(x, variational_components, latent_multisamples)

    def split_sample_rpart(self, x, variational_components, latent_multisamples):
        """ Implements basic reparameterization sampling for a mean-field gaussian """
        assert x.dim() >= 2, f"we expect input to be at least 2D, got {x.dim()}"
        # x presumed to be ...ND in shape
        mu, log_var = variational_components  # ND
        log_var = log_var.expand([latent_multisamples] + list(x.shape[:-2]) + list(log_var.shape))  # W...ND
        epsilon = torch.randn(log_var.shape, dtype=log_var.dtype, device=log_var.device)
        q = log_var.exp().sqrt() * epsilon
        return q.squeeze(0)  # remove multisampling if it was multisamples == 1

    def split_sample_dpart(self, x, variational_components, latent_multisamples):
        """ Implements basic reparameterization sampling for a mean-field gaussian """
        assert x.dim() >= 2, f"we expect input to be at least 2D, got {x.dim()}"
        # x presumed to be ...ND in shape
        mu = self.mu  # ND
        # expand mu, log_var to match size of x in all dims except last
        mu = mu.expand([latent_multisamples] + list(x.shape[:-2]) + list(mu.shape))  # W...ND
        return mu.squeeze(0)  # remove multisampling if it was multisamples == 1

    def log_importance_ratio(self, sample, x, variational_components, latent_multisamples):
        """ should compute log(p(x)) - log(q(x)), where x ~ q."""
        # expecting sample to have shape ...SND, but components to just have shape D
        assert sample.dim() >= 2, f"we expect input to be at least 2D, got {sample.dim()}"
        mu, log_var = self.mu, self.log_var  # ND
        assert sample.shape[-2] == mu.shape[-2]
        # expand mu, log_var to match size of x in all dims except last
        mu = mu.expand(list(sample.shape[:-2]) + list(mu.shape))  # ...ND
        log_var = log_var.expand(list(sample.shape[:-2]) + list(log_var.shape))  # ...ND
        logp = std_normal_log_density(sample)
        logq = mean_field_normal_log_density(sample, mu, log_var)
        if self._post_process_iw is None:
            return logp - logq
        else:
            return self._post_process_iw(logp - logq)

    @property
    def in_features(self):
        return 0

    @property
    def out_features(self):
        return self.num_features

    @property
    def sample_shape(self):
        if self.latent_multisamples > 1:
            shape = ['W', None, -2, 'D']
        else:
            shape = [None, -2, 'D']
        return shape

    def variational_parameters(self):
        # all parameters in this layer are `variational`
        return self.parameters()

    def named_variational_parameters(self):
        return self.named_parameters()

    def hyper_parameters(self):
        # all parameters in this layer are `variational`, so none are hyper
        return []

    def named_hyper_parameters(self):
        return []


class DenseSkipLayer(torch.nn.Module):
    r"""Applies a linear transformation to the incoming data: :math:`y = phi (xA^T + b) + x`
        where phi is the activation function (Tanh by default).

        Args:
            features: size of each input sample, which will also be size of output sample
            bias: If set to ``False``, the layer will not learn an additive bias.
                Default: ``True``

        Shape:
            - Input: :math:`(N, *, H_{in})` where :math:`*` means any number of
              additional dimensions and :math:`H_{in} = \text{in\_features}`
            - Output: :math:`(N, *, H_{out})` where all but the last dimension
              are the same shape as the input and :math:`H_{out} = \text{out\_features}`.

        Attributes:
            weight: the learnable weights of the module of shape
                :math:`(\text{features}, \text{features})`. The values are
                initialized from :math:`\mathcal{U}(-\sqrt{k}, \sqrt{k})`, where
                :math:`k = \frac{1}{\text{features}}`
            bias:   the learnable bias of the module of shape :math:`(\text{features})`.
                    If :attr:`bias` is ``True``, the values are initialized from
                    :math:`\mathcal{U}(-\sqrt{k}, \sqrt{k})` where
                    :math:`k = \frac{1}{\text{features}}`

        Examples::

            >>> m = nn.Linear(20, 30)
            >>> input = torch.randn(128, 20)
            >>> output = m(input)
            >>> print(output.size())
            torch.Size([128, 30])
        """
    __constants__ = ['bias', 'features', 'features']

    def __init__(self, features, activation=None, bias=True):
        super().__init__()
        self.features = features
        self.activation = activation or torch.nn.Tanh()
        self.weight = torch.nn.Parameter(torch.Tensor(features, features))
        if bias:
            self.bias = torch.nn.Parameter(torch.Tensor(features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        torch.nn.init.kaiming_uniform_(self.weight, a=math.sqrt(5))
        if self.bias is not None:
            fan_in, _ = torch.nn.init._calculate_fan_in_and_fan_out(self.weight)
            bound = 1 / math.sqrt(fan_in)
            torch.nn.init.uniform_(self.bias, -bound, bound)

    def forward(self, inp):
        return self.activation(F.linear(inp, self.weight, self.bias)) + inp

    def extra_repr(self):
        return 'in_features={}, out_features={}, bias={}'.format(
            self.features, self.features, self.bias is not None
        )


class RBFEncoder(torch.nn.Module):
    def __init__(self, in_features: int, latent_features: int, num_inducing: int = 128, **kwargs):
        super().__init__()
        if 'dtype' in kwargs:
            self.dtype = kwargs['dtype']
        else:
            self.dtype = torch.get_default_dtype()
        self.in_features = in_features
        self.latent_features = latent_features
        Z = inducing_factory({'Shared': {'num_gps': 1, 'num_inducing': num_inducing, 'inducing_dim': self.in_features,
                                                          'range_min': -math.pi, 'range_max': math.pi,
                                                      'normal_inducing': True, 'dtype': torch.float64}})
        self.Z = torch.nn.Parameter(Z.Z, requires_grad=True)
        self.kernel = kernel_factory({'RBFKernel':
        {'lengthscale': self.in_features*[math.sqrt(float(self.in_features))], 'dtype': self.dtype}})
        self.hidden_encoder = torch.nn.Linear(num_inducing, 2 * self.latent_features, bias=True).type(self.dtype)

    def forward(self, x):
        Kfu = self.kernel(x, self.Z).squeeze()
        return self.hidden_encoder(Kfu)

class AmortizedMeanFieldGaussianLatentLayer(AbstractLatentLayer):
    """ A feed-forward neural network encoder encodes the mean and log_var of a mean field gaussian """

    def __init__(self, in_features: int, out_features: int, name='', dtype=torch.get_default_dtype()):
        super().__init__(name)
        self._in_features = int(in_features)
        self._out_features = int(out_features)
        self._build_config['kind'] = type(self).__name__
        self._build_config['in_features'] = self._in_features
        self._build_config['out_features'] = self._out_features
        self.dtype = dtype
        self.encoder = self._default_encoder(self._in_features, self._out_features).type(self.dtype)
        # self.encoder = RBFEncoder(self._in_features, self._out_features, dtype=dtype)


    @staticmethod
    def _default_encoder_init(m):
        if type(m) == torch.nn.Linear:
            xavier_std = (2. / (m.in_features + m.out_features)) ** 0.5
            m.weight.data.normal_(mean=0., std=1.0) * xavier_std
            m.bias.data.fill_(0.)
        elif type(m) == DenseSkipLayer:
            xavier_std = (1. / (m.features)) ** 0.5
            m.weight.data.normal_(mean=0., std=1.0) * xavier_std
            m.bias.data.fill_(0.)

    @staticmethod
    def _tensor_to_variational_tuple(x, out_features):
        mu = x[..., :out_features]
        # copied this particular activation of the variance from the original DGP_IWVI paper code implementation
        var = F.softplus(x[..., -out_features:] - 3.)
        log_var = var.log()  # for consistency
        return mu, log_var

    @staticmethod
    def _default_encoder(in_features, out_features):
        # meant to match Hugh's code. In general, an encoder should be a torch module that
        # consumes a tensor of -1 dim = in_features and returns a -1 dim of 2*out_features
        # in this implementation, we will transform the output like F.softplus([...,-out_features:] - 3), ensuring
        # that they are positive (rather than using a log transform) and also that they are initially small
        encoder = torch.nn.Sequential(torch.nn.Linear(in_features, 20, bias=True),
                                      torch.nn.Tanh(),
                                      # torch.nn.Linear(20, 20, bias=True),  # hidden layer
                                      DenseSkipLayer(20, bias=True, activation=torch.nn.Tanh()),
                                      # torch.nn.Tanh(),
                                      torch.nn.Linear(20, 2 * out_features, bias=True),
                                      )
        encoder.apply(AmortizedMeanFieldGaussianLatentLayer._default_encoder_init)
        return encoder

    @property
    def in_features(self):
        return self._in_features

    @property
    def out_features(self):
        return self._out_features

    def update_encoder(self, encoder=None, **unused):
        if encoder is None:
            encoder = self._default_encoder(self.in_features, self.out_features).type(self.dtype)
            self.build_config['encoder'] = {'args': None, 'kwargs': {}}
        else:
            # TODO: We need to make it possible to write a build config for a general encoder
            # some kind of wrapper for a torch.nn.Sequential should cover it. That's really all we're missing.
            raise NotImplementedError('arbitrary encoder support not yet implemented')
        self.encoder = encoder
        return self

    @property
    def ready_to_build(self):
        return True  # ready out of the box

    def build(self):
        if self.encoder is None:
            self.update_encoder()
        if self.ready_to_build:
            self._compile()
        else:
            raise ValueError('not yet ready to build')

    def variational_components(self, x):
        encoded = self.encoder(x)  # expected to return a tensor
        return self._tensor_to_variational_tuple(encoded, self.out_features)

    def prior_variational_components(self, x):
        return (torch.zeros(list(x.shape[:-1]) + [self.out_features], dtype=x.dtype, device=x.device),
                torch.zeros(list(x.shape[:-1]) + [self.out_features], dtype=x.dtype, device=x.device))

    def kl(self, x, variational_components):
        """ We assume the prior is N(O,I). mu and log_var should have shape ...ND """
        mu, log_var = variational_components
        return kl_to_std_normal(mu.reshape(-1), log_var.reshape(-1))

    def sample_posterior(self, x, variational_components, latent_multisamples):
        """ Implements basic reparameterization sampling for a mean-field gaussian."""
        return self.split_sample_rpart(x, variational_components, latent_multisamples) + \
               self.split_sample_dpart(x, variational_components, latent_multisamples)

    def split_sample_rpart(self, x, variational_components, latent_multisamples):
        """ Implements basic reparameterization sampling for a mean-field gaussian """
        _, log_var = variational_components  # ...ND
        var = log_var.exp().expand([latent_multisamples] + list(log_var.shape))  # W...ND
        epsilon = torch.randn(var.shape, dtype=var.dtype, device=var.device)
        q = var.sqrt() * epsilon
        return q.squeeze(0)  # remove latent multisampling if it was latent multisamples == 1

    def split_sample_dpart(self, x, variational_components, latent_multisamples):
        """ Implements basic reparameterization sampling for a mean-field gaussian """
        mu, _ = variational_components  # ...ND
        # expand mu, log_var to match size of x in all dims except last
        mu = mu.expand([latent_multisamples] + list(mu.shape))  # W...ND
        return mu.squeeze(0)  # remove latent multisampling if it was latent multisamples == 1

    def log_importance_ratio(self, sample, x, variational_components, latent_multisamples):
        """ should compute log(p(x)) - log(q(x)), where sample ~ q."""
        # expecting sample to have shape W...ND
        assert sample.dim() >= 2, f"we expect input to be at least 2D, got {sample.dim()}"
        mu, log_var = variational_components  # ND
        if latent_multisamples > 1:
            mud = mu.dim()
            vard = log_var.dim()
            mu = mu.unsqueeze(0).expand([latent_multisamples] + mud*[-1])
            log_var = log_var.unsqueeze(0).expand([latent_multisamples] + vard*[-1])
        # assert sample.shape == mu.shape
        # expand mu, log_var to match size of x in all dims except last
        logp = std_normal_log_density(sample)
        logq = mean_field_normal_log_density(sample, mu, log_var)
        if self._post_process_iw is None:
            return logp - logq
        else:
            return self._post_process_iw(logp - logq)

    @property
    def sample_shape(self):
        if self.latent_multisamples > 1:
            shape = ['W', None, -2, 'D']
        else:
            shape = [None, -2, 'D']
        return shape

    def variational_parameters(self):
        # none of the parameters in this layer are `variational`
        return []

    def named_variational_parameters(self):
        return []

    def hyper_parameters(self):
        # all parameters in this layer are `hyper`, so none are variational, ergo they are `amortized`
        return self.parameters()

    def named_hyper_parameters(self):
        return self.named_parameters()
