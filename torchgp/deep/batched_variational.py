import torch
from ..transforms import SoftplusTransform, IdentityTransform
from collections import OrderedDict
from .util import validate_scalar_parameter

__all__ = [
    "BatchedMVNPlain",
    "BatchedMVNSqrtVar"
]


### Here we create a couple key storage modules needed for the semi-symbolic VariationalParameterization objects

class BatchedVector(torch.nn.Module):
    """
    This stores a collection of vectors as a matrix internally, provides initializers (default is zero_init)

    batch dim is 0th dim. So when unpacked, this thing has shape batch x N x 1
    """

    def __init__(self, M, batch=1, init=None, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        if init is None:
            init = self.zero_init
        self.storage = torch.nn.Parameter(torch.empty(batch, M, dtype=self.dtype))
        self.register_buffer("batch", torch.tensor(batch, dtype=torch.int64))
        self.register_buffer("M", torch.tensor(M, dtype=torch.int64))
        init(self.storage)

    def forward(self):
        return self.unpack()

    def pack(self, unpacked):
        assert unpacked.dim() == 3, f"expected unpacked to be 3D, got {unpacked.dim()}"
        allocated = torch.zeros(
            self.batch, self.M, dtype=unpacked.dtype, device=unpacked.device
        )
        allocated[:, :self.M] = unpacked[:, :, 0]
        return allocated

    def unpack(self, packed=None):
        if packed is None:
            packed = self.storage
        assert packed.shape[-2] == self.batch, f"expected packed.shape[-2] to be {self.batch}, got {packed.shape[-2]}"
        assert packed.shape[-1] == self.M, f"expected packed.shape[-2] to be {self.M}, got {packed.shape[-1]}"
        allocated = torch.zeros(
            self.batch, self.M, 1, dtype=packed.dtype, device=packed.device
        )
        allocated[:, :, 0] = packed
        return allocated

    @staticmethod
    def random_init(x):
        f = torch.randn_like(x)
        x.data[...] = (f / x.shape[1]).data
        return x

    @staticmethod
    def zero_init(x):
        f = torch.zeros_like(x)
        x.data[...] = f.data
        return x

    def custom_initialization(self, init_vals):
        packed_init_vals = self.pack(init_vals)
        self.storage.data[...] = packed_init_vals.data
        return self.unpack()


class ConstrainedBatchedLowerTriangular(torch.nn.Module):
    """
    Initializes to the batched identity matrix. You can change that via custom_initializer.

    select transform=None if you don't want the diagonal to be transformed in storage/unpacking
    """

    def __init__(
        self, batch: int, n: int, transform=SoftplusTransform(), dtype=torch.get_default_dtype()
    ):
        super().__init__()
        self.dtype = dtype
        self.register_buffer("M", torch.tensor(int(n), dtype=torch.int64))
        self.register_buffer("batch", torch.tensor(int(batch), dtype=torch.int64))
        ti = torch.tril_indices(n, n, offset=-1)
        rL, cL = ti[0], ti[1]
        rD, cD = torch.arange(n), torch.arange(n)
        rI = torch.cat([rD, rL])
        cI = torch.cat([cD, cL])
        self.register_buffer("rI", rI)
        self.register_buffer("cI", cI)
        if transform is None:
            # hack where for some reason we don't want an instance to get access to .inv method
            # I think that's a bug in the definition of IdentityTransform
            transform = IdentityTransform
        self.diag_transform = transform
        packed = torch.zeros(batch, len(rI), dtype=self.dtype)
        packed[:, :n] = self.diag_transform.inv(torch.ones(n, dtype=self.dtype)).expand(self.batch, -1)
        self.packed = torch.nn.Parameter(packed)

    def unpack(self, packed=None):
        if packed is None:
            packed = self.packed
        allocated = torch.zeros(
            (self.batch, self.M, self.M), dtype=packed.dtype, device=packed.device
        )
        allocated[:, self.rI[self.M:], self.cI[self.M:]] = packed[:, self.M: len(self.rI)]
        allocated[:, self.rI[: self.M], self.cI[: self.M]] = self.diag_transform(
            packed[:, : self.M]
        )
        return allocated

    def pack(self, tril):
        allocated = torch.zeros(self.batch, len(self.rI), dtype=tril.dtype, device=tril.device)
        allocated[:, : self.M] = self.diag_transform.inv(
            tril[:, self.rI[: self.M], self.cI[: self.M]]
        )
        allocated[:, self.M:] = tril[:, self.rI[self.M:], self.cI[self.M:]]
        return allocated

    def forward(self):
        return self.unpack()

    def custom_initialization(self, tril):
        if tril.dim() == 2:
            tril = tril.expand(self.batch, -1, -1)
        with torch.no_grad():
            v = self.pack(tril)
            self.packed.data[:, : v.shape[1]] = v.data


class UnconstrainedBatchedSymmetric(torch.nn.Module):
    """
    Initializes to the batched identity matrix. You can change that via custom_initializer.
    """

    def __init__(
        self, batch: int, M: int, dtype=torch.get_default_dtype()
    ):
        super().__init__()
        self.dtype = dtype
        self.register_buffer("M", torch.tensor(int(M), dtype=torch.int64))
        self.register_buffer("batch", torch.tensor(int(batch), dtype=torch.int64))
        ti = torch.tril_indices(M, M, offset=-1)
        rL, cL = ti[0], ti[1]
        rD, cD = torch.arange(M), torch.arange(M)
        rI = torch.cat([rD, rL])
        cI = torch.cat([cD, cL])
        self.register_buffer("rI", rI)
        self.register_buffer("cI", cI)
        packed = torch.zeros(batch, len(rI), dtype=self.dtype)
        packed[:, :M] = torch.ones(M, dtype=self.dtype).expand(self.batch, -1)
        self.packed = torch.nn.Parameter(packed)

    def unpack(self, packed=None):
        if packed is None:
            packed = self.packed
        allocated = torch.zeros(
            (self.batch, self.M, self.M), dtype=packed.dtype, device=packed.device
        )
        allocated[:, self.rI[self.M:], self.cI[self.M:]] = packed[:, self.M: len(self.rI)]
        allocated[:, self.cI[self.M:], self.rI[self.M:]] = packed[:, self.M: len(self.rI)]
        allocated[:, self.rI[: self.M], self.cI[: self.M]] = packed[:, : self.M]
        return allocated

    def pack(self, tril):
        allocated = torch.zeros(self.batch, len(self.rI), dtype=tril.dtype, device=tril.device)
        allocated[:, : self.M] = tril[:, self.rI[: self.M], self.cI[: self.M]]
        allocated[:, self.M:] = tril[:, self.rI[self.M:], self.cI[self.M:]]
        return allocated

    def forward(self):
        return self.unpack()

    def custom_initialization(self, tril):
        if tril.dim() == 2:
            tril = tril.expand(self.batch, -1, -1)
        with torch.no_grad():
            v = self.pack(tril)
            self.packed.data[:, : v.shape[1]] = v.data


class BatchedMVNSqrtVar(torch.nn.Module):
    """
    This module represents a batched MVN gaussian with dense parameterization, such that:

    mean = self.m, with shape batch x M x 1
    cov = torch.bmm(self.L, self.L.t()) with shape batch x M x M, where L is lower triangular

    if constrained = True, then the diagonal of L is constrained to be positive so that L corresponds
    to a Cholesky factor of the covariance, as opposed to simply a matrix square root

    """
    def __init__(self, batch, M, constrained=True, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        self.register_buffer("M", validate_scalar_parameter(M, 'M', torch.int64))
        self.register_buffer("batch", validate_scalar_parameter(batch, 'batch', torch.int64))
        batch = int(self.batch)
        M = int(self.M)
        if constrained:
            L = ConstrainedBatchedLowerTriangular(batch, M, dtype=self.dtype)
        else:
            L = ConstrainedBatchedLowerTriangular(batch, M, transform=None, dtype=self.dtype)
        self.packed_params = torch.nn.ModuleDict(
            OrderedDict(
                m=BatchedVector(batch=batch, M=M, dtype=self.dtype, init=BatchedVector.zero_init),
                L=L,
            )
        )

    def xi(self):
        m_packed = self.packed_params["m"].storage
        L_packed = self.packed_params["L"].packed
        return [m_packed, L_packed]

    def unpack(self, xi_packed=None):
        if xi_packed is None:
            xi_packed = self.xi()
        m_packed, L_packed = xi_packed
        m_unpacked = self.packed_params["m"].unpack(packed=m_packed)
        L_unpacked = self.packed_params["L"].unpack(packed=L_packed)
        return [m_unpacked, L_unpacked]

    def pack(self, xi):
        m_unpacked, L_unpacked = xi
        m_packed = self.packed_params["m"].pack(m_unpacked)
        L_packed = self.packed_params["L"].pack(L_unpacked)
        return [m_packed, L_packed]

    def custom_L_init(self, lower):
        assert lower.dim() == 3, f"expected a batched matrix with lower.dim() = 3, got {lower.dim()}"
        assert lower.shape[-2:] == torch.Size([int(self.M), int(self.M)]), \
            f"expected lower's final two dims to be the same and have shape {self.M}, got {lower.shape[-2:]}"
        assert lower.shape[-3] == int(self.batch), \
            f"expected batch dim to have shape {self.batch}, got {lower.shape[-3]}"
        self.packed_params["L"].custom_initialization(lower)

    def custom_m_init(self, v):
        assert v.dim() == 3, f"expected a batched vector with v.dim() = 3, got {v.dim()}"
        assert v.shape[-1] == 1, f"expected last dim to have shape 1, got {v.shape[-1]}"
        self.packed_params["m"].custom_initialization(v)

class BatchedMVNPlain(torch.nn.Module):
    """
    This module represents a batched MVN gaussian with dense parameterization, such that:

    mean = self.m, with shape batch x M x 1
    cov = self.L + self.L.t() with shape batch x M x M, where L is lower triangular

    This parameterization does not enforce positive semi-definiteness of the covariance, so its use
    needs more care. Typically employed for natural parameterization in a natural gradient environment
    """
    def __init__(self, batch, M, dtype=torch.get_default_dtype()):
        super().__init__()
        self.dtype = dtype
        self.register_buffer("M", validate_scalar_parameter(M, 'M', torch.int64))
        self.register_buffer("batch", validate_scalar_parameter(batch, 'batch', torch.int64))
        batch = int(self.batch)
        M = int(self.M)
        L = UnconstrainedBatchedSymmetric(batch, M, dtype=self.dtype)
        self.packed_params = torch.nn.ModuleDict(
            OrderedDict(
                m=BatchedVector(batch=batch, M=M, dtype=self.dtype, init=BatchedVector.zero_init),
                L=L,
            )
        )

    def xi(self):
        m_packed = self.packed_params["m"].storage
        L_packed = self.packed_params["L"].packed
        return [m_packed, L_packed]

    def unpack(self, xi_packed=None):
        if xi_packed is None:
            xi_packed = self.xi()
        m_packed, L_packed = xi_packed
        m_unpacked = self.packed_params["m"].unpack(packed=m_packed)
        L_unpacked = self.packed_params["L"].unpack(packed=L_packed)
        return [m_unpacked, L_unpacked]

    def pack(self, xi):
        m_unpacked, L_unpacked = xi
        m_packed = self.packed_params["m"].pack(m_unpacked)
        L_packed = self.packed_params["L"].pack(L_unpacked)
        return [m_packed, L_packed]

    def custom_L_init(self, lower):
        assert lower.dim() == 3, f"expected a batched matrix with lower.dim() = 3, got {lower.dim()}"
        assert lower.shape[-2:] == torch.Size([int(self.M), int(self.M)]), \
            f"expected lower's final two dims to be the same and have shape {self.M}, got {lower.shape[-2:]}"
        assert lower.shape[-3] == int(self.batch), \
            f"expected batch dim to have shape {self.batch}, got {lower.shape[-3]}"
        self.packed_params["L"].custom_initialization(lower)

    def custom_m_init(self, v):
        assert v.dim() == 3, f"expected a batched vector with v.dim() = 3, got {v.dim()}"
        assert v.shape[-1] == 1, f"expected last dim to have shape 1, got {v.shape[-1]}"
        self.packed_params["m"].custom_initialization(v)
