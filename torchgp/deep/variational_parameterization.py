from torchgp.symbolic import *
import torch
from .batched_variational import *
from .util import *
from ..natural_gradient.parameterizations import *
from torchgp.symbolic.core import (
    Shape,
    Select,
    Constant,
)
from ..natural_gradient.optimizer import general_fisher_norm_limited_ng_update, natural_fisher_norm_limited_ng_update

__all__ = [
    "AbstractVariationalParameterization",
    "SqrtVarParameterization",
    "WhitenedSqrtVarParameterization",
    "variational_parameterization_factory"
]

class AbstractVariationalParameterization(torch.nn.Module):
    def __init__(self, num_gps: int, num_inducing: int, name: str, jitter=None, dtype=torch.get_default_dtype()):
        super().__init__()
        self.name = name
        self.dtype = dtype
        if jitter is None:
            jitter = 1e-4
        self.register_buffer("num_gps", torch.tensor(num_gps, dtype=torch.int64))
        self.register_buffer("num_inducing", torch.tensor(num_inducing, dtype=torch.int64))
        self.register_buffer('jitter_val', torch.tensor(jitter, dtype=self.dtype))

    @property
    def natural_gradient_capable(self):
        return False

    def build_natural_gradient_stepper(self):
        """ If implemented, it should return a stepper usable by ClippedNaturalGradient optimizer """
        raise NotImplementedError()

    def _populate_symbols(self):
        """
        This function should return a dictionary with keys corresponding to symbols held by the module,
        and tensor values corresponding to the storage that should fill them.
        """
        raise NotImplementedError()

    def posterior_covariance(self, inducing_kernel, cross_kernel, data_kernel):
        """
        This function should evaluate the covariance for a data axis of shape ...JNI
        and return a covariance of shape ...JNN, i.e. the covariance should be full for all N, but
        marginalize for ...J dims. (representing a block diagonal covariance)

        inputs should represent evaluated functions:
        inducing_kernel = Kuu
        cross_kernel = Kfu
        data_kernel = Kff
        """
        raise NotImplementedError()

    def marginal_posterior_covariance(self, inducing_kernel, cross_kernel, data_kernel):
        """
        This function should evaluate the covariance for a data axis of shape ...JNI
        and return a covariance of shape ...JN1, i.e. the covariance should be marginalized over all
        dimensions (representing a diagonal covariance)

        inputs should represent evaluated functions:
        inducing_kernel = Kuu
        cross_kernel = Kfu
        data_kernel = Kff
        """
        raise NotImplementedError()

    def posterior_mean(self, inducing_kernel, cross_kernel, inducing_prior_mean=None, data_prior_mean=None ):
        """
        This function should evaluate the mean for a data axis of shape ...JNI
        and return a mean of shape ...JN1

        inputs should represent evaluated functions:
        inducing_kernel = Kuu
        cross_kernel = Kfu
        inducing_prior_mean = mu(Z)
        data_prior_mean = mu(X)
        """
        raise NotImplementedError()

    def kl(self, inducing_kernel, inducing_prior_mean=None):
        """
        This function should evaluate the kl between u-space posterior and u-space prior (represented by the inducing kernel)
        and return a covariance of shape ...JNN, i.e. the covariance should be full for all N, but
        marginalize for ...J dims.

        inputs should represent evaluated functions:
        inducing_kernel = Kuu
        inducing_prior_mean = mu(u)
        """
        raise NotImplementedError()

    def _init_exprs(self, inducing_kernel, inducing_prior_mean=None):
        """
        This function should provide expressions use to initialize the storage
        """
        raise NotImplementedError()

    def storage_init(self, *args):
        """ Given tensors that match the exprs from init_exprs, initialize the storage"""
        raise NotImplementedError()


class SqrtVarParameterization(AbstractVariationalParameterization):
    """
    This parameterization represents a MVN distribution which has an inducing covariance parameterized as:
     S = L @ L.t()
     m = m
    """
    def __init__(self, num_gps: int, num_inducing: int, name: str, jitter=None, dtype=torch.get_default_dtype()):
        """

        :param num_gps: number of independent processes
        :param num_inducing: number of inducing points per process
        :param name: a string to distinguish this parameterization's symbols from others like it that may coexist
        :param jitter (optional): a constant number added to the diagonal of inducing_kernel to ensure it is positive definite,
                       defaults to 1e-4, which works well for 32 bit. 64 bit can go down to ~1e-7
        :param dtype (optional): torch.float32 or torch.float64, defaults to 32 bit
        """
        super().__init__(num_gps, num_inducing, name, jitter=jitter, dtype=dtype)
        self.storage = BatchedMVNSqrtVar(num_gps, num_inducing, constrained=False, dtype=self.dtype)

        # the following are symbols owned by this module and will be populated by populate_symbols
        self.m = Tensor(self.name + '_inducing_q_mu')  # a batched vector JxMx1
        self.L = Tensor(self.name + '_inducing_q_sqrt')  # a batched matrix JxMxM
        self.jitter = Scalar(self.name + '_jitter')

    @property
    def natural_gradient_capable(self):
        return True

    def build_natural_gradient_stepper(self):
        """ Used to build a stepper for use in ClippedNaturalGradient optimizer"""
        source_mean = Tensor(self.name + '_source_mean')
        source_var = Tensor(self.name + '_source_var')
        self._xi_to_source = compile_expr(sqrtvar_to_source(self.m, self.L))
        self._source_to_xi = compile_expr(source_to_sqrtvar(source_mean, source_var, self.jitter))

        def stepper(lr, xi, norm_limit = 0.3):
            lr = general_fisher_norm_limited_ng_update(lr, xi, self.xi_to_source, self.source_to_xi,
                                                       norm_limit=norm_limit)
            return lr

        return stepper

    def _populate_symbols(self):
        m, L = self.storage.unpack()
        r = {
            self.name + '_inducing_q_mu': m,
            self.name + '_inducing_q_sqrt': L,
            self.name + '_jitter': self.jitter_val,
        }
        return r

    def posterior_covariance(self, inducing_kernel, cross_kernel, data_kernel):
        """
        A = Luu^-1 @ Kuf
        B =  Luu^-1 @ Lp
        C = A.t() @ B
        Sigma = Kff - A.t() @ A + C @ C.t()
        """
        kuu = AddJitter(inducing_kernel, self.jitter) #JMM
        Luu = CholeskyRoot(kuu) #JMM
        A = CholeskySolve(Luu, Transpose(cross_kernel)) #JMM @ ...JMN -> ...JMN
        B = CholeskySolve(Luu, self.L) #JMM
        C = Transpose(A) @ B #...JNM @ JMM -> ...JNM
        return data_kernel - Transpose(A) @ A + C @ Transpose(C) #...JNN

    def marginal_posterior_covariance(self, inducing_kernel, cross_kernel, data_kernel):
        """
        A = Luu^-1 @ Kuf
        B =  Luu^-1 @ Lp
        C = A.t() @ B
        diag(Sigma) = diag(Kff) - (1.t() @ (A*A)).t() + (C * C) @ 1
        """
        kuu = AddJitter(inducing_kernel, self.jitter) #JMM
        Luu = CholeskyRoot(kuu) #JMM
        A = CholeskySolve(Luu, Transpose(cross_kernel)) #JMM @ ...JMN -> ...JMN
        B = CholeskySolve(Luu, self.L) #JMM
        C = Transpose(A) @ B #...JNM @ JMM -> ...JNM
        D = ExtractDiagonal(data_kernel) #...JN1
        result = D - Transpose(PartialSum(A*A, Constant(-2))) + PartialSum(C*C, Constant(-1)) #...JN1
        return result

    def posterior_mean(self, inducing_kernel, cross_kernel, inducing_prior_mean=None, data_prior_mean=None):
        """
        A = Luu^-1 @ Kuf
        mu_diff = m - m0(Z)
        b = Luu^-1 @ (mu_diff)
        mu = m0(X) + A.t() @ b
        """
        kuu = AddJitter(inducing_kernel, self.jitter)  # JMM
        Luu = CholeskyRoot(kuu)  # JMM
        if inducing_prior_mean is not None:
            mu_diff = self.m - inducing_prior_mean  # JMD
        else:
            mu_diff = self.m
        A = Transpose(CholeskySolve(Luu, Transpose(cross_kernel)))  # JMM @ ...JMN -> ...JMN
        b = CholeskySolve(Luu, mu_diff)  # JMD
        if data_prior_mean is not None:
            result = data_prior_mean + A @ b # ...JND + ...JNM @ JMD -> ...JND
        else:
            result = A @ b  # ...JNM @ JMD -> ...JND
        return result

    def kl(self, inducing_kernel, inducing_prior_mean=None):
        """
            This function computes:
            KL( q(m,L) || N(inducing_prior_mean, inducing_kernel) ); L is assumed to have positive diagonal

            all matrices/vectors assumed evaluated on the same axis (usually inducing axis)

            :param inducing_prior_mean: prior mean evaluated at Z
            :param inducing_kernel: kernel evaluated at Z,Z
            """
        # note all batch dims should be just J, nothing more nor less, as evaluated tensors are on inducing space,
        # which has shape JMM
        kuu = AddJitter(inducing_kernel, self.jitter)
        Luu = CholeskyRoot(kuu)
        # log det = logsumdiag(Luu)
        # t1 = PartialSumNoKeepDims(Log(ExtractDiagonalNoKeepDims(Luu)),Constant(-1)) #JMM -> JM -> J
        t1 = PartialSumNoKeepDims(BatchLogSquareDiag(Luu), Constant(-1))  # JMM -> JM -> J
        # t2 = PartialSumNoKeepDims(Log(ExtractDiagonalNoKeepDims(self.L)),Constant(-1)) #JMM -> JM -> J
        t2 = PartialSumNoKeepDims(BatchLogSquareDiag(self.L),Constant(-1))  # JMM -> JM -> J
        A = CholeskySolve(Luu, self.L) #JMM -> JMM
        t3 = PartialSumNoKeepDims(PartialSumNoKeepDims(A * A, Constant(-1)),Constant(-1)) # JMM -> JM -> J
        if inducing_prior_mean is not None:
            mdiff = self.m - inducing_prior_mean # JM1
        else:
            mdiff = self.m
        b = CholeskySolve(Luu, mdiff)  # JM1
        t4 = SqueezeDim(SqueezeDim(Transpose(b) @ b,Constant(-1)),Constant(-1)) # J1M @ JM1 = J11 -> J
        t5 = Select(Constant(-1), Shape(Luu)) #scalar
        r = Constant(0.5) * (t1 - t2 + t3 + t4 - t5)  # J
        return r

    def xi_to_source(self, xi):
        m, L = self.storage.unpack(xi_packed=xi)
        leaves = {
            self.name + '_inducing_q_mu': m,
            self.name + '_inducing_q_sqrt': L,
        }
        m, S = self._xi_to_source(**leaves).expr
        return [m, S]

    def source_to_xi(self, m, S):
        leaves = {
            self.name + '_source_mean': m,
            self.name + '_source_var': S,
            self.name + '_jitter': self.jitter,
        }
        xi_unpacked = self._source_to_xi(**leaves).expr
        xi = self.storage.pack(xi_unpacked)
        if torch.is_tensor(xi):
            return [xi]
        else:
            return list(xi)

    def _init_exprs(self, inducing_kernel, inducing_prior_mean=None):
        Kuu = AddJitter(inducing_kernel, self.jitter)
        Luu = CholeskyRoot(Kuu)
        if inducing_prior_mean is not None:
            m = inducing_prior_mean
        else:
            m = ZerosLike(self.m)
        return m, Luu
        # return m, IdentityLike(self.L)

    def storage_init(self, *args):
        m = args[0]
        L = args[1]
        self.storage.custom_m_init(m)
        self.storage.custom_L_init(L)




class WhitenedSqrtVarParameterization(AbstractVariationalParameterization):
    """
    This parameterization represents a MVN distribution which has an inducing covariance parameterized as:
     S = Luu @ L @ L.t() @ Luu.t()
     m = Luu @ m

     for larger numbers of inducing points (e.g. > 200), whitening can help reduce slow convergence for vanilla gradient
     descent. Not recommended for natural gradient descent; use unwhitened for that as the precondition handles coupling
     of the elements.
    """
    def __init__(self, num_gps: int, num_inducing: int, name: str, jitter=None, dtype=torch.get_default_dtype()):
        """

        :param num_gps: number of independent processes
        :param num_inducing: number of inducing points per process
        :param name: a string to distinguish this parameterization's symbols from others like it that may coexist
        :param jitter (optional): a constant number added to the diagonal of inducing_kernel to ensure it is positive definite,
                       defaults to 1e-4, which works well for 32 bit. 64 bit can go down to ~1e-7
        :param dtype (optional): torch.float32 or torch.float64, defaults to 32 bit
        """
        super().__init__(num_gps, num_inducing, name, jitter=jitter, dtype=dtype)
        self.storage = BatchedMVNSqrtVar(num_gps, num_inducing, constrained=False, dtype=self.dtype)

        # the following are symbols owned by this module and will be populated by populate_symbols
        self.m = Tensor(self.name + '_inducing_q_mu')  # a batched vector JxMx1
        self.L = Tensor(self.name + '_inducing_q_sqrt')  # a batched matrix JxMxM
        self.jitter = Scalar(self.name + '_jitter')

    @property
    def natural_gradient_capable(self):
        return True

    def build_natural_gradient_stepper(self):
        """ Used to build a stepper for use in ClippedNaturalGradient optimizer"""
        source_mean = Tensor(self.name + '_source_mean')
        source_var = Tensor(self.name + '_source_var')
        self._xi_to_source = compile_expr(sqrtvar_to_source(self.m, self.L))
        self._source_to_xi = compile_expr(source_to_sqrtvar(source_mean, source_var, self.jitter))

        def stepper(lr, xi, norm_limit=0.3):
            lr = general_fisher_norm_limited_ng_update(lr, xi, self.xi_to_source, self.source_to_xi,
                                                       norm_limit=norm_limit)
            return lr

        return stepper

    def _populate_symbols(self):
        m, L = self.storage.unpack()
        r = {
            self.name+'_inducing_q_mu': m,
            self.name + '_inducing_q_sqrt': L,
            self.name + '_jitter': self.jitter_val,
        }
        return r

    def posterior_covariance(self, inducing_kernel, cross_kernel, data_kernel):
        """
        Sigma =  Kff + Kfu @ Kuu^-1 @ (S - Kuu) @ Kuu^-1 @ Kuf
              =  Kff + Kfu @ Luu^-T @ Luu^-1 @ Luu @ (Lp @ Lp.t() - I) Luu.t() @ Luu^-T @ Luu^-1 @ Kuf
              =  Kff + Kfu @ Luu^-T @ (Lp @ Lp.t() - I) @ Luu^-1 @ Kuf
        A = Luu^-1 @ Kuf
              =  Kff + A.t() @ (Lp @ Lp.t() - I) @ A
        B = Lp @ Lp.t() - I
              =  Kff + A.t() @ B @ A
        """
        kuu = AddJitter(inducing_kernel, self.jitter) #JMM -> JMM
        Luu = CholeskyRoot(kuu) #JMM -> JMM
        # JMM @ ...JMN -> ...JMN (I did test that cholesky solve broadcasts in this scenario)
        A = CholeskySolve(Luu, Transpose(cross_kernel))
        # Swhite = self.L @ Transpose(self.L)
        # B = Swhite - IdentityLike(self.L)  # JMM
        # C = Transpose(A) @ B @ A  #...JNM @ JMM -> ...JNM
        # return data_kernel + C #...JNN
        return data_kernel - Transpose(A) @ A + Transpose(A) @ self.L @ Transpose(self.L) @ A

    def marginal_posterior_covariance(self, inducing_kernel, cross_kernel, data_kernel):
        """
        Sp = Luu @ Lp @ Lp.t() @ Luu.t()
        Sp - Luu = Luu @ (Lp @ Lp.t() - I) @ Luu.t()
        Sigma =  Kff + Kfu @ Kuu^-1 @ (Sp - Kuu) @ Kuu^-1 @ Kuf
              =  Kff + Kfu @ Luu^-T @ Luu^-1 @ Luu @ (Lp @ Lp.t() - I) @ Luu.t() @ Luu^-T @ Luu^-1 @ Kuf
              =  Kff + Kfu @ Luu^-T @ (Lp @ Lp.t() - I) @ Luu^-1 @ Kuf
        A = Luu^-1 @ Kuf
              =  Kff + A.t() @ (Lp @ Lp.t() - I) @ A
        B = Lp @ Lp.t() - I
              =  Kff + A.t() @ B @ A
        diag(Sigma) = diag(Kff) - (1.t() @ (A * (B @ A))).t()

        A = Luu^-1 @ Kuf
        B =  Luu^-1 @ Luu @ Lp = Lp
        C = A.t() @ B
        diag(Sigma) = diag(Kff) - (1.t() @ (A*A)).t() + (C * C) @ 1
        """
        kuu = AddJitter(inducing_kernel, self.jitter)  # JMM -> JMM
        Luu = CholeskyRoot(kuu)  # JMM -> JMM
        # JMM @ ...JMN -> ...JMN (I did test that cholesky solve broadcasts in this scenario)
        A = CholeskySolve(Luu, Transpose(cross_kernel))
        # Swhite = self.L @ Transpose(self.L)
        # B = Swhite - IdentityLike(self.L)  # JMM
        # result = ExtractDiagonal(data_kernel) + Transpose(PartialSum(A * (B @ A), Constant(-2)))
        D = ExtractDiagonal(data_kernel) #...JN1
        B = Transpose(A) @ self.L
        result = D - Transpose(PartialSum(A * A, Constant(-2))) + \
                 PartialSum(B * B, Constant(-1))

        # kuu = AddJitter(inducing_kernel, self.jitter) #JMM
        # Luu = CholeskyRoot(kuu) #JMM
        # A = CholeskySolve(Luu, Transpose(cross_kernel)) #JMM @ ...JMN -> ...JMN
        # B = self.L #JMM
        # C = Transpose(A) @ B #...JNM @ JMM -> ...JNM
        # D = ExtractDiagonal(data_kernel) #...JN1
        # result = D - Transpose(PartialSum(A*A, Constant(-2))) + PartialSum(C*C, Constant(-1)) #...JN1
        return result

    def posterior_mean(self, inducing_kernel, cross_kernel, inducing_prior_mean=None, data_prior_mean=None):
        """
        A = Luu^-1 @ Kuf
        mu = m0(X) + A.t() @ b = m0(X) + Kfu @ m - Luu^-1 @ m0(Z)
        """
        kuu = AddJitter(inducing_kernel, self.jitter)
        Luu = CholeskyRoot(kuu)
        A = Transpose(CholeskySolve(Luu, Transpose(cross_kernel)))
        if inducing_prior_mean is not None:
            mdiff = self.m - CholeskySolve(Luu, inducing_prior_mean)
        else:
            mdiff = self.m
        if data_prior_mean is not None:
            result = data_prior_mean + A @ mdiff
        else:
            result = A @ mdiff
        return result

    def kl(self, inducing_kernel, inducing_prior_mean = None):
        """
            This function computes:
            KL( q(m,L) || N(inducing_prior_mean, inducing_kernel) );

            all matrices/vectors assumed evaluated on the same axis (usually inducing axis)

            :param inducing_prior_mean: prior mean evaluated at Z
            :param inducing_kernel: kernel evaluated at Z,Z
            """
        kuu = AddJitter(inducing_kernel, self.jitter)
        Luu = CholeskyRoot(kuu)
        #log det term; prior term disappears in whitened representation
        # t2 = PartialSumNoKeepDims(Log(ExtractDiagonalNoKeepDims(self.L)), Constant(-1))  # JMM -> JM -> J
        t2 = PartialSumNoKeepDims(BatchLogSquareDiag(self.L),Constant(-1))  # JMM -> JM -> J
        #trace(Kuu^-1 @ Luu @ L @ L.t() @ Luu.t()) -> trace(L @ L.t()) -> Sum(L * L)
        t3 = PartialSumNoKeepDims(PartialSumNoKeepDims(self.L * self.L, Constant(-1)), Constant(-1))  # JMM -> JM -> J
        if inducing_prior_mean is not None:
            # m.t() @ Kuu @ m ::  J1M @ JM1 -> J11 -> J
            t4a = SqueezeDim(SqueezeDim(Transpose(self.m) @ inducing_kernel @ self.m,Constant(-1)),Constant(-1))
            # m0.t() @ m :: J1M @ JM1 -> J11 -> J
            t4b = SqueezeDim(SqueezeDim(Transpose(self.m) @ inducing_prior_mean,Constant(-1)),Constant(-1))
            # m0.t() @ Kuu^-1 @ m0
            t4c = SqueezeDim(SqueezeDim(Transpose(inducing_prior_mean) @
                                        CholeskySolveT(Luu, CholeskySolve(Luu, inducing_prior_mean)),
                                        Constant(-1)),Constant(-1)) #J1M @ JM1 -> J11 - > J
            t4 = t4a - Constant(2.0)*t4b + t4c
        else:
            t4 = SqueezeDim(SqueezeDim(Transpose(self.m) @ inducing_kernel @ self.m, Constant(-1)), Constant(-1))
        t5 = Select(Constant(-1), Shape(Luu))  # scalar
        r = Constant(0.5) * (-t2 + t3 + t4 - t5)  # J
        return r

    def xi_to_source(self, xi):
        m, L = self.storage.unpack(xi_packed=xi)
        leaves = {
            self.name + '_inducing_q_mu': m,
            self.name + '_inducing_q_sqrt': L,
        }
        m, S = self._xi_to_source(**leaves).expr
        return [m, S]

    def source_to_xi(self, m, S):
        leaves = {
            self.name + '_source_mean': m,
            self.name + '_source_var': S,
            self.name + '_jitter': self.jitter,
        }
        xi_unpacked = self._source_to_xi(**leaves).expr
        xi = self.storage.pack(xi_unpacked)
        if torch.is_tensor(xi):
            return [xi]
        else:
            return list(xi)

    def _init_exprs(self, inducing_kernel, inducing_prior_mean=None):
        Kuu = AddJitter(inducing_kernel, self.jitter)
        Luu = CholeskyRoot(Kuu)
        if inducing_prior_mean is not None:
            m = CholeskySolveT(Luu, CholeskySolve(Luu, inducing_prior_mean))

        else:
            m = ZerosLike(self.m)
        return m, IdentityLike(self.L)

    def storage_init(self, *args):
        m = args[0]
        L = args[1]
        self.storage.custom_m_init(m)
        self.storage.custom_L_init(L)

def variational_parameterization_factory(dict_in, **kwargs):
    """

    :param dict_in: Something like {'SqrtVar': {'num_gps': 2, 'num_inducing': 128, 'dtype': torch.float64}}
    :param kwargs: handles special kwargs that may be over-ridden by higher level constructors. This is provided
                    to define a way to over-write certain keys without knowing the potentially nested structure
                    of the factory dict_in. The factory handles the insertion of these special keys into the structure.
    :return:
    """
    registry = {
        'SqrtVar': SqrtVarParameterization,
        'WhitenedSqrtVar': WhitenedSqrtVarParameterization,
    }
    num_gps = None if not 'num_gps' in kwargs else int(kwargs['num_gps'])
    dtype = None if not 'dtype' in kwargs else kwargs['dtype']
    for key in dict_in.keys():
        if key in registry.keys():
            d = dict_in[key]
            # over-write special keys if they are present
            if dtype is not None:
                d['dtype'] = dtype
            if num_gps is not None:
                d['num_gps'] = num_gps
            return registry[key](**d)
