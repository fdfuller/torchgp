import torch
from .layers import *
from .gpmm import GPMM
from .sde_flow import *
from .non_stationary import *
from .util import dtype_str_to_dtype
from .latent_layers import *

__all__ = [
    'layer_factory',
    'layer_sequential_factory',
    'output_concatenation_layer_factory',
    'skip_layer_factory',
    'product_scale_layer_factory',
    'ti_sde_flow_factory',
    'gpmm_factory',
    'layer_registry',
    'gpmm_sase_aux_factory'
]


def layer_registry(string_in):
    # used to handle various types of layers
    registry = {
        'Layer': layer_factory,
        'LayerSequential': layer_sequential_factory,
        'SkipLayer': skip_layer_factory,
        'OutputConcatenationLayer': output_concatenation_layer_factory,
        'GPMM': gpmm_factory,
        'SliceInputLayer': slice_layer_factory,
        'ProductScaleLayer': product_scale_layer_factory,
        'TimeInvariantSDEFlow': ti_sde_flow_factory,
        'TimeInvariantSDEFlowConstInit': tici_sde_flow_factory,
        'GPMM_sase_aux': gpmm_sase_aux_factory,
        'AuxInpToKernelLayer': aux_in_2k_layer_factory,
        'NSBiLayer': ns_bilayer_factory,
        'MeanFieldGaussianLatentLayer': mfg_latent_layer_factory,
        'MeanFieldGaussianLatentLayerAxisDependent': mfg_ad_latent_layer_factory,
        'MeanFieldGaussianLatentLayerND': mfg_nd_latent_layer_factory,
        'CorrelateLatentLayer': correlate_latent_layer_factory,
        'FixMarginalizationState': fix_marg_state_factory,
        'AmortizedMeanFieldGaussianLatentLayer': amortized_latent_layer_factory,
        'AuxSkipLayer': aux_skip_layer_factory,
    }
    if string_in in registry:
        return registry[string_in]
    else:
        ValueError('kind not understood')


def layer_factory(dict_in):
    # filter out the construction keys by a known registry.
    # the key indicates a known value and the value indicates a function that is used to process the key
    # None indicates the key needs no further processing
    construction_registry = {'num_gps': int,
                             'num_inducing': int,
                             'in_features': int,
                             'out_features': int,
                             }
    leaves = {}
    for k ,v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]

    default_registry = {
        'dtype': dtype_str_to_dtype,
        'name': None,
    }
    for k ,v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = Layer(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def layer_sequential_factory(dict_in):
    construction_registry = {'layers': lambda ls: [layer_registry(l['kind'])(l) for l in ls]}
    leaves = {}
    for k ,v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    layers = leaves.pop('layers')
    L = LayerSequential(*layers, **leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def skip_layer_factory(dict_in):
    construction_registry = {'layer': lambda l: layer_registry(l['kind'])(l)}
    leaves = {}
    for k ,v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = SkipLayer(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def aux_skip_layer_factory(dict_in):
    construction_registry = {'layer': lambda l: layer_registry(l['kind'])(l),
                             'x_features': int}
    leaves = {}
    for k ,v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = AuxSkipLayer(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def slice_layer_factory(dict_in):
    construction_registry = {'layer': lambda l: layer_registry(l['kind'])(l),
                             'dim_slice': lambda x: slice(*x),
                             'in_features': int}
    leaves = {}
    for k ,v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]

    default_registry = {'name': None}
    for k ,v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = SliceInputLayer(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def output_concatenation_layer_factory(dict_in):
    construction_registry = {'layers': lambda ls: [layer_registry(l['kind'])(l) for l in ls]}
    leaves = {}
    for k ,v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    layers = leaves.pop('layers')
    L = OutputConcatenationLayer(*layers, **leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L

def product_scale_layer_factory(dict_in):
    construction_registry = {'layers': lambda ls: [layer_registry(l['kind'])(l) for l in ls]}
    leaves = {}
    for k ,v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    layers = leaves.pop('layers')
    L = ProductScaleLayer(*layers, **leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def gpmm_factory(dict_in):
    construction_registry = {
        'layer': lambda l: layer_registry(l['kind'])(l),
        'input_axis_len': int,
        'input_features': int,
        'output_axis_len': int,
    }
    leaves = {}
    for k ,v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]

    default_registry = {
        'dtype': dtype_str_to_dtype,
    }
    for k, v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = GPMM(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L

def gpmm_sase_aux_factory(dict_in):
    construction_registry = {
        'layer': lambda l: layer_registry(l['kind'])(l),
        'input_axis_len': int,
        'input_features': int,
        'output_axis_len': int,
        'aux_scale': float,
        'aux_center': float,
    }
    leaves = {}
    for k ,v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]

    default_registry = {
        'dtype': dtype_str_to_dtype,
    }
    for k, v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = GPMM(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L

def ti_sde_flow_factory(dict_in):
    construction_registry = {
        'layer': lambda l: layer_registry(l['kind'])(l),
        'delta_t': float,
        'nsteps': int,
    }
    leaves = {}
    for k, v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]

    default_registry = {
        'dtype': dtype_str_to_dtype,
    }
    for k, v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = TimeInvariantSDEFlow(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def tici_sde_flow_factory(dict_in):
    construction_registry = {
        'layer': lambda l: layer_registry(l['kind'])(l),
        'delta_t': float,
        'nsteps': int,
        'constant': float,
    }
    leaves = {}
    for k, v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]

    default_registry = {
        'dtype': dtype_str_to_dtype,
    }
    for k, v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = TimeInvariantSDEFlowConstInit(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def aux_in_2k_layer_factory(dict_in):
    # filter out the construction keys by a known registry.
    # the key indicates a known value and the value indicates a function that is used to process the key
    # None indicates the key needs no further processing
    construction_registry = {'num_gps': int,
                             'num_inducing': int,
                             'in_features': int,
                             'out_features': int,
                             }
    leaves = {}
    for k ,v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]

    default_registry = {
        'dtype': dtype_str_to_dtype,
        'name': None,
    }
    for k ,v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = AuxInpToKernelLayer(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L

def ns_bilayer_factory(dict_in):
    construction_registry = {'layers': lambda ls: [layer_registry(l['kind'])(l) for l in ls]}
    leaves = {}
    for k ,v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    layers = leaves.pop('layers')
    L = NSBiLayer(*layers, **leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def mfg_latent_layer_factory(dict_in):
    # filter out the construction keys by a known registry.
    # the key indicates a known value and the value indicates a function that is used to process the key
    # None indicates the key needs no further processing
    construction_registry = {'num_features': int,
                             }
    leaves = {}
    for k, v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]

    default_registry = {
        'dtype': dtype_str_to_dtype,
        'name': None,
    }
    for k, v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = MeanFieldGaussianLatentLayer(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def mfg_ad_latent_layer_factory(dict_in):
    # filter out the construction keys by a known registry.
    # the key indicates a known value and the value indicates a function that is used to process the key
    # None indicates the key needs no further processing
    construction_registry = {'in_features': int,
                             'out_features': int,
                             'axis_shape': lambda xs: tuple([int(x) for x in xs]),
                             }
    leaves = {}
    for k, v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]

    default_registry = {
        'dtype': dtype_str_to_dtype,
        'name': None,
    }
    for k, v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = MeanFieldGaussianLatentLayerAxisDependent(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L

def mfg_nd_latent_layer_factory(dict_in):
    # filter out the construction keys by a known registry.
    # the key indicates a known value and the value indicates a function that is used to process the key
    # None indicates the key needs no further processing
    construction_registry = {'num_obs': int,
                             'num_features': int,
                             }
    leaves = {}
    for k, v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]

    default_registry = {
        'dtype': dtype_str_to_dtype,
        'name': None,
    }
    for k, v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = MeanFieldGaussianLatentLayerND(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def correlate_latent_layer_factory(dict_in):
    construction_registry = {'layer': lambda l: layer_registry(l['kind'])(l)}
    leaves = {}
    for k ,v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    default_registry = {
        'name': None,
    }
    for k, v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = CorrelateLatentLayer(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def fix_marg_state_factory(dict_in):
    # filter out the construction keys by a known registry.
    # the key indicates a known value and the value indicates a function that is used to process the key
    # None indicates the key needs no further processing
    construction_registry = {'layer': lambda l: layer_registry(l['kind'])(l),
                             'marginalize_input': lambda x: bool(x) if x is not None else None,
                             'marginalize_output': lambda x: bool(x) if x is not None else None,
                             }
    leaves = {}
    for k, v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]

    default_registry = {
        'name': None,
    }
    for k, v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = FixMarginalizationState(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L


def amortized_latent_layer_factory(dict_in):
    # filter out the construction keys by a known registry.
    # the key indicates a known value and the value indicates a function that is used to process the key
    # None indicates the key needs no further processing
    construction_registry = {'in_features': int,
                             'out_features': int,
                             }
    leaves = {}
    for k, v in construction_registry.items():
        if k not in dict_in:
            raise ValueError(f'key {k} is required for construction')
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]

    default_registry = {
        'dtype': dtype_str_to_dtype,
        'name': None,
    }
    for k, v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    L = AmortizedMeanFieldGaussianLatentLayer(**leaves)
    L.update_from_dict(dict_in)
    L.build()
    return L
