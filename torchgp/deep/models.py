import torch
from .batched_likelihood import *
from .layers import *
from .latent_layers import *
from typing import Union
from torch.optim import Adam
from abc import ABC, abstractmethod
from .symbolic_shape import *
import collections
import numpy as np
from .util import dtype_str_to_dtype, extract_concrete_layers
from .layer_factories import layer_registry
from operator import add
from functools import reduce

__all__ = ["Model"]


class AbstractModel(torch.nn.Module, ABC):
    """
    Abstract class for a model that takes a single input tensor and single output tensor
    """

    def __init__(self, name: str = '', dtype=torch.get_default_dtype()):
        super().__init__()
        self.name = str(name)
        self.dtype = dtype
        self.nobs = None
        self.multisamples = None
        self.layer = None
        self.likelihood = None
        self.symbolic_input_shape = None
        self.symbolic_output_Shape = None
        self._build_config = {'name': self.name, 'dtype': str(self.dtype)}

    def update_layer(self, layer: Union[AbstractLayerContainer, AbstractLayerWrapper, Layer, dict], **unused):
        if isinstance(layer, dict):
            # passing layer in as a build_config, rather than an instantiated layer
            layer = layer_registry(layer['kind'])(layer)
        elif isinstance(layer, (AbstractLayerContainer, AbstractLayerWrapper, Layer)):
            pass
        else:
            raise ValueError(f'unexpected type for layer, got: {type(layer)}')
        self.layer = layer
        self._build_config['layer'] = {'arg': layer.build_config, 'kwargs': {}}
        return self

    def update_likelihood(self, likelihood: Union[AbstractAnalyticLikelihood, AbstractMCLikelihood, dict], **unused):
        if isinstance(likelihood, dict):
            # passing layer in as a build_config, rather than an instantiated layer
            likelihood = likelihood_factory(likelihood)
        elif isinstance(likelihood, (AbstractAnalyticLikelihood, AbstractMCLikelihood)):
            pass
        else:
            raise ValueError(f'unexpected type for layer, got: {type(likelihood)}')
        self.likelihood = likelihood
        self._build_config['likelihood'] = {'arg': likelihood.build_config, 'kwargs': {}}
        return self

    def update_nobs(self, nobs: int, **unused):
        self.nobs = int(nobs)
        self._build_config['nobs'] = {'arg': int(nobs), 'kwargs': unused}
        return self

    def update_multisamples(self, num: int, **unused):
        self.multisamples = num
        self._build_config['multisamples'] = {'arg': int(num), 'kwargs': unused}
        return self

    def update_symbolic_input_shape(self, shape, **unused):
        self.symbolic_input_shape = shape
        assert len(self.symbolic_input_shape) == len(list(flatten(self.symbolic_input_shape))), \
            "expected input shape to be flattened (i.e. no merged axes)"
        self._build_config['symbolic_input_shape'] = {'arg': shape, 'kwargs': unused}
        return self

    def update_symbolic_output_shape(self, shape, **unused):
        self.symbolic_output_shape = shape
        assert len(self.symbolic_output_shape) == len(list(flatten(self.symbolic_output_shape))), \
            "expected output shape to be flattened (i.e. no merged axes)"
        self._build_config['symbolic_output_shape'] = {'arg': shape, 'kwargs': unused}
        return self

    def update_from_dict(self, dict_in):
        for key in dict_in:
            if hasattr(self, 'update_' + str(key)):
                if dict_in[key]['arg'] is not None:
                    getattr(self, 'update_' + str(key))(dict_in[key]['arg'], **dict_in[key]['kwargs'])
                else:
                    getattr(self, 'update_' + str(key))(**dict_in[key]['kwargs'])
        return self

    def build_variational_optimizer(self, default_lr, **kwargs):
        """
        Use this to obtain a list of variational parameter optimizers. Will apply default_lr
        and kwargs to all optimizers. Returns an optimizer for the variational parameters
        of each layer. Hold onto those optimizers, rather than calling this method
        everytime, since this method will construct them from scratch and you will lose
        state.

        ex:
        variational_ops = model.build_variational_optimizer(default_lr=1e-3)
        ...
        for vop in variational_ops:
            vop.zero_grad()
        loss = model.loss()
        loss.backward()
        for vop in variational_ops:
            vop.step()
        """
        ops = list(self.layer.build_variational_optimizer(default_lr, **kwargs))
        return ops

    def build_hyper_optimizer(self, default_lr, **kwargs):
        """
        Use this to obtain a list of hyper parameter optimizers. Will apply default_lr
        and kwargs to all optimizers. Returns an optimizer for the hyper parameters
        of each layer. Hold onto those optimizers, rather than calling this method
        everytime, since this method will construct them from scratch and you will lose
        state.

        ex:
        hyper_ops = model.build_hyper_optimizer(default_lr=1e-3, inducing_lr = 1e-5)
        ...
        for hop in hyper_ops:
            hop.zero_grad()
        loss = model.loss()
        loss.backward()
        for hop in hyper_ops:
            hop.step()
        """
        lik_op = Adam([{'params': self.likelihood.parameters(), 'lr': default_lr}])
        ops = list(self.layer.build_hyper_optimizer(default_lr, **kwargs))
        ops.append(lik_op)
        return ops

    @property
    def build_config(self):
        return self._build_config

    @property
    @abstractmethod
    def ready_to_build(self):
        pass

    @abstractmethod
    def build(self, **kwargs):
        pass

    @abstractmethod
    def loss(self, inputs, outputs, **kwargs):
        pass

    @abstractmethod
    def predict(self, inputs, **kwargs):
        pass


def find_num_layers(layer, count = 0):
    if hasattr(layer, 'layers'):
        for el in layer.layers:
            count = find_num_layers(el, count = count)
    else:
        count += 1
    return count


def add_metadata_post_process(propagated_shape, permute_plan, expansion_plan):
    """Father forgive me for this bullshit"""
    def _post_process(t):
        return t, (propagated_shape, permute_plan, expansion_plan)
    return _post_process


class Model(AbstractModel):
    def __init__(self,
                 name: str = '',
                 dtype=torch.get_default_dtype(), **unused):
        super().__init__(name, dtype)
        self._build_config['kind'] = type(self).__name__

    @property
    def ready_to_build(self):
        flag = False
        if (self.layer is not None and
            self.likelihood is not None and
            self.nobs is not None and
            self.multisamples is not None and
            self.symbolic_input_shape is not None and
            self.symbolic_output_shape is not None):
                flag = True
        return flag

    def build(self):
        if self.ready_to_build:
            # set marginalization state from likelihood
            self.layer.update_marginalization(**self.likelihood.requirements)
            # propagate multisamples down to layers
            nlayers = find_num_layers(self.layer)
            if nlayers>1 or isinstance(self.likelihood, AbstractMCLikelihood):
                self.layer.update_multisamples(self.multisamples)
            else:
                # this special case is added because symbolic shape cannot know how many layers are in the model
                # and will always include the  multisample index if multisamples > 1. However, a single layer
                # with an analytic likelihood will never evaluate multisampling, as this has no meaning in that
                # case (the mean/cov of the GP is computed without sampling and passed directly to the likelihood).
                # This is a bit of a hack for that case.
                self.multisamples = 1
                self.layer.update_multisamples(1)
            self.layer.build()
            self.propagated_mean_shape = propagate_shape(self.symbolic_input_shape, self.layer.mean_shape)
            self.propagated_cov_shape = propagate_shape(self.symbolic_input_shape, self.layer.cov_shape)
            self.propagated_sample_shape = propagate_shape(self.symbolic_input_shape, self.layer.sample_shape)
            if isinstance(self.likelihood, AbstractAnalyticLikelihood):
                # flatten the shape (we will unmerge_all the tensor as well)
                layer_cov_shape = list(flatten(self.propagated_cov_shape))
                layer_mean_shape = list(flatten(self.propagated_mean_shape))
                # make all symbols unique in covariance (where repeat symbols can happen)
                layer_cov_shape = convert_repeated_symbols(layer_cov_shape)
                self._mean_permute_plan = permute_plan(layer_mean_shape, self.likelihood.symbolic_mean_shape)
                self._cov_permute_plan = permute_plan(layer_cov_shape,
                                                      convert_repeated_symbols(self.likelihood.symbolic_cov_shape))
                self._output_permute_plan = permute_plan(self.symbolic_output_shape,
                                                         self.likelihood.symbolic_mean_shape)
                mean_shape_reshaped = list(np.array(layer_mean_shape)[self._mean_permute_plan])
                output_shape_reshaped = list(np.array(self.symbolic_output_shape)[self._output_permute_plan])
                # saving these for diagnostic purposes
                self._mean_shape_reshaped = mean_shape_reshaped
                self._cov_shape_reshaped = list(np.array(layer_cov_shape)[self._cov_permute_plan])
                self._output_shape_reshaped = output_shape_reshaped
                self._output_expansion_plan = expansion_plan(output_shape_reshaped, mean_shape_reshaped)
                if isinstance(self.likelihood, AnalyticMOGaussianLikelihoodIW):
                    # a special case
                    latent_layers = [l for l in extract_concrete_layers(self) if isinstance(l, AbstractLatentLayer)]
                    for l in latent_layers:
                        # This will add a tuple of information to the return of the importance weights
                        # we can then use this tuple, coupled with replacements, in the loss function
                        # to expand and permute the weights to line up appropriately
                        propagated_iw_shape = list(flatten(propagate_shape(self.symbolic_input_shape, l.iw_shape)))
                        iw_permute_plan = permute_plan_incomplete(propagated_iw_shape,
                                                                  self.likelihood.symbolic_mean_shape)
                        iw_shape_reshaped = list(np.array(propagated_iw_shape)[iw_permute_plan])
                        iw_expansion_plan = expansion_plan(iw_shape_reshaped, mean_shape_reshaped)
                        l._post_process_iw = add_metadata_post_process(propagated_iw_shape, iw_permute_plan,
                                                                       iw_expansion_plan)
                    self._iw_ind = self._mean_shape_reshaped.index('W')
            elif isinstance(self.likelihood, AbstractMCLikelihood):
                # flatten the sample shape
                layer_sample_shape = list(flatten(self.propagated_sample_shape))
                self._sample_permute_plan = permute_plan(layer_sample_shape, self.likelihood.symbolic_sample_shape)
                self._output_permute_plan = permute_plan(self.symbolic_output_shape,
                                                     self.likelihood.symbolic_sample_shape)
                sample_shape_reshaped = list(np.array(layer_sample_shape)[self._sample_permute_plan])
                output_shape_reshaped = list(np.array(self.symbolic_output_shape)[self._output_permute_plan])
                #saving these for diagnoistic purposes
                self._sample_shape_reshaped = sample_shape_reshaped
                self._output_shape_reshaped = output_shape_reshaped
                self._output_expansion_plan = expansion_plan(output_shape_reshaped, sample_shape_reshaped)
            self._build_config['layer'] = {'arg': self.layer.build_config, 'kwargs': {}}
            self._build_config['likelihood'] = {'arg': self.likelihood.build_config, 'kwargs': {}}

        else:
            if self.likelihood is None:
                print('no likelihood specified, use update_likelihood to add one')
            if self.layer is None:
                print('no GP layers specified, use update_layer to add one')
            if self.nobs is None:
                print('no nobs specified, use update_nobs to add it')
            if self.multisamples is None:
                print('number of samples not specified, use update_nsamples to add it')
            if self.symbolic_input_shape is None:
                print('symbolic shape of input not specified, use update_symbolic_input_shape to add it')
            if self.symbolic_output_shape is None:
                print('symbolic shape of output not specified, use update_symbolic_output_shape to add it')
            raise ValueError('not yet ready to build')

    def loss(self, inputs, outputs, **kwargs):
        """

        :param inputs: a tensor
        :param outputs: a tensor
        :return:
        """
        assert torch.is_tensor(inputs), "expected inputs to be a tensor"
        assert torch.is_tensor(outputs), "expected outputs to be a tensor"

        replacements = populate_replacements_from_observables((self.symbolic_input_shape, list(inputs.shape)),
                                                              (self.symbolic_output_shape, list(outputs.shape)))

        if isinstance(self.likelihood, AbstractAnalyticLikelihood):
            mu, var, reg = self.layer.components(inputs, **kwargs)
            mu_unmerged = unmerge_all(mu, self.propagated_mean_shape, replacements)
            var_unmerged = unmerge_all(var, self.propagated_cov_shape, replacements)
            mu_reshaped = mu_unmerged.permute(*self._mean_permute_plan)
            var_reshaped = var_unmerged.permute(*self._cov_permute_plan)
            outputs_reshaped = outputs.permute(*self._output_permute_plan)
            outputs_reshaped = unsqueeze_multiple(outputs_reshaped, self._output_expansion_plan)
            if isinstance(self.likelihood, AnalyticMOGaussianLikelihoodIW):
                fixed_iws = []
                for el in reg.iw:  # merging should always make this a list
                    iw, meta = el
                    iw_unmerged = unmerge_all(iw, meta[0], replacements)
                    iw_reshaped = iw_unmerged.permute(*meta[1])
                    fixed_iws.append(unsqueeze_multiple(iw_reshaped, meta[2]))
                # for multiple iws, we should compute their product, but these are log_iws, so we sum
                fixed_iw = reduce(add, fixed_iws)
                ob, mb, vb, iwf = torch.broadcast_tensors(outputs_reshaped, mu_reshaped, var_reshaped, fixed_iw)
                variational_expectation = self.likelihood.expected_log_likelihood(ob, mb, vb, log_iw=iwf,
                                                                                  dim=self._iw_ind)
            else:
                ob, mb, vb = torch.broadcast_tensors(outputs_reshaped, mu_reshaped, var_reshaped)
                variational_expectation = self.likelihood.expected_log_likelihood(ob, mb, vb)


        elif isinstance(self.likelihood, AbstractMCLikelihood):
            sample, reg = self.layer(inputs, **kwargs)

            sample_unmerged = unmerge_all(sample, self.propagated_sample_shape, replacements)
            sample_reshaped = sample_unmerged.permute(*self._sample_permute_plan)
            outputs_reshaped = outputs.permute(*self._output_permute_plan)
            outputs_reshaped = unsqueeze_multiple(outputs_reshaped, self._output_expansion_plan)
            ob, sb = torch.broadcast_tensors(outputs_reshaped, sample_reshaped)
            variational_expectation = self.likelihood.expected_log_likelihood(ob, sb)
        else:
            raise TypeError(f'expected either AbstractAnalyticLikelihood or AbstractMCLikelihood,'
                            f' got: {type(self.likelihood)}')
        # scale up variational_expectation due to minibatching. number of batch samples for inputs should be given in
        # -2 dim.
        loss = -variational_expectation*self.nobs + reg.kl.sum()
        return loss

    def predict(self, inputs, **kwargs):
        multisamples = 1 if 'multisamples' not in kwargs else kwargs['multisamples']
        z = self.layer(inputs, multisamples=multisamples)
        return z

def model_factory(dict_in):
    default_registry = {
        'dtype': dtype_str_to_dtype,
        'name': None,
    }
    leaves = {}
    for k, v in default_registry.items():
        if k not in dict_in:
            continue
        else:
            leaves[k] = v(dict_in[k]) if v is not None else dict_in[k]
    if 'kind' not in dict_in:
        raise ValueError('expected to find key kind in dict_in')
    m = Model(**leaves)
    m.update_from_dict(dict_in)
    m.build()
    return m
