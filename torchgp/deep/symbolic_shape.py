from numbers import Integral
import copy
import collections
from functools import reduce
from operator import mul
from .util import split_axis
import numpy as np
from .util import to_neg
import torch

__all__ = ["propagate_shape", "concretize_shape", "replace_nested", "conditional_replace_nested",
           "flatten", "traverse", 'unmerge_all', 'permute_list', 'permute_plan', 'convert_repeated_symbols',
           'unsqueeze_multiple', 'expansion_plan', 'populate_replacements_from_observables', 'plan_broadcast',
           'plan_expand_and_permute', 'plan_expand_and_permute_ND_special', 'adjust_plan', 'permute_plan_incomplete']


def traverse(o, tree_types=(list, tuple)):
    if isinstance(o, collections.Iterable) and not isinstance(o, (str, bytes)):
        for k, value in enumerate(o):
            for subvalue in traverse(value, tree_types):
                yield k, subvalue
    else:
        yield o


def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el


def neg_integer_condition(x):
    return isinstance(x, Integral) and x < 0

# Warning: the following will mutate target


def _flat_insert(l, index, l2):
    assert isinstance(l, collections.Iterable) and not isinstance(l, (str, bytes)), f"got {l}"
    if isinstance(l2, collections.Iterable) and not isinstance(l2, (str, bytes)):
        assert len(l)>index, f"got {len(l)}"
        if len(l2)>0:  # cover case where l2 = []
            l[index] = l2[0]
            for k,el2 in enumerate(l2[1:]):
                l.insert(index+k+1,el2)
        else:
            l[index] = []  # we let this case pass through
    else:
        l[index] = l2


def _delete_empty_lists(l):
    return [el for el in l if not el == []]


def _conditional_replace_address(target, address, condition, replacements):
    if isinstance(target, collections.Iterable) and not isinstance(target, (str, bytes)):
        v = address[0]
        if isinstance(target[v], collections.Iterable) and not isinstance(target[v], (str, bytes)):
            address = address[1:]
            _conditional_replace_address(target[v], address, condition, replacements)
        else:
            if condition(target[v]) and address[-1] >= -len(replacements):
                _flat_insert(target, v, replacements[address[-1]])


def _replace_address(target, address, replacements):
    if isinstance(target, collections.Iterable) and not isinstance(target, (str, bytes)):
        v = address[0]
        if isinstance(target[v], collections.Iterable) and not isinstance(target[v], (str, bytes)):
            address = address[1:]
            _replace_address(target[v], address, replacements)
        else:
            if address[-1] in replacements:
                _flat_insert(target, v, replacements[address[-1]])


# Added deep copy to prevent mutation of args; public api should be safe

def conditional_replace_nested(target, condition, replacements):
    target = copy.deepcopy(target)
    replacements = copy.deepcopy(replacements)
    for item in traverse(target):
        address = list(flatten(item))
        ct = target
        _conditional_replace_address(ct, address, condition, replacements)
    target = _delete_empty_lists(target)
    return target


def replace_neg_integers(target, replacements):
    # this version does not resolve nested structures and leaves list elements as lists
    target = copy.deepcopy(target)
    replacements = copy.deepcopy(replacements)
    for k,item in enumerate(target):
        if neg_integer_condition(item):
            # then item is a negative integer, but we should check that it's within range of replacmenents
            if item >= -len(replacements):
                target[k] = replacements[item]
    target = _delete_empty_lists(target)
    return target


def replace_nested(target, replacements):
    target = copy.deepcopy(target)
    replacements = copy.deepcopy(replacements)
    for item in traverse(target):
        address = list(flatten(item))
        ct = target
        _replace_address(ct, address, replacements)
    target = _delete_empty_lists(target)
    return target


def _propagate_shape(shape_0, shape_1, flatten=True):
    assert None in shape_1, "expected a place for batch dims signified by a None element"
    assert shape_1.count(None) == 1, "expected only a single batch insertion point"
    batch_insertion_index = shape_1.index(None)
    # insert batch dims from shape_0 into shape_1
    new_batch_dims = []
    for symbol in shape_0[0:-2]:
        new_batch_dims.append(symbol)
    # insert special dim symbols from shape_0 into shape_1
    if flatten:
        new_shape = conditional_replace_nested(shape_1, neg_integer_condition, shape_0)
    else:
        new_shape = replace_neg_integers(shape_1, shape_0)
    _flat_insert(new_shape, batch_insertion_index, new_batch_dims)
    return new_shape


def propagate_shape(*shapes, flatten=True):
    new_shape = shapes[0]
    for shape in shapes[1:]:
        new_shape = _propagate_shape(new_shape, shape, flatten=flatten)
    new_shape = _delete_empty_lists(new_shape)
    return new_shape


def concretize_shape(shape, replacements):
    shape = replace_nested(shape, replacements)  # this will leave things ('O', 'N') as (4, 5).
    for k,el in enumerate(shape):
        if isinstance(el, collections.Iterable) and not isinstance(el, (str, bytes)):
            elf = list(flatten(el))
            for e in elf:
                assert isinstance(e, Integral), f"expected all symbols to be replaced to integral values, got {e}"
            p = reduce(mul, elf)
            shape[k] = p
        elif el=='1': # a special case
            shape[k] = 1
        else:
            continue
    return shape


def unmerge_all(t, symbolic_shape, replacements):
    for k,d in enumerate(symbolic_shape):
        if isinstance(d, collections.Iterable) and not isinstance(d, (str, bytes)):
            # we found a merged axis. We will unmerge one at a time through recursion.
            if d[0] in replacements:
                t = split_axis(t, k, replacements[d[0]])
                symbolic_shape.insert(k,d[0])
                symbolic_shape[k+1] = d[1:]
                if len(symbolic_shape[k+1]) == 1:
                    symbolic_shape[k+1] = d[-1]
                unmerge_all(t, symbolic_shape, replacements)
            else:
                raise ValueError('found a merged axis, but I cannot unmerge because \
                                 their concrete shapes are not in replacements')
    return t


def contained_in(l1, l2):
    """are all elements in l1 in l2?"""
    for el in l1:
        if not el in l2:
            return False
    return True


def permute_list(l,p):
    ll = np.atleast_1d(l)
    ll = ll[p]
    return list(ll)


def convert_repeated_symbols(l):
    seen = {}
    for k,el in enumerate(l):
        if el in seen:
            seen[el] += 1
            l[k] = el + str(seen[el])
        else:
            seen[el] = 0
    return l


def permute_plan(shape, target_shape):
    """ Given a fully specified and unique symbolic shape (i.e. no batch dim, all symbols unique), we compute
     a permutation of it to match the target_shape. Unmatched symbols are grouped in the order they appear
     as in the batch position (dim 0). E.g.:

     shape: ['W1', 'S', 'N', 'D', 'O', 1]
     target_shape: ['N', 'O']

     returns [0, 1, 3, 5, 2, 4]
     which permutes shape as: ['W1', 'S', 'D', 1, 'N', 'O']

     """
    assert len(set(shape)) == len(shape), "all elements of shape must be unique"
    assert len(set(target_shape)) == len(target_shape), "all elements of target_shape must be unique"
    assert len(target_shape) <= len(shape), "target_shape must be smaller in size than shape"
    assert contained_in(target_shape, shape), \
        f"target_shape must be contained within shape, got: {target_shape} and {shape}"

    N = len(shape)
    batch_dims = []
    tdims = []
    for k,el in enumerate(shape):
        if not el in target_shape:
            batch_dims.append(k)
        else:
            tdims.append(k)

    sub_shape = list(np.atleast_1d(shape)[tdims])
    assert len(sub_shape) == len(target_shape), "odd, this means target shape is not contained within shape"

    p = []
    for el in target_shape:
        p.append(sub_shape.index(el))

    p = batch_dims + list(np.arange(N)[tdims][p])
    return p


def permute_plan_incomplete(shape, target_shape):
    """ Given a fully specified and unique symbolic shape (i.e. no batch dim, all symbols unique), we compute
     a permutation of it to match the target_shape. Unmatched symbols are grouped in the order they appear
     as in the batch position (dim 0). E.g.:

     shape: ['W1', 'S', 'N', 'D', 'E', 'O']
     target_shape: ['N', 'O', 'P', 'E']

     returns [0, 1, 3, 2, 5, 4]
     which permutes shape as: ['W1', 'S', 'D', 'N', 'O', 'E']

     This variant allows for incomplete matching, i.e. that the target shape may not be fully contained within
     shape. If target_shape is not in shape at all, then the permute plan is essentially a no-op because all
     dims in shape are treated as batch dims.

     """
    assert len(set(shape)) == len(shape), "all elements of shape must be unique"
    assert len(set(target_shape)) == len(target_shape), "all elements of target_shape must be unique"

    N = len(shape)
    batch_dims = []
    tdims = []
    for k, el in enumerate(shape):
        if not el in target_shape:
            batch_dims.append(k)
        else:
            tdims.append(k)

    sub_shape = list(np.atleast_1d(shape)[tdims])

    p = []
    for el in target_shape:
        if el in sub_shape:
            p.append(sub_shape.index(el))

    p = batch_dims + list(np.arange(N)[tdims][p])
    return p


def expansion_plan(shape, target_shape):
    """
        Given a fully specified and unique symbol shape (i.e. no batch dim, all symbols unique), we compute a plan to
        expand shape so that it matches target_shape.

        We assert that shape is contained within target_shape and implicitly assume that shape is aligned with
        target shape, i.e. the dims that match appear in the same order in shape as they do in target_shape
    """
    assert len(set(shape)) == len(shape), "all elements of shape must be unique"
    assert len(set(target_shape)) == len(target_shape), "all elements of target_shape must be unique"
    assert len(shape) <= len(target_shape), "shape must be smaller in size than target_shape"
    assert contained_in(shape, target_shape), "shape must be contained within target_shape"

    batch_dims = []
    for k, el in enumerate(target_shape):
        if not el in shape:
            batch_dims.append(k)

    return batch_dims


def unsqueeze_multiple(t, dims):
    for d in dims:
        t = t.unsqueeze(int(d))
    return t


def populate_replacements_from_observables(*obs):
    """
    obs should be a list of tuples, where each tuple is (symbolic_shape, concrete_shape)
    """
    replacements = {}
    for syms, cons in obs:
        for sym, con in zip(syms, cons):
            if sym in replacements:
                assert replacements[sym] == con, \
                    f"attempted to update existing symbol ({sym}) with a different value {replacements[sym]}->{con}; " \
                        f"your observables are not consistent"
                continue
            else:
                replacements[sym] = con
    return replacements


def _unrepeated_symbols(shape):
    seen = {}
    for s in shape:
        s = str(s)
        if s in seen:
            seen[s] += 1
        else:
            seen[s] = 1
    return [k for k, v in seen.items() if v == 1]


def recover_integral(l):
    r = []
    for el in l:
        try:
            r.append(int(el))
        except ValueError:
            r.append(el)
    return r


def _pairwise_broadcast_plan(shape1, shape2):
    """ We are to return an expansion plan, which is a list of dims to expand, for shape1 and shape 2.
        Additionally, we will return a merged symbolic shape that contains all the batch dims present
        in both shape1 and shape2. This will allow one to chain through many shapes and make the
        whole set of shapes compatible """
    shape1 = [str(s) for s in shape1]
    shape2 = [str(s) for s in shape2]
    batch_dims1 = _unrepeated_symbols(shape1)
    batch_dims2 = _unrepeated_symbols(shape2)
    unique_to_1 = np.setdiff1d(batch_dims1, batch_dims2)
    unique_to_2 = np.setdiff1d(batch_dims2, batch_dims1)
    husk1 = np.setdiff1d(shape1, unique_to_1)
    husk2 = np.setdiff1d(shape2, unique_to_2)
    if len(husk1) != len(husk2) or np.any(husk1 != husk2):
        raise ValueError('these two shapes cannot be made broadcast compatible by expansion alone')
    """First, we make a super-set list comprised of all batch_dims in both shapes. We do this by
        scanning through shape1 and adding unique_t_2 elements to it. Elements are inserted before
        the next common element shared by both shapes. So, for example:

        shape1 = [a, b, c, d]
        shape2 = [a, e, c, g]

        combined_shape = [a, e, b, c, g, d]
    """
    common = husk1
    expanded_shape = [s for s in shape1]
    bd_locs_2 = np.array([shape2.index(s) for s in unique_to_2])
    bd_locs_2.sort()
    common_locs_2 = np.array([shape2.index(s) for s in common] + [len(shape2)])
    common_locs_2.sort()
    common_locs_1 = np.array([shape1.index(s) for s in common] + [len(shape1)])
    common_locs_1.sort()
    for k, (loc1, loc2) in enumerate(zip(common_locs_1[::-1], common_locs_2[::-1])):
        if len(common_locs_1) > (k + 1):
            next_loc_2 = common_locs_2[::-1][k + 1]
        else:
            next_loc_2 = -1
        valid_indices = bd_locs_2[np.logical_and(bd_locs_2 < loc2, bd_locs_2 > next_loc_2)]
        for i in valid_indices[::-1]:
            expanded_shape.insert(loc1, shape2[i])
    # now we can find where to expand shape1, based on the target template given by expanded_shape
    plan_1 = np.array([expanded_shape.index(s) for s in unique_to_2])
    plan_1.sort()
    plan_2 = np.array([expanded_shape.index(s) for s in unique_to_1])
    plan_2.sort()
    # we return plans as negative indices in case batch indices are inserted (at the 0th position) that we
    # are not anticipating here. Convention throughout is that batch dims are inserted at 0th position.
    return (recover_integral(list(plan_1)),
            recover_integral(list(plan_2)),
            recover_integral(expanded_shape))


def plan_broadcast(*shapes):
    """ Given a set of shapes, this computes the final symbolic shape and a set of
        expansion plans to apply to each shape in order that all shapes can be broadcast together
    """
    if len(shapes) < 2:
        return None
    else:
        previous_shape = shapes[0]
        plans = len(shapes) * [-1]
        for current_shape in shapes[1:]:
            # this just builds up the final symbolic shape
            _, _, previous_shape = \
                _pairwise_broadcast_plan(previous_shape, current_shape)
        # now that we have the final shape, we can generate the plans
        for k, current_shape in enumerate(shapes):
            plans[k] = _pairwise_broadcast_plan(current_shape, previous_shape)[0]
    return plans, previous_shape


def set_with_occurances(shape):
    seen = {}
    for sym in shape:
        if sym in seen:
            seen[sym] += 1
        else:
            seen[sym] = 1
    return seen


def plan_expand_and_permute(shape, target_shape, assert_no_reps=False):
    """
    Given a shape, whose dim symbols are a subset of target_shape, we compute a plan to expand and
    permute shape so that it will be broadcast compatible with target_shape.

    Furthermore, we require that the number of occurances for a given symbol in shape be <= the
    number of occurances of that same symbol in target_shape.
    """
    shape = list(shape)
    target_shape = list(target_shape)
    assert len(shape) <= len(target_shape), "shape must be smaller than or equal to target in length"
    assert set(target_shape).intersection(set(shape)) == set(shape), "symbols in shape must appear in target"
    # replace Nones with a batch place_holder
    cshape = [s if s is not None else 'batch_placeholder' for s in shape]
    ctarget = [s if s is not None else 'batch_placeholder' for s in target_shape]
    so_target = set_with_occurances(target_shape)
    so_shape = set_with_occurances(shape)
    for k, v in so_target.items():
        if k in so_shape:
            assert so_shape[k] <= v, \
                f'number of occurances for symbol {k} is greater in shape ({so_shape[k]}), than in target ({v})'
    if assert_no_reps:
        for k, v in so_target.items():
            assert v <= 1, f"no repeated symbols allowed in target shape, got one for {k}"
    p = []
    L = len(cshape)
    expansion_plan = []
    for symbol in ctarget:
        if symbol in cshape:
            cind = cshape.index(symbol)
            p.append(cind)
            cshape[cind] = 'used'
        else:
            p.append(L)
            expansion_plan.append(L)
            L += 1
    return expansion_plan, p


def plan_expand_and_permute_ND_special(shape, target_shape):
    """
    Given a shape, whose dim symbols are a subset of target_shape, we compute a plan to expand and
    permute shape so that it will be broadcast compatible with target_shape.

    Furthermore, we require that the number of occurances for a given symbol in shape be <= the
    number of occurances of that same symbol in target_shape.

    In this specialized version, symbols 'N' and 'D' are treated specially. If target_shape
    has repeated 'N' or 'D' dims, we output more than 1 plan. We assert the repeat number does
    not exceed 2 and thus we expect to receive up to 2 plans. Further, we assert that the number
    or repeats in 'N' and 'D' for shape must be exactly 1 (no more or less)
    """
    shape = list(shape)
    target_shape = list(target_shape)
    assert len(shape) <= len(target_shape), "shape must be smaller than or equal to target in length"
    assert set(target_shape).intersection(set(shape)) == set(shape), \
        f"symbols in shape ({set(shape)}) must appear in target ({set(target_shape)})"
    # replace Nones with a batch place_holder
    cshape = [s if s is not None else 'batch_placeholder' for s in shape]
    ctarget = [s if s is not None else 'batch_placeholder' for s in target_shape]
    so_target = set_with_occurances(target_shape)
    so_shape = set_with_occurances(shape)
    for k, v in so_target.items():
        if k in so_shape:
            assert so_shape[k] <= v, \
                f'number of occurances for symbol {k} is greater in shape ({so_shape[k]}), than in target ({v})'
    for k, v in so_shape.items():
        if k in ['N', 'D']:
            assert so_shape[k] == 1, \
                f"expecting symbol {k} to only be present 1 time in shape, got {so_shape[k]}"
    p = []
    L = len(cshape)
    expansion_plan = []
    for symbol in ctarget:
        if symbol in cshape:
            cind = cshape.index(symbol)
            p.append(cind)
            cshape[cind] = 'used'
        else:
            p.append(L)
            expansion_plan.append(L)
            L += 1

    p2 = [e for e in p]

    flag = False
    if 'N' in so_target and so_target['N'] > 1:
        flag = True
        ind1 = ctarget.index('N')
        ind2 = len(ctarget) - 1 - ctarget[::-1].index('N')
        p2[ind1], p2[ind2] = p2[ind2], p2[ind1]

    if 'D' in so_target and so_target['D'] > 1:
        flag = True
        ind1 = ctarget.index('D')
        ind2 = len(ctarget) - 1 - ctarget[::-1].index('D')
        p2[ind1], p2[ind2] = p2[ind2], p2[ind1]

    if flag:
        return expansion_plan, [p, p2]
    else:
        return expansion_plan, [p]


def adjust_plan(plan, adjustment, batch_loc):
    """ Pray I do not alter it further.

        Anyway, this fixes the fact that "plans" assume there is exactly 1 batch dim. Sometimes there is 1, sometimes 2,
        sometimes 0. The adjustment will be 0 if # batch dims == 1. It will be 1 if # batch dims == 0, and -1 if # batch
        dims == 2, and so on. Basically it's adjustment = 1 - real # batch dims. We can't know the real batch dim until
        run time, hence the plan always needs to be adjusted
    """
    with torch.no_grad():
        tplan = torch.tensor(plan, dtype=torch.int64)
        tplan = torch.where(tplan >= torch.tensor(batch_loc), tplan - adjustment, tplan)
    return [int(t) for t in tplan]



