import math
from .util import validate_scalar_parameter, ExtractDiagonalNoKeepDims
from ..constraints import default_mapping
from torch.distributions import constraints
from ..symbolic import *
from matchpy import Arity
import torch
from abc import ABC, abstractmethod
import torch.nn.functional as F
from .util import dtype_str_to_dtype

__all__ = [
    'AbstractAnalyticLikelihood',
    'AnalyticMOGaussianLikelihood',
    'AnalyticMOGaussianLikelihoodIW',
    'AnalyticOAIGaussianLikelihood',
    'AnalyticMOPoissonLikelihood',
    'AnalyticOAIPoissonLikelihood',
    'AbstractMCLikelihood',
    'MCMOPoissonLikelihood',
    'MCOAIPoissonLikelihood',
    'MCMOGaussianLikelihood',
    'MCOAIGaussianLikelihood',
    'likelihood_factory'
]


def conditional_gaussian_log_likelihood(y, mu, var, sigma):
    """
    computes log( N(y; f, sigma**2) ) for f ~ N(mu, var)

    formula per obs is:
    -(1/2)log(2pi*sigma2) -(1/(2*sigma2))*(y**2 + (var - mu**2) - 2*y*mu)

    it is assumed that for each element of y, there is a latent mu and var, and that
    all such elements are independent observations. Computes the average value
    of the likelihood over all observations. When using on a dataset, it should be
    multiplied by the total number of observations.

    :param y: tensor of shape ND
    :param mu: tensor of shape ND (note: no trailing 1s, nor extra batches)
    :param var: positive tensor of shape ND (must be marginalized, note: same shape as mean and y)
    :param sigma: positive scalar tensor
    :return: scalar
    """
    assert y.shape == mu.shape, f"y and mu must have same shape, got {y.shape} for y, {mu.shape} for mu"
    assert y.shape == var.shape, f"y and var must have same shape, got {y.shape} for y, {var.shape} for var"
    sigma2 = sigma * sigma
    log2pi = 1.8378770664093453
    N = y.numel()
    R = 0.5 * sigma2.reciprocal()
    t1 = 0.5 * log2pi
    t2 = sigma.log()
    # reshaping implies that we treat all observations over N and D as independent, which is why if
    # this likelihood is used, it should request marginalize_output = True
    t3 = (1/N) * R * ((y.reshape(-1,1) - mu.reshape(-1,1)).t() @ (y.reshape(-1,1) - mu.reshape(-1,1))).squeeze()
    t4 = (1/N) * R * var.sum()
    return -(t1 + t2 + t3 + t4)

def conditional_poisson_log_likelihood(y, mu, var, bin_size):
    """
    computes log( Poisson(y; f.exp()) for f ~ N(mu, var)

    formula per obs is:
    y*mu - (mu + var/2).exp().mul(bin_size) - lgamma(y + 1) + ylog(bin_size)

    :param y: tensor of shape ND
    :param mu: tensor of shape ND (note: no trailing 1s)
    :param var: positive tensor of shape ND (must be marginalized and same shape as y and mean)
    :param bin_size: positive scalar not to be optimized
    :return: scalar
    """
    # implementation copied from: https://github.com/GPflow/GPflow/blob/develop/gpflow/likelihoods/likelihoods.py#L221
    t1 = y * mu  #  ND
    t2 = (mu + var/2).exp().mul(bin_size)  # ND
    t3 = (y + 1).lgamma()  # ND
    t4 = y * (bin_size).log()  # ND
    result = t1 - t2 - t3 + t4
    return result.mean()


def importance_weighted_conditional_gaussian_likelihood(y: torch.Tensor, mu: torch.Tensor, var: torch.Tensor,
                                                        sigma: torch.Tensor, log_iw: torch.Tensor, dim: int):
    """
    for:
    L = conditional_gaussian_log_likelihood(y, mu, var)
    log_iw = log(p(w)) - log(q(w));
    Note: in Hugh's code, he returns log(q(w)) - log(p(w)) and calls this the weight. Whereas I use the opposite sign
    Of course, this just means that in the next line we have L + log_iw, instead of L - log_iw
    we implement:
    log[1/K*Sum(L.exp() * p(w) / q(w))] = log[1/K*Sum(L.exp() * log_iw.exp())] = log[1/K*Sum((L + log_iw).exp())];
    using:
    logsumexp(L + log_iw) - log(K) #where K is the number of importance samples at the dim
    """
    Lw = conditional_gaussian_log_likelihood(y, mu, var, sigma) + log_iw
    K = torch.tensor(log_iw.shape[dim], dtype=Lw.dtype, device=Lw.device)
    return (torch.logsumexp(Lw, dim=dim) - K.log()).mean()


class AbstractAnalyticLikelihood(torch.nn.Module, ABC):
    """
    For analytic likelihoods, integral E[p(y|z)p(z)] is analytic and we can implement it directly, given
    the mean and var of p(z). The function must return a scalar, though it is not expected to scale that
    scalar to compensate for minibatching. That is to be done at a higher level.
    """
    def __init__(self, name: str = '', dtype=torch.get_default_dtype(), constraint_mapping=None,
                 **unused):
        super().__init__()
        self.name = str(name)
        self.dtype = dtype
        self._build_config = {}
        self._build_config['name'] = self.name
        self._build_config['constraint_mapping'] = constraint_mapping
        if constraint_mapping is None:
            self.constraint_mapping = default_mapping
        self._build_config['dtype'] = str(self.dtype)

    def expected_log_likelihood(self, y, mu, var, **kwargs):
        raise NotImplementedError()

    def forward(self, y, mu, var):
        self.expected_log_likelihood(y, mu, var)

    @property
    def build_config(self):
        return self._build_config

    @property
    def requirements(self):
        return {'marginal_input': True, 'marginal_output': True}

    @property
    @abstractmethod
    def symbolic_mean_shape(self):
        pass

    @property
    @abstractmethod
    def symbolic_cov_shape(self):
        pass


class AnalyticMOGaussianLikelihood(AbstractAnalyticLikelihood):
    """
    Gaussian likelihood which assumes that all observations are independent. Assumes latent space will match in
    N and D coordinates (MO indicates matching in D).
    """
    def __init__(self, std_lik_init = 1.0, name: str = '', dtype=torch.get_default_dtype(), constraint_mapping=None,
                 **unused):
        super().__init__(name, dtype=dtype, constraint_mapping=constraint_mapping)
        self.constraints = {'std_lik': constraints.positive}
        #storage that this module owns
        std_lik_init = validate_scalar_parameter(std_lik_init, 'std_lik_init', self.dtype)
        assert std_lik_init > 0, f"expected std_lik initial value to be positive, got {std_lik_init}"
        self._build_config['std_lik_init'] = float(std_lik_init)
        self._build_config['kind'] = type(self).__name__
        self._std_lik_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["std_lik"]).inv(std_lik_init)
        )

    @property
    def std_lik(self):
        transform = self.constraint_mapping(self.constraints["std_lik"])
        return transform(self._std_lik_storage)

    @property
    def symbolic_mean_shape(self):
        return ['N', 'D']

    @property
    def symbolic_cov_shape(self):
        return ['N', 'D']

    def expected_log_likelihood(self, y, mu, var, **unused):
        return conditional_gaussian_log_likelihood(y, mu, var, self.std_lik)


class AnalyticMOGaussianLikelihoodIW(AbstractAnalyticLikelihood):
    """
    The importance weighted flavor of Analytic MOGaussian
    """
    def __init__(self, std_lik_init = 1.0, name: str = '', dtype=torch.get_default_dtype(), constraint_mapping=None,
                 **unused):
        super().__init__(name, dtype=dtype, constraint_mapping=constraint_mapping)
        self.constraints = {'std_lik': constraints.positive}
        #storage that this module owns
        std_lik_init = validate_scalar_parameter(std_lik_init, 'std_lik_init', self.dtype)
        assert std_lik_init > 0, f"expected std_lik initial value to be positive, got {std_lik_init}"
        self._build_config['std_lik_init'] = float(std_lik_init)
        self._build_config['kind'] = type(self).__name__
        self._std_lik_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["std_lik"]).inv(std_lik_init)
        )

    @property
    def std_lik(self):
        transform = self.constraint_mapping(self.constraints["std_lik"])
        return transform(self._std_lik_storage)

    @property
    def symbolic_mean_shape(self):
        return ['N', 'D']

    @property
    def symbolic_cov_shape(self):
        return ['N', 'D']

    def expected_log_likelihood(self, y, mu, var, **kwargs):
        log_iw = torch.zeros_like(mu) if 'log_iw' not in kwargs else kwargs['log_iw']
        if 'dim' not in kwargs:
            # this is a hack to allow one to turn this likelihood into a regular gaussian one by simply not supplying
            # any importance weights
            dim = 0
            y = y.unsqueeze(0)
            mu = mu.unsqueeze(0)
            var = var.unsqueeze(0)
            log_iw = log_iw.unsqueeze(0)
        else:
            dim = kwargs['dim']
        return importance_weighted_conditional_gaussian_likelihood(y, mu, var, self.std_lik, log_iw, dim)


class AnalyticOAIGaussianLikelihood(AnalyticMOGaussianLikelihood):
    """
    Gaussian likelihood which assumes that all observations are independent. Assumes latent space will match in
    N and O coordinates (OAI = "Output As Input" indicates matching in O).
    """

    def __init__(self, std_lik_init=1.0, name: str = '', dtype=torch.get_default_dtype(), constraint_mapping=None,
                 **unused):
        super().__init__(std_lik_init, name, dtype=dtype, constraint_mapping=constraint_mapping)
        self._build_config['std_lik_init'] = float(std_lik_init)
        self._build_config['kind'] = type(self).__name__

    @property
    def symbolic_mean_shape(self):
        return ['N', 'O']

    @property
    def symbolic_cov_shape(self):
        return ['N', 'O']


class AnalyticMOPoissonLikelihood(AbstractAnalyticLikelihood):
    """
    Poisson likelihood which assumes that all observations are independent. Assumes latent space is exponentially linked
    and will match in N and D coordinates (MO indicates matching in D)

    """

    def __init__(self, name: str = '', bin_size = 1.0,
                 dtype=torch.get_default_dtype(), constraint_mapping=None, **unused):
        super().__init__(name, dtype=dtype, constraint_mapping=constraint_mapping)
        self.constraints = {'bin_size': constraints.positive}
        self._build_config['kind'] = type(self).__name__

        # storage that this module owns
        bin_size = validate_scalar_parameter(bin_size, 'bin_size', self.dtype)
        assert bin_size > 0, f"expected bin_size to be positive, got {bin_size}"
        self._build_config['bin_size'] = float(bin_size)
        self.register_buffer('_bin_size_storage',
             self.constraint_mapping(self.constraints["bin_size"]).inv(bin_size))


    @property
    def bin_size(self):
        transform = self.constraint_mapping(self.constraints["bin_size"])
        return transform(self._bin_size_storage)

    @property
    def symbolic_mean_shape(self):
        return ['N', 'D']

    @property
    def symbolic_cov_shape(self):
        return ['N', 'D']

    def expected_log_likelihood(self, y, mu, var, **unused):
        result = conditional_poisson_log_likelihood(y, mu, var, self.bin_size)
        return result


class AnalyticOAIPoissonLikelihood(AnalyticMOPoissonLikelihood):
    """
    Poisson likelihood which assumes that all observations are independent. Assumes latent space is exponentially linked
    and will match in N and O coordinates (OAI indicates matching in O)

    """

    def __init__(self, name: str = '', bin_size=1.0,
                 dtype=torch.get_default_dtype(), constraint_mapping=None, **unused):
        super().__init__(name, bin_size=bin_size, dtype=dtype, constraint_mapping=constraint_mapping)
        self._build_config['kind'] = type(self).__name__
        # NOTE: inherited from AnalyticMOPoissonLikelihood, not abstract class so we don't need to handle bin_size
        # that's handled by parent. All this does, really, is change symbolic shape defs.

    @property
    def symbolic_mean_shape(self):
        return ['N', 'O']

    @property
    def symbolic_cov_shape(self):
        return ['N', 'O']


def normal_log_prob(y, sample, sigma):
    var = (sigma ** 2)
    log_scale = sigma.log()
    result = -((y - sample) ** 2) / (2 * var) - log_scale - math.log(math.sqrt(2 * math.pi))
    return result.mean()


def poisson_log_prob(y, sample, bin_size):
    # implementation adapted from https://github.com/GPflow/GPflow/blob/develop/gpflow/logdensities.py#L34

    f = bin_size * sample
    t1 = y * f.log()  # ND * ...ND -> ...ND
    t2 = f  # ...ND
    t3 = (y + 1).lgamma()  # ...ND
    result = t1 - t2 - t3
    return result.mean()


class AbstractMCLikelihood(torch.nn.Module, ABC):
    """
    In contrast to analytic likelihoods, here the integral E[p(y|z)p(z)] is not analytic and thus we
    approximate it by MC sampling: E[p(y|z)p(z)] ~= Mean[p(y|z_i) for z_i ~ p(z_i)]

    z is assumed to have shape ...ND, and y is assumed to have shape ND. The specific implementation will
    indicate how reductions take place, but typically a sum is taken over ND and a mean over the batch dims.
    The likelihood is responsible for carrying out all these reductions so that in the end, a scalar is
    returned. Note that the likelihood is not responsible for scaling the scalar in the case of minibatching;
    that is to be handled at a higher level.
    """
    def __init__(self, name: str = '', dtype=torch.get_default_dtype(), constraint_mapping=None, **unused):
        super().__init__()
        self.name = name
        self.dtype = dtype
        self._build_config = {}
        self._build_config['name'] = self.name
        self._build_config['constraint_mapping'] = constraint_mapping
        self._build_config['dtype'] = str(self.dtype)
        if constraint_mapping is None:
            self.constraint_mapping = default_mapping

    def expected_log_likelihood(self, y, z, **kwargs):
        raise NotImplementedError()

    def forward(self, y, z):
        self.expected_log_likelihood(y, z)

    @property
    def build_config(self):
        return self._build_config

    @property
    def requirements(self):
        return {'marginal_input': True, 'marginal_output': True}

    @property
    @abstractmethod
    def symbolic_sample_shape(self):
        pass


class MCMOPoissonLikelihood(AbstractMCLikelihood):
    """
    A poisson likelihood evaluated using MC approximation. Assumes latent space will match in N and D dims, as indicated
    by "MO" in the name

    We require that samples passed in be strictly positive. So you must apply an inv_link somehow before passing to the
    likelihood

        """

    def __init__(self, name: str = '', bin_size=1.0, dtype=torch.get_default_dtype(),
                 constraint_mapping=None, **unused):
        super().__init__(name, dtype=dtype, constraint_mapping=constraint_mapping)
        self._build_config['kind'] = type(self).__name__
        self.constraints = {'bin_size': constraints.positive}
        if constraint_mapping is None:
            self.constraint_mapping = default_mapping
        # storage that this module owns
        bin_size = validate_scalar_parameter(bin_size, 'bin_size', self.dtype)
        assert bin_size > 0, f"expected bin_size to be positive, got {bin_size}"
        self._build_config['bin_size'] = float(bin_size)
        self.register_buffer('_bin_size_storage',
                             self.constraint_mapping(self.constraints["bin_size"]).inv(bin_size))

    @property
    def bin_size(self):
        transform = self.constraint_mapping(self.constraints["bin_size"])
        return transform(self._bin_size_storage)

    @property
    def symbolic_sample_shape(self):
        return ['N', 'D']

    def expected_log_likelihood(self, y, z, **unused):
        return poisson_log_prob(y, z, self.bin_size)


class MCOAIPoissonLikelihood(MCMOPoissonLikelihood):
    """
    A poisson likelihood evaluated using MC approximation. Assumes latent space will match in N and O dims, as indicated
    by "OAI" in the name

    Furthermore, the samples z are expected to be *strictly* positive, before being passed in. This gives one the
    freedom to select any link function they want.

        """

    def __init__(self, name: str = '', bin_size=1.0, dtype=torch.get_default_dtype(),
                 constraint_mapping=None, **unused):
        super().__init__(name, bin_size, dtype=dtype, constraint_mapping=constraint_mapping)

    @property
    def symbolic_sample_shape(self):
        return ['N', 'O']


class MCMOGaussianLikelihood(AbstractMCLikelihood):
    """
     A gaussian likelihood evaluated using MC approximation. Assumes latent space will match in N and D dims, as
    indicated by "MO" in the name

    This likelihood mainly included for testing MC likelihoods; you should prefer to use the analytic version
    """

    def __init__(self, std_lik_init=1.0, name: str = '', dtype=torch.get_default_dtype(), constraint_mapping=None,
                 **unused):
        super().__init__(name, dtype=dtype, constraint_mapping=constraint_mapping)
        self.constraints = {'std_lik': constraints.positive}
        # storage that this module owns
        std_lik_init = validate_scalar_parameter(std_lik_init, 'std_lik_init', self.dtype)
        assert std_lik_init > 0, f"expected std_lik initial value to be positive, got {std_lik_init}"
        self._build_config['std_lik_init'] = float(std_lik_init)
        self._std_lik_storage = torch.nn.Parameter(
            self.constraint_mapping(self.constraints["std_lik"]).inv(std_lik_init)
        )

    @property
    def std_lik(self):
        transform = self.constraint_mapping(self.constraints["std_lik"])
        return transform(self._std_lik_storage)

    @property
    def symbolic_sample_shape(self):
        return ['N', 'D']

    def expected_log_likelihood(self, y, z, **unused):
        return normal_log_prob(y, z, self.std_lik)


class MCOAIGaussianLikelihood(MCMOGaussianLikelihood):
    """
     A gaussian likelihood evaluated using MC approximation. Assumes latent space will match in N and O dims, as
    indicated by "OAI" in the name

    This likelihood mainly included for testing MC likelihoods; you should prefer to use the analytic version
    """

    def __init__(self, std_lik_init=1.0, name: str = '', dtype=torch.get_default_dtype(), constraint_mapping=None,
                 **unused):
        super().__init__(std_lik_init, name, dtype=dtype, constraint_mapping=constraint_mapping)

    @property
    def symbolic_sample_shape(self):
        return ['N', 'O']


def likelihood_factory(dict_in):
    kind_registry = {
        'AnalyticMOGaussianLikelihood': AnalyticMOGaussianLikelihood,
        'AnalyticMOGaussianLikelihoodIW': AnalyticMOGaussianLikelihoodIW,
        'AnalyticOAIGaussianLikelihood': AnalyticOAIGaussianLikelihood,
        'AnalyticMOPoissonLikelihood': AnalyticMOPoissonLikelihood,
        'AnalyticOAIPoissonLikelihood': AnalyticOAIPoissonLikelihood,
        'MCMOPoissonLikelihood': MCMOPoissonLikelihood,
        'MCOAIPoissonLikelihood': MCOAIPoissonLikelihood,
        'MCMOGaussianLikelihood': MCMOGaussianLikelihood,
        'MCOAIGaussianLikelihood': MCOAIGaussianLikelihood,
    }
    assert 'kind' in dict_in, "require key kind to be in arg dict_in"
    if dict_in['kind'] in kind_registry:
        # a hack, but god damn I'm getting lazy
        if 'dtype' in dict_in:
            dict_in['dtype'] = dtype_str_to_dtype(dict_in['dtype'])
        likelihood = kind_registry[dict_in['kind']](**dict_in)
        return likelihood
    else:
        raise ValueError('unknown likelihood kind')
