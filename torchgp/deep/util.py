import numpy as np
import torch
from ast import parse
from matchpy import Arity
from torchgp.symbolic.core import (
    Label,
    String,
    TensorOperation
)
from ..symbolic.utils import expr2graph, graph2ast
import numbers

__all__ = [
    'dtype_str_to_dtype',
    "invperm",
    "move_axis",
    "broadcast_cat",
    "move_axis_limited",
    "MoveAxis",
    "merge_axis",
    "MergeAxis",
    "unmerge_axis",
    "UnmergeAxis",
    "split_axis",
    "expand_dim",
    "SplitAxis",
    "compile_expr",
    "validate_scalar_parameter",
    "SwapDims",
    "ExpandDim",
    "ExtractDiagonalNoKeepDims",
    "BatchLogSquareDiag",
    'to_neg',
    'to_pos',
    'extract_concrete_layers'
]

def dtype_str_to_dtype(string_in):
    if string_in == 'torch.float32':
        return torch.float32
    elif string_in == 'torch.float64':
        return torch.float64
    else:
        raise ValueError('dtype not understood')

def compile_expr(expr, name='expr'):
    graph = expr2graph(Label(String(name), expr))
    f = graph2ast(graph, compiled=True, debug=False)
    return f

def validate_scalar_parameter(inp, name, dtype):
    """
    Useful to clean up scalar parameters. We allow them to be passed in as scalar tensors or something belonging
    to numbers.Number. Returns a scalar tensor and handles error checking. If inp is None, then this returns None.

    if type/device are already correct, the inp is passed through, else we clone and enforce

    :param inp: the parameter to be validated
    :param name: the parameter name (a string), which is used for error statements
    :return: a scalar tensor, None, or some exception
    """
    if inp is not None:
        if torch.is_tensor(inp):
            if inp.dim() == 0:
                if inp.dtype == dtype:
                    return inp
                else:
                    return inp.clone().detach().type(dtype)
            else:
                raise ValueError(f'Expected scalar tensor for parameter {name}')
        elif isinstance(inp, numbers.Number):
            return torch.tensor(inp).type(dtype)
        else:
            raise ValueError(f'unexpected type for {name}, got: {inp}')
    else:
        return None


def to_pos(t, N):
    _t = np.atleast_1d(t)
    m = _t < 0
    _t[m] = _t[m] + N
    return _t


def to_neg(t, N):
    _t = np.atleast_1d(t)
    m = _t >= 0
    _t[m] = _t[m] - N
    return _t


def invperm(a):
    a = np.atleast_1d(a)  # handles case of tuple, list, numpy array, even scalar
    b = np.empty_like(a)
    b[a] = np.arange(len(a))
    return list(b)


def move_axis_plan(t: torch.tensor, source, dest):
    """
    computes the permutation needed to move axes like numpy.moveaxis
    https://docs.scipy.org/doc/numpy-1.15.1/reference/generated/numpy.moveaxis.html
    """
    N = t.dim()
    source = to_pos(np.atleast_1d(source), N)
    dest = to_pos(np.atleast_1d(dest), N)
    order = [n for n in range(N) if not n in source]
    for d, s in sorted(zip(dest, source)):
        order.insert(d, s)
    return order


def move_axis(t: torch.tensor, source, dest):
    """

    Behaves like numpy.moveaxis
    https://docs.scipy.org/doc/numpy-1.15.1/reference/generated/numpy.moveaxis.html
    """
    order = move_axis_plan(t, source, dest)
    return t.permute(order)

def move_axis_limited(t: torch.tensor, source: int, dest: int):
    assert np.isscalar(source), "only scalar source dim allowed"
    assert np.isscalar(dest), "only scalar dest dim allowed"
    order = move_axis_plan(t, int(source), int(dest))
    return t.permute(order)


def merge_axis(t: torch.tensor, source_dim: int, dest_dim: int):
    dest_dim = int(dest_dim)
    source_dim = int(source_dim)
    if source_dim == dest_dim:
        return t
    N = t.dim()
    S = t.shape[source_dim]
    D = t.shape[dest_dim]
    # we want the source dim to be at -2 and dest dim to be at -1
    p = move_axis_plan(t, [source_dim, dest_dim], [-2, -1])
    _t = t.permute(p)
    other_dims = [i for i in range(N) if not i in [to_pos(source_dim, N), to_pos(dest_dim, N)]]
    other_shapes = list(np.array(t.shape)[other_dims])
    _t = _t.reshape(*(other_shapes + [1, S * D]))
    _t = _t.permute(invperm(p))
    return _t.squeeze(source_dim)

def split_axis(t: torch.tensor, target_dim: int, target_size: int):
    """
    given tensor of shape [A, B, C, D], this will split the target dimension (e.g. -2) so that the
    resulting shape is [A, B, target_size, C//target_size, D]. The portion that is split off is always
    placed one dimension earlier than the originally selected dimension. Obviously tensor dim will
    increase by 1 as a result
    """
    N = t.dim()
    target_size = int(target_size)
    target_dim = int(to_pos(target_dim, N))
    _t = move_axis(t, target_dim, -1)
    other_dims = [i for i in range(N) if not i == target_dim]
    other_shapes = list(torch.tensor(t.shape)[other_dims])
    S = t.shape[target_dim] // target_size
    assert S * target_size == t.shape[target_dim], "expecting target size to integer divisor of target_dim"
    _t = _t.reshape(*(other_shapes + [target_size, S]))
    n_target_dim = int(to_neg(target_dim, N))
    _t = move_axis(_t, [-2, -1], [n_target_dim - 1, n_target_dim])
    return _t

def unmerge_axis(t: torch.tensor, target_dim: int, source_dim: int, source_dim_shape: int):
    """Intended to be the inverse operation of merge_axis. target_dim specifies the dim in the
       merged tensor that is to be split and moved to source_dim. Source_dim position is specified
       in the final tensor's dimension. example:

       target_dim = -1
       source_dim = -2
       source_dim_shape = 3
       t = [A, B, D] -> [A, B, 3, D//3]

       or

       target_dim = -2
       source_dim = -1
       source_dim_shape = 3
       t = [A, B, D] -> [A, B//3, D, 3]
       """

    N = t.dim()
    source_dim = int(source_dim)
    target_dim = int(target_dim)
    _t = split_axis(t, target_dim, source_dim_shape)
    _t = move_axis(_t, [int(to_neg(target_dim, N)) -1], [int(to_neg(source_dim, N+1))])
    return _t

def expand_dim(t: torch.tensor, dim: int, size: int):
    dim = int(dim)
    size = int(size)
    assert t.shape[dim] == 1, f"can only expand singleton dimensions, got t.shape[{dim}] = {t.shape[dim]}"
    s = t.dim()*[-1]
    s[dim] = size
    return t.expand(s)


def broadcast_cat(*ts,dim=0):
    """
    all input tensors should have same dimensionality. We will broadcast singleton dimensions as appropriate

    this is an enhancement of torch.cat, which requires that all dims except `dim` be the same size.
    :param ts: a sequence of tensors
    :param dim: dimension to concatenate tensors along
    :return:
    """
    dims = [int(t.dim()) for t in ts]
    shapes = np.array([list(t.shape) for t in ts])
    assert np.alltrue(np.array(dims) == np.array(len(ts) * [dims[0]])), \
        f"all inputs should have same dim, got {shapes}"
    max_shape = shapes.max(axis=0)
    target_shapes = [list(max_shape) for s in shapes]
    for k, shape in enumerate(shapes):
        target_shapes[k][dim] = shape[dim]
    ts = [t.expand(*target_shape) for t, target_shape in zip(ts, target_shapes)]
    return torch.cat(ts,dim=dim)


class MergeAxis(TensorOperation):
    name = "MergeAxis"
    arity = Arity(3, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        source_dim = self.operands[1].evaluated
        dest_dim = self.operands[2].evaluated
        return merge_axis(op, source_dim, dest_dim)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        source_dim = arglabels[1]
        dest_dim = arglabels[2]

        src = f"{label} = merge_axis({op}, {source_dim}, {dest_dim})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

class UnmergeAxis(TensorOperation):
    name = "UnmergeAxis"
    arity = Arity(4, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        target_dim = self.operands[1].evaluated
        source_dim = self.operands[2].evaluated
        source_shape = self.operands[3].evaluated
        return unmerge_axis(op, target_dim, source_dim, source_shape)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        target_dim = arglabels[1]
        source_dim = arglabels[2]
        source_shape = arglabels[3]

        src = f"{label} = unmerge_axis({op}, {target_dim}, {source_dim}, {source_shape})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

class SplitAxis(TensorOperation):
    name = "SplitAxis"
    arity = Arity(3, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        target_dim = self.operands[1].evaluated
        target_size = self.operands[2].evaluated
        return split_axis(op, target_dim, target_size)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        target_dim = arglabels[1]
        target_size = arglabels[2]

        src = f"{label} = split_axis({op}, {target_dim}, {target_size})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

class MoveAxis(TensorOperation):
    """a more limited version of move_axis, where source and target dims must be scalars rather than iterables"""
    name = "MoveAxis"
    arity = Arity(3, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        source_dim = self.operands[1].evaluated
        dest_dim = self.operands[2].evaluated
        return move_axis_limited(op, source_dim, dest_dim)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        source_dim = arglabels[1]
        dest_dim = arglabels[2]

        src = f"{label} = move_axis_limited({op}, {source_dim}, {dest_dim})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

class ExpandDim(TensorOperation):
    name = "ExpandDim"
    arity = Arity(3, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        dim = self.operands[1].evaluated
        size = self.operands[2].evaluated
        return expand_dim(op, dim, size)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        dim = arglabels[1]
        size = arglabels[2]

        src = f"{label} = expand_dim({op}, {dim}, {size})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

class SwapDims(TensorOperation):
    name = "SwapDims"
    arity = Arity(3, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        dim1 = self.operands[1].evaluated
        dim2 = self.operands[2].evaluated
        return torch.transpose(op, dim1, dim2)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        dim1 = arglabels[1]
        dim2 = arglabels[2]
        src = f"{label} = torch.transpose({op}, {dim1}, {dim2})"
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result

class ExtractDiagonalNoKeepDims(TensorOperation):
    name = "batch_idiag"
    arity = Arity.unary

    @classmethod
    def _evaluate(cls, left, right=None):
        #the following impl allows for batch dims
        return torch.diagonal(left.evaluated, dim1=-1, dim2=-2)

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = torch.diagonal({op_name}, dim1=-1, dim2=-2)".format(
            label=label, op_name=op_name
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


class BatchLogSquareDiag(TensorOperation):
    name = "batch_logsumsquare_diag"
    arity = Arity.unary

    @classmethod
    def _evaluate(cls, left, right=None):
        #the following impl allows for batch dims
        return torch.diagonal(left.evaluated, dim1=-1, dim2=-2).pow(2).log()

    def to_ast(self, label, *arglabels, source=False):
        op_name = arglabels[0]
        src = "{label} = torch.diagonal({op_name}, dim1=-1, dim2=-2).pow(2).log()".format(
            label=label, op_name=op_name
        )
        _ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        _ast = _ast.body[0]

        result = (_ast, src) if source else _ast
        return result


def extract_concrete_layers(o):
    if hasattr(o, 'layer'):
        for el in [o.layer]:
            if hasattr(el, 'layer') or hasattr(el, 'layers'):
                yield from extract_concrete_layers(el)
            else:
                yield el
    else:
        for el in o.layers:
            if hasattr(el, 'layer') or hasattr(el, 'layers'):
                yield from extract_concrete_layers(el)
            else:
                yield el

