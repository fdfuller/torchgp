from torchgp.symbolic import *
import torch
import math
from .util import *
from .variational_parameterization import *
from .inducing import *
from .mo_transforms import *
from ..kernels import *
from ast import parse
from matchpy import Arity
from ..means import BatchConstantMean, LinearMean, MeanSymbol, EvaluateMean
from ..constraints import default_mapping
from torch.distributions import constraints
from .layers import Layer, AbstractLayerContainer, AbstractLayerWrapper
from .symbolic_shape import propagate_shape

__all__ = ["GPMM", "GPMM_sase_aux"]

"""
"GPMM", aka "Gaussian Process Mixture of Measurements", refers to a concept/name that I took from a paper:
 https://arxiv.org/pdf/1707.05909.pdf
"""

class CartesianRepProduct(TensorOperation):
    """
    A special case for use in GPMM. Takes three arguments: output axis, input axis, and input axis reps (a scalar)
    """
    name = "CartesianProduct"
    arity = Arity(3, True)

    @property
    def evaluated(self):
        ops = [op.evaluated for op in self.operands]
        return torch.cartesian_prod(ops[0],*ops[2]*[ops[1]])

    def to_ast(self, label, *arglabels, source=False):
        out_axis = arglabels[0]
        in_axis = arglabels[1]
        reps = arglabels[2]

        src = f"{label} = torch.cartesian_prod({out_axis}, *int({reps})*[{in_axis}])"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

def transform_batched_matrix(K, W):
    """ J(OR)(OR) -> ...J(OM)(OM)

        einsum operation: 'ir,...oruq,lq->...oiul'
    """
    assert K.shape[-1] % W.shape[-1] == 0, f"expected W.shape[-1] to be integer divisor of K.shape[-1]"
    # K has shape ...(OR)(OR)
    K = split_axis(K, -2, K.shape[-2] // W.shape[-1])  # ...(OR)(OR) -> ...OR(OR)
    K = split_axis(K, -1, K.shape[-1] // W.shape[-1])  # ...OR(OR) -> ...OROR
    t1 = torch.tensordot(K, W, [[-3], [-1]])  # ...ORUQ,MR -> OUQM
    t2 = torch.tensordot(t1, W, [[-2], [-1]])  # ...OUQM,NQ -> OUMN
    t3 = t2.transpose(-3, -2)
    return merge_axis(merge_axis(t3, -4, -3), -2, -1)  # ...(OM)(UN)

def transform_batched_matrix_full_marginal(K, W):
    """ ...OJRR -> ...OJM1

       einsum operation: 'ir,...ojrq,iq->...oji'
    """
    assert K.shape[-1] == W.shape[-1], \
        f"expected K.shape[-1] to be compatible with W.shape[-1], got {K.shape} and {W.shape}"
    assert K.shape[-2] == K.shape[-1], f"expected K.shape[-1] to be equal to K.shape[-2]"
    t1 = torch.tensordot(K, W, [[-1], [-1]])  # ...OJRR,MR -> ...OJRM
    t2 = (t1 * W.t()).sum(dim=-2).unsqueeze(-1)  # ...OJRM * RM -> ...OJRM -> ...OJM
    return t2  # ...OJM1

def transform_batched_matrix_marginal_output(K, W):
    """OJRR -> OJMM

        einsum operation: 'mr,...ojrq,nq->...ojmn'
    """
    assert K.shape[-1] == W.shape[-1], f"expected K.shape[-1] to be compatible with W.shape[-1]"
    assert K.shape[-2] == K.shape[-1], f"expected K.shape[-1] to be equal to K.shape[-2]"
    # assume K has shape ...OJRR, W has shape MR
    t1 = torch.tensordot(K, W, [[-1], [-1]])  # ...OJQR,MR -> ...OJQM
    t2 = torch.tensordot(t1, W, [[-2], [-1]])  # ...OJQM,NQ -> ...OJMN
    return t2.transpose(-2, -1)  # ...OJNM

def transform_batched_matrix_marginal_input(K, W):
    """input is J(OR)(OR) -> MJOO

       einsum operation: 'mr,...joriq,mq->...mjoi' (after splitting K)
    """
    assert K.shape[-1] % W.shape[-1] == 0, f"expected W.shape[-1] to be integer divisor of K.shape[-1]"
    O = K.shape[-1] // W.shape[-1]
    Ks = split_axis(K, -2, O)
    Ks = split_axis(Ks, -1, O)  # ...JOROR
    W2 = W.unsqueeze(-1) * W.unsqueeze(1)  # MR1 * M1Q = MRQ
    t1 = torch.tensordot(Ks, W2, [[-3, -1], [-1, -2]])  # ...JOROQ,MRQ -> ...JOOM
    t2 = t1.transpose(-4, -1).transpose(-3, -1).transpose(-2, -1)  # ...JOOM -> ...MOOJ -> ...MJOO -> ...MJOO
    return t2

def transform_batched_vector_marg_out(K, W):
    """...OJR1 -> ...OJM1

       einsum operation: 'mr,...ojrs,mr->...ojms'
    """
    assert K.shape[-2] == W.shape[-1], f"expected K.shape[-2] to be compatible with W.shape[-1]"
    t1 = torch.tensordot(K,W,[[-2], [-1]]).transpose(-1,-2)  # ...OJRS, MR -> ...OJSM -> OJMS
    return t1

def transform_batched_vector(K, W):
    """...J(OR)1 -> ...OJM1

      einsum operation: 'mr,...jors,mr->...joms' (after splitting K)
   """
    assert K.shape[-2] % W.shape[-1] == 0, f"expected W.shape[-1] to be integer divisor of K.shape[-2]"
    O = K.shape[-2] // W.shape[-1]
    t1 = torch.tensordot(split_axis(K, -2, O), W, [[-2], [-1]]).transpose(-1, -2)
    # ...J(OR)S -> ...JORS -> ...JOSM -> ...JOMS
    return merge_axis(t1, -3, -2)  # -> ...J(OM)S


class GPMM(AbstractLayerWrapper):
    def __init__(self, layer,
                 input_axis_len: int,
                 input_features: int,
                 output_axis_len: int,
                 dtype=torch.get_default_dtype()):
        """
        This is a layer wrapper that accepts layers and layer sequences, but not
        totally arbitrary layer containers, as its job is to alter the input that child sequence
        sees. It only supplies a single input tensor to its child and is not generally
        compatible with layer containers that may want a sequence of input tensors.

        :param internal_in_features: defines the fixed internal input axis size
        :param internal_in_dim: defines the dimensionality of the fixed internal input axis size
        :param internal_out_features: defines the fixed internal output axis size
        :param layer: the layer we are modifying
        :param dtype: type to be use for tensors that this module owns and instantiates (the axes)

        Note: the internal input size grows very large. The axis is basically:
        torch.cartesian_prod(out_axis, *internal_in_dim*[in_axis]). So it grows exponentially in
        internal_in_dim and grows as the product of internal_in_features and internal_out_features.
        A 200 x 200 state has 40000 elements in the input. Imagine that this is your batch size.
        """
        super().__init__(layer)
        self.dtype = dtype
        internal_in_features = validate_scalar_parameter(input_axis_len, '_input_axis_len', torch.int64)
        internal_out_features = validate_scalar_parameter(output_axis_len, '_output_axis_len', torch.int64)
        internal_in_dim = validate_scalar_parameter(input_features, '_input_features', torch.int64)
        self._build_config['dtype'] = str(self.dtype)
        self._build_config['input_axis_len'] = int(internal_in_features)
        self._build_config['input_features'] = int(internal_in_dim)
        self._build_config['output_axis_len'] = int(internal_out_features)
        self._build_config['kind'] = type(self).__name__
        self.register_buffer("_input_axis_len", internal_in_features)
        self.register_buffer("_input_features", internal_in_dim)
        self.register_buffer("_output_axis_len", internal_out_features)
        self.register_buffer("_input_axis", torch.linspace(-3,3, int(self._input_axis_len), dtype=self.dtype))
        if self._output_axis_len > 1:
            self.register_buffer("_output_axis", torch.linspace(-3,3, int(self._output_axis_len), dtype=self.dtype))
        else:
            self.register_buffer("_output_axis", torch.zeros(1, dtype=self.dtype))
        # these next two "merge_to_" options refer to how the output dimension from this module should be
        # treated, when marginalize_output = True. When marginalize_output = True, we create a new batch dim
        # that may be unexpected elsewhere in the codebase. If merge_to_output is True, we will use merge axis
        # to combine the gpmm batch output dimension into the special dim associated with output (-3 for mean/cov
        # when marginalize_output is True and -1 for samples). If merge to input is True (and merge_to_output False),
        # then we will combine it with the N dim (-2 for mean/cov, -2 for samples). If both are false, it's left alone
        # and subsequent code will need to "handle it".

        # to be fulfilled by builder patten
        self.marginalize_input = None
        self.marginalize_output = None
        self.activation = None
        self._sample_shape = None
        self._mean_shape = None
        self._cov_shape = None

        self.layer = layer
        self.name = "gpmm_" + layer.name
        self._build_config['name'] = self.name
        # we should check that the in_features of the layer is compatible with the axis we'll generate
        if isinstance(self.layer, AbstractLayerContainer):
            assert self.layer.layers[0].in_features == (self._input_features + 1), \
                "internal axis number of features does not match the input features of the first layer passed in"
        else:
            assert self.layer.in_features == (self._input_features + 1), \
                "internal axis number of features does not match the input features of the layer passed in"

    def update_marginalization(self, **kwargs):
        marginalize_input = True if 'marginalize_input' not in kwargs else kwargs['marginalize_input']
        marginalize_output = True if 'marginalize_output' not in kwargs else kwargs['marginalize_output']
        self.marginalize_input = bool(marginalize_input)
        self.marginalize_output = bool(marginalize_output)
        # fixed marginalization state for the layer is to be enforced
        internal_kwargs = {
            'marginalize_input': False,
            'marginalize_output': True,
        }
        if self.layer is not None:
            self.layer.update_marginalization(**internal_kwargs)
        self._build_config['marginalization'] = {'arg': None, 'kwargs': {'marginalize_input': self.marginalize_input,
                                                      'marginalize_output': self.marginalize_output}}
        return self

    @property
    def ready_to_build(self):
        flag = False
        if self.layer is not None and \
            self.marginalize_input is not None and \
            self.marginalize_output is not None and \
            self.multisamples is not None and \
            self.name is not None and \
            self.layer.ready_to_build:
            flag = True
        return flag

    def update_activation(self, func, **unused):
        self.activation = func
        self._build_config['activation'] = {'arg': func, 'kwargs': unused}
        return self

    @property
    def in_features(self):
        return self._input_axis_len

    @property
    def out_features(self):
        return self.layer.out_features

    @property
    def mean_shape(self):
        if self._mean_shape is None:
            raise ValueError('layer not yet built')
        return self._mean_shape

    @property
    def sample_shape(self):
        if self._sample_shape is None:
            raise ValueError('layer not yet built')
        return self._sample_shape

    @property
    def cov_shape(self):
        if self._cov_shape is None:
            raise ValueError('layer not yet built')
        return self._cov_shape

    def build(self):
        if self.ready_to_build:
            # force a re-update of the marginalization state in case layer was added after
            self.update_marginalization(marginalize_input=self.marginalize_input,
                                        marginalize_output=self.marginalize_output)
            self._compile()
            self.layer.build()  # rebuild layer
            ## TODO: assert that layer has no output transform?
            self._build_config['layer'] = self.layer.build_config
            self._sample_shape = self._calc_sample_shape()
            self._mean_shape = self._calc_mean_shape()
            self._cov_shape = self._calc_cov_shape()

        else:
            print('The following must be updated before building:')
            if self.layer is None:
                print('layer')
            if self.marginalize_output is None:
                print('marginalize output flag')
            if self.marginalize_input is None:
                print('marginalize input flag')
            if self.multisamples is None:
                print('multisamples not yet set')
            if self.layer is not None and not self.layer.ready_to_build:
                print('layer is added, but the layer itself is not ready to build')
            raise ValueError('cannot build until we are ready for it')
        return self

    def _matrix_reduction_expr(self, x, W, marginalize_input, marginalize_output):
        if marginalize_input and marginalize_output:
            L = Lambda(self.name + '_full_marg_cov_transform')
            e = Apply(L, x, W)
        elif marginalize_input and not marginalize_output:
            L = Lambda(self.name + '_marg_in_cov_transform')
            e = Apply(L, x, W)
        elif not marginalize_input and marginalize_output:
            L = Lambda(self.name + '_marg_out_cov_transform')
            e = Apply(L, x, W)
        else:
            L = Lambda(self.name + '_full_cov_transform')
            e = Apply(L, x, W)
        return e

    def _vector_reduction_expr(self, x, W, marginalize_input, marginalize_output):
        if marginalize_output:
            L = Lambda(self.name + '_marg_out_vec_transform')
            e = Apply(L, x, W)
        else:
            L = Lambda(self.name + '_vec_transform')
            e = Apply(L, x, W)
        return e

    def _transform_components_expr(self):
        mu = Tensor(self.name + '_mu')
        var = Tensor(self.name + '_var')
        kl = Tensor(self.name + '_kl')
        W = Matrix(self.name + '_W')

        tvar = self._matrix_reduction_expr(var, W, self.marginalize_input, self.marginalize_output)
        tmu = self._vector_reduction_expr(mu, W, self.marginalize_input, self.marginalize_output)
        return tmu, tvar, kl

    def _transform_sample_expr(self, split_sample=False):
        sample = Tensor(self.name + '_sample')
        W = Matrix(self.name + '_W')
        if not split_sample:
            kl = Tensor(self.name + '_kl')
            tsample = self._vector_reduction_expr(sample, W, self.marginalize_input, self.marginalize_output)
            return tsample, kl
        else:
            ssample = Tensor(self.name + '_split_sample')
            smean = Tensor(self.name + '_split_mean')
            tsample = self._vector_reduction_expr(ssample, W, self.marginalize_input, self.marginalize_output)
            tmean = self._vector_reduction_expr(smean, W, self.marginalize_input, self.marginalize_output)
            return tsample, tmean

    def _internal_axis_expr(self):

        in_axis = Tensor(self.name + '_in_axis')
        out_axis = Tensor(self.name + '_out_axis')
        in_axis_reps = Scalar(self.name + '_in_axis_reps')

        axis = CartesianRepProduct(out_axis, in_axis, in_axis_reps)
        # axis has shape OR^reps x reps + 1
        if self.marginalize_output:
            final_axis = SplitAxis(axis, Constant(-2),
                                   Select(Constant(0), Shape(out_axis)))  # O x R^reps x reps + 1
        else:
            final_axis = axis
        return final_axis

    def _populate_symbols(self):
        leaves = {
            self.name + '_in_axis': self._input_axis,
            self.name + '_out_axis': self._output_axis,
            self.name + '_in_axis_reps': self._input_features,
            self.name + '_vec_transform': transform_batched_vector,
            self.name + '_marg_out_vec_transform': transform_batched_vector_marg_out,
            self.name + '_full_marg_cov_transform': transform_batched_matrix_full_marginal,
            self.name + '_marg_in_cov_transform': transform_batched_matrix_marginal_input,
            self.name + '_marg_out_cov_transform': transform_batched_matrix_marginal_output,
            self.name + '_full_cov_transform': transform_batched_matrix,
        }

        return leaves

    def _compile(self):
        internal_axis_graph = expr2graph(Label(String(self.name + '_axis'), self._internal_axis_expr()))
        self._internal_axis = graph2ast(internal_axis_graph, compiled=True, debug=False)

        tmu, tvar, component_kl = self._transform_components_expr()
        transformed_mean_graph = expr2graph(Label(String(self.name + '_transformed_mu'), tmu))
        transformed_var_graph = expr2graph(Label(String(self.name + '_transformed_var'), tvar))
        kl_component_graph = expr2graph(Label(String(self.name + '_component_kl'), component_kl))
        tcomponents_graph = compose_all([transformed_mean_graph, transformed_var_graph, kl_component_graph])
        self._components = graph2ast(tcomponents_graph, compiled=True, debug=False)

        tsample, sample_kl = self._transform_sample_expr()
        transformed_sample_graph = expr2graph(Label(String(self.name + '_transformed_sample'), tsample))
        kl_sample_graph = expr2graph(Label(String(self.name + '_sample_kl'), sample_kl))
        tsample_graph = compose_all([transformed_sample_graph, kl_sample_graph])
        self._sample = graph2ast(tsample_graph, compiled=True, debug=False)

        zm_tsample, tmean = self._transform_sample_expr(split_sample=True)
        zm_sample_graph = expr2graph(Label(String(self.name + '_transformed_split_sample'), zm_tsample))
        tmean_graph = expr2graph(Label(String(self.name + '_transformed_split_mean'), tmean))
        tsplit_sample_graph = compose_all([zm_sample_graph, tmean_graph])
        self._split_sample = graph2ast(tsplit_sample_graph, compiled=True, debug=False)

    def internal_input(self):
        return getattr(self._internal_axis(**self._populate_symbols()), self.name + '_axis')

    def sample(self, x, **kwargs):
        multisamples = self.layer.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        sample, kl = self.layer(self.internal_input(), multisamples=multisamples)
        if self.activation is not None:
            sample = self.activation(sample)
        leaves = {
            self.name + '_sample': sample,
            self.name + '_kl': kl,
            self.name + '_W': x,
            **self._populate_symbols(),
        }
        assert x.dim() == 2, "only designed for 2D weights: NxR"
        r = self._sample(**leaves)
        tsample = getattr(r, self.name + '_transformed_sample')
        kl = getattr(r, self.name + '_sample_kl')
        return tsample, kl

    def split_sample(self, x, **kwargs):
        multisamples = self.layer.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        s, d = self.layer.split_sample(self.internal_input(), multisamples=multisamples)
        if self.activation is not None:
            # WARNING: if any non-linear activation is applied, we break the gaussian assumption of SDE flow
            # (the main use-case for split sample). If you have some other use case, by all means, go on...
            s = self.activation(s)
        leaves = {
            self.name + '_split_sample': s,
            self.name + '_split_mean': d,
            self.name + '_W': x,
            **self._populate_symbols(),
        }
        assert x.dim() == 2, "only designed for 2D weights: NxR"
        r = self._split_sample(**leaves)
        ssample = getattr(r, self.name + '_transformed_split_sample')
        split_mean = getattr(r, self.name + '_transformed_split_mean')
        return ssample, split_mean

    def components(self, x, **unused):
        if self.activation is None:
            mu, var, kl = self.layer.components(self.internal_input())
            leaves = {
                self.name + '_mu': mu,
                self.name + '_var': var,
                self.name + '_kl': kl,
                self.name + '_W': x,
                **self._populate_symbols(),
            }
            assert x.dim() == 2, "only designed for 2D weights: NxR"
            r = self._components(**leaves)
            tmu = getattr(r, self.name + '_transformed_mu')
            tvar = getattr(r, self.name + '_transformed_var')
            return tmu, tvar, getattr(r, self.name + '_component_kl')
        else:
            raise ValueError('Cannot compute components if an activation is supplied, we can only sample in this case')

    def _calc_sample_shape(self):
        """ input here is the weight matrix, of size ND. So N -> -2, D = -1"""
        if not self.marginalize_output:
            shape = propagate_shape([None,  ['O', -2], 'I'], self.layer.sample_shape, flatten=False)
        else:
            shape = propagate_shape([None,'O',-2,'I'], self.layer.sample_shape, flatten=False)
        return shape

    def _calc_mean_shape(self):
        if not self.marginalize_output:
            shape = propagate_shape([None,  ['O', -2], 'I'], self.layer.mean_shape, flatten=False)
        else:
            shape = propagate_shape([None,'O',-2,'I'], self.layer.mean_shape, flatten=False)
        return shape

    def _calc_cov_shape(self):
        """
        Note that some hacking of the shape is needed here, since our transformation alters the covariance shape
        beyond what propagate shape can know or account for.
        """
        if self.marginalize_input and self.marginalize_output:
            shape = propagate_shape([None, 'O', -2, 'I'], self.layer.cov_shape, flatten=False)
            shape[-1] = 1
        elif self.marginalize_input and not self.marginalize_output:
            shape = propagate_shape([None, ['O', -2], 'I'], self.layer.cov_shape, flatten=False)
            shape[-1] = shape[-1][0]
            shape[-2] = shape[-2][0]
            shape.insert(-3, -2)
        elif not self.marginalize_input and self.marginalize_output:
            shape = propagate_shape([None,'O',-2,'I'], self.layer.cov_shape, flatten=False)
        else:
            shape = propagate_shape([None, ['O', -2], 'I'], self.layer.cov_shape, flatten=False)
            # shape = [None, 'D', ['O',-2], ['O',-2]]
        return shape

"""
What follows here are a number of transforms needed in order to do what I am calling 'Nlatent' GPMM. In this case,
we suppose that the latent space is allowed to change for each instance (N) of the weights. This means effectively that
the kernel now has 3 kronecker components: K_out(x_out) ⊗ K_in(x_in) ⊗ K_n(x_n), resulting from an axis that
is kronecker structured: X = x_out ⊗ x_in ⊗ x_n. In the case of x_n, we allow it to be non-scalar valued, so that for
some x_n = ...ND, we compute a kernel of size NxN as normal, whereas the K_in kernel is treated as a Kronecker product
over single dimensional inputs giving M^rep x M^rep in size, where rep corresponds to the number of dims in x_in. 
Normally, x_in is single dimensional, and we also expect x_out to be single dimensional.

For the sake of expedience, I will just code these all with einsum, rather than tensordot

With respect to marginalization, we will always treat x_n as being marginalized. This is consistent with the assumption
of minibatching; all shots are presumed independent. However, this is *not* consistent with GPMM and I'm not sure which
we should follow ultimately. In GPMM, we allow input marginalization False, require it in fact, over R for the wrapped
layer, but let the GPMM input marginalization state vary independently from what is enforced on the wrapped layer. The
usual use-case, then, is that the GPMM layer has input marginalization True, while the wrapped layer is set to False. If
we stuck to that here, we'd need to have full covariance over N on the wrapped layer, which doesn't make sense. I hesi-
tate to say that it never makes sense to return an NxN full covariance kernel; what even is the use case for that?

Anyway, this also means that we only need to code up the cases for marginalize output = True or False, and we'll fix
marginalize input = True
"""


def transform_batched_matrix_full_marginal_nlatent(K, W):
    """ X = ...NO(R^rep)I -> K(X,X) = NOJ(R^rep)(R^rep); abv as NOJRR. We want:
        NR, ...NOJRR, NR -> ...OJN1

       einsum operation: 'nr,...nojrq,nq->...ojn'
    """
    assert K.shape[-1] == W.shape[-1], \
        f"expected K.shape[-1] to be compatible with W.shape[-1], got {K.shape} and {W.shape}"
    assert K.shape[-2] == K.shape[-1], f"expected K.shape[-1] to be equal to K.shape[-2]"
    t = torch.einsum('nr,...nojrq,nq->ojn', W, K, W)
    return t.unsqueeze(-1)  # ...OJN1


def transform_batched_matrix_full_output_nlatent(K, W):
    """X = N(OR)I -> K(X,X) = NJ(OR)(OR)
        NR, ...NJ(OR)(OR) -> ...NJOO
    """
    assert K.shape[-1] % W.shape[-1] == 0, \
        f"expected W.shape[-1] to be integer divisor of K.shape[-1], got {W.shape[-1]} and {K.shape[-1]} respectively"
    assert K.shape[-2] == K.shape[-1], f"expected K.shape[-1] to be equal to K.shape[-2]"
    O = K.shape[-1] // W.shape[-1]
    Ks = split_axis(K, -2, O)
    Ks = split_axis(Ks, -1, O)  # ...JOROR
    t = torch.einsum('nr,...njorpq,nq->...njop',W,Ks,W)
    return t  # ...NJOO


def transform_batched_vector_full_marginal_nlatent(K, W):
    """X = ...NO(R^rep)I, so vectors have shape ...NOJ(R^rep)1 (no output transform assumed, so last dim is 1)
        NR, ...NOJRZ -> ...OJNZ   *Z = trailing 1*
    """
    assert K.shape[-2] == W.shape[-1], f"expected K.shape[-2] to be compatible with W.shape[-1]"
    t = torch.einsum('nr,...nojrz->ojnz',W,K)
    return t


def transform_batched_vector_full_output_nlatent(K, W):
    """X = ...N(OR^rep)I, so vector has shape ...NJ(OR^rep)1
        NR, ...NJ(OR)Z -> OJNZ *Z = trailing 1*

      Note: I break from the original GPMM implementation here in that I don't think we should merge O and N. If any
      thing it should be NJOZ. We don't use this branch in reality anyway, so I'm ambivalent
   """
    assert K.shape[-2] % W.shape[-1] == 0, f"expected W.shape[-1] to be integer divisor of K.shape[-2]"
    O = K.shape[-2] // W.shape[-1]
    Ks = split_axis(K, -2, O)
    t = torch.einsum('nr,...njorz->ojnz',W,Ks)
    return t


class GPMM_sase_aux(GPMM):
    """
    A flavor of GPMM where we feed the wrapped layer torch.cat(internal_axis, g(X)), where g is a function that reduces
    the input to this layer to some manageable dimension and also tiles it to match the internal_axis shape.

    For now, I will hard code g(X) to simply return the first moment of X
    """
    def __init__(self, layer, input_axis_len, input_features, output_axis_len, aux_scale, aux_center,
                 dtype=torch.get_default_dtype()):
        super().__init__(layer, input_axis_len, input_features, output_axis_len, dtype)
        self._build_config['kind'] = type(self).__name__
        aux_scale = validate_scalar_parameter(aux_scale, 'aux_scale', dtype=self.dtype)
        aux_center = validate_scalar_parameter(aux_center, 'aux_center', dtype=self.dtype)
        self._build_config['aux_scale'] = float(aux_scale)
        self._build_config['aux_center'] = float(aux_center)
        self.register_buffer('aux_scale', aux_scale)
        self.register_buffer('aux_center', aux_center)

    def _matrix_reduction_expr(self, x, W, marginalize_input, marginalize_output):
        if marginalize_input and marginalize_output:
            L = Lambda(self.name + '_full_marg_cov_transform')
            e = Apply(L, x, W)
        elif marginalize_input and not marginalize_output:
            L = Lambda(self.name + '_marg_in_cov_transform')
            e = Apply(L, x, W)
        elif not marginalize_input and marginalize_output:
            raise NotImplementedError('in sase aux, we forbid setting marginalize_input to False')
        else:
            raise NotImplementedError('in sase aux, we forbid setting marginalize_input to False')
        return e

    def _vector_reduction_expr(self, x, W, marginalize_input, marginalize_output):
        if marginalize_output:
            L = Lambda(self.name + '_marg_out_vec_transform')
            e = Apply(L, x, W)
        else:
            L = Lambda(self.name + '_vec_transform')
            e = Apply(L, x, W)
        return e

    def _populate_symbols(self):
        leaves = {
            self.name + '_in_axis': self._input_axis,
            self.name + '_out_axis': self._output_axis,
            self.name + '_in_axis_reps': self._input_features,
            self.name + '_vec_transform': transform_batched_vector_full_output_nlatent,
            self.name + '_marg_out_vec_transform': transform_batched_vector_full_marginal_nlatent,
            self.name + '_full_marg_cov_transform': transform_batched_matrix_full_marginal_nlatent,
            self.name + '_marg_in_cov_transform': transform_batched_matrix_full_output_nlatent,
        }

        return leaves

    def aux_input(self, x):
        # ix = internal axis. Has shape depending on marginalization state:
        # ix = CartesianRepProduct(out_axis, in_axis, in_axis_reps), so it will be (OR^reps)(reps +1). When
        # marginalize_output = True, we get O(R^reps)(reps +1), a 3D tensor.
        with torch.no_grad():
            # we have asserted elswhere that x is always 2D: NxR. So we can be pretty specific in code here.
            # 1R * NR -> N -> N1
            first_moment = ((self._input_axis.unsqueeze(-2)*x).sum(dim=-1)/x.sum(dim=-1)).unsqueeze(-1)
            first_moment = (first_moment - self.aux_center)/self.aux_scale
            first_moment = first_moment.unsqueeze(-3).expand(int(self._output_axis_len), -1, -1)  # ON1
            # We want to have N pushed to dim = -4, so it interacts as a batch dim and isn't confused with O
            # need to clean that up in the sample / split sample / components methods
            first_moment = first_moment.transpose(-3,-2)  # NO1
            first_moment = first_moment.unsqueeze(-2)  # NO11
            # NOR^p1
            first_moment = first_moment.expand(-1, -1, int(self._input_axis_len)**int(self._input_features), -1)
            if not self.marginalize_output:
                first_moment = merge_axis(first_moment, -3, -2)  # N(OR^p)1
            # the intent here was to match the shape of the internal input up to the final dim, so that we can
            # concatenate them and form single input of shape NOR^p(p+2) or N(OR^p)(p+2). Normally the internal
            # input shape is NOR^p(p+1) or N(OR^p)(p+1)
        return first_moment

    def sample(self, x, **kwargs):
        multisamples = self.layer.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        ai = self.aux_input(x)
        i = self.internal_input().unsqueeze(0)
        i = i.expand(*([ai.shape[0]] + (i.dim() - 1) * [-1]))
        ix = torch.cat((i, ai), dim=-1)
        sample, kl = self.layer(ix, **{**kwargs, 'multisamples': multisamples})
        if self.activation is not None:
            sample = self.activation(sample)
        leaves = {
            self.name + '_sample': sample,
            self.name + '_kl': kl,
            self.name + '_W': x,
            **self._populate_symbols(),
        }
        assert x.dim() == 2, "only designed for 2D weights: NxR"
        r = self._sample(**leaves)
        tsample = getattr(r, self.name + '_transformed_sample')
        kl = getattr(r, self.name + '_sample_kl')
        return tsample, kl

    def split_sample(self, x, **kwargs):
        multisamples = self.layer.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        ai = self.aux_input(x)
        i = self.internal_input().unsqueeze(0)
        i = i.expand(*([ai.shape[0]] + (i.dim() - 1) * [-1]))
        ix = torch.cat((i, ai), dim=-1)
        s, d = self.layer.split_sample(ix, **{**kwargs, 'multisamples': multisamples})
        if self.activation is not None:
            # WARNING: if any non-linear activation is applied, we break the gaussian assumption of SDE flow
            # (the main use-case for split sample). If you have some other use case, by all means, go on...
            s = self.activation(s)
        leaves = {
            self.name + '_split_sample': s,
            self.name + '_split_mean': d,
            self.name + '_W': x,
            **self._populate_symbols(),
        }
        assert x.dim() == 2, "only designed for 2D weights: NxR"
        r = self._split_sample(**leaves)
        ssample = getattr(r, self.name + '_transformed_split_sample')
        split_mean = getattr(r, self.name + '_transformed_split_mean')
        return ssample, split_mean

    def components(self, x, **kwargs):
        ai = self.aux_input(x)
        i = self.internal_input().unsqueeze(0)
        i = i.expand(*([ai.shape[0]] + (i.dim() - 1) * [-1]))
        ix = torch.cat((i, ai), dim=-1)
        if self.activation is None:
            mu, var, kl = self.layer.components(ix, **kwargs)
            leaves = {
                self.name + '_mu': mu,
                self.name + '_var': var,
                self.name + '_kl': kl,
                self.name + '_W': x,
                **self._populate_symbols(),
            }
            assert x.dim() == 2, "only designed for 2D weights: NxR"
            r = self._components(**leaves)
            tmu = getattr(r, self.name + '_transformed_mu')
            tvar = getattr(r, self.name + '_transformed_var')
            return tmu, tvar, getattr(r, self.name + '_component_kl')
        else:
            raise ValueError('Cannot compute components if an activation is supplied, we can only sample in this case')

    def _calc_sample_shape(self):
        """ input here is the weight matrix, of size ND. So N -> -2, D = -1"""
        if not self.marginalize_output:
            # This is a change from vanilla GPMM
            shape = propagate_shape([None,'O',-2,'I'], self.layer.sample_shape, flatten=False)
        else:
            shape = propagate_shape([None,'O',-2,'I'], self.layer.sample_shape, flatten=False)
        return shape

    def _calc_mean_shape(self):
        if not self.marginalize_output:
            # this is a change from vanilla GPMM
            shape = propagate_shape([None,'O',-2,'I'], self.layer.mean_shape, flatten=False)
        else:
            shape = propagate_shape([None,'O',-2,'I'], self.layer.mean_shape, flatten=False)
        return shape

    def _calc_cov_shape(self):
        """
        Note that some hacking of the shape is needed here, since our transformation alters the covariance shape
        beyond what propagate shape can know or account for.
        """
        if self.marginalize_input and self.marginalize_output:
            shape = propagate_shape([None, 'O', -2, 'I'], self.layer.cov_shape, flatten=False)
            shape[-1] = 1
        elif self.marginalize_input and not self.marginalize_output:
            shape = propagate_shape([None, ['O', -2], 'I'], self.layer.cov_shape, flatten=False)
            shape[-1] = shape[-1][0]
            shape[-2] = shape[-2][0]
            shape.insert(-3, -2)
        elif not self.marginalize_input and self.marginalize_output:
            raise NotImplementedError('GPMM_sase_aux forbids marginalize_input = False')
        else:
            raise NotImplementedError('GPMM_sase_aux forbids marginalize_input = False')
        return shape

