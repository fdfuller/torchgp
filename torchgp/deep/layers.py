from torchgp.symbolic import *
import torch
import numpy as np
import math
from .util import *
from .variational_parameterization import *
from .inducing import *
from .mo_transforms import *
from ..kernels import *
from ast import parse
from matchpy import Arity
from ..means import BatchConstantMean, LinearMean, MeanSymbol, EvaluateMean, mean_factory
from ..constraints import default_mapping
from torch.distributions import constraints
from collections import OrderedDict
from itertools import chain
from torch.optim import Adam
from ..natural_gradient.optimizer import ClippedNaturalGradient
from .symbolic_shape import *
from abc import ABC, abstractmethod
import collections
import torch.nn.functional as F

__all__ = [
    "Layer",
    "AbstractLayerContainer",
    "AbstractLayerWrapper",
    "LayerSequential",
    "OutputConcatenationLayer",
    "SkipLayer",
    "SliceInputLayer",
    "ProductScaleLayer",
    "AuxSkipLayer",
    "Regularizer",
    "merge_regularizers",
    'sqrt_var_final_minimal',
    'whitened_sqrt_var_final_minimal',
    'sqrt_var_hidden_minimal',
    'whitened_sqrt_var_hidden_minimal',
    'sqrt_var_hidden_flexible',
    'sqrt_var_final_flexible',
    'whitened_sqrt_var_hidden_flexible',
    'whitened_sqrt_var_final_flexible',
    'whitened_sqrt_var_final_minimal_nat_grad',
    'whitened_sqrt_var_hidden_minimal_nat_grad'
]


# some utility functions that are highly specific to the layer module

def _merge_last_four_to_two(C):
    """
    When we ask for full covariance after transformation we get a tensor with shape ...DNDN. In order
    to sample, we need to merge that down to ...(DN)(DN), where (DN) indicates a single axis of size D*N.
    With the axes merged, we can now call CholeskyRoot on the tensor and get a root over all 4 axes
    """
    C = MergeAxis(C, Constant(-2), Constant(-1)) #...DNDN -> ...DN(DN)
    C = MergeAxis(C, Constant(-3), Constant(-2)) #...DN(DN) -> ...(DN)(DN)
    return C


def _unmerge_last_two_to_four(C, D_shape):
    """
    The inverse of _merge_last_four_to_two, which is to be used on a tensor after the cholesky factor is computed;
    in the case that such a thing is desired. To be clear, we implement the transformation:

    ...(DN)(DN) -> ...DNDN. We need to know what size D is, however, which is why it's an argument
    """
    C = UnmergeAxis(C, Constant(-1), Constant(-2), D_shape) #...(DN)(DN) -> ...(DN)DN
    C = UnmergeAxis(C, Constant(-3), Constant(-4), D_shape) #...(DN)DN -> ...DNDN
    return C


class BatchedStdNormal(TensorOperation):
    """
    For a root of shape ...NM, where M = N for full cov and M = 1 for marginalized, then this produces
    a sample of shape ...NS, i.e. the last dim shape is ignored and replaced with S, the number of multiple ind samples
    one wants to take for a given state of the process
    """
    name = "BatchedStdNormal"
    arity = Arity(2, True)

    @property
    def evaluated(self):
        op = self.operands[0].evaluated
        S = self.operands[1].evaluated
        return torch.randn(list(op.shape[:-1])+[S], dtype=op.dtype, device=op.device)

    def to_ast(self, label, *arglabels, source=False):
        op = arglabels[0]
        S = arglabels[1]

        src = f"{label} = torch.randn(list({op}.shape[:-1])+[{S}], dtype={op}.dtype, device={op}.device)"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


def ind_posterior_expr(variational_parameterization, Kuu, Kfu, Kff,
                               marginalize_input=True, inducing_prior_mean=None, data_prior_mean=None):
    """ returns an expression for the posterior of the independent GPs, handling some conditional options"""
    if marginalize_input:
        ind_post_cov = variational_parameterization.marginal_posterior_covariance(Kuu, Kfu, Kff)  # ...JN1
    else:
        ind_post_cov = variational_parameterization.posterior_covariance(Kuu, Kfu, Kff)  # ...JNN
    # ...JN1
    ind_post_mean = variational_parameterization.posterior_mean(Kuu, Kfu, inducing_prior_mean=inducing_prior_mean,
                                                                data_prior_mean=data_prior_mean)
    return ind_post_mean, ind_post_cov


def transformed_posterior_expr(transform_symbol, ind_post_mean, ind_post_cov,
                                     marginalize_input = True, marginalize_output = True):
    """ returns an expression for the posterior after transformation, handling relevant options """
    if transform_symbol is not None:
        # apply the transforms
        if not marginalize_input and not marginalize_output:
            # full cov case
            post_cov = ApplyMOTranFullCov(transform_symbol, ind_post_cov)  # ...DNDN
        elif marginalize_input and not marginalize_output:
            # full cov on output
            post_cov = ApplyMOTranMarginalInput(transform_symbol, ind_post_cov)  # ...DNN
        elif not marginalize_input and marginalize_output:
            # full cov on input
            post_cov = ApplyMOTranMarginalOutput(transform_symbol, ind_post_cov)  # ...NDD
        else:
            # full marginal
            post_cov = ApplyMOTranFullMarginal(transform_symbol, ind_post_cov)  # ...DN1
        post_mean = ApplyMOTran2Vec(transform_symbol, ind_post_mean)  # ...DN1
    else:
        if not marginalize_input and not marginalize_output:
            # full cov case
            raise ValueError('No transform supplied. You should specify marginal_output = True')
        elif marginalize_input and not marginalize_output:
            # full cov on output.
            raise ValueError('No transform supplied. You should specify marginal_output = True')
        else:
            post_cov = ind_post_cov  # note: marginalization conditional logic already handled
            post_mean = ind_post_mean  # ...JN1
    return post_mean, post_cov


def _fallback_sampler_expr(post_mean, post_cov, multi_samples, jitter, marginalize_input=True, marginalize_output=True,
                           split_sample=False):
    # target shape for sample is ...SND
    deterministic_part = None
    if not marginalize_input and not marginalize_output:
        # full cov case
        D = Select(Constant(-2), Shape(post_cov))
        # To compute the root we need to merge axes so ...DNDN -> ...(DN)(DN)
        post_cov_reshaped = _merge_last_four_to_two(post_cov)
        # we also need to reshape the mean to match ...DN1 -> ...(DN)1
        post_mean_reshaped = MergeAxis(post_mean, Constant(-3), Constant(-2))
        post_cov_root = CholeskyRoot(AddJitter(post_cov_reshaped, jitter))  # ...(DN)(DN)
        epsilon = BatchedStdNormal(post_cov_root, multi_samples)  # ...(DN)S
        if not split_sample:
            sample = post_cov_root @ epsilon + post_mean_reshaped  # ...(DN)(DN) @ ...(DN)S -> ...(DN)S + ...(DN)1 -> ...(DN)S
        else:
            sample = post_cov_root @ epsilon  # ...(DN)(DN) @ ...(DN)S -> ...(DN)S
            deterministic_part = ExpandDim(post_mean_reshaped, Constant(-1), multi_samples)  # (DN)1 -> (DN)S
        sample = UnmergeAxis(sample, Constant(-2), Constant(-3), D)  # ...DNS
        sample = SwapDims(sample, Constant(-1), Constant(-3))  # ...SND
    elif marginalize_input and not marginalize_output:
        # full cov on output
        post_cov_root = CholeskyRoot(AddJitter(post_cov, jitter))  # ...NDD
        epsilon = BatchedStdNormal(post_cov_root, multi_samples)  # ...NDS
        post_mean_reshaped = SwapDims(post_mean, Constant(-3), Constant(-2))  # ...DN1 -> ...ND1
        if not split_sample:
            sample = post_cov_root @ epsilon + post_mean_reshaped  # ...NDD @ ...NDS -> ...NDS + ...ND1 -> ...NDS
        else:
            sample = post_cov_root @ epsilon
            deterministic_part = ExpandDim(post_mean_reshaped, Constant(-1), multi_samples)  # ND1 -> NDS
        sample = MoveAxis(sample, Constant(-1), Constant(-3))  # ...NDS -> ...SND
    elif not marginalize_input and marginalize_output:
        # full cov on input
        post_cov_root = CholeskyRoot(AddJitter(post_cov, jitter))  # ...DNN
        epsilon = BatchedStdNormal(post_cov_root, multi_samples)  # ...DNS
        if not split_sample:
            sample = post_cov_root @ epsilon + post_mean  # ...DNN @ ...DNS + ...DN1 -> ...DNS
        else:
            sample = post_cov_root @ epsilon
            deterministic_part = ExpandDim(post_mean, Constant(-1), multi_samples)  # ...DN1 -> ...DNS
            deterministic_part = SwapDims(deterministic_part, Constant(-1), Constant(-3))  # DNS -> SND
        sample = SwapDims(sample, Constant(-1), Constant(-3))  # ...SND
    else:
        # full marginal
        post_cov_root = Sqrt(post_cov)  # ...DN1
        epsilon = BatchedStdNormal(post_cov_root, multi_samples)  # ...DNS
        if not split_sample:
            sample = post_cov_root * epsilon + post_mean  # ...DN1 * ...DNS + ...DN1 -> ...DNS
        else:
            sample = post_cov_root * epsilon
            deterministic_part = ExpandDim(post_mean, Constant(-1), multi_samples)  # DN1 -> DNS
            deterministic_part = SwapDims(deterministic_part, Constant(-1), Constant(-3))  # DNS -> SND
        sample = SwapDims(sample, Constant(-1), Constant(-3))  # ...SND
    if not split_sample:
        return sample
    else:
        return sample, deterministic_part


def _linear_coregionalization_sampler_expr(ind_post_mean, ind_post_cov, multi_samples, transform_symbol, jitter,
                                           marginalize_input=True, split_sample=False):
    deterministic_part = None  # check if None downstream for some configuration error
    if not marginalize_input:
        ind_post_cov_root = CholeskyRoot(AddJitter(ind_post_cov, jitter))  # ...JNN
        epsilon = BatchedStdNormal(ind_post_cov_root, multi_samples)  # ...JNS
        if not split_sample:
            ind_sample = ind_post_cov_root @ epsilon + ind_post_mean  # ...JNS + ...JN1 -> ...JNS
        else:
            ind_sample = ind_post_cov_root @ epsilon  # ...JNS
            deterministic_part = ExpandDim(ind_post_mean, Constant(-1), multi_samples)  # JN1 -> JNS
    else:
        ind_post_cov_root = Sqrt(ind_post_cov)  # ...JN1
        epsilon = BatchedStdNormal(ind_post_cov_root, multi_samples)  # ...JNS
        if not split_sample:
            ind_sample = ind_post_cov_root * epsilon + ind_post_mean  # ...JN1 * ...JNS + ...JN1 -> ...JNS
        else:
            ind_sample = ind_post_cov_root * epsilon
            deterministic_part = ExpandDim(ind_post_mean, -1, multi_samples)  # JN1 -> JNS
    ind_sample = UnsqueezeDim(MoveAxis(ind_sample, Constant(-1), Constant(-3)), Constant(-1))  # ...JNS -> ...SJN1
    sample = ApplyMOTran2Vec(transform_symbol, ind_sample)  # ...SDN1
    sample = Transpose(SqueezeDim(sample, Constant(-1)))  # ...SDN1 -> ...SDN -> ...SND
    if not split_sample:
        return sample
    else:
        # we need to transform the "deterministic part" as the sample was transformed
        # ...JNS -> ...SJN1
        deterministic_part = UnsqueezeDim(MoveAxis(deterministic_part, Constant(-1), Constant(-3)), Constant(-1))
        deterministic_part = ApplyMOTran2Vec(transform_symbol, deterministic_part)  # ...SJN1 -> ...SDN1
        deterministic_part = Transpose(SqueezeDim(deterministic_part, Constant(-1)))  # ...SDN1 -> ...SDN -> ...SND
        return sample, deterministic_part


def sampler_expr(multi_samples, ind_post_mean, ind_post_cov, post_mean, post_cov,
                 transform_symbol, jitter,
                 marginalize_input=True, marginalize_output=True, linear_coregionalization=True, split_sample=False):
    """ generates a symbolic expression for the sampler, handling various options"""
    if linear_coregionalization and not marginalize_output:
        sample = _linear_coregionalization_sampler_expr(ind_post_mean, ind_post_cov, multi_samples, transform_symbol,
                                                        jitter, marginalize_input, split_sample=split_sample)
    else:
        sample = _fallback_sampler_expr(post_mean, post_cov, multi_samples, jitter,
                                        marginalize_input, marginalize_output, split_sample=split_sample)
    return sample  # when split_sample = True, this is a tuple of (zero_mean_sample, deterministic part)


Regularizer = collections.namedtuple('Regularizer', ('kl','iw'), defaults=(None,None))


def flatten_lists(l):
    for el in l:
        if isinstance(el, list):
            yield from flatten_lists(el)
        else:
            yield el

def merge_regularizers(*xs):
    kls = [x.kl for x in xs if x.kl is not None]
    iws = [x.iw for x in xs if x.iw is not None]
    iw = None
    kl = None
    if len(kls)>0:
        kl = torch.cat(kls)
    if len(iws)>0:
        iw = list(flatten_lists(iws)) #torch.cat(iws,dim=-1)
    return Regularizer(kl,iw)


class Layer(torch.nn.Module):
    def __init__(self,
                 num_gps: int,
                 num_inducing: int,
                 in_features: int,
                 out_features: int,
                 dtype=torch.get_default_dtype(),
                 name='L'
                 ):
        super().__init__()
        # options required for construction
        self.num_gps = int(num_gps)
        self.num_inducing = int(num_inducing)
        self._in_features = int(in_features)
        self._out_features = int(out_features)
        self.dtype = dtype
        self.name = name
        self._use_natural_gradients = False

        # options to be fufilled by builder pattern
        self.inducing = None
        self.data_mean_func = None
        self.prior_mean_func = None
        self.kernel = None
        self.vp = None
        self.output_transform = None
        self.marginalize_input = None
        self.marginalize_output = None
        self.multisamples = None
        self._mean_shape = None
        self._cov_shape = None
        self._sample_shape = None

        # storing information related to construction
        self._build_config = {'kind': type(self).__name__, 'num_gps': num_gps, 'num_inducing': num_inducing,
                              'in_features': in_features, 'out_features': out_features, 'dtype': str(dtype),
                              'name': name}


        # Symbols that this object holds
        self.K = Kernel(self.name + '_kernel')
        self.Z = Tensor(self.name + '_inducing')
        self.X = Tensor(self.name + '_data_axis')
        self.multi_samples = Scalar(self.name + '_multi_samples')
        self.jitter = Scalar(self.name + '_jitter')
        self.transform_symbol = None if self.output_transform is None else MOTransformSymbol(self.name + '_motransform')
        self.prior_mu = None if self.prior_mean_func is None else MeanSymbol(self.name + '_prior_mean_func')
        self.data_mu = None  if self.data_mean_func is None else MeanSymbol(self.name + '_data_mean_func')

    @property
    def in_features(self):
        return self._in_features

    @property
    def out_features(self):
        return self._out_features

    @property
    def num_layers(self):
        return 1

    @property
    def sample_shape(self):
        if self._sample_shape is None:
            raise ValueError('layer not yet built')
        return self._sample_shape

    @property
    def mean_shape(self):
        if self._mean_shape is None:
            raise ValueError('layer not yet built')
        return self._mean_shape

    @property
    def cov_shape(self):
        if self._cov_shape is None:
            raise ValueError('layer not yet built')
        return self._cov_shape

    @property
    def use_natural_gradients(self):
        return self._use_natural_gradients

    @property
    def ready_to_build(self):
        flag = False
        if self.inducing is not None and \
            self.kernel is not None and \
            self.vp is not None and \
            self.marginalize_input is not None and \
            self.multisamples is not None and \
            self.marginalize_output is not None:
            flag = True
        return flag

    @property
    def build_config(self):
        return self._build_config


    def update_inducing(self, inducing_kind: str, **kwargs):
        extra_kwargs = {}
        self._build_config['inducing'] = {'arg': inducing_kind, 'kwargs': kwargs}
        Z = None if 'Z' not in kwargs else kwargs['Z']
        range_min = None if 'range_min' not in kwargs else kwargs['range_min']
        range_max = None if 'range_max' not in kwargs else kwargs['range_max']
        if Z is not None:
            extra_kwargs['Z'] = Z
        if range_min is not None:
            extra_kwargs['range_min'] = range_min
        if range_max is not None:
            extra_kwargs['range_max'] = range_max
        normal_inducing = None if 'normal_inducing' not in kwargs else kwargs['normal_inducing']
        if normal_inducing is not None:
            extra_kwargs['normal_inducing'] = normal_inducing
        inducing = inducing_factory({inducing_kind: {'num_gps': self.num_gps, 'num_inducing': self.num_inducing,
                                                     'inducing_dim': self.in_features, 'dtype': self.dtype,
                                                     **extra_kwargs}})
        self.inducing = inducing
        return self

    def update_kernel(self, kernel_dict, **kwargs):
        kwargs = {**kwargs, 'num_gps': self.num_gps, 'dtype': self.dtype}
        self._build_config['kernel'] = {'arg': kernel_dict, 'kwargs': kwargs}
        kernel = kernel_factory(kernel_dict, **kwargs)
        self.kernel = kernel
        return self

    def update_variational_parameterization(self, vp_kind: str, **kwargs):
        vp_kind = str(vp_kind)
        self._build_config['variational_parameterization'] = {'arg': vp_kind, 'kwargs': kwargs}
        jitter = None if 'jitter' not in kwargs else kwargs['jitter']
        vp = variational_parameterization_factory({vp_kind: {'name': self.name + '_vp', 'dtype': self.dtype,
                                                             'num_gps': self.num_gps,
                                                             'num_inducing': self.num_inducing, 'jitter': jitter}})
        self.vp = vp
        return self

    def update_prior_mean(self, prior_mean_kind: str, **kwargs):
        prior_mean_kind = str(prior_mean_kind)
        n_out = self.out_features if self.transform_symbol is not None else self.num_gps
        self._build_config['prior_mean'] = {'arg': prior_mean_kind, 'kwargs': kwargs}
        prior_mean_func = mean_factory(
            {prior_mean_kind: {'in_features': self.in_features, 'out_features': n_out,
                               'dtype': self.dtype}})
        self.prior_mean_func = prior_mean_func
        self.prior_mu = None if self.prior_mean_func is None else MeanSymbol(self.name + '_prior_mean_func')
        return self

    def update_data_mean(self, data_mean_kind: str, **kwargs):
        data_mean_kind = str(data_mean_kind)
        n_out = self.out_features if self.transform_symbol is not None else self.num_gps
        self._build_config['data_mean'] = {'arg': data_mean_kind, 'kwargs': kwargs}
        data_mean_func = mean_factory(
            {data_mean_kind: {'in_features': self.in_features, 'out_features': n_out,
                              'dtype': self.dtype}})
        self.data_mean_func = data_mean_func
        self.data_mu = None if self.data_mean_func is None else MeanSymbol(self.name + '_data_mean_func')
        return self

    def update_output_transform(self, output_transform_kind: str, **kwargs):
        output_transform_kind = str(output_transform_kind)
        self._build_config['output_transform'] = {'arg': output_transform_kind, 'kwargs': kwargs}
        output_transform = transform_factory({output_transform_kind: {'in_features': self.num_gps,
                                                                      'out_features': self.out_features,
                                                                      'dtype': self.dtype}})
        self.output_transform = output_transform
        self.transform_symbol = None if self.output_transform is None else MOTransformSymbol(self.name + '_motransform')
        return self

    def update_marginalization(self, **kwargs):
        marginalize_input = True if 'marginalize_input' not in kwargs else kwargs['marginalize_input']
        marginalize_output = True if 'marginalize_output' not in kwargs else kwargs['marginalize_output']
        self._build_config['marginalization'] = {'arg': None, 'kwargs': kwargs}
        self.marginalize_input = bool(marginalize_input)
        self.marginalize_output = bool(marginalize_output)
        return self

    def update_natural_gradient_preference(self, **kwargs):
        use_natural_gradients = False if 'use_natural_gradients' not in kwargs else kwargs['use_natural_gradients']
        if self.vp is not None and self.vp.natural_gradient_capable:
            self._use_natural_gradients = use_natural_gradients
        else:
            use_natural_gradients = False
            print('variational parameterization does not support natural gradients or is None')
        self._build_config['natural_gradient_preference'] = {'arg': None,
                                    'kwargs': {'use_natural_gradients': use_natural_gradients}}
        return self

    def update_multisamples(self, num: int, **unused):
        assert int(num) >= 1, "must be a strictly positive integer"
        self.multisamples = int(num)
        self._build_config['multisamples'] = {'arg': int(num), 'kwargs': unused}
        return self

    def update_from_dict(self, dict_in):
        for key in dict_in:
            if hasattr(self, 'update_' + str(key)):
                if dict_in[key]['arg'] is not None:
                    getattr(self, 'update_' + str(key))(dict_in[key]['arg'], **dict_in[key]['kwargs'])
                else:
                    getattr(self, 'update_' + str(key))(**dict_in[key]['kwargs'])
        return self

    def variational_parameters(self):
        return self.vp.parameters()

    def named_variational_parameters(self):
        return self.vp.named_parameters()

    def hyper_parameters(self):
        params = []
        if self.inducing is not None:
            params = chain(params, filter(lambda x: x.requires_grad, self.inducing.parameters()))
        if self.data_mean_func is not None:
            params = chain(params, filter(lambda x: x.requires_grad, self.data_mean_func.parameters()))
        if self.prior_mean_func is not None:
            params = chain(params, filter(lambda x: x.requires_grad, self.prior_mean_func.parameters()))
        if self.kernel is not None:
            params = chain(params, filter(lambda x: x.requires_grad, self.kernel.parameters()))
        if self.output_transform is not None:
            params = chain(params, filter(lambda x: x.requires_grad, self.output_transform.parameters()))
        return params

    def named_hyper_parameters(self):
        params = []
        if self.inducing is not None:
            params = chain(params, filter(lambda x: x[1].requires_grad, self.inducing.named_parameters()))
        if self.data_mean_func is not None:
            params = chain(params, filter(lambda x: x[1].requires_grad, self.data_mean_func.named_parameters()))
        if self.prior_mean_func is not None:
            params = chain(params, filter(lambda x: x[1].requires_grad, self.prior_mean_func.named_parameters()))
        if self.kernel is not None:
            params = chain(params, filter(lambda x: x[1].requires_grad, self.kernel.named_parameters()))
        if self.output_transform is not None:
            params = chain(params, filter(lambda x: x[1].requires_grad, self.output_transform.named_parameters()))
        return params

    def build_hyper_optimizer(self, default_lr = 1e-3, **kwargs):
        inducing_lr = default_lr if 'inducing_lr' not in kwargs else float(kwargs['inducing_lr'])
        data_mean_lr = default_lr if 'data_mean_lr' not in kwargs else float(kwargs['data_mean_lr'])
        prior_mean_lr = default_lr if 'prior_mean_lr' not in kwargs else float(kwargs['prior_mean_lr'])
        kernel_lr = default_lr if 'kernel_lr' not in kwargs else float(kwargs['kernel_lr'])
        output_transform_lr = default_lr if 'output_transform_lr' not in kwargs else float(kwargs['output_transform_lr'])
        if self.inducing is not None:
            inducing_param_dict = {'params': filter(lambda x: x.requires_grad, self.inducing.parameters()),
                               'lr': inducing_lr}
        else:
            inducing_param_dict = None
        if self.data_mean_func is not None:
            data_mean_param_dict = {'params': filter(lambda x: x.requires_grad, self.data_mean_func.parameters()),
                                'lr': data_mean_lr}
        else:
            data_mean_param_dict = None
        if self.prior_mean_func is not None:
            prior_mean_dict = {'params': filter(lambda x: x.requires_grad, self.prior_mean_func.parameters()),
                           'lr': prior_mean_lr}
        else:
            prior_mean_dict = None
        if self.kernel is not None:
            kernel_dict = {'params': filter(lambda x: x.requires_grad, self.kernel.parameters()),
                       'lr': kernel_lr}
        else:
            kernel_dict = None
        if self.output_transform is not None:
            output_transform_dict = {'params': filter(lambda x: x.requires_grad, self.output_transform.parameters()),
                                 'lr': output_transform_lr}
        else:
            output_transform_dict = None
        dicts = [d for d in [inducing_param_dict, data_mean_param_dict, prior_mean_dict, kernel_dict,
                                              output_transform_dict] if d is not None ]
        if len(dicts) == 0:
            raise ValueError('found no optimizable children, did you build the layer yet?')
        return (Adam(dicts,eps=1e-7),)  # needs to be iterable so that I can chain them in layer containers

    def build_variational_optimizer(self, default_lr = 1e-3, **kwargs):
        norm_limit = 0.3 if 'norm_limit' not in kwargs else kwargs['norm_limit']
        natural_lr = None if 'natural_lr' not in kwargs else kwargs['natural_lr']
        if self.vp is not None:
            if self.use_natural_gradients:
                if natural_lr is not None:
                    lr = natural_lr
                else:
                    lr = default_lr
                op = ClippedNaturalGradient(params=self.variational_parameters(),
                                       stepper=self.vp.build_natural_gradient_stepper(),
                                       lr=lr,
                                       norm_limit=norm_limit)
            else:
                op = Adam([{'params': self.variational_parameters(), 'lr': default_lr, 'eps': 1e-7}])
            return (op,)  # needs to be iterable so that I can chain them in layer containers
        else:
            raise ValueError('found no variational parameterization, did you build the layer yet?')

    def build(self):
        if self.ready_to_build:
            self._init_vp()
            if self.output_transform is None:
                assert self.num_gps == self.out_features, "without a transform num_gps must equal out_features"
            # because the mean functions' dimensionality depends on whether or not a transformation has been
            # specified, we update them once user indicates its clear to build, just in case they added means before
            # they added a transformation.
            self.update_from_dict({'data_mean': self.build_config['data_mean'],
                                   'prior_mean': self.build_config['prior_mean']})
            post_mean, post_cov, sample, kl, split_sample, split_mean = self._produce_exprs(self.marginalize_input,
                                                                                    self.marginalize_output)
            self._compile(post_mean, post_cov, sample, kl, split_sample, split_mean)
            if self.use_natural_gradients:
                self.vp.build_natural_gradient_stepper()
            self._sample_shape = self._calc_sample_shape()
            self._mean_shape = self._calc_mean_shape()
            self._cov_shape = self._calc_cov_shape()
            self._build_config['kind'] = type(self).__name__
        else:

            print('The following must be updated before building:')
            if self.inducing is None:
                print('inducing')
            if self.kernel is None:
                print('kernel')
            if self.vp is None:
                print('variational parameterization')
            if self.marginalize_output is None:
                print('marginalize output flag')
            if self.marginalize_input is None:
                print('marginalize input flag')
            if self.multisamples is None:
                print('multisamples is not set')
            raise ValueError('cannot initialize until build is complete')
        return self

    def _populate_symbols(self):
        """ For populating symbols the layer owns (and symbolic children) with tensors """
        leaves = {
            self.name + '_kernel': self.kernel,
            self.name + '_inducing': self.inducing.Z,
            self.name + '_motransform': self.output_transform,
            self.name + '_prior_mean_func': self.prior_mean_func,
            self.name + '_data_mean_func': self.data_mean_func,
            self.name + '_jitter': self.vp.jitter_val,
            ** self.vp._populate_symbols(),
        }
        return leaves

    def sample(self, x, **kwargs):
        """
        The main purpose of the layer is to allow one to sample from the posterior of the layer,
        which is why the forward calls this method (by default).

        args x: input axis to evaluate sample on
        kwargs:
            'multi_samples': integer number of independent samples to compute per batched axis condition, defaults to 1

        :return: a tuple of:
                1. a sample from the posterior of the layer,
                2. the kl of the posterior q to the prior, evaluated on the inducing points
        """
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        leaves = {
            self.name + '_data_axis': x,
            **self._populate_symbols(),
            self.name + '_multi_samples': multisamples,
        }
        comps = self._sample(**leaves)
        s = getattr(comps, self.name + '_sample')
        kl = getattr(comps, self.name + '_kl')
        s = s.squeeze(-3) #if S == 1, then batch shape of input to next layer will change shape without this squeeze
        return s, Regularizer(kl)

    def split_sample(self, x, **kwargs):
        """
        Similar to sample, but split into deterministic part (the mean) and random part. Used mainly in SDE flows.
        Allows one to scale the deterministic part separate from the random part. The two parts will be
        broadcast compatible; i.e. you should be able to add them together without issue after scaling.

        args x: input axis to evaluate sample on
        kwargs:
            'multi_samples': integer number of independent samples to compute per batched axis condition, defaults to 1

        :return: tuple of a sample from the posterior of the layer with zero mean, and the posterior mean as a separate
                    tensor
        """
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        leaves = {
            self.name + '_data_axis': x,
            **self._populate_symbols(),
            self.name + '_multi_samples': multisamples,
        }
        comps = self._split_sample(**leaves)
        s = getattr(comps, self.name + '_split_sample')
        mu = getattr(comps, self.name + '_split_mean')
        s = s.squeeze(-3)  # if S == 1, then batch shape of input to next layer will change shape without this squeeze
        mu = mu.squeeze(-3)  # as above
        return s, mu

    def forward(self, x, **kwargs):
        return self.sample(x, **kwargs)

    def components(self, x, **unused):
        """
        Intended for use when you want the parameters of the gaussian posterior (as well as the KL)

        args x: input axis to evaluate sample on
        :return: a tuple of:
                1. mean of the gaussian posterior
                2. covariance of the gaussian posterior
                2. the kl of the posterior q to the prior, evaluated on the inducing points
        """
        leaves = {
            self.name + '_data_axis': x,
            **self._populate_symbols(),
        }
        comps = self._components(**leaves)
        mu = getattr(comps, self.name + '_post_mean')
        cov = getattr(comps, self.name + '_post_cov')
        kl = getattr(comps, self.name + '_kl')
        return mu, cov, Regularizer(kl)

    def _produce_exprs(self, marginalize_input: bool, marginalize_output: bool):
        Kfu = self._cross_kernel()
        Kuu = self._inducing_kernel()
        Kff = self._data_kernel()
        mu_u = self._inducing_prior_mean()
        mu_f = self._data_prior_mean()
        kl = self.vp.kl(Kuu, inducing_prior_mean=mu_u)
        ind_post_mean, ind_post_cov = ind_posterior_expr(self.vp, Kuu, Kfu, Kff, marginalize_input,
                                                         inducing_prior_mean=mu_u, data_prior_mean=mu_f)
        post_mean, post_cov = transformed_posterior_expr(self.transform_symbol, ind_post_mean, ind_post_cov,
                                                         marginalize_input=marginalize_input,
                                                         marginalize_output=marginalize_output)
        sample = sampler_expr(self.multi_samples, ind_post_mean, ind_post_cov, post_mean, post_cov,
                              self.transform_symbol, self.jitter, marginalize_input=marginalize_input,
                              marginalize_output=marginalize_output,
                              linear_coregionalization=isinstance(self.output_transform,
                                                                  LinearCoregionalizationTransform))

        split_sample, split_mean = sampler_expr(self.multi_samples, ind_post_mean, ind_post_cov, post_mean, post_cov,
                              self.transform_symbol, self.jitter, marginalize_input=marginalize_input,
                              marginalize_output=marginalize_output,
                              linear_coregionalization=isinstance(self.output_transform,
                                                                  LinearCoregionalizationTransform), split_sample=True)
        # we modify posterior mean after passing it into sample, so that both can be modified now with the data_space
        # mean function.
        post_mean = self._add_data_space_mean_to_component(post_mean)  # ...DN1 + ...DN1 -> ...DN1
        sample = self._add_data_space_mean_to_sample(sample)  # ...SND + ND - > ...SND
        # note: split_mean represents the mean, but it is shaped identically to the sample. Hence use of the sample
        # function, rather than component function to add the data mean
        split_mean = self._add_data_space_mean_to_sample(split_mean)  # ...SND + ND - > ...SND
        return post_mean, post_cov, sample, kl, split_sample, split_mean

    def _compile(self, post_mean, post_cov, sample, kl, split_sample, split_mean):
        post_cov_graph = expr2graph(Label(String(self.name + '_post_cov'), post_cov))
        post_mean_graph = expr2graph(Label(String(self.name + '_post_mean'), post_mean))
        kl_graph = expr2graph(Label(String(self.name + '_kl'), kl))
        components_graph = compose_all([post_mean_graph, kl_graph, post_cov_graph])
        self._components = graph2ast(components_graph, compiled=True, debug=False)

        sample_graph = expr2graph(Label(String(self.name + '_sample'), sample))
        sample_and_kl_graph = compose_all([sample_graph, kl_graph])
        self._sample = graph2ast(sample_and_kl_graph, compiled=True, debug=False)
        split_sample_graph = expr2graph(Label(String(self.name + '_split_sample'), split_sample))
        split_mean_graph = expr2graph(Label(String(self.name + '_split_mean'), split_mean))
        split_sample_and_mean_graph = compose_all([split_sample_graph, split_mean_graph])
        self._split_sample = graph2ast(split_sample_and_mean_graph, compiled=True, debug=False)

    def _expand_data_axis(self, x):
        """
       We expect X to be of shape ...NI, but MO kernels demand that it be of shape ...JNI.
       This shape property is automatically satisfied for Z. So we must expand X.

       Fear not, we can use a replacement rule to exchange the order of expansion to outside the
       kernel call in the special case that the kernel is of type CombinationSharedKernel
       """
        e = ExpandDim(UnsqueezeDim(x, Constant(-3)), Constant(-3), Constant(self.vp.num_gps))
        return e

    def _add_data_space_mean_to_sample(self, x):
        """
            intended to add the data_space mean to a sample of shape ...ND, e.g. samples have shape ...SND.
            broadcasts over any dim except -2 and -1.
        """
        # sample shape is ...SND, data mean will produce ...ND. We need to unsqueeze the special -3 dim in X
        if self.data_mu is not None:
            data_space_mean = EvaluateMean(self.data_mu, UnsqueezeDim(self.X, Constant(-3)))  # ...1NI -> ...1ND
            x = x + data_space_mean  # ...SND + ...1ND -> ...SND
        return x

    def _add_data_space_mean_to_component(self, x):
        """
            intended to add the data_space mean to a component mean of shape ...DN1.
        """
        if self.data_mu is not None:
            data_space_mean = EvaluateMean(self.data_mu, self.X)  # ...NI -> ...ND
            data_space_mean_reshaped = UnsqueezeDim(Transpose(data_space_mean),Constant(-1))
            x = x + data_space_mean_reshaped  # ...DN1 + ...DN1 -> ...DN1
        return x

    def _cross_kernel(self):
        return EvaluateKernel(self.K, self._expand_data_axis(self.X), self.Z)

    def _data_kernel(self):
        return EvaluateKernelSymmetric(self.K, self._expand_data_axis(self.X))

    def _inducing_kernel(self):
        return EvaluateKernelSymmetric(self.K, self.Z)

    def _inducing_prior_mean(self):
        if self.prior_mu is not None:
            mu_u = EvaluateMean(self.prior_mu, self.Z)
        else:
            mu_u = None
        return mu_u

    def _data_prior_mean(self):
        if self.prior_mu is not None:
            mu_f = EvaluateMean(self.prior_mu, self._expand_data_axis(self.X))
        else:
            mu_f = None
        return mu_f

    def _init_vp(self):
        # create cholesky factor for vp initialization
        init_exprs = self.vp._init_exprs(self._inducing_kernel(), inducing_prior_mean=self._inducing_prior_mean())
        label_base = 'expr_'
        graphs = []
        names = []
        for k,expr in enumerate(init_exprs):
            names.append(label_base + str(k))
            graphs.append(expr2graph(Label(String(names[-1]), expr)))
        init_graphs = compose_all(graphs)
        self._vp_init = graph2ast(init_graphs, compiled=True, debug=False)
        leaves = self._populate_symbols()
        init_items = self._vp_init(**leaves)
        self.vp.storage_init(*[getattr(init_items, n) for n in names])

    # def _calc_mean_shape(self):
    #     if self.multisamples>1:
    #         shape = [None, 'S', 'D', -2, 1]
    #     else:
    #         shape = [None, 'D', -2, 1]
    #     return shape
    #
    # def _calc_cov_shape(self):
    #     """ expecting input [B, N, D], where N (-2) & D (-1) are special dims"""
    #     if self.marginalize_input and self.marginalize_output:
    #         if self.multisamples > 1:
    #             shape = [None, 'S', 'D', -2, 1]
    #         else:
    #             shape = [None, 'D', -2, 1]
    #     elif self.marginalize_input and not self.marginalize_output:
    #         if self.multisamples > 1:
    #             shape = [None, 'S', -2, 'D', 'D']
    #         else:
    #             shape = [None, -2, 'D', 'D']
    #     elif not self.marginalize_input and self.marginalize_output:
    #         if self.multisamples > 1:
    #             shape = [None, 'S', 'D', -2, -2]
    #         else:
    #             shape = [None, 'D', -2, -2]
    #     else:
    #         if self.multisamples > 1:
    #             shape = [None, 'S', 'D', -2, 'D', -2]
    #         else:
    #             shape = [None, 'D', -2, 'D', -2]
    #     return shape

    def _calc_mean_shape(self):
        shape = [None, 'D', -2, 1]
        return shape

    def _calc_cov_shape(self):
        """ expecting input [B, N, D], where N (-2) & D (-1) are special dims"""
        if self.marginalize_input and self.marginalize_output:
            shape = [None, 'D', -2, 1]
        elif self.marginalize_input and not self.marginalize_output:
            shape = [None, -2, 'D', 'D']
        elif not self.marginalize_input and self.marginalize_output:
            shape = [None, 'D', -2, -2]
        else:
            shape = [None, 'D', -2, 'D', -2]
        return shape

    def _calc_sample_shape(self):
        if self.multisamples>1:
            shape = [None, 'S', -2, 'D']
        else:
            shape = [None, -2, 'D']
        return shape


class AbstractLayerContainer(torch.nn.Module, ABC):
    """
    The purpose of a layer container is to allow several layers to be combined in some way and yet appear, in API, to
    be a single layer.

    1. The API is single layer consistant in-so-far as:
        A. the forward method for the LayerSequential will give a tuple (sample, kl), where the kl is a concatenation of
        all kl losses from each layer in the sequential.
        B. the components method returns the mean, cov, and kl of the final layer (after having propagated through all
        the layers in the sequence).

    2. Convenience function to change marginalization state (whether or not marginalize_input or marginalize_output are
     True or False) to all layers and rebuild the whole stack. Layers need not be built before being passed in.
     The changes that may be applied over all layers are. The marginalization state change is a useful feature,
     as different likelihoods will dictate which marginalization state should be used and I don't want to bother the
     user to know what one to select. Thus they can build layers with default marginalization, select the likelihood
     they want, and then they can all be made to a consistent state before being built (or rebuilt).


    """

    def __init__(self, *layers, name=''):
        super().__init__()
        self.layers = torch.nn.ModuleList(layers)
        self.name = name
        self.marginalize_input = None
        self.marginalize_output = None
        self._build_config = {'layers': [l.build_config for l in self.layers], 'name': self.name}

    @property
    def build_config(self):
        return self._build_config

    def update_marginalization(self, **kwargs):
        marginalize_input = True if not 'marginalize_input' in kwargs else kwargs['marginalize_input']
        marginalize_output = True if not 'marginalize_output' in kwargs else kwargs['marginalize_output']
        self.marginalize_input = marginalize_input
        self.marginalize_output = marginalize_output
        self._build_config['marginalization'] = {'arg': None, 'kwargs': {'marginalize_input': self.marginalize_input,
                                                                          'marginalize_output': self.marginalize_output}
                                                 }
        for l in self.layers:
            l.update_marginalization(marginalize_input=marginalize_input, marginalize_output=marginalize_output)
        return self

    def update_from_dict(self, dict_in):
        for key in dict_in:
            if hasattr(self, 'update_' + str(key)):
                if dict_in[key]['arg'] is not None:
                    getattr(self, 'update_' + str(key))(dict_in[key]['arg'], **dict_in[key]['kwargs'])
                else:
                    getattr(self, 'update_' + str(key))(**dict_in[key]['kwargs'])
        return self

    @abstractmethod
    def update_multisamples(self, num: int, **unused):
        pass


    @property
    @abstractmethod
    def multisamples(self):
        pass

    @property
    def ready_to_build(self):
        flags = [l.ready_to_build for l in self.layers]
        return np.alltrue(flags)


    @property
    @abstractmethod
    def in_features(self):
        pass

    @property
    @abstractmethod
    def out_features(self):
        pass

    @property
    @abstractmethod
    def mean_shape(self):
        pass

    @property
    @abstractmethod
    def sample_shape(self):
        pass

    @property
    @abstractmethod
    def cov_shape(self):
        pass

    @property
    def num_layers(self):
        n = 0
        for l in self.layers:
            n += l.num_layers
        return n

    @abstractmethod
    def build(self):
        pass

    def forward(self, x, **kwargs):
        return self.sample(x, **kwargs)

    @abstractmethod
    def sample(self, x, **kwargs):
        pass

    @abstractmethod
    def split_sample(self, x, **kwargs):
        pass

    @abstractmethod
    def components(self, x, **unused):
        pass

    def hyper_parameters(self):
        params = []
        for l in self.layers:
            params = chain(params, l.hyper_parameters())
        return params

    def named_hyper_parameters(self):
        params = []
        for l in self.layers:
            params = chain(params, l.named_hyper_parameters())
        return params

    def variational_parameters(self):
        params = []
        for l in self.layers:
            params = chain(params, l.variational_parameters())
        return params

    def named_variational_parameters(self):
        params = []
        for l in self.layers:
            params = chain(params, l.named_variational_parameters())
        return params

    def build_variational_optimizer(self, default_lr, **kwargs):
        ops = []
        for l in self.layers:
            ops = chain(ops, l.build_variational_optimizer(default_lr, **kwargs))
        return list(ops)

    def build_hyper_optimizer(self, default_lr, **kwargs):
        ops = []
        for l in self.layers:
            ops = chain(ops, l.build_hyper_optimizer(default_lr, **kwargs))
        return list(ops)


class AbstractLayerWrapper(torch.nn.Module, ABC):
    """ A layer wrapper should enclose a single layer (or layer container), modifying it in some way"""
    def __init__(self, layer, name=''):
        super().__init__()
        self.layer = layer
        self.name = str(name)
        self._build_config = {'layer': self.layer.build_config, 'name': self.name}

    def update_marginalization(self, **kwargs):
        marginalize_input = True if not 'marginalize_input' in kwargs else kwargs['marginalize_input']
        marginalize_output = True if not 'marginalize_output' in kwargs else kwargs['marginalize_output']
        self._build_config['marginalization'] = {'arg': None,
                                                 'kwargs': {'marginalize_input': marginalize_input,
                                                            'marginalize_output': marginalize_output}}
        self.layer.update_marginalization(marginalize_input=marginalize_input, marginalize_output=marginalize_output)
        return self

    def update_multisamples(self, num: int, **unused):
        self.layer.update_multisamples(int(num))
        self._build_config['multisamples'] = {'arg': int(num), 'kwargs': unused}
        return self

    def update_from_dict(self, dict_in):
        for key in dict_in:
            if hasattr(self, 'update_' + str(key)):
                if dict_in[key]['arg'] is not None:
                    getattr(self, 'update_' + str(key))(dict_in[key]['arg'], **dict_in[key]['kwargs'])
                else:
                    getattr(self, 'update_' + str(key))(**dict_in[key]['kwargs'])
        return self

    @property
    def multisamples(self):
        return self.layer.multisamples

    @property
    def build_config(self):
        return self._build_config

    @property
    def ready_to_build(self):
        return self.layer.ready_to_build

    @property
    @abstractmethod
    def in_features(self):
        pass

    @property
    @abstractmethod
    def out_features(self):
        pass

    @property
    @abstractmethod
    def mean_shape(self):
        pass

    @property
    @abstractmethod
    def sample_shape(self):
        pass

    @property
    @abstractmethod
    def cov_shape(self):
        pass

    @abstractmethod
    def build(self):
        pass

    @property
    def num_layers(self):
        return 1

    @abstractmethod
    def sample(self, x, **kwargs):
        pass

    @abstractmethod
    def split_sample(self, x, **kwargs):
        pass

    @abstractmethod
    def components(self, x, **kwargs):
        pass

    def forward(self, x, **kwargs):
        return self.sample(x, **kwargs)

    def hyper_parameters(self):
        return self.layer.hyper_parameters()

    def named_hyper_parameters(self):
        return self.layer.named_hyper_parameters()

    def variational_parameters(self):
        return self.layer.variational_parameters()

    def named_variational_parameters(self):
        return self.layer.named_variational_parameters()

    def build_variational_optimizer(self, default_lr, **kwargs):
        return self.layer.build_variational_optimizer(default_lr, **kwargs)

    def build_hyper_optimizer(self, default_lr, **kwargs):
        return self.layer.build_hyper_optimizer(default_lr, **kwargs)


class LayerSequential(AbstractLayerContainer):
    """
    This is a collection that represents a composition of the layers supplied in the order they are supplied. i.e.:
    output = Layer0(Layer1(Layer2(...LayerN(X))))
    """

    def __init__(self, *layers, **unused):
        super().__init__(*layers)
        self._build_config['kind'] = type(self).__name__

    def update_multisamples(self, num: int, **unused):
        self.layers[0].update_multisamples(int(num))
        self._build_config['multisamples'] = {'arg': int(num), 'kwargs': unused}
        for l in self.layers[1:]:
            l.update_multisamples(1)
        return self

    @property
    def multisamples(self):
        return self.layers[0].multisamples

    def sample(self, x, **kwargs):
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        kls = []
        if isinstance(self.layers[0], Layer):
            assert torch.is_tensor(x), "Expecting a single tensor as input to the component Layer"
        for n,l in enumerate(self.layers):
            if n == 0:
                # print(f"I got x.shape: {x.shape} and aux_in.shape: {kwargs['aux_in'].shape}")
                x, kl = l(x, **{**kwargs, 'multisamples': multisamples})
            else:
                # print(f"I got x.shape: {x.shape} and aux_in.shape: {kwargs['aux_in'].shape}")
                x, kl = l(x, **{**kwargs, 'multisamples': 1})
            kls.append(kl)
        return x, merge_regularizers(*kls)

    def split_sample(self, x, **kwargs):
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        if isinstance(self.layers[0], Layer):
            assert torch.is_tensor(x), "Expecting a single tensor as input to the component Layer"
        for n, l in enumerate(self.layers[:-1]):
            if n == 0:
                x, d = l.split_sample(x, **{**kwargs, 'multisamples': multisamples})
                x = x + d  # this just generates the normal "sample", unsplit, but we avoid computing kl
            else:
                x, d = l.split_sample(x, **{**kwargs, 'multisamples': 1})
                x = x + d
        x, d = self.layers[-1].split_sample(x, **{**kwargs, 'multisamples': 1})  # we only return the split sample of the final layer
        return x, d

    def components(self, x, **kwargs):
        kls = []
        if len(self.layers) > 1:
            for n,l in enumerate(self.layers[:-1]):
                if n == 0:
                    x, kl = l(x, **kwargs)  # ...SND
                else:
                    x, kl = l(x, **kwargs)  # ...SND
                kls.append(kl)
        mu, var, kl = self.layers[-1].components(x, **kwargs)  # ...DN1 for mu
        kls.append(kl)
        return mu, var, merge_regularizers(*kls)

    def build(self):
        for k,l in enumerate(self.layers):
            l.build()
        self._build_config['layers'] = [l.build_config for l in self.layers]

    @property
    def in_features(self):
        return self.layers[0].in_features

    @property
    def out_features(self):
        return self.layers[-1].out_features

    @property
    def mean_shape(self):
        shapes = [['batch_dummy', -2, 'D']] + [l.sample_shape for l in self.layers[:-1]] + [self.layers[-1].mean_shape]
        shape = propagate_shape(*shapes, flatten=False)
        return [None if s == 'batch_dummy' else s for s in shape]

    @property
    def sample_shape(self):
        shapes = [['batch_dummy', -2, 'D']] + [l.sample_shape for l in self.layers[:-1]] + [self.layers[-1].sample_shape]
        shape = propagate_shape(*shapes, flatten=False)
        return [None if s == 'batch_dummy' else s for s in shape]

    @property
    def cov_shape(self):
        shapes = [['batch_dummy', -2, 'D']] + [l.sample_shape for l in self.layers[:-1]] + [self.layers[-1].cov_shape]
        shape = propagate_shape(*shapes, flatten=False)
        return [None if s == 'batch_dummy' else s for s in shape]



class ProductScaleLayer(AbstractLayerContainer):
    """
        The first layer in layers is special here. We will be multiplying layers[0]*layers[1]. This produces a similar
        effect to a non-stationary signal variance, but without needing to code up a special kernel for it. We report
        component values that are simply mu_0*sample_1, sample_1*var_0*sample_1, where those products broadcast.

        We only expect two layers, and enforce this.
    """

    def __init__(self, *layers, **unused):
        assert len(layers) == 2, "we only need two layers"
        if not layers[1].out_features == 1:
            assert layers[1].out_features == layers[0].out_features, \
                "number of output dims should match or be broadcastable"
        super().__init__(*layers)
        self._build_config['kind'] = type(self).__name__
        self._s2sample_eplan = None
        self._s2mean_eplan = None
        self._s2cov_eplan = None
        self._s2sample_pplan = None
        self._s2mean_pplan = None
        self._s2cov_pplan = None
        self._mean_symbolic_shape = None
        self._cov_symbolic_shape = None
        self._sample_symbolic_shape = None

    def update_multisamples(self, num: int, **unused):
        # we update multisamples for both, as they will be broadcast multiplied together
        for l in self.layers:
            l.update_multisamples(int(num))
        self._build_config['multisamples'] = {'arg': int(num), 'kwargs': unused}
        return self

    @property
    def multisamples(self):
        return self.layers[0].multisamples

    def update_marginalization(self, **kwargs):
        marginalize_input = True if 'marginalize_input' not in kwargs else kwargs['marginalize_input']
        marginalize_output = True if 'marginalize_output' not in kwargs else kwargs['marginalize_output']
        self.marginalize_input = bool(marginalize_input)
        self.marginalize_output = bool(marginalize_output)
        self.layers[1].update_marginalization(marginalize_input=marginalize_input,
                                              marginalize_output=marginalize_output)
        self.layers[0].update_marginalization(marginalize_input=marginalize_input,
                                              marginalize_output=marginalize_output)
        self._build_config['marginalization'] = {'arg': None, 'kwargs': {'marginalize_input': self.marginalize_input,
                                                      'marginalize_output': self.marginalize_output}}
        return self

    def _plan_sample_to_cov_shape(self):
        sample_shape = propagate_shape(['batch_dummy', 'N', 'D'], self.layers[1].sample_shape)
        cov_shape = propagate_shape(['batch_dummy', 'N', 'D'], self.layers[0].cov_shape)
        cov_shape = [None if s == 'batch_dummy' else s for s in cov_shape]
        sample_shape = [None if s == 'batch_dummy' else s for s in sample_shape]
        eplan, pplan = plan_expand_and_permute_ND_special(sample_shape, cov_shape)
        self._s2cov_eplan = eplan
        self._s2cov_pplan = pplan
        self._cov_symbolic_shape = cov_shape

    def _plan_sample_to_mean_shape(self):
        sample_shape = propagate_shape(['batch_dummy', 'N', 'D'], self.layers[1].sample_shape)
        mean_shape = propagate_shape(['batch_dummy', 'N', 'D'], self.layers[0].mean_shape)
        mean_shape = [None if s == 'batch_dummy' else s for s in mean_shape]
        sample_shape = [None if s == 'batch_dummy' else s for s in sample_shape]
        eplan, pplan = plan_expand_and_permute(sample_shape, mean_shape, assert_no_reps=True)
        self._s2mean_eplan = eplan
        self._s2mean_pplan = pplan
        self._mean_symbolic_shape = mean_shape

    def _plan_sample_to_sample_shape(self):
        sample_shape = propagate_shape(['batch_dummy', 'N', 'D'], self.layers[1].sample_shape)
        t_shape = propagate_shape(['batch_dummy', 'N', 'D'], self.layers[0].sample_shape)
        t_shape = [None if s == 'batch_dummy' else s for s in t_shape]
        sample_shape = [None if s == 'batch_dummy' else s for s in sample_shape]
        eplan, pplan = plan_expand_and_permute(sample_shape, t_shape, assert_no_reps=True)
        self._s2sample_eplan = eplan
        self._s2sample_pplan = pplan
        self._sample_symbolic_shape = sample_shape

    def sample(self, x, **kwargs):
        multisamples = self.multisamples if 'multisamples' not in kwargs else None
        if isinstance(self.layers[0], Layer):
            assert torch.is_tensor(x), "Expecting a single tensor as input to the component Layer"
        out = [l(x, **{**kwargs, 'multisamples': multisamples}) for l in self.layers]
        unzipped_out = list(zip(*out))
        samples = unzipped_out[0]
        kls = unzipped_out[1]
        # we need to figure out how many batch dims were added.
        bl = len(self.layers[0].sample_shape) - int(samples[0].dim())
        sample_matched = unsqueeze_multiple(samples[1], list(np.array(self._s2sample_eplan) - bl))
        sample_matched = sample_matched.permute(list(np.array(self._s2sample_pplan) - bl))
        # return F.softplus(sample_matched)*samples[0], torch.cat(kls)
        return sample_matched * samples[0], merge_regularizers(*kls)

    def split_sample(self, x, **kwargs):
        # WARNING: I'm doing a good faith effort here to return the split sample, i.e. something distributed
        # identically to sample when you sum the two parts, separated into random and non-random parts. Split Sample
        # is intended for use in SDE flow applications. But this product layer does not conform to expectations of
        # SDE flow, since it's not gaussian distributed anymore.
        multisamples = self.multisamples if 'multisamples' not in kwargs else None
        if isinstance(self.layers[0], Layer):
            assert torch.is_tensor(x), "Expecting a single tensor as input to the component Layer"
        out = [l.split_sample(x, **{**kwargs, 'multisamples': multisamples}) for l in self.layers]
        unzipped_out = list(zip(*out))
        samples = unzipped_out[0]
        det_bits = unzipped_out[1]
        # we need to figure out how many batch dims were added.
        bl = len(self.layers[0].sample_shape) - int(samples[0].dim())
        # match samples[1] to samples[0] in shape
        sample_matched = unsqueeze_multiple(samples[1], list(np.array(self._s2sample_eplan) - bl))
        sample_matched = sample_matched.permute(list(np.array(self._s2sample_pplan) - bl))
        # and do the same to the det_bits[1]
        det_bits_matched = unsqueeze_multiple(det_bits[1], list(np.array(self._s2sample_eplan) - bl))
        det_bits_matched = det_bits_matched.permute(list(np.array(self._s2sample_pplan) - bl))
        # To match the sample we need to account for cross terms. (mu1 + eps1)*(mu0 + eps0) =
        # mu1*mu0 + eps1*mu0 + eps1*eps0 + mu1*eps0
        # the deterministic part is just mu1*mu0, while the rest is random.
        random_part = sample_matched * samples[0] + \
                      det_bits_matched * samples[0] + \
                      det_bits[0] * sample_matched
        deterministic_part = det_bits_matched * det_bits[0]
        return random_part, deterministic_part

    def components(self, x, **kwargs):
        # We take component values from layer[0], and scale by layer[1]
        multisamples = self.multisamples if 'multisamples' not in kwargs else None
        sample, kl1 = self.layers[1].sample(x, **{**kwargs, 'multisamples': multisamples})
        mean, cov, kl0 = self.layers[0].components(x, **{**kwargs, 'multisamples': multisamples})
        # we need to figure out how many batch dims were added.
        bl = len(self.layers[0].mean_shape) - int(mean.dim())
        sample_mean_matched = unsqueeze_multiple(sample, list(np.array(self._s2mean_eplan) - bl))
        s2mean_pp = list(np.array(self._s2mean_pplan) - bl)
        s2mean_pp = [s for s in s2mean_pp if s >=0]
        sample_mean_matched = sample_mean_matched.permute(s2mean_pp)
        sample_cov_matched = unsqueeze_multiple(sample, list(np.array(self._s2cov_eplan) - bl))
        s2cov_pp = [list(np.array(plan) - bl) for plan in self._s2cov_pplan]
        s2cov_pp_fixed = []
        for S in s2cov_pp:
            s2cov_pp_fixed.append([s for s in S if s >=0])
        sample_cov_matched = [sample_cov_matched.permute(plan) for plan in s2cov_pp_fixed]
        # sample_cov_matched = [F.softplus(s) for s in sample_cov_matched]
        if len(sample_cov_matched) == 1:
            scaled_cov = cov*sample_cov_matched[0]*sample_cov_matched[0]
        elif len(sample_cov_matched) == 2:
            scaled_cov = cov * (sample_cov_matched[0] * sample_cov_matched[1])
        else:
            raise ValueError('expected 1 or 2 different sample matching plans')
        # scaled_mean = F.softplus(sample_mean_matched)*mean
        scaled_mean = sample_mean_matched * mean
        return scaled_mean, scaled_cov, merge_regularizers(kl0,kl1)

    def build(self):
        #force update of marginalization state
        self.update_marginalization(marginalize_input=self.marginalize_input,
                                    marginalize_output=self.marginalize_output)
        for k,l in enumerate(self.layers):
            l.build()
        self._build_config['layers'] = [l.build_config for l in self.layers]
        self._plan_sample_to_cov_shape()
        self._plan_sample_to_mean_shape()
        self._plan_sample_to_sample_shape()

    @property
    def in_features(self):
        return self.layers[0].in_features

    @property
    def out_features(self):
        return self.layers[0].out_features

    @property
    def mean_shape(self):
        return self._mean_symbolic_shape

    @property
    def sample_shape(self):
        return self._sample_symbolic_shape

    @property
    def cov_shape(self):
        return self._cov_symbolic_shape


class SliceInputLayer(AbstractLayerWrapper):
    """ Applies a slice along -1 dim to input before passing to a layer. This is
        useful for removing dims or splitting up dims input to various layers"""
    def __init__(self, layer, dim_slice: slice, in_features: int, **unused):
        super().__init__(layer)
        self._in_features = int(in_features)
        self.build_config['kind'] = type(self).__name__
        self.dim_slice = dim_slice
        assert dim_slice.stop <= self._in_features, "slice exceeds expected in_features"
        if dim_slice.step is None:
            real_in_features = (dim_slice.stop - dim_slice.start)
        else:
            real_in_features = (dim_slice.stop - dim_slice.start)//dim_slice.step
        assert real_in_features >= 1, "expected at least 1 input dim after slicing"
        self.build_config['dim_slice'] = [self.dim_slice.start, self.dim_slice.stop, self.dim_slice.step]
        self.build_config['in_features'] = int(in_features)

    @property
    def in_features(self):
        return self._in_features

    @property
    def out_features(self):
        return self.layer.out_features

    @property
    def mean_shape(self):
        return self.layer.mean_shape

    @property
    def sample_shape(self):
        return self.layer.sample_shape

    @property
    def cov_shape(self):
        return self.layer.cov_shape

    def build(self):
        self.layer.build()

    def sample(self, x, **kwargs):
        return self.layer.sample(x[...,self.dim_slice], **kwargs)

    def split_sample(self, x, **kwargs):
        return self.layer.split_sample(x[...,self.dim_slice], **kwargs)

    def components(self, x, **kwargs):
        return self.layer.components(x[...,self.dim_slice], **kwargs)


class SkipLayer(AbstractLayerWrapper):
    """ Given a layer L that consumes a single input x, returns broadcast_cat(x, L(x)). Only allows for sampling,
        as we cannot compute the covariance or mean x in a meaningful way. """
    def __init__(self, layer, **unused):
        super().__init__(layer)
        self.build_config['kind'] = type(self).__name__
        self._sample_symbolic_shape = None
        self._sample_broadcast_plan = None

    @property
    def in_features(self):
        return self.layer.in_features

    @property
    def out_features(self):
        """ Hopefully self explanatory """
        return self.layer.out_features + self.layer.in_features

    @property
    def mean_shape(self):
        raise NotImplementedError('SkipLayer only supports sampling')

    def _compute_sample_broadcast_plan_and_shape(self):
        sample_shape = propagate_shape(['batch_dummy', -2, 'D'], self.layer.sample_shape)
        plan, expanded_shape = plan_broadcast(['batch_dummy', -2, 'D'], sample_shape)
        sample_shape = [None if s == 'batch_dummy' else s for s in sample_shape]
        expanded_shape = [None if s == 'batch_dummy' else s for s in expanded_shape]
        self._sample_broadcast_plan = plan
        # self._sample_symbolic_shape = sample_shape
        self._sample_symbolic_shape = expanded_shape

    @property
    def sample_shape(self):
        return self._sample_symbolic_shape

    @property
    def sample_broadcast_plan(self):
        if self._sample_broadcast_plan is None:
            raise ValueError('sample broadcast plan is not yet built, please run build on this layer first')
        return self._sample_broadcast_plan

    @property
    def cov_shape(self):
        raise NotImplementedError('SkipLayer only supports sampling')

    def build(self):
        if self.ready_to_build:
            self.layer.build()
            self._build_config['layer'] = self.layer.build_config
            self._compute_sample_broadcast_plan_and_shape()
        else:
            raise ValueError('Not yet ready to build')

    def components(self, x, **unused):
        raise NotImplementedError('SkipLayer only supports sampling')

    def sample(self, x, **kwargs):
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        if isinstance(self.layer, Layer):
            assert torch.is_tensor(x), "Expecting a single tensor as input to the component Layer"
        sample, kl = self.layer(x, **{**kwargs, 'multisamples': multisamples})
        adjustment = len(self.layer.sample_shape) - int(sample.dim())
        # sample = unsqueeze_multiple(sample, list(np.array(self.sample_broadcast_plan[-1]) - sample_batch_length))
        adjusted_sample_plan = adjust_plan(self.sample_broadcast_plan[-1], adjustment, self.sample_shape.index(None))
        sample = unsqueeze_multiple(sample, adjusted_sample_plan)
        # ex = unsqueeze_multiple(x, list(np.array(self.sample_broadcast_plan[0]) - sample_batch_length))
        adjusted_ex_plan = adjust_plan(self.sample_broadcast_plan[0], adjustment, self.sample_shape.index(None))
        ex = unsqueeze_multiple(x, adjusted_ex_plan)
        return broadcast_cat(ex, sample, dim=-1), kl

    def split_sample(self, x, **kwargs):
        # WARNING: This is a good-faith effort to provide a consistent Layer API, but split sample does not make a
        # ton of sense here. What I return is indeed split into a deterministic part and random part, such that
        # summing the two components should yield the same result (in distribution) as the sample method. However,
        # note that the random part of a skip layer corresponding to the part the skips through has zero variance. So
        # it's just a vector of zeros concatenated there.
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        if isinstance(self.layer, Layer):
            assert torch.is_tensor(x), "Expecting a single tensor as input to the component Layer"
        sample, det_part = self.layer.split_sample(x, **{**kwargs, 'multisamples': multisamples})
        adjustment = len(self.layer.sample_shape) - int(sample.dim())
        # sample = unsqueeze_multiple(sample, list(np.array(self.sample_broadcast_plan[-1]) - sample_batch_length))
        sample = unsqueeze_multiple(sample, adjust_plan(self.sample_broadcast_plan[-1], adjustment,
                                                        self.sample_shape.index(None)))
        # det_part = unsqueeze_multiple(det_part, list(np.array(self.sample_broadcast_plan[-1]) - sample_batch_length))
        det_part = unsqueeze_multiple(det_part, adjust_plan(self.sample_broadcast_plan[-1], adjustment,
                                                        self.sample_shape.index(None)))
        # ex = unsqueeze_multiple(x, list(np.array(self.sample_broadcast_plan[0]) - sample_batch_length))
        ex = unsqueeze_multiple(x, adjust_plan(self.sample_broadcast_plan[0], adjustment, 0))
        return broadcast_cat(torch.zeros_like(ex), sample, dim=-1), broadcast_cat(ex, det_part, dim=-1)


class AuxSkipLayer(AbstractLayerWrapper):
    """ Given a layer L that consumes a single input aux, returns broadcast_cat(x, L(aux)), where x is a separate input
        that is just passed through. Only allows for sampling, as we cannot compute the covariance or mean x in a
        meaningful way.

        In other words, [x, aux] -> broadcast_cat([x, L(aux)])
    """
    def __init__(self, layer, x_features: int, **unused):
        super().__init__(layer)
        self.build_config['kind'] = type(self).__name__
        self._x_features = int(x_features)
        self.build_config['x_features'] = self._x_features
        self._sample_symbolic_shape = None
        self._sample_broadcast_plan = None

    @property
    def in_features(self):
        return self.layer.in_features + self._x_features

    @property
    def out_features(self):
        """ Hopefully self explanatory """
        return self.layer.out_features + self._x_features

    @property
    def mean_shape(self):
        raise NotImplementedError('SkipLayer only supports sampling')

    def _compute_sample_broadcast_plan_and_shape(self):
        sample_shape = propagate_shape(['batch_dummy', -2, 'D'], self.layer.sample_shape)
        plan, expanded_shape = plan_broadcast(['batch_dummy', -2, 'D'], sample_shape)
        sample_shape = [None if s == 'batch_dummy' else s for s in sample_shape]
        expanded_shape = [None if s == 'batch_dummy' else s for s in expanded_shape]
        self._sample_broadcast_plan = plan
        # self._sample_symbolic_shape = sample_shape
        self._sample_symbolic_shape = expanded_shape

    @property
    def sample_shape(self):
        return self._sample_symbolic_shape

    @property
    def sample_broadcast_plan(self):
        if self._sample_broadcast_plan is None:
            raise ValueError('sample broadcast plan is not yet built, please run build on this layer first')
        return self._sample_broadcast_plan

    @property
    def cov_shape(self):
        raise NotImplementedError('SkipLayer only supports sampling')

    def build(self):
        if self.ready_to_build:
            self.layer.build()
            self._build_config['layer'] = self.layer.build_config
            self._compute_sample_broadcast_plan_and_shape()
        else:
            raise ValueError('Not yet ready to build')

    def components(self, x, **unused):
        raise NotImplementedError('SkipLayer only supports sampling')

    def sample(self, x, **kwargs):
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        if 'aux_in' not in kwargs:
            raise ValueError('Expected to find aux_in as a keyword argument')
        assert x.shape[-1] == self._x_features, \
            f"expected pass through to have dim shape {self._x_features}, got {x.shape[-1]}"
        sample, kl = self.layer(x, **{**kwargs, 'multisamples': multisamples})
        adjustment = len(self.layer.sample_shape) - int(sample.dim())
        # sample = unsqueeze_multiple(sample, list(np.array(self.sample_broadcast_plan[-1]) - sample_batch_length))
        adjusted_sample_plan = adjust_plan(self.sample_broadcast_plan[-1], adjustment, self.sample_shape.index(None))
        sample = unsqueeze_multiple(sample, adjusted_sample_plan)
        # ex = unsqueeze_multiple(x, list(np.array(self.sample_broadcast_plan[0]) - sample_batch_length))
        adjusted_ex_plan = adjust_plan(self.sample_broadcast_plan[0], adjustment, self.sample_shape.index(None))
        ex = unsqueeze_multiple(x, adjusted_ex_plan)
        return broadcast_cat(ex, sample, dim=-1), kl

    def split_sample(self, x, **kwargs):
        # WARNING: This is a good-faith effort to provide a consistent Layer API, but split sample does not make a
        # ton of sense here. What I return is indeed split into a deterministic part and random part, such that
        # summing the two components should yield the same result (in distribution) as the sample method. However,
        # note that the random part of a skip layer corresponding to the part the skips through has zero variance. So
        # it's just a vector of zeros concatenated there.
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        if 'aux_in' not in kwargs:
            raise ValueError('Expected to find aux_in as a keyword argument')
        assert x.shape[-1] == self._x_features, \
            f"expected pass through to have dim shape {self._x_features}, got {x.shape[-1]}"
        sample, det_part = self.layer.split_sample(x, **{**kwargs, 'multisamples': multisamples})
        adjustment = len(self.layer.sample_shape) - int(sample.dim())
        # sample = unsqueeze_multiple(sample, list(np.array(self.sample_broadcast_plan[-1]) - sample_batch_length))
        sample = unsqueeze_multiple(sample, adjust_plan(self.sample_broadcast_plan[-1], adjustment,
                                                        self.sample_shape.index(None)))
        # det_part = unsqueeze_multiple(det_part, list(np.array(self.sample_broadcast_plan[-1]) - sample_batch_length))
        det_part = unsqueeze_multiple(det_part, adjust_plan(self.sample_broadcast_plan[-1], adjustment,
                                                        self.sample_shape.index(None)))
        # ex = unsqueeze_multiple(x, list(np.array(self.sample_broadcast_plan[0]) - sample_batch_length))
        ex = unsqueeze_multiple(x, adjust_plan(self.sample_broadcast_plan[0], adjustment, 0))
        return broadcast_cat(torch.zeros_like(ex), sample, dim=-1), broadcast_cat(ex, det_part, dim=-1)


class OutputConcatenationLayer(AbstractLayerContainer):
    """
    This is a collection that takes layers, each with their own input (or a single input shared over all layers)
    and forms a single layer that takes all those inputs and forms a single output, merged along D dim.
    It has the special requirement that every component layer must have marginalize_output = True.

    In order to concatenate tensors, they must have identical shapes except at one dimension. We analyze the symbolic
    shapes of the layers to:
        1. enforce that shapes of sample, mean, and cov must all match except at the D dimension and except at any
           dimension which appears only once; such unique dimensions are expected to be batch dimensions
        2. compute an "expansion plan" for each layer which injects singleton dimensions where appropriate in order
            that batch dims can be aligned by broadcasting

    All the symbolic shape calculations and expansion plans are made during the build phase, so execution should just
    boil down to some unsqueeze calls and a broadcast_tensors call.

    Thus if one layer, given input shape BND outputs cov shape BDNN and another one gives BWDNN, then the expected
    behavior is that the first layer would be expanded as B1DNN and then the covariances would be merged as BW*NN, where
    * denotes the concatenated dimension.
    """

    def __init__(self, *layers, **unused):
        super().__init__(*layers)
        self.build_config['kind'] = type(self).__name__
        self._mean_broadcast_plan = None
        self._cov_broadcast_plan = None
        self._sample_broadcast_plan = None
        self._mean_symbolic_shape = None
        self._cov_symbolic_shape = None
        self._sample_symbolic_shape = None
        self._mean_concat_dim = None
        self._cov_concat_dim = None

    def update_multisamples(self, num: int, **unused):
        """ We need to update multisampling of all constituent layers, not just the first"""
        for l in self.layers:
            l.update_multisamples(int(num))
        self._build_config['multisamples'] = {'arg': int(num), 'kwargs': unused}
        return self

    @property
    def multisamples(self):
        return self.layers[0].multisamples #all should be the same

    def sample(self, x, **kwargs):
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        if isinstance(x, collections.Iterable) and not isinstance(x, (str, bytes, torch.Tensor)):
            assert len(x) == len(self.layers), f"expected {len(self.layers)} inputs, got {len(x)}"
        elif torch.is_tensor(x):
            x = len(self.layers)*[x]
        else:
            raise ValueError(f'unexpected type for x, got {type(x)}')
        out = [l(u, **{**kwargs, 'multisamples': multisamples}) for l,u in zip(self.layers, x)]
        unzipped_out = list(zip(*out))
        samples = unzipped_out[0]
        kls = unzipped_out[1]
        # we need to figure out how many batch dims were added.
        adjustment = len(self.layers[0].sample_shape) - samples[0].dim()
        adjusted_plans = [adjust_plan(p, adjustment, self.sample_shape.index(None)) for p in self.sample_broadcast_plan]
        # apply the expansion plans & broadcast to each output
        # samples = [unsqueeze_multiple(o, list(np.array(p)-bl)) for o,p,bl in
        #            zip(samples, self.sample_broadcast_plan, sample_batch_length)]
        samples = [unsqueeze_multiple(o, p) for o, p in zip(samples, adjusted_plans)]
        return broadcast_cat(*samples, dim=-1), merge_regularizers(kls)

    def split_sample(self, x, **kwargs):
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        if isinstance(x, collections.Iterable) and not isinstance(x, (str, bytes, torch.Tensor)):
            assert len(x) == len(self.layers), f"expected {len(self.layers)} inputs, got {len(x)}"
        elif torch.is_tensor(x):
            x = len(self.layers) * [x]
        else:
            raise ValueError(f'unexpected type for x, got {type(x)}')
        out = [l.split_sample(u, **{**kwargs, 'multisamples': multisamples}) for l, u in zip(self.layers, x)]
        unzipped_out = list(zip(*out))
        samples = unzipped_out[0]
        det_parts = unzipped_out[1]
        # we need to figure out how many batch dims were added.
        adjustment = len(self.layers[0].sample_shape) - samples[0].dim()
        adjusted_plans = [adjust_plan(p, adjustment, self.sample_shape.index(None)) for p in self.sample_broadcast_plan]
        # apply the expansion plans & broadcast to each output
        # samples = [unsqueeze_multiple(o, list(np.array(p)-bl)) for o,p,bl in
        #            zip(samples, self.sample_broadcast_plan, sample_batch_length)]
        # det_parts = [unsqueeze_multiple(o, list(np.array(p) - bl)) for o, p, bl in
        #            zip(det_parts, self.sample_broadcast_plan, sample_batch_length)]
        samples = [unsqueeze_multiple(o, p) for o, p in zip(samples, adjusted_plans)]
        det_parts = [unsqueeze_multiple(o, p) for o, p in zip(det_parts, adjusted_plans)]
        return broadcast_cat(*samples, dim=-1), broadcast_cat(*det_parts, dim=-1)

    def components(self, x, **kwargs):
        """ concatenation along D only makes sense if we assume D is independent, i.e. marginalize_output is True for
        concatenation layer. So, we enforce that. Additionally, there's the challenge that different layers may
        produce different output shapes, e.g. mixing a GPMM layer with a regular one. """
        out = [l.components(u, **kwargs) for l,u in zip(self.layers, x)]
        unzipped_out = list(zip(*out))
        means = unzipped_out[0]
        covs = unzipped_out[1]
        kls = unzipped_out[2]
        # we need to figure out how many batch dims were added. We assume that all batch dims are the same for all
        # inputs
        adjustment = len(self.layers[0].mean_shape) - means[0].dim()
        adjusted_cov_plans = [adjust_plan(p, adjustment, self.cov_shape.index(None)) for p in self.cov_broadcast_plan]
        adjusted_mean_plans = [adjust_plan(p, adjustment, self.mean_shape.index(None))
                               for p in self.mean_broadcast_plan]
        # apply the expansion plans & broadcast to each of the means
        means = [unsqueeze_multiple(o, p) for o, p in
                 zip(means, adjusted_mean_plans)]
        # apply the expansion plans & broadcast to each of the covs
        covs = [unsqueeze_multiple(o, p) for o, p in
                zip(covs, adjusted_cov_plans)]
        # we need to concatenate on the D dim
        return (broadcast_cat(*means, dim=self._mean_concat_dim), broadcast_cat(*covs, dim=self._cov_concat_dim),
                merge_regularizers(*kls))

    def _compute_mean_broadcast_plan_and_shape(self):
        mean_shapes = [propagate_shape(['batch_dummy', -2, 'D'], l.mean_shape) for l in self.layers]
        plan, mean_shape = plan_broadcast(*mean_shapes)
        mean_shape = [None if s == 'batch_dummy' else s for s in mean_shape]
        self._mean_broadcast_plan = plan
        self._mean_symbolic_shape = mean_shape

    @property
    def mean_broadcast_plan(self):
        if self._mean_broadcast_plan is None:
            raise ValueError('mean broadcast plan is not yet built, please run build on this layer first')
        return self._mean_broadcast_plan

    def _compute_cov_broadcast_plan_and_shape(self):
        cov_shapes = [propagate_shape(['batch_dummy', -2, 'D'], l.cov_shape) for l in self.layers]
        plan, cov_shape = plan_broadcast(*cov_shapes)
        cov_shape = [None if s == 'batch_dummy' else s for s in cov_shape]
        self._cov_broadcast_plan = plan
        self._cov_symbolic_shape = cov_shape

    @property
    def cov_broadcast_plan(self):
        if self._cov_broadcast_plan is None:
            raise ValueError('cov broadcast plan is not yet built, please run build on this layer first')
        return self._cov_broadcast_plan

    def _compute_sample_broadcast_plan_and_shape(self):
        sample_shapes = [propagate_shape(['batch_dummy', -2, 'D'], l.sample_shape) for l in self.layers]
        plan, sample_shape = plan_broadcast(*sample_shapes)
        sample_shape = [None if s == 'batch_dummy' else s for s in sample_shape]
        self._sample_broadcast_plan = plan
        self._sample_symbolic_shape = sample_shape

    @property
    def sample_broadcast_plan(self):
        if self._sample_broadcast_plan is None:
            raise ValueError('sample broadcast plan is not yet built, please run build on this layer first')
        return self._sample_broadcast_plan

    def build(self):
        for k,l in enumerate(self.layers):
            l.build()
        self._build_config['layers'] = [l.build_config for l in self.layers]
        self._compute_mean_broadcast_plan_and_shape()
        self._compute_cov_broadcast_plan_and_shape()
        self._compute_sample_broadcast_plan_and_shape()
        self._mean_concat_dim = self.mean_shape.index('D') - len(self.mean_shape)
        self._cov_concat_dim = self.cov_shape.index('D') - len(self.cov_shape)
        assert self.mean_shape.count('D') == 1, \
            f"expected D to have count 1 in mean shape, no more or less, got {self.mean_shape.count('D')}"
        assert self.cov_shape.count('D') == 1, \
            f"expected D to have count 1 in cov shape, no more or less, got {self.cov_shape.count('D')}"

    @property
    def in_features(self):
        k = 0
        for l in self.layers:
            k+= l.in_features
        return k

    @property
    def out_features(self):
        k = 0
        for l in self.layers:
            k += l.out_features
        return k

    @property
    def mean_shape(self):
        return self._mean_symbolic_shape

    @property
    def sample_shape(self):
        return self._sample_symbolic_shape

    @property
    def cov_shape(self):
        return self._cov_symbolic_shape

# Here I define some default layer configurations to ease their construction. To be used with "update_from_dict" method


sqrt_var_hidden_minimal = OrderedDict({
    'inducing': {'arg': 'Shared', 'kwargs': {'range_min': -3., 'range_max': 3.}},
    'kernel': {'arg': {'CombinationSharedKernel': {'RBFKernel': {'lengthscale': [1.0,]}}}, 'kwargs': {'overwrite_ard': True}},
    'marginalization': {'arg': None, 'kwargs': {'marginalize_input': True, 'marginalize_output': True}},
    'output_transform': {'arg': 'Linear', 'kwargs': {}},
    'variational_parameterization': {'arg': 'SqrtVar', 'kwargs': {}},
    'data_mean': {'arg': 'Linear', 'kwargs': {}},
    'prior_mean': {'arg': 'None', 'kwargs': {}},
    'natural_gradient_preference': {'arg': None, 'kwargs': {'use_natural_gradients': True}},
    'multisamples': {'arg': 1, 'kwargs': {}}
})


sqrt_var_hidden_flexible = OrderedDict({
    'inducing': {'arg': 'Separate', 'kwargs': {'range_min': -3., 'range_max': 3.}},
    'kernel': {'arg': {'CombinationKernel': [{'RBFKernel': {'lengthscale': [1.0,]}}]}, 'kwargs': {'overwrite_ard': True, 'rep_kernels': True}},
    'marginalization': {'arg': None, 'kwargs': {'marginalize_input': True, 'marginalize_output': True}},
    'output_transform': {'arg': 'Linear', 'kwargs': {}},
    'variational_parameterization': {'arg': 'SqrtVar', 'kwargs': {}},
    'data_mean': {'arg': 'Linear', 'kwargs': {}},
    'prior_mean': {'arg': 'None', 'kwargs': {}},
    'natural_gradient_preference': {'arg': None, 'kwargs': {'use_natural_gradients': True}},
    'multisamples': {'arg': 1, 'kwargs': {}}
})

sqrt_var_final_minimal = OrderedDict({
    'inducing': {'arg': 'Shared', 'kwargs': {'range_min': -3., 'range_max': 3.}},
    'kernel': {'arg': {'CombinationSharedKernel': {'RBFKernel': {'lengthscale': [1.0,]}}}, 'kwargs': {'overwrite_ard': True}},
    'marginalization': {'arg': None, 'kwargs': {'marginalize_input': True, 'marginalize_output': True}},
    'output_transform': {'arg': 'Linear', 'kwargs': {}},
    'variational_parameterization': {'arg': 'SqrtVar', 'kwargs': {}},
    'data_mean': {'arg': 'Constant', 'kwargs': {}},
    'prior_mean': {'arg': 'None', 'kwargs': {}},
    'natural_gradient_preference': {'arg': None, 'kwargs': {'use_natural_gradients': True}},
    'multisamples': {'arg': 1, 'kwargs': {}}
})


sqrt_var_final_flexible = OrderedDict({
    'inducing': {'arg': 'Separate', 'kwargs': {'range_min': -3., 'range_max':3.}},
    'kernel': {'arg': {'CombinationKernel': [{'RBFKernel': {'lengthscale': [1.0,]}}]}, 'kwargs': {'overwrite_ard': True, 'rep_kernels': True}},
    'marginalization': {'arg': None, 'kwargs': {'marginalize_input': True, 'marginalize_output': True}},
    'output_transform': {'arg': 'Linear', 'kwargs': {}},
    'variational_parameterization': {'arg': 'SqrtVar', 'kwargs': {}},
    'data_mean': {'arg': 'Constant', 'kwargs': {}},
    'prior_mean': {'arg': 'None', 'kwargs': {}},
    'natural_gradient_preference': {'arg': None, 'kwargs': {'use_natural_gradients': True}},
    'multisamples': {'arg': 1, 'kwargs': {}}
})


whitened_sqrt_var_hidden_minimal = OrderedDict({
    'inducing': {'arg': 'Shared', 'kwargs': {'range_min': -3., 'range_max': 3.}},
    'kernel': {'arg': {'CombinationSharedKernel': {'RBFKernel': {'lengthscale': [1.0,]}}}, 'kwargs': {'overwrite_ard': True}},
    'marginalization': {'arg': None, 'kwargs': {'marginalize_input': True, 'marginalize_output': True}},
    'output_transform': {'arg': 'Linear', 'kwargs': {}},
    'variational_parameterization': {'arg': 'WhitenedSqrtVar', 'kwargs': {}},
    'data_mean': {'arg': 'Linear', 'kwargs': {}},
    'prior_mean': {'arg': 'None', 'kwargs': {}},
    'multisamples': {'arg': 1, 'kwargs': {}}
})


whitened_sqrt_var_hidden_flexible = OrderedDict({
    'inducing': {'arg': 'Separate', 'kwargs': {'range_min': -3., 'range_max': 3.}},
    'kernel': {'arg': {'CombinationKernel': [{'RBFKernel': {'lengthscale': [1.0,]}}]}, 'kwargs': {'overwrite_ard': True, 'rep_kernels': True}},
    'marginalization': {'arg': None, 'kwargs': {'marginalize_input': True, 'marginalize_output': True}},
    'output_transform': {'arg': 'Linear', 'kwargs': {}},
    'variational_parameterization': {'arg': 'WhitenedSqrtVar', 'kwargs': {}},
    'data_mean': {'arg': 'Linear', 'kwargs': {}},
    'prior_mean': {'arg': 'None', 'kwargs': {}},
    'multisamples': {'arg': 1, 'kwargs': {}}
})


whitened_sqrt_var_final_minimal = OrderedDict({
    'inducing': {'arg': 'Shared', 'kwargs': {'range_min': -3., 'range_max': 3.}},
    'kernel': {'arg': {'CombinationSharedKernel': {'RBFKernel': {'lengthscale': [1.0,]}}}, 'kwargs': {'overwrite_ard': True}},
    'marginalization': {'arg': None, 'kwargs': {'marginalize_input': True, 'marginalize_output': True}},
    'output_transform': {'arg': 'Linear', 'kwargs': {}},
    'variational_parameterization': {'arg': 'WhitenedSqrtVar', 'kwargs': {}},
    'data_mean': {'arg': 'Constant', 'kwargs': {}},
    'prior_mean': {'arg': 'None', 'kwargs': {}},
    'multisamples': {'arg': 1, 'kwargs': {}}
})


whitened_sqrt_var_final_flexible = OrderedDict({
    'inducing': {'arg': 'Separate', 'kwargs': {'range_min': -3., 'range_max': 3.}},
    'kernel': {'arg': {'CombinationKernel': [{'RBFKernel': {'lengthscale': [1.0,]}}]}, 'kwargs': {'overwrite_ard': True, 'rep_kernels': True}},
    'marginalization': {'arg': None, 'kwargs': {'marginalize_input': True, 'marginalize_output': True}},
    'output_transform': {'arg': 'Linear', 'kwargs': {}},
    'variational_parameterization': {'arg': 'SqrtVar', 'kwargs': {}},
    'data_mean': {'arg': 'Constant', 'kwargs': {}},
    'prior_mean': {'arg': 'None', 'kwargs': {}},
    'multisamples': {'arg': 1, 'kwargs': {}}
})

whitened_sqrt_var_final_minimal_nat_grad = OrderedDict({
    'inducing': {'arg': 'Shared', 'kwargs': {'range_min': -3., 'range_max': 3.}},
    'kernel': {'arg': {'CombinationSharedKernel': {'RBFKernel': {'lengthscale': [1.0,]}}}, 'kwargs': {'overwrite_ard': True}},
    'marginalization': {'arg': None, 'kwargs': {'marginalize_input': True, 'marginalize_output': True}},
    'output_transform': {'arg': 'Linear', 'kwargs': {}},
    'variational_parameterization': {'arg': 'WhitenedSqrtVar', 'kwargs': {}},
    'data_mean': {'arg': 'Constant', 'kwargs': {}},
    'prior_mean': {'arg': 'None', 'kwargs': {}},
    'natural_gradient_preference': {'arg': None, 'kwargs': {'use_natural_gradients': True}},
    'multisamples': {'arg': 1, 'kwargs': {}}
})

whitened_sqrt_var_hidden_minimal_nat_grad = OrderedDict({
    'inducing': {'arg': 'Shared', 'kwargs': {'range_min': -3., 'range_max': 3.}},
    'kernel': {'arg': {'CombinationSharedKernel': {'RBFKernel': {'lengthscale': [1.0,]}}}, 'kwargs': {'overwrite_ard': True}},
    'marginalization': {'arg': None, 'kwargs': {'marginalize_input': True, 'marginalize_output': True}},
    'output_transform': {'arg': 'Linear', 'kwargs': {}},
    'variational_parameterization': {'arg': 'WhitenedSqrtVar', 'kwargs': {}},
    'data_mean': {'arg': 'Linear', 'kwargs': {}},
    'prior_mean': {'arg': 'None', 'kwargs': {}},
    'natural_gradient_preference': {'arg': None, 'kwargs': {'use_natural_gradients': True}},
    'multisamples': {'arg': 1, 'kwargs': {}}
})

