from .layers import *
import torch
from ..symbolic import *
from .symbolic_shape import *

__all__ = ['AuxInpToKernelLayer', 'NSBiLayer']

class AuxInpToKernelLayer(Layer):
    """
    A modification of Layer, which expects auxillary inputs to be passed in to sample, split_sample, and components.
    These auxillary inputs are passed to the kernel calculation (cross kernel and so on).
    """

    def __init__(self,
                 num_gps: int,
                 num_inducing: int,
                 in_features: int,
                 out_features: int,
                 dtype=torch.get_default_dtype(),
                 name='L'
                 ):
        super().__init__(num_gps,num_inducing,in_features,out_features,dtype,name)
        self._build_config['kind'] = type(self).__name__
        self.AUX = Tensor(self.name + '_auxinp')

    def _populate_symbols(self):
        """ For populating symbols the layer owns (and symbolic children) with tensors """
        #TODO: This initialization assumes that we only have 1 aux input dim
        init_aux = torch.cat((self.inducing.Z,
                              0*self.inducing.Z[...,-1].unsqueeze(-1).expand((self.inducing.Z.dim()-1)*[-1] + [1])),
                             dim=-1)
        leaves = {
            self.name + '_kernel': self.kernel,
            self.name + '_inducing': self.inducing.Z,
            self.name + '_auxinp': init_aux,
            self.name + '_motransform': self.output_transform,
            self.name + '_prior_mean_func': self.prior_mean_func,
            self.name + '_data_mean_func': self.data_mean_func,
            self.name + '_jitter': self.vp.jitter_val,
            ** self.vp._populate_symbols(),
        }
        return leaves

    def _cross_kernel(self):
        """AUX takes the role of Z, and it is expected that the input passed in will match Z in batch dims, thus we
           don't need to expand it."""
        return EvaluateKernel(self.K, self._expand_data_axis(self.X), self.AUX)

    def _data_kernel(self):
        return EvaluateKernelSymmetric(self.K, self._expand_data_axis(self.X))

    def _inducing_kernel(self):
        return EvaluateKernelSymmetric(self.K, self.AUX)

    def sample(self, x, **kwargs):
        """
        The main purpose of the layer is to allow one to sample from the posterior of the layer,
        which is why the forward calls this method (by default).

        args x: input axis to evaluate sample on
        kwargs:
            'multi_samples': integer number of independent samples to compute per batched axis condition, defaults to 1

        :return: a tuple of:
                1. a sample from the posterior of the layer,
                2. the kl of the posterior q to the prior, evaluated on the inducing points
        """
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        auxin = self.inducing.Z if 'auxin' not in kwargs else kwargs['auxin']
        leaves = {
            self.name + '_data_axis': x,
            self.name + '_auxinp': auxin,
            **self._populate_symbols(),
            self.name + '_multi_samples': multisamples,
        }
        comps = self._sample(**leaves)
        s = getattr(comps, self.name + '_sample')
        kl = getattr(comps, self.name + '_kl')
        s = s.squeeze(-3) #if S == 1, then batch shape of input to next layer will change shape without this squeeze
        return s, kl

    def split_sample(self, x, **kwargs):
        """
        Similar to sample, but split into deterministic part (the mean) and random part. Used mainly in SDE flows.
        Allows one to scale the deterministic part separate from the random part. The two parts will be
        broadcast compatible; i.e. you should be able to add them together without issue after scaling.

        args x: input axis to evaluate sample on
        kwargs:
            'multi_samples': integer number of independent samples to compute per batched axis condition, defaults to 1

        :return: tuple of a sample from the posterior of the layer with zero mean, and the posterior mean as a separate
                    tensor
        """
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        auxin = self.inducing.Z if 'auxin' not in kwargs else kwargs['auxin']
        leaves = {
            self.name + '_data_axis': x,
            self.name + '_auxinp': auxin,
            **self._populate_symbols(),
            self.name + '_multi_samples': multisamples,
        }
        comps = self._split_sample(**leaves)
        s = getattr(comps, self.name + '_split_sample')
        mu = getattr(comps, self.name + '_split_mean')
        s = s.squeeze(-3)  # if S == 1, then batch shape of input to next layer will change shape without this squeeze
        mu = mu.squeeze(-3)  # as above
        return s, mu

    def components(self, x, **kwargs):
        """
        Intended for use when you want the parameters of the gaussian posterior (as well as the KL)

        args x: input axis to evaluate sample on
        :return: a tuple of:
                1. mean of the gaussian posterior
                2. covariance of the gaussian posterior
                2. the kl of the posterior q to the prior, evaluated on the inducing points
        """
        auxin = self.inducing.Z if 'auxin' not in kwargs else kwargs['auxin']
        leaves = {
            self.name + '_data_axis': x,
            self.name + '_auxinp': auxin,
            **self._populate_symbols(),
        }
        comps = self._components(**leaves)
        mu = getattr(comps, self.name + '_post_mean')
        cov = getattr(comps, self.name + '_post_cov')
        kl = getattr(comps, self.name + '_kl')
        return mu, cov, kl


class NSBiLayer(AbstractLayerContainer):
    """
    Use case here is for non-stationary kernels, which want 1-3 GPs to model various functions to parameterize aspects
    of non-stationarity in the kernel of a GP in the subsequent layer. We will group the first layer into an output
    concatenation layer, which then feeds forward the functions concatenated as an output dim. The kernel will need to
    consume an input like this.

    An extra feature required in this case is that we will evaluate the auxilliary GPs at the inducing points of the
    second layer. All GPs will receive the same input X. The symbolic shape of the output will be that of the second
    layer, such that externally this container should simply appear to be the second layer GP.

    The layers argument will be set up such that the last layer passed in is the second layer GP, while all the previous
    layers are assumed to be auxillary GP functions needed to evaluate the kernel of the final layer. The kernels of
    the auxillary GPs are presumed to be stationary.

    This is different than the "deeply non-stationary" idea published. The non-stationarity in this case is not deep.
    However, with this abstraction we can stack these non-stationary layers if desired and obtain a deep non-
    stationarity, but it will not have the same guarantee of avoiding pathologies that the "deeply non-stationary" idea
    has. On the other hand, this style of implementation more closely matches a lot of prior literature on using
    GPs to parameterize non-stationary kernels. I mainly expect a single instance of this NSBiLayer to be used.
    """

    def __init__(self, *layers, **unused):
        # we want to intercept the input layers first before passing to super, as I want to wrap the first few layers
        # in a OutputConcatenation layer (unless there's only 1)
        if len(layers) <= 1:
            raise ValueError('Expected at least two layers')
        elif len(layers) == 2:
            aux_layer = SkipLayer(layers[0])
            final_layer = layers[-1]
        else:
            aux_layer = SkipLayer(OutputConcatenationLayer(*layers[:-1]))
            final_layer = layers[-1]
        layers = [aux_layer, final_layer]
        assert isinstance(layers[-1], AuxInpToKernelLayer), "expected final layer to accept aux inputs"
        super().__init__(*layers)
        self._build_config['kind'] = type(self).__name__

    def update_multisamples(self, num: int, **unused):
        self.layers[0].update_multisamples(int(num))
        self._build_config['multisamples'] = {'arg': int(num), 'kwargs': unused}
        for l in self.layers[1:]:
            l.update_multisamples(1)
        return self

    @property
    def multisamples(self):
        return self.layers[0].multisamples

    def build(self):
        for l in self.layers:
            l.build()

    @property
    def in_features(self):
        return self.layers[-1].in_features

    @property
    def out_features(self):
        return self.layers[-1].out_features

    @property
    def mean_shape(self):
        shapes = [['batch_dummy', -2, 'D']] + [l.sample_shape for l in self.layers[:-1]] + [self.layers[-1].mean_shape]
        shape = propagate_shape(*shapes)
        return [None if s == 'batch_dummy' else s for s in shape]

    @property
    def sample_shape(self):
        shapes = [['batch_dummy', -2, 'D']] + [l.sample_shape for l in self.layers[:-1]] + [
            self.layers[-1].sample_shape]
        shape = propagate_shape(*shapes)
        return [None if s == 'batch_dummy' else s for s in shape]

    @property
    def cov_shape(self):
        shapes = [['batch_dummy', -2, 'D']] + [l.sample_shape for l in self.layers[:-1]] + [self.layers[-1].cov_shape]
        shape = propagate_shape(*shapes)
        return [None if s == 'batch_dummy' else s for s in shape]

    def sample(self, x, **kwargs):
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        Z = self.layers[-1].inducing.Z
        # this will produce an output of shape JSND or JND if S == 1.
        auxin, _ = self.layers[0].sample(Z, **{**kwargs, 'multisamples': multisamples})  # we pay for an extra kl compute
        x, kl_aux = self.layers[0](x, **{**kwargs, 'multisamples': multisamples})
        if multisamples > 1:
            auxin = auxin.transpose(-3,-4)  # now it is SJND, as required to match inducing
        sample, kl = self.layers[1](x, **{**kwargs, 'multisamples': 1, 'auxin': auxin})
        return sample, torch.cat([kl_aux, kl])

    def split_sample(self, x, **kwargs):
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        Z = self.layers[-1].inducing.Z
        # this will produce an output of shape JSND or JND if S == 1.
        auxin, _ = self.layers[0].sample(Z, **{**kwargs, 'multisamples': multisamples})  # we pay for an extra kl compute
        x, kl_aux = self.layers[0](x, **{**kwargs, 'multisamples': multisamples})
        if multisamples > 1:
            auxin = auxin.transpose(-3, -4)  # now it is SJND, as required to match inducing
        x, d = self.layers[1].split_sample(x, **{**kwargs, 'multisamples': 1, 'auxin': auxin})
        return x, d

    def components(self, x, **kwargs):
        multisamples = self.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        Z = self.layers[-1].inducing.Z
        # this will produce an output of shape JSND or JND if S == 1.
        auxin, _ = self.layers[0].sample(Z, **{**kwargs, 'multisamples': multisamples})  # we pay for an extra kl compute
        x, kl_aux = self.layers[0](x, **{**kwargs, 'multisamples': multisamples})
        if multisamples > 1:
            auxin = auxin.transpose(-3, -4)  # now it is SJND, as required to match inducing
        mu, cov, kl = self.layers[1].components(x, **{**kwargs, 'multisamples': 1, 'auxin': auxin})
        return mu, cov, torch.cat([kl_aux, kl])
