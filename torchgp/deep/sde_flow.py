import torch
from .util import *
from .layers import Layer, AbstractLayerContainer, AbstractLayerWrapper

__all__ = ['TimeInvariantSDEFlow', 'TimeInvariantSDEFlowConstInit']


class TimeInvariantSDEFlow(AbstractLayerWrapper):
    def __init__(self, layer,
                 delta_t: float,
                 nsteps: int,
                 dtype=torch.get_default_dtype()):
        """
        This will transform a layer into a flow layer, inspired from https://arxiv.org/pdf/1810.04066.pdf

        In this vanilla flavor, we use the wrapped layer to supply a distribution for dx ~ N(mu(x)dt, sigma(x)dt).

        We then propagate x0 = x forward nsteps, each time adding a draw from dx. As x evolves, the distribution
        for dx changes.


        """
        super().__init__(layer)
        self.dtype = dtype
        delta_t = validate_scalar_parameter(delta_t, 'delta_t', self.dtype)
        nsteps = validate_scalar_parameter(nsteps, 'nsteps', torch.int64)
        self._build_config['dtype'] = str(self.dtype)
        self._build_config['delta_t'] = float(delta_t)
        self._build_config['nsteps'] = int(nsteps)
        self._build_config['kind'] = type(self).__name__
        self.register_buffer("delta_t", delta_t)
        self.register_buffer("nsteps", nsteps)

        # to be fulfilled by builder patten
        self._sample_shape = None
        self._mean_shape = None
        self._cov_shape = None

        self.layer = layer
        self.name = "ti_sde_" + layer.name
        self._build_config['name'] = self.name

    @property
    def in_features(self):
        return self.layer.in_features

    @property
    def out_features(self):
        return self.layer.out_features

    @property
    def mean_shape(self):
        return self.layer.mean_shape

    @property
    def sample_shape(self):
        return self.layer.sample_shape

    @property
    def cov_shape(self):
        return self.layer.cov_shape

    def build(self):
        self.layer.build()

    def sample(self, x, **kwargs):
        multisamples = self.layer.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        nsteps = self.nsteps if 'nsteps' not in kwargs else kwargs['nsteps']
        # hack for now, since I didn't implement a separate kl method
        _, kl = self.layer.sample(x, **{**kwargs, 'multisamples': multisamples})
        if nsteps == 0:
            xnew = x
        else:
            # so we compute it only once.
            sigmaz, mu = self.layer.split_sample(x, **{**kwargs, 'multisamples': multisamples})
            delta_x = self.delta_t * mu + (self.delta_t.sqrt()) * sigmaz
            xnew = x + delta_x
            for step in range(1, nsteps):
                sigmaz, mu = self.layer.split_sample(xnew, **{**kwargs, 'multisamples': 1})
                delta_x = self.delta_t * mu + (self.delta_t.sqrt()) * sigmaz
                xnew = xnew + delta_x
        return xnew, kl

    def split_sample(self, x, **kwargs):
        # In case we want to start nesting SDE flows...
        multisamples = self.layer.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        nsteps = self.nsteps if 'nsteps' not in kwargs else kwargs['nsteps']
        if nsteps == 0:
            return torch.zeros_like(x), x
        else:
            # so we compute it only once.
            sigmaz, mu = self.layer.split_sample(x, **{**kwargs, 'multisamples': multisamples})
            delta_x = self.delta_t*mu + (self.delta_t.sqrt())*sigmaz
            xnew = x + delta_x
            for step in range(1,nsteps):
                sigmaz, mu = self.layer.split_sample(xnew, **{**kwargs, 'multisamples': 1})
                delta_x = self.delta_t * mu + (self.delta_t.sqrt()) * sigmaz
                xnew = xnew + delta_x
            return sigmaz, mu

    def components(self, x, **kwargs):
        # We will step nsteps - 1 forward, then report the mean / cov of the distribution there
        if self.nsteps>1:
            xnew, _ = self.sample(x, **{**kwargs, 'nsteps': self.nsteps-1})
        else:
            xnew = x
        return self.layer.components(xnew, **kwargs)


class TimeInvariantSDEFlowConstInit(TimeInvariantSDEFlow):
    def __init__(self, layer,
                 delta_t: float,
                 nsteps: int,
                 constant: float,
                 dtype=torch.get_default_dtype()):
        """
        This will transform a layer into a flow layer, inspired from https://arxiv.org/pdf/1810.04066.pdf

        In this flavor, we use the wrapped layer to supply a distribution for dx ~ N(mu(x)dt, sigma(x)dt).

        We then propagate x0 = constant vector; forward nsteps, each time adding a draw from dx. As x evolves, the distribution
        for dx changes. Note that we do not start with x0 = x, as in the vanilla flavor.

        Note: the constant will be a non-optimized hyper parameter.


        """
        super().__init__(layer, delta_t, nsteps, dtype)
        self._build_config['kind'] = type(self).__name__
        constant = validate_scalar_parameter(constant, 'constant', dtype=self.dtype)
        self._build_config['constant'] = float(constant)
        self.register_buffer('const_init', constant)

    def sample(self, x, **kwargs):
        multisamples = self.layer.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        nsteps = self.nsteps if 'nsteps' not in kwargs else kwargs['nsteps']
        # hack for now, since I didn't implement a separate kl method
        _, kl = self.layer.sample(x, **{**kwargs, 'multisamples': multisamples})
        if nsteps == 0:
            xnew = torch.ones_like(x)*self.const_init
        else:
            # so we compute it only once.
            sigmaz, mu = self.layer.split_sample(x, **{**kwargs, 'multisamples': multisamples})
            delta_x = self.delta_t * mu + (self.delta_t.sqrt()) * sigmaz
            xnew = torch.ones_like(x)*self.const_init + delta_x
            for step in range(1, nsteps):
                sigmaz, mu = self.layer.split_sample(xnew, **{**kwargs, 'multisamples': 1})
                delta_x = self.delta_t * mu + (self.delta_t.sqrt()) * sigmaz
                xnew = xnew + delta_x
        return xnew, kl

    def split_sample(self, x, **kwargs):
        # In case we want to start nesting SDE flows...
        multisamples = self.layer.multisamples if 'multisamples' not in kwargs else kwargs['multisamples']
        nsteps = self.nsteps if 'nsteps' not in kwargs else kwargs['nsteps']
        if nsteps == 0:
            return torch.zeros_like(x), torch.ones_like(x)*self.const_init
        else:
            # so we compute it only once.
            sigmaz, mu = self.layer.split_sample(x, **{**kwargs, 'multisamples': multisamples})
            delta_x = self.delta_t*mu + (self.delta_t.sqrt())*sigmaz
            xnew = torch.ones_like(x)*self.const_init + delta_x
            for step in range(1,nsteps):
                sigmaz, mu = self.layer.split_sample(xnew, **{**kwargs, 'multisamples': 1})
                delta_x = self.delta_t * mu + (self.delta_t.sqrt()) * sigmaz
                xnew = xnew + delta_x
            return sigmaz, mu

    def components(self, x, **kwargs):
        # We will step nsteps - 1 forward, then report the mean / cov of the distribution there
        if self.nsteps>1:
            xnew, _ = self.sample(x, **{**kwargs, 'nsteps': self.nsteps-1})
        else:
            xnew = torch.ones_like(x)*self.const_init
        return self.layer.components(xnew, **kwargs)
