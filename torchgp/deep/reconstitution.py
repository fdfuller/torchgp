from .layer_factories import layer_registry
from .models import model_factory
import pickle
from pathlib import Path
from pathvalidate import sanitize_filename, sanitize_filepath

__all__ = [
    'save_snapshot',
    'load_snapshot',
]


def kind_registry(string_in):
    # used to dispatch on either models or layers based on a string, returns a factory function
    registry = {
        'Layer': layer_registry(string_in),
        'LayerSequential': layer_registry(string_in),
        'SkipLayer': layer_registry(string_in),
        'OutputConcatenationLayer': layer_registry(string_in),
        'GPMM': layer_registry(string_in),
        'Model': model_factory,
    }
    if string_in in registry:
        return registry[string_in]
    else:
        ValueError('kind not understood')


def save_snapshot(obj, fname, fpath=Path.cwd()):
    """ Used to save models or layers"""
    fpath = sanitize_filepath(Path(fpath),platform='auto')
    assert fpath.exists(), f"file path {fpath} does not exist"
    fname = sanitize_filename(str(fname), platform='auto')
    if fname[-4:] != '.pth':
        fname = fname + '.pth'
    fullpath = fpath / fname
    # assume obj is some module from torchgp that has a .build_config property
    assert hasattr(obj, 'build_config'), "expected to find a build_config property"
    assert hasattr(obj, 'state_dict'), "expected to find a state_dict property"
    state_dict = obj.state_dict()
    build_config = obj.build_config
    data = {'state_dict': state_dict, 'build_config': build_config}
    with open(fullpath, 'wb') as fp:
        pickle.dump(data, fp, protocol=pickle.HIGHEST_PROTOCOL)


def load_snapshot(fname, fpath=Path.cwd()):
    """ Used to load models or layers"""
    fpath = sanitize_filepath(Path(fpath), platform='auto')
    fname = sanitize_filename(str(fname), platform='auto')
    if fname[-4:] != '.pth':
        fname = fname + '.pth'
    fullpath = fpath / fname
    assert fullpath.exists(), f"file path {fpath} does not exist"
    with open(fullpath, 'rb') as fp:
        data = pickle.load(fp)
    build_config = data['build_config']
    state_dict = data['state_dict']
    obj = kind_registry(build_config['kind'])(build_config)
    obj.load_state_dict(state_dict)
    return obj

