import torch
import collections

__all__ = [
    "AbstractInducingPoints",
    "SeparateInducingPoints",
    "SharedInducingPoints",
    "inducing_factory"
]

def make_sobol_inducing(num_gps, num_inducing, inducing_dim, range_min, range_max, dtype=torch.get_default_dtype()):
    Z0 = torch.quasirandom.SobolEngine(inducing_dim,scramble=True).draw(num_inducing*num_gps).type(dtype)
    Z0 = Z0.reshape(num_gps,num_inducing,inducing_dim)
    if not isinstance(range_min, collections.Sized):
        range_min = torch.tensor(int(inducing_dim)*[range_min], dtype=Z0.dtype)
    if not isinstance(range_max, collections.Sized):
        range_max = torch.tensor(int(inducing_dim)*[range_max], dtype=Z0.dtype)
    if not torch.is_tensor(range_min):
        range_min = torch.tensor(range_min, dtype=dtype)
    if not torch.is_tensor(range_max):
        range_max = torch.tensor(range_max, dtype=dtype)
    S = range_max - range_min
    Z = S[None, None, :]*Z0 + range_min[None, None, :]
    return Z


def make_normal_inducing(num_gps, num_inducing, inducing_dim, range_min, range_max, dtype=torch.get_default_dtype()):
    Z0 = torch.randn(num_inducing*num_gps,inducing_dim).type(dtype)
    Z0 = Z0.reshape(num_gps,num_inducing,inducing_dim)
    if not isinstance(range_min, collections.Sized):
        range_min = torch.tensor(int(inducing_dim)*[range_min], dtype=Z0.dtype)
    if not isinstance(range_max, collections.Sized):
        range_max = torch.tensor(int(inducing_dim)*[range_max], dtype=Z0.dtype)
    if not torch.is_tensor(range_min):
        range_min = torch.tensor(range_min, dtype=dtype)
    if not torch.is_tensor(range_max):
        range_max = torch.tensor(range_max, dtype=dtype)
    std = (range_max - range_min)/6.28318530718
    mu = (range_min + range_max)/2
    Z = std[None, None, :]*Z0 + mu[None, None, :]
    return Z


class AbstractInducingPoints(torch.nn.Module):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.dtype = torch.get_default_dtype() if not 'dtype' in kwargs else kwargs['dtype']

    @property
    def Z(self):
        raise NotImplementedError()

    @property
    def M(self):
        raise NotImplementedError()

    @property
    def J(self):
        raise NotImplementedError()

    @property
    def D(self):
        raise NotImplementedError()

class SeparateInducingPoints(AbstractInducingPoints):
    """
    required kwargs:
    Z: a 3D tensor that are the inducing point locations desired. If this is passed in, it will short-circuit the rest.
        -1 dim should be inducing point dimension, -2 dim should be number of inducing points, and -3 dim should
        correspond to number of independent GPs in the layer

    <<if you do not pass in Z, you must pass in these next 3:>>
    num_inducing = number of inducing points
    inducing_dim = dimension of inducing points
    num_gps = number of independent GPs in the layer

    optional kwargs:
    range_min: if generating inducing points, this is the lower bound for locations in any dimension (float), -1 is default
    range_max: if generating inducing points, this is the upper bound for locations in any dimension (float), 1 is default

    Note: inducing points are generated according to a Sobol quasi-random number generator. This should give reasonable
    coverage in the hyper rectangle provided by range_min to range_max
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'Z' in kwargs:
            assert torch.is_tensor(kwargs['Z']), f"expected kwarg Z to hold a tensor, got {kwargs['Z']}"
            Z = kwargs['Z']
            assert Z.dim() == 3, f"expected Z to be 3D, got Z.dim() = {Z.dim()}"
            self.register_buffer('num_inducing',torch.tensor(int(Z.shape[-2]), dtype=torch.int64))
            self.register_buffer('inducing_dim',torch.tensor(int(Z.shape[-1]), dtype=torch.int64))
            self.register_buffer('num_gps',torch.tensor(int(Z.shape[-3]), dtype=torch.int64))

        elif 'num_inducing' in kwargs and 'inducing_dim' in kwargs and 'num_gps' in kwargs:
            self.register_buffer('num_inducing', torch.tensor(int(kwargs['num_inducing']), dtype=torch.int64))
            self.register_buffer('inducing_dim', torch.tensor(int(kwargs['inducing_dim']), dtype=torch.int64))
            self.register_buffer('num_gps', torch.tensor(int(kwargs['num_gps']), dtype=torch.int64))
            if 'range_min' in kwargs:
                range_min = kwargs['range_min']
            else:
                range_min = -3.0
            if 'range_max' in kwargs:
                range_max = kwargs['range_max']
            else:
                range_max = 3.0
            if 'normal_inducing' in kwargs:
                Z = make_normal_inducing(self.num_gps, self.num_inducing, self.inducing_dim, range_min,
                                         range_max, dtype=self.dtype)
            else:
                Z = make_sobol_inducing(self.num_gps, self.num_inducing, self.inducing_dim, range_min,
                                        range_max, dtype=self.dtype)

        else:
            raise ValueError('cannot find the right kwargs to instantiate object;'
                             ' I need either Z, or num_inducing, num_gps, and inducing_dim')

        self.inducing_point_storage = torch.nn.Parameter(Z.clone().detach().type(self.dtype))

    @property
    def Z(self):
        return self.inducing_point_storage

    @property
    def M(self):
        return self.num_inducing

    @property
    def D(self):
        return self.inducing_dim

    @property
    def J(self):
        return self.num_gps


class SharedInducingPoints(AbstractInducingPoints):
    """
    required kwargs:
    Z: a 2D tensor that are the inducing point locations desired. If this is passed in, it will short-circuit the rest.
        -1 dim should be inducing point dimension, -2 dim should be number of inducing points
    num_gps = number of independent GPs in the layer

    <<if you do not pass in Z, you must pass in these next 2:>>
    num_inducing = number of inducing points
    inducing_dim = dimension of inducing points
    num_gps = number of independent GPs in the layer


    optional kwargs:
    range_min: if generating inducing points, this is the lower bound for locations in any dimension (float), -1 is default
    range_max: if generating inducing points, this is the upper bound for locations in any dimension (float), 1 is default

    Note: inducing points are generated according to a Sobol quasi-random number generator. This should give reasonable
    coverage in the hyper rectangle provided by range_min to range_max
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'Z' in kwargs and 'num_gps' in kwargs:
            assert torch.is_tensor(kwargs['Z']), f"expected kwarg Z to hold a tensor, got {kwargs['Z']}"
            Z = kwargs['Z']
            assert Z.dim() == 2, f"expected Z to be 2D, got Z.dim() = {Z.dim()}"
            self.register_buffer('num_inducing',torch.tensor(int(Z.shape[-2]), dtype=torch.int64))
            self.register_buffer('inducing_dim',torch.tensor(int(Z.shape[-1]), dtype=torch.int64))
            self.register_buffer('num_gps',torch.tensor(int(kwargs['num_gps']), dtype=torch.int64))

        elif 'num_inducing' in kwargs and 'inducing_dim' in kwargs and 'num_gps' in kwargs:
            self.register_buffer('num_inducing', torch.tensor(int(kwargs['num_inducing']), dtype=torch.int64))
            self.register_buffer('inducing_dim', torch.tensor(int(kwargs['inducing_dim']), dtype=torch.int64))
            self.register_buffer('num_gps', torch.tensor(int(kwargs['num_gps']), dtype=torch.int64))
            if 'range_min' in kwargs:
                range_min = kwargs['range_min']
            else:
                range_min = -3.0
            if 'range_max' in kwargs:
                range_max = kwargs['range_max']
            else:
                range_max = 3.0
            if 'normal_inducing' in kwargs:
                Z = make_normal_inducing(1, self.num_inducing, self.inducing_dim, range_min,
                                         range_max, dtype=self.dtype)
            else:
                Z = make_sobol_inducing(1, self.num_inducing, self.inducing_dim, range_min,
                                        range_max, dtype=self.dtype)
            Z = Z.squeeze(0)

        else:
            raise ValueError('cannot find the right kwargs to instantiate object;'
                             ' I need either Z and num_gps, or num_inducing, num_gps, and inducing_dim')

        self.inducing_point_storage = torch.nn.Parameter(Z.clone().detach().type(self.dtype))

    @property
    def Z(self):
        return self.inducing_point_storage.unsqueeze(0).expand(self.J,-1,-1)

    @property
    def M(self):
        return self.num_inducing

    @property
    def D(self):
        return self.inducing_dim

    @property
    def J(self):
        return self.num_gps


def inducing_factory(dict_in, **kwargs):
    """

    :param dict_in: a dictionary like {'Shared': {'num_gps': 3, 'num_inducing': 128, 'inducing_dim': 4}}
    :param kwargs: used to over-ride either dtype, num_gps, or inducing_dim, as these are often over-ridden by higher
                    lying objects and I don't want to get into details of the internal dict_in structure. Yes, this
                    particular case is simple, but others are not so simple and this way we get a common over-ride API.
    :return:
    """
    registry = {
        'Shared': SharedInducingPoints,
        'Separate': SeparateInducingPoints,
    }
    num_gps = None if not 'num_gps' in kwargs else int(kwargs['num_gps'])
    inducing_dim = None if not 'inducing_dim' in kwargs else int(kwargs['inducing_dim'])
    dtype = None if not 'dtype' in kwargs else kwargs['dtype']
    for key in dict_in.keys():
        if key in registry.keys():
            d = dict_in[key]
            #over_ride with special kwargs
            if num_gps is not None:
                d['num_gps'] = num_gps
            if dtype is not None:
                d['dtype'] = dtype
            if inducing_dim is not None:
                d['inducing_dim'] = inducing_dim
            return registry[key](**d)
