import torch
from ast import parse
from matchpy import Arity
from torchgp.symbolic.core import (
    AstMixin,
    Symbol,
    TensorOperation
)
from .util import validate_scalar_parameter
from ..symbolic.utils import expr2graph, graph2ast

__all__ = [
    "AbstractMOTransform",
    "LinearCoregionalizationTransform",
    "MOTransformSymbol",
    "ApplyMOTran2Vec",
    "ApplyMOTranFullCov",
    "ApplyMOTranFullMarginal",
    "ApplyMOTranMarginalInput",
    "ApplyMOTranMarginalOutput",
    "transform_factory"
]

def transform_factory(dict_in):
    registry = {
        'Linear': LinearCoregionalizationTransform,
        'None': None,
    }
    for key in dict_in.keys():
        if key in registry.keys():
            return None if registry[key] is None else registry[key](**dict_in[key])

class AbstractMOTransform(torch.nn.Module):
    def __init__(self, in_features, out_features):
        super().__init__()
        in_features = validate_scalar_parameter(in_features, 'in_features', dtype=torch.int64)
        out_features = validate_scalar_parameter(out_features, 'out_features', dtype=torch.int64)
        self.register_buffer("in_features", in_features)
        self.register_buffer("out_features", out_features)

    def transform_batched_matrix(self, x):
        """
        Expecting input of size ...JNN
        Should return a tensor of size ...DNDN, where D is output dim size, N is data size"""
        raise NotImplementedError()

    def transform_batched_matrix_full_marginal(self, x):
        """
        Expecting input of size ...JN1 (data is marginalized before passing in)
        Should return a tensor of size ...ND1, where D is output dim size, N is data size"""
        raise NotImplementedError()

    def transform_batched_matrix_marginal_output(self, x):
        """
        Expecting input of size ...JNN
        Should return a tensor of size ...DNN, where D is output dim size, N is data size"""
        raise NotImplementedError()

    def transform_batched_matrix_marginal_input(self, x):
        """
        Expecting input of size ...JN1 (data is marginalized before passing in)
        Should return a tensor of size ...NDD, where D is output dim size, N is data size"""
        raise NotImplementedError()

    def transform_batched_vector(self, x):
        """
        Expecting input of size ...JN1
        Should return tensor of size ...DN1, where D is output dim size, N is data size"""
        raise NotImplementedError()


class LinearCoregionalizationTransform(AbstractMOTransform):
    def __init__(self, in_features, out_features, dtype=torch.get_default_dtype(), **kwargs):
        super().__init__(in_features, out_features)
        self.dtype = dtype
        P = torch.zeros(self.out_features, self.in_features, dtype=self.dtype)
        P.diagonal(dim1=-1,dim2=-2).add_(1.)  # effectively initialize to rectangular identity
        self.P = torch.nn.Parameter(P.clone().detach())
        if 'custom_init' in kwargs:
            W = kwargs['custom_init']
            assert W.dim() == 2, f"custom init must be 2D, got dim = {W.dim()}"
            assert W.shape[-2] == self.out_features, f"custom init must have {self.out_features} rows"
            assert W.shape[-1] == self.in_features, f"custom init must have {self.in_features} columns"
            with torch.no_grad():
                self.P.data[...] = W.data
        if 'freeze_weights' in kwargs:
            self.P.requires_grad_(False)

    def transform_batched_matrix(self, x):
        """full in / full out: ...JNN -> ...DNDN

            einsum operation: 'ij,...jmn,lj->...imln'
        """
        assert x.shape[-3] == self.P.shape[-1], f"expected batch dim -3 to be {self.P.shape[-1]}, got {x.shape[-3]}"
        P2 = self.P.unsqueeze(0) * self.P.unsqueeze(1)  # 1DJ*E1J -> EDJ
        t1 = torch.tensordot(x, P2, [[-3], [-1]])  # ...JMN,EDJ  -> ...MNED
        # ...MNED -> ...MEND -> ...MEDN -> ...EMDN
        result = t1.transpose(-2, -3).transpose(-2, -1).transpose(-4, -3)
        return result


    def transform_batched_matrix_full_marginal(self, x):
        """marg in / marg out: ...JN1 -> ...DN1

           einsum operation: 'ij,...jmn,ij->...imn'
        """
        assert x.shape[-3] == self.P.shape[-1], f"expected x.shape[-3] to be {self.P.shape[-1]}, got {x.shape[-3]}"
        assert x.shape[-1] == 1, f'Expecting last dim to be 1, got {x.shape[-2:]}, you should marginalize over N first'
        P2 = self.P * self.P
        t1 = torch.tensordot(x, P2, [[-3], [-1]])  # ...JN1, DJ -> ...N1D
        result = t1.transpose(-3, -1).transpose(-2, -1)  # ...N1D -> ...D1N -> ...DN1
        return result

    def transform_batched_matrix_marginal_output(self, x):
        """full in / marg out: ...JNN -> ...DNN

           einsum operation: 'ij,...jmn,ij->...imn'
        """
        assert x.shape[-3] == self.P.shape[-1], f"expected batch dim -3 to be {self.P.shape[-1]}, got {x.shape[-3]}"
        P2 = self.P * self.P
        t1 = torch.tensordot(x, P2, [[-3], [-1]])  # ...JNM, DJ -> ...NMD
        result = t1.transpose(-3, -1).transpose(-2, -1)  # ...NMD -> ...DMN -> ...DNM
        return result

    def transform_batched_matrix_marginal_input(self, x):
        """full in / marg out: ...JN1 -> ...NDD

           einsum operation: 'ij,...jmn,lj->...mil'
        """
        assert x.shape[-3] == self.P.shape[-1], f"expected batch dim -3 to be {self.P.shape[-1]}, got {x.shape[-3]}"
        assert x.shape[-1] == 1, f'Expecting last dim to be 1, got {x.shape[-2:]}, you should marginalize over N first'
        # ...JN1, E1J @ 1DJ -> ...NED
        result = torch.tensordot(x.squeeze(-1),self.P.unsqueeze(1)*self.P.unsqueeze(0),[[-2], [-1]])
        return result

    def transform_batched_vector(self, x):
        """vector transform: ...JN1 -> ...DN1

           einsum operation: 'dj,...jnx->...dnx'
        """
        assert x.dim() >= 3, f"expected a 3D or greater tensor for x, got {x.dim()}"
        assert x.shape[-3] == self.P.shape[-1], f"expected batch dim -3 to be {self.P.shape[-1]}, got {x.shape[-3]}"
        assert x.shape[-1] == 1, f"expected final dim to be 1"
        # ...JNS, DJ -> ...NSD -> ...DSN -> ...DNS
        result = torch.tensordot(x, self.P, [[-3], [-1]]).transpose(-1, -3).transpose(-1, -2)
        return result


class ApplyMOTranFullCov(TensorOperation):
    name = "ApplyMOTranFullCov"
    arity = Arity(2, True)

    def __str__(self):
        return "{operator}[{operand}]".format(
            operator=self.operands[0], operand=", ".join(map(str, self.operands[1:]))
        )

    @property
    def evaluated(self):
        f = self.operands[0].evaluated
        op = self.operands[1].evaluated
        return f.transform_batched_matrix(op)

    def to_ast(self, label, *arglabels, source=False):
        func = arglabels[0]
        op = arglabels[1]

        src = f"{label} = {func}.transform_batched_matrix({op})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class MOTransformSymbol(AstMixin, Symbol):
    default_function = lambda obj, *args: print(
        obj.name + " did not receive an implementation"
    )

    def __init__(self, name, data=None, variable_name=None):
        self.data = self.default_function if not data else data

        super().__init__(name, variable_name=variable_name)

    @property
    def evaluated(self):
        return self.data

    def to_ast(self, label, source=False):
        # construct a rename operation
        src = "{label} = {self_name}".format(label=label, self_name=self.name)
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result

    def __repr__(self):
        return "{type}('{name}')".format(type=type(self).__name__, name=self.name)


class ApplyMOTranFullMarginal(TensorOperation):
    name = "ApplyMOTranFullMarginal"
    arity = Arity(2, True)

    def __str__(self):
        return "{operator}[{operand}]".format(
            operator=self.operands[0], operand=", ".join(map(str, self.operands[1:]))
        )

    @property
    def evaluated(self):
        f = self.operands[0].evaluated
        op = self.operands[1].evaluated
        return f.transform_batched_matrix_full_marginal(op)

    def to_ast(self, label, *arglabels, source=False):
        func = arglabels[0]
        op = arglabels[1]

        src = f"{label} = {func}.transform_batched_matrix_full_marginal({op})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class ApplyMOTranMarginalInput(TensorOperation):
    name = "ApplyMOTranMarginalInput"
    arity = Arity(2, True)

    def __str__(self):
        return "{operator}[{operand}]".format(
            operator=self.operands[0], operand=", ".join(map(str, self.operands[1:]))
        )

    @property
    def evaluated(self):
        f = self.operands[0].evaluated
        op = self.operands[1].evaluated
        return f.transform_batched_matrix_marginal_input(op)

    def to_ast(self, label, *arglabels, source=False):
        func = arglabels[0]
        op = arglabels[1]

        src = f"{label} = {func}.transform_batched_matrix_marginal_input({op})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class ApplyMOTranMarginalOutput(TensorOperation):
    name = "ApplyMOTranMarginalOutput"
    arity = Arity(2, True)

    def __str__(self):
        return "{operator}[{operand}]".format(
            operator=self.operands[0], operand=", ".join(map(str, self.operands[1:]))
        )

    @property
    def evaluated(self):
        f = self.operands[0].evaluated
        op = self.operands[1].evaluated
        return f.transform_batched_matrix_marginal_output(op)

    def to_ast(self, label, *arglabels, source=False):
        func = arglabels[0]
        op = arglabels[1]

        src = f"{label} = {func}.transform_batched_matrix_marginal_output({op})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result


class ApplyMOTran2Vec(TensorOperation):
    name = "ApplyMOTran2Vec"
    arity = Arity(2, True)

    def __str__(self):
        return "{operator}[{operand}]".format(
            operator=self.operands[0], operand=", ".join(map(str, self.operands[1:]))
        )

    @property
    def evaluated(self):
        f = self.operands[0].evaluated
        op = self.operands[1].evaluated
        return f.transform_batched_vector(op)

    def to_ast(self, label, *arglabels, source=False):
        func = arglabels[0]
        op = arglabels[1]

        src = f"{label} = {func}.transform_batched_vector({op})"
        ast = parse(src, filename="<{type}>".format(type=type(self)), mode="exec")

        # extract just the assign
        ast = ast.body[0]

        result = (ast, src) if source else ast
        return result
