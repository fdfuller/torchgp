#!/usr/bin/env python

import sys
from pathlib import Path
import os

from pkg_resources import parse_version
from setuptools import find_packages, setup

# Dependencies of torchgp
requirements = [
    'numpy>=1.16.2',
    'astor>=0.7.1',
    'networkx>=2.2',
    'pydot>=1.3.0',
    'matchpy>=0.5.1',
    'pathvalidate>=2.2.0'
    # 'scipy>=1.2.1',
    # 'papermill>=1.2.1',
    # 'pillow>=5.4.1',
    # 'h5py>=2.9.0',
    # 'pandas>=0.24.1',
    # 'matplotlib>=3.1.1'
]

min_pytorch_version = '1.2.0'
pytorch = 'torch'  # when installed via pip, it will autodetect whether cuda is available


try:
    # If torch not installed, import raises ImportError
    import torch
    if parse_version(torch.__version__) < parse_version(min_pytorch_version):
        # torch pre-installed, but below the minimum required version
        raise DeprecationWarning("PyTorch version below minimum requirement")
except (ImportError, DeprecationWarning):
    # Add torch to dependencies to trigger installation/update
    requirements.append(pytorch)

repo_path = str(os.path.abspath(os.path.dirname(__file__)))
with open(str(Path(repo_path, "VERSION").absolute())) as version_file:
    version = version_file.read().strip()

packages = find_packages('.')

setup(name='torchgp',
      version=version,
      author="Franklin Fuller, Anton Loukianov",
      author_email="fdfuller@slac.stanford.edu",
      description="Variational Gaussian Models in PyTorch",
      license="MIT",
      keywords="gaussian-processes deep-gaussian-processes pytorch",
      url="https://bitbucket.org/fdfuller/torchgp",
      packages=packages,
      include_package_data=True,
      install_requires=requirements,
      python_requires=">=3.6",
      classifiers=[
          'License :: OSI Approved :: MIT',
          'Natural Language :: English',
          'Operating System :: MacOS :: MacOS X',
          'Operating System :: POSIX :: Linux',
          'Programming Language :: Python :: 3.6',
          'Topic :: Scientific/Engineering :: Artificial Intelligence'
      ])
