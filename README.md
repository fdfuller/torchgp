to install, we expect a python 3.6 environment and recommend to set this up with anaconda.

Once in the environment, populate dependencies via:

```
conda install numpy
conda install scipy
conda install pytorch torchvision cudatoolkit=10.1 -c pytorch
pip install matchpy
conda install numba
pip install networkx
pip install astor
pip install pydot
pip install xrt
conda install jupyter
pip install jupytext
pip install papermill
conda install xarray dask netCDF4 bottleneck
conda install h5py
pip install pathvalidate
```


Then, finally, install torchgp:

1. clone the repo
2. cd to the repo directory

```
pip install -e .
```

This *should* work.
